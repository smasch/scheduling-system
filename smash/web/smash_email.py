# coding=utf-8

import logging

from django.core.mail import EmailMessage

from web.models import ConfigurationItem
from web.models.constants import DEFAULT_FROM_EMAIL

logger = logging.getLogger(__name__)


class EmailSender:
    @staticmethod
    def send_email(subject, body, recipients, cc_recipients=None):
        if cc_recipients is None:
            cc_recipients = []
        email_from = ConfigurationItem.objects.get(type=DEFAULT_FROM_EMAIL).value
        recipient_list = []
        for recipient in recipients.split(";"):
            recipient_list.append(recipient)
        cc_recipients.append(email_from)

        message = EmailMessage(
            subject,
            body,
            email_from,
            recipient_list,
            cc=cc_recipients
        )
        message.content_subtype = "html"
        message.send()
        logger.info('Email sent. Subject: ' + subject + "; Recipients: " + recipients)
