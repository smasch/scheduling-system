verhoeff_multiplication_table = (
    (0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
    (1, 2, 3, 4, 0, 6, 7, 8, 9, 5),
    (2, 3, 4, 0, 1, 7, 8, 9, 5, 6),
    (3, 4, 0, 1, 2, 8, 9, 5, 6, 7),
    (4, 0, 1, 2, 3, 9, 5, 6, 7, 8),
    (5, 9, 8, 7, 6, 0, 4, 3, 2, 1),
    (6, 5, 9, 8, 7, 1, 0, 4, 3, 2),
    (7, 6, 5, 9, 8, 2, 1, 0, 4, 3),
    (8, 7, 6, 5, 9, 3, 2, 1, 0, 4),
    (9, 8, 7, 6, 5, 4, 3, 2, 1, 0))
verhoeff_permutation_table = (
    (0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
    (1, 5, 7, 6, 2, 8, 3, 0, 9, 4),
    (5, 8, 0, 3, 7, 9, 6, 1, 4, 2),
    (8, 9, 1, 6, 0, 4, 3, 5, 2, 7),
    (9, 4, 5, 3, 1, 2, 6, 8, 7, 0),
    (4, 2, 8, 6, 5, 7, 3, 9, 0, 1),
    (2, 7, 9, 3, 8, 0, 6, 4, 1, 5),
    (7, 0, 4, 6, 9, 1, 3, 2, 5, 8))


class VerhoeffAlgorithm:
    def __init__(self):
        pass

    @staticmethod
    def verhoeff_checksum(number):
        """Calculate the Verhoeff checksum over the provided number. The checksum
        is returned as an int. Valid numbers should have a checksum of 0."""
        # transform number list
        number = tuple(int(n) for n in reversed(str(number)))
        # calculate checksum
        check = 0
        for i, n in enumerate(number):
            check = verhoeff_multiplication_table[check][verhoeff_permutation_table[i % 8][n]]
        return check

    @staticmethod
    def is_valid_verhoeff(number):
        return VerhoeffAlgorithm.verhoeff_checksum(number) == 0

    @staticmethod
    def calculate_verhoeff_check_sum(number):
        return str(verhoeff_multiplication_table[VerhoeffAlgorithm.verhoeff_checksum(str(number) + '0')].index(0))
