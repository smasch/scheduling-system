def digits_of(n):
    return [int(d) for d in str(n)]


class LuhnAlgorithm:

    def __init__(self):
        pass

    @staticmethod
    def luhn_checksum(card_number):
        digits = digits_of(card_number)
        odd_digits = digits[-1::-2]
        even_digits = digits[-2::-2]
        checksum = 0
        checksum += sum(odd_digits)
        for d in even_digits:
            checksum += sum(digits_of(d * 2))
        return checksum % 10

    @staticmethod
    def is_luhn_valid(card_number):
        return LuhnAlgorithm.luhn_checksum(card_number) == 0

    @staticmethod
    def calculate_luhn_checksum(card_number):
        return (10 - LuhnAlgorithm.luhn_checksum(card_number + '0')) % 10
