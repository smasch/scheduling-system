"""smash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""
from django.urls import re_path

from web.api_views import (
    worker,
    location,
    subject,
    appointment_type,
    appointment,
    configuration,
    daily_planning,
    redcap,
    flying_team,
    visit,
    voucher,
    voucher_type,
    provenance,
    language,
    room,
)

urlpatterns = [
    # appointments
    re_path(r"^appointments/(?P<appointment_type>[A-z]+)$", appointment.appointments, name="web.api.appointments"),
    re_path(
        r"^appointments:columns/(?P<appointment_list_type>[A-z]+)$",
        appointment.get_appointment_columns,
        name="web.api.appointments.columns",
    ),
    # appointment types
    re_path(r"^appointment_types$", appointment_type.appointment_types, name="web.api.appointment_types"),
    # languages
    re_path(r"^languages$", language.languages, name="web.api.languages"),
    # configuration items
    re_path(r"^configuration_items$", configuration.configuration_items, name="web.api.configuration_items"),
    re_path(
        r"^configuration_items/update$",
        configuration.update_configuration_item,
        name="web.api.update_configuration_item",
    ),
    # subjects data
    re_path(r"^cities$", subject.cities, name="web.api.cities"),
    re_path(r"^referrals$", subject.referrals, name="web.api.referrals"),
    re_path(r"^export_log$", provenance.ExportLog.as_view(), name="web.api.export_log"),
    re_path(r"^provenances$", provenance.CompleteLog.as_view(), name="web.api.provenances"),
    re_path(r"^subjects/(?P<subject_list_type>[A-z]+)$", subject.subjects, name="web.api.subjects"),
    re_path(
        r"^subjects:columns/(?P<subject_list_type>[A-z]+)$",
        subject.get_subject_columns,
        name="web.api.subjects.columns",
    ),
    re_path(r"^subject_types", subject.types, name="web.api.subject_types"),
    # visits data
    re_path(r"^visits/(?P<visit_list_type>[A-z]+)$", visit.visits, name="web.api.visits"),
    re_path(r"^visits:columns/(?P<visit_list_type>[A-z]+)$", visit.get_visit_columns, name="web.api.visits.columns"),
    # locations
    re_path(r"^locations$", location.locations, name="web.api.locations"),
    # flying_teams
    re_path(r"^flying_teams$", flying_team.flying_teams, name="web.api.flying_teams"),
    # worker data
    re_path(r"^specializations$", worker.specializations, name="web.api.specializations"),
    re_path(r"^units$", worker.units, name="web.api.units"),
    # workers
    re_path(r"^workers/(?P<worker_role>[A-z]+)/$", worker.get_workers, name="web.api.workers"),
    re_path(
        r"^workers/add_availability/(?P<worker_id>\d+)/(?P<weekday>[0-9])/"
        r"(?P<start_hour>\d{2}:\d{2})/(?P<end_hour>\d{2}:\d{2})/$",
        worker.add_worker_availability,
        name="web.api.workers.add_availability",
    ),
    re_path(
        r"^workers/add_extra_availability/(?P<worker_id>\d+)/"
        r"(?P<start_str_date>\d{4}-\d{2}-\d{2}-\d{2}-\d{2})/(?P<end_str_date>\d{4}-\d{2}-\d{2}-\d{2}-\d{2})/$",
        worker.add_worker_extra_availability,
        name="web.api.workers.add_extra_availability",
    ),
    re_path(
        r"^worker/accept_privacy_notice/$", worker.accept_privacy_notice, name="web.api.workers.accept_privacy_notice"
    ),
    # daily planning data
    re_path(r"^daily_planning/workers/$", worker.workers_for_daily_planning, name="web.api.workers.daily_planning"),
    re_path(
        r"^daily_planning/workers/availabilities$",
        worker.availabilities,
        name="web.api.workers.daily_planning.availabilities",
    ),
    # workers
    re_path(r"^workers/(?P<worker_role>[A-z]+)/$", worker.get_workers, name="web.api.workers"),
    # daily planning events
    re_path(
        r"^events/(?P<date>\d{4}-\d{2}-\d{2})/include_all$",
        daily_planning.events,
        {"include_all": True},
        name="web.api.events_all",
    ),
    re_path(
        r"^events/(?P<date>\d{4}-\d{2}-\d{2})/$", daily_planning.events, {"include_all": False}, name="web.api.events"
    ),
    re_path(
        r"^availabilities/(?P<date>\d{4}-\d{2}-\d{2})/$", daily_planning.availabilities, name="web.api.availabilities"
    ),
    re_path(r"^events_persist$", daily_planning.events_persist, name="web.api.events_persist"),
    # worker availability
    re_path(r"^worker_availability/$", worker.get_worker_availability, name="web.api.get_worker_availability"),
    # worker data
    re_path(
        r"^redcap/missing_subjects/(?P<missing_subject_id>\d+):ignore$",
        redcap.ignore_missing_subject,
        name="web.api.redcap.ignore_missing_subject",
    ),
    re_path(
        r"^redcap/missing_subjects/(?P<missing_subject_id>\d+):unignore$",
        redcap.unignore_missing_subject,
        name="web.api.redcap.unignore_missing_subject",
    ),
    # vouchers data
    re_path(r"^vouchers/$", voucher.get_vouchers, name="web.api.vouchers"),
    re_path(r"^vouchers:columns$", voucher.get_voucher_columns, name="web.api.vouchers.columns"),
    # voucher types data
    re_path(r"^voucher_types/$", voucher_type.get_voucher_types, name="web.api.voucher_types"),
    re_path(r"^vouchers:columns$", voucher.get_voucher_columns, name="web.api.vouchers.columns"),
    # room availabilities (fullcalendar resources)
    re_path(r"rooms/(?P<location_id>\d+)$", room.rooms_for_location, name="web.api.rooms.rooms_for_location"),
    re_path(
        r"rooms/appointments/(?P<location_id>\d+)$",
        room.appointments_for_location_and_date,
        name="web.api.rooms.appointments_for_location_and_date",
    ),
]
