from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.deprecation import MiddlewareMixin

from web.models import Worker, Study
from web.models.constants import GLOBAL_STUDY_ID
from web.views.privacy_notice import privacy_notice_accept


class PrivacyNoticeMiddleware(MiddlewareMixin):

    # pylint: disable=no-self-use
    def process_view(self, request, view_func, view_args, view_kwargs):  # pylint: disable=unused-argument
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        if request.user.is_authenticated \
                and not view_func == privacy_notice_accept \
                and not request.user.is_superuser \
                and not request.path == reverse('logout'):
            study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
            worker = Worker.get_by_user(request.user)
            if worker is None:
                return None
            if study.study_privacy_notice \
                    and study.acceptance_of_study_privacy_notice_required \
                    and not worker.privacy_notice_accepted \
                    and study.study_privacy_notice.document.url != request.path:
                messages.add_message(request, messages.WARNING,
                                     "You can't use the system until you accept the privacy notice.")
                return redirect(
                    reverse('web.views.accept_privacy_notice', kwargs={'pk': study.study_privacy_notice.id}))

        # Code to be executed for each request/response after
        # the view is called.
        return None
