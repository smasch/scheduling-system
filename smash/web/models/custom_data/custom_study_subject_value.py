# coding=utf-8

from django.db import models


class CustomStudySubjectValue(models.Model):
    value = models.TextField(max_length=2048, null=True, blank=True)

    study_subject_field = models.ForeignKey(
        "web.CustomStudySubjectField", verbose_name="Custom Field", editable=False, null=False, on_delete=models.CASCADE
    )
    study_subject = models.ForeignKey(
        "web.StudySubject", verbose_name="Study", editable=False, null=False, on_delete=models.CASCADE
    )
