# coding=utf-8

from django.db import models


class CustomStudySubjectVisibility(models.Model):
    visible = models.BooleanField(default=False, null=False)

    study_subject_field = models.ForeignKey("web.CustomStudySubjectField",
                                            verbose_name='Custom Field',
                                            editable=False,
                                            null=False,
                                            on_delete=models.CASCADE
                                            )
    visible_subject_study_columns = models.ForeignKey("web.StudyColumns",
                                                      verbose_name='List of visible columns',
                                                      editable=False,
                                                      null=False,
                                                      on_delete=models.CASCADE
                                                      )
