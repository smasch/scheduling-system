# coding=utf-8

from django.db import models

from web.models.constants import CUSTOM_FIELD_TYPE


class CustomStudySubjectField(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False)
    type = models.CharField(max_length=20, choices=CUSTOM_FIELD_TYPE, null=False, blank=False)

    possible_values = models.TextField(max_length=1024, null=True, blank=True, default="")

    default_value = models.CharField(max_length=256, null=True, blank=True)
    readonly = models.BooleanField(default=False)

    required = models.BooleanField(default=False)

    unique = models.BooleanField(default=False)

    tracked = models.BooleanField(default=False)

    study = models.ForeignKey("web.Study", verbose_name="Study", editable=False, null=False, on_delete=models.CASCADE)


def get_study_subject_field_id(study_subject_field: CustomStudySubjectField) -> str:
    return "custom_field-" + str(study_subject_field.id)
