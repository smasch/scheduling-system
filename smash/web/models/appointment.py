# coding=utf-8
import datetime

from django.db import models

from . import ConfigurationItem
from .constants import APPOINTMENT_TYPE_DEFAULT_COLOR, APPOINTMENT_TYPE_DEFAULT_FONT_COLOR, \
    CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE, NO_SHOW_APPOINTMENT_COLOR_CONFIGURATION_TYPE


class Appointment(models.Model):
    class Meta:
        app_label = 'web'
        permissions = [
            ("send_sample_mail_for_appointments", "Can send sample collection list"),
            ("view_statistics", "Can see statistics"),
        ]

    APPOINTMENT_STATUS_SCHEDULED = 'SCHEDULED'
    APPOINTMENT_STATUS_FINISHED = 'FINISHED'
    APPOINTMENT_STATUS_CANCELLED = 'CANCELLED'
    APPOINTMENT_STATUS_NO_SHOW = 'NO_SHOW'
    APPOINTMENT_STATUS_CHOICES = {
        APPOINTMENT_STATUS_SCHEDULED: 'Scheduled',
        APPOINTMENT_STATUS_FINISHED: 'Finished',
        APPOINTMENT_STATUS_CANCELLED: 'Cancelled',
        APPOINTMENT_STATUS_NO_SHOW: 'No Show',
    }

    flying_team = models.ForeignKey("web.FlyingTeam",
                                    verbose_name='Flying team (if applicable)',
                                    null=True, blank=True, on_delete=models.PROTECT
                                    )
    worker_assigned = models.ForeignKey("web.Worker",
                                        verbose_name='Worker conducting the assessment (if applicable)',
                                        null=True, blank=True, on_delete=models.CASCADE
                                        )

    appointment_types = models.ManyToManyField("web.AppointmentType",
                                               verbose_name='Appointment types',
                                               blank=True, through="web.AppointmentTypeLink",
                                               related_name="new_appointment"
                                               )
    room = models.ForeignKey("web.Room",
                             verbose_name='Room ID',
                             null=True,
                             blank=True,
                             on_delete=models.PROTECT,
                             limit_choices_to={"removed": False}
                             )

    location = models.ForeignKey("web.Location",
                                 verbose_name='Location', on_delete=models.PROTECT,
                                 limit_choices_to={"removed": False}
                                 )

    visit = models.ForeignKey("web.Visit",
                              verbose_name='Visit ID',
                              editable=False,
                              null=True,
                              blank=True, on_delete=models.CASCADE
                              )
    comment = models.TextField(max_length=1024,
                               verbose_name='Comment',
                               null=True,
                               blank=True
                               )
    datetime_when = models.DateTimeField(
        verbose_name='Appointment on',
        null=True, blank=True
    )
    length = models.IntegerField(
        verbose_name='Appointment length (in minutes)'
    )  # Potentially redundant; but can be used to manually adjust appointment's length

    status = models.CharField(max_length=20, choices=list(APPOINTMENT_STATUS_CHOICES.items()),
                              verbose_name='Status',
                              default=APPOINTMENT_STATUS_SCHEDULED
                              )
    post_mail_sent = models.BooleanField(
        verbose_name='Post mail sent',
        default=False
    )

    def __str__(self):
        start = self.datetime_when.strftime('%Y-%m-%d %H:%M')
        if self.visit is not None:
            subject = self.visit.subject.subject
            return f'#{self.visit.visit_number:02} ' \
                   f'| {start} ({self.length} min) ' \
                   f'| {subject.first_name} {subject.last_name} ' \
                   f'| {self.status}'
        else:
            return f'{start} ({self.length} min) | {self.status}'

    def mark_as_finished(self):
        self.status = Appointment.APPOINTMENT_STATUS_FINISHED
        self.save()

    def mark_as_cancelled(self):
        self.status = Appointment.APPOINTMENT_STATUS_CANCELLED
        self.save()

    def mark_as_no_show(self):
        self.status = Appointment.APPOINTMENT_STATUS_NO_SHOW
        self.save()

    def datetime_until(self):
        if self.datetime_when is None:
            return None
        else:
            return self.datetime_when + datetime.timedelta(minutes=max(self.length, 15))

    def color(self):
        result = APPOINTMENT_TYPE_DEFAULT_COLOR
        if self.status == Appointment.APPOINTMENT_STATUS_NO_SHOW:
            result = ConfigurationItem.objects.get(type=NO_SHOW_APPOINTMENT_COLOR_CONFIGURATION_TYPE).value
        elif self.status == Appointment.APPOINTMENT_STATUS_CANCELLED:
            result = ConfigurationItem.objects.get(type=CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE).value
        elif (self.location.color is not None) and (self.location.color != ""):
            result = self.location.color
        else:
            priority = 1000000
            for appointment_type in self.appointment_types.all():
                if appointment_type.calendar_color_priority < priority:
                    priority = appointment_type.calendar_color_priority
                    result = appointment_type.calendar_color
        return result

    def font_color(self):
        result = APPOINTMENT_TYPE_DEFAULT_FONT_COLOR
        priority = 1000000
        for appointment_type in self.appointment_types.all():
            if appointment_type.calendar_color_priority < priority:
                priority = appointment_type.calendar_color_priority
                result = appointment_type.calendar_font_color
        return result

    def title(self):
        if self.visit is None:
            title = "N/A"
            if self.comment is not None:
                title = self.comment.replace("\n", ";").replace("\r", ";")
            if title == "":
                title = "N/A"
            return title
        else:
            title = self.visit.subject.subject.first_name + " " + self.visit.subject.subject.last_name + " type: "
            for appointment_type in self.appointment_types.all():
                title += appointment_type.code + ", "
            return title
