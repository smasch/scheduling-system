# coding=utf-8
from django.db import models


class Room(models.Model):
    equipment = models.ManyToManyField("web.Item",
                                       verbose_name='On-site equipment',
                                       blank=True
                                       )
    owner = models.CharField(max_length=50,
                             verbose_name='Owner'
                             )
    address = models.CharField(max_length=255,
                               verbose_name='Address'
                               )
    city = models.CharField(max_length=50,
                            verbose_name='City'
                            )
    room_number = models.IntegerField(
        verbose_name='Room number'
    )
    floor = models.IntegerField(
        verbose_name='Floor'
    )
    is_vehicle = models.BooleanField(
        verbose_name='Is a vehicle?'
    )

    removed = models.BooleanField(default=False,
                                  editable=False,
                                  )

    def __str__(self):
        return f"{self.room_number} {self.address} {self.city}"
