# coding=utf-8
import logging

from dateutil.relativedelta import relativedelta
from django.db import models
from django.db import transaction
from django.db.models.signals import post_save
from django.dispatch import receiver

from web.models.constants import BOOL_CHOICES
from web import disable_for_loaddata

logger = logging.getLogger(__name__)


class Visit(models.Model):
    class Meta:
        app_label = "web"

    subject = models.ForeignKey(
        "web.StudySubject", on_delete=models.CASCADE, verbose_name="Subject"
    )
    datetime_begin = models.DateTimeField(verbose_name="Visit starts at")
    datetime_end = models.DateTimeField(
        verbose_name="Visit ends at"
    )  # Deadline before which all appointments need to be scheduled

    is_finished = models.BooleanField(verbose_name="Has ended", default=False)
    post_mail_sent = models.BooleanField(
        choices=BOOL_CHOICES, verbose_name="Post mail sent", default=False
    )
    appointment_types = models.ManyToManyField(
        "web.AppointmentType",
        verbose_name="Requested appointments",
        blank=True,
    )

    # this value is automatically computed by signal handled by
    # update_visit_number method
    visit_number = models.IntegerField(verbose_name="Visit number", default=1)

    @property
    def next_visit(self):
        return (
            Visit.objects.filter(
                subject=self.subject, visit_number=self.visit_number + 1
            )
            .order_by("datetime_begin", "datetime_end")
            .first()
        )

    @property
    def future_visits(self):
        return (
            Visit.objects.filter(subject=self.subject)
            .filter(visit_number__gt=self.visit_number)
            .order_by("datetime_begin", "datetime_end")
        )

    def __str__(self):
        start = self.datetime_begin.strftime("%Y-%m-%d")
        end = self.datetime_end.strftime("%Y-%m-%d")
        finished = "✓" if self.is_finished else ""
        return (
            f"#{self.visit_number:02} "
            f"| {start} / {end} "
            f"| {self.subject.subject.first_name} {self.subject.subject.last_name} "
            f"| {finished}"
        )

    def mark_as_finished(self):
        self.is_finished = True
        self.save()

        create_follow_up = True
        if self.subject.subject.dead:
            create_follow_up = False
        elif self.subject.resigned:
            create_follow_up = False
        elif self.subject.excluded:
            create_follow_up = False
        elif self.subject.endpoint_reached:
            create_follow_up = False
        elif not self.subject.type.auto_create_follow_up:
            create_follow_up = False

        if create_follow_up:
            if self.subject.visit_used_to_compute_followup_date is not None:
                visit_started = (
                    self.subject.visit_used_to_compute_followup_date.datetime_begin
                )
                start_number = (
                    self.subject.visit_used_to_compute_followup_date.visit_number
                )
            else:
                visit_started = (
                    Visit.objects.filter(subject=self.subject, visit_number=1)
                    .first()
                    .datetime_begin
                )
                start_number = 1

            follow_up_number = Visit.objects.filter(subject=self.subject).count() + 1

            study = self.subject.study

            args = {
                self.subject.type.follow_up_delta_units: self.subject.type.follow_up_delta_time
            }

            time_to_next_visit = relativedelta(**args) * (
                follow_up_number - start_number
            )

            logger.warning(
                "new visit: %s %s %s", args, relativedelta(**args), time_to_next_visit
            )

            Visit.objects.create(
                subject=self.subject,
                datetime_begin=visit_started + time_to_next_visit,
                datetime_end=visit_started
                + time_to_next_visit
                + relativedelta(months=study.default_visit_duration_in_months),
            )

    def unfinish(self):
        # if ValueError messages are changed, change test/view/test_visit.py
        # check visit is indeed finished
        if not self.is_finished:
            raise ValueError("The visit is not finished.")

        # check if there are some unfinished visits before this visit
        unfinished_visits = Visit.objects.filter(
            subject=self.subject,
            is_finished=False,
            datetime_begin__lt=self.datetime_begin,
        ).count()
        if unfinished_visits > 0:
            raise ValueError(
                "Visit can't be unfinished. There is at least one unfinished visit."
            )

        # check that there is only one future visit
        future_visits = self.future_visits
        if len(future_visits) > 1:
            raise ValueError(
                "Visit can't be unfinished. "
                "Only visits with one immediate future visit (without appointments) can be unfinished."
            )
        elif len(future_visits) == 1:
            # check that the future visit has no appointments
            # remove visit if it has no appointments
            next_visit = future_visits[0]
            if len(next_visit.appointment_set.all()) == 0:
                if self.subject.visit_used_to_compute_followup_date == next_visit:
                    self.subject.visit_used_to_compute_followup_date = self
                    self.subject.save()
                next_visit.delete()
            else:
                raise ValueError(
                    "Visit can't be unfinished. The next visit has appointments."
                )

        else:
            # this can happen when there is no auto follow up visit
            pass

        self.is_finished = False
        self.save()


@receiver(post_save, sender=Visit)
@disable_for_loaddata
def check_visit_number(
    sender, instance, created, **kwargs
):  # pylint: disable=unused-argument
    # no other solution to ensure the visit_number is in chronological order than to sort the whole list if there are
    # future visits
    visit = instance
    if (
        visit.subject is not None
    ):  # not sure if select_for_update has an effect, the tests work as well without it
        # new visit, sort only future visit respect to the new one
        if created:
            visits_before = (
                Visit.objects.select_for_update()
                .filter(subject=visit.subject)
                .filter(datetime_begin__lt=visit.datetime_begin)
                .count()
            )
            # we need to sort the future visits respect to the new one, if any
            visits = (
                Visit.objects.select_for_update()
                .filter(subject=visit.subject)
                .filter(datetime_begin__gte=visit.datetime_begin)
                .order_by("datetime_begin", "id")
            )
            with transaction.atomic():  # not sure if it has an effect, the tests work as well without it
                for i, v in enumerate(visits):
                    expected_visit_number = visits_before + i + 1
                    if v.visit_number != expected_visit_number:
                        # does not rise post_save, we avoid recursion
                        Visit.objects.filter(id=v.id).update(
                            visit_number=expected_visit_number
                        )
                        if (
                            v.id == visit.id
                        ):  # if the iteration visit is the same that the instance that produced the
                            # signal call this ensures that the upper saved object is also updated, otherwise,
                            # refresh_from_db should be called
                            visit.visit_number = v.visit_number
        else:
            # if visits are modified, then, check everything
            visits = (
                Visit.objects.select_for_update()
                .filter(subject=visit.subject)
                .order_by("datetime_begin", "id")
            )
            with transaction.atomic():
                for i, v in enumerate(visits):
                    expected_visit_number = i + 1
                    if (
                        v.visit_number != expected_visit_number
                    ):  # update only those with wrong numbers
                        Visit.objects.filter(id=v.id).update(
                            visit_number=expected_visit_number
                        )
