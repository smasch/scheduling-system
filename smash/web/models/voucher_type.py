# coding=utf-8
from django.db import models

from web.models import Study


class VoucherType(models.Model):
    class Meta:
        app_label = 'web'

    code = models.CharField(max_length=20, verbose_name='Code', blank=False, null=False)

    description = models.CharField(max_length=1024, verbose_name='Description', blank=True, null=False)

    study = models.ForeignKey(
        Study,
        on_delete=models.CASCADE,
        null=False,
    )

    def __str__(self):
        return f"{self.code} ({self.description})"
