# coding=utf-8
import datetime
import logging
from typing import Optional, Union

from django.contrib.auth.models import Permission
from django.contrib.auth.models import AnonymousUser
from django.db import models
from django.db.models import Q
from django.utils import timezone

from django.contrib.auth import get_user_model

from web.models.appointment import Appointment
from web.models.appointment_type_link import AppointmentTypeLink
from web.models.constants import GLOBAL_STUDY_ID, COUNTRY_OTHER_ID, AVAILABILITY_HOLIDAY
from web.models.worker_study_role import STUDY_ROLE_CHOICES, HEALTH_PARTNER_ROLE_CHOICES, \
    VOUCHER_PARTNER_ROLE_CHOICES, WORKER_STAFF, WORKER_HEALTH_PARTNER, WORKER_VOUCHER_PARTNER, ROLE_CHOICES
from web.officeAvailability import OfficeAvailability
from web.utils import get_today_midnight_date
from web.utils import get_weekdays_in_period

logger = logging.getLogger(__name__)


def roles_by_worker_type(worker_type):
    try:
        role_choices = role_choices_by_worker_type(worker_type)
    except TypeError:
        role_choices = []

    roles = []

    for role_type, _ in role_choices:
        roles.append(role_type)
    return roles


def role_choices_by_worker_type(worker_type):
    if worker_type == WORKER_STAFF:
        return STUDY_ROLE_CHOICES
    elif worker_type == WORKER_HEALTH_PARTNER:
        return HEALTH_PARTNER_ROLE_CHOICES
    elif worker_type == WORKER_VOUCHER_PARTNER:
        return VOUCHER_PARTNER_ROLE_CHOICES
    else:
        raise TypeError(f"{worker_type} Unknown worker type")


def worker_type_by_worker(worker):
    roles = worker.roles.filter(study=GLOBAL_STUDY_ID)
    if roles.count() == 0:
        return WORKER_STAFF
    role = roles[0].name
    for role_type, _ in STUDY_ROLE_CHOICES:
        if role_type == role:
            return WORKER_STAFF
    for role_type, _ in HEALTH_PARTNER_ROLE_CHOICES:
        if role_type == role:
            return WORKER_HEALTH_PARTNER
    for role_type, _ in VOUCHER_PARTNER_ROLE_CHOICES:
        if role_type == role:
            return WORKER_VOUCHER_PARTNER
    raise TypeError("Unknown worker role")


class Worker(models.Model):
    class Meta:
        app_label = 'web'

    languages = models.ManyToManyField("web.Language",
                                       verbose_name='Known languages',
                                       blank=True
                                       )
    locations = models.ManyToManyField("web.Location",
                                       verbose_name='Locations',
                                       help_text='Locations where user can perform tasks: '
                                                 'see appointments, be assigned to appointments',
                                       blank=True,
                                       limit_choices_to={"removed": False}
                                       )
    user = models.OneToOneField(get_user_model(), blank=True, null=True,
                                verbose_name='Username', on_delete=models.CASCADE
                                )
    first_name = models.CharField(max_length=50,
                                  verbose_name='First name',
                                  blank=True,
                                  )
    last_name = models.CharField(max_length=50,
                                 verbose_name='Last name',
                                 blank=True,
                                 )
    name = models.CharField(max_length=50,
                            verbose_name='Name',
                            default='',
                            blank=True,
                            null=False
                            )
    phone_number = models.CharField(max_length=20,
                                    verbose_name='Phone number',
                                    blank=True
                                    )
    phone_number_2 = models.CharField(max_length=20,
                                      verbose_name='Phone number 2',
                                      blank=True
                                      )
    fax_number = models.CharField(max_length=20,
                                  verbose_name='Fax number',
                                  blank=True
                                  )
    address = models.CharField(max_length=255,
                               blank=True,
                               verbose_name='Address'
                               )

    postal_code = models.CharField(max_length=7,
                                   blank=True,
                                   verbose_name='Postal code'
                                   )

    city = models.CharField(max_length=50,
                            blank=True,
                            verbose_name='City'
                            )

    country = models.ForeignKey('web.Country',
                                null=False,
                                blank=False,
                                default=COUNTRY_OTHER_ID,
                                verbose_name='Country', on_delete=models.CASCADE
                                )

    voucher_types = models.ManyToManyField("web.VoucherType",
                                           verbose_name='Voucher types',
                                           blank=True
                                           )

    unit = models.CharField(max_length=50,
                            verbose_name='Unit',
                            blank=True
                            )
    email = models.EmailField(
        verbose_name='E-mail',
        blank=True
    )

    specialization = models.CharField(max_length=20,
                                      verbose_name='Specialization',
                                      blank=True
                                      )

    voucher_partner_code = models.CharField(max_length=10,
                                            verbose_name='Code',
                                            blank=True
                                            )

    comment = models.TextField(max_length=1024,
                               verbose_name='Comment',
                               null=True,
                               blank=True
                               )

    privacy_notice_accepted = models.BooleanField(
        default=False,
        verbose_name="Has accepted privacy notice?")

    ldap_user = models.BooleanField(
        default=False,
        verbose_name="Use LDAP authentication")

    def is_on_leave(self):
        if len(self.holiday_set.filter(datetime_end__gt=timezone.now(),
                                       datetime_start__lt=timezone.now(),
                                       kind=AVAILABILITY_HOLIDAY)):
            return True
        return False

    def current_leave_details(self):
        holidays = self.holiday_set.filter(datetime_end__gt=timezone.now(),
                                           datetime_start__lt=timezone.now(),
                                           kind=AVAILABILITY_HOLIDAY).order_by('-datetime_end')
        if len(holidays) > 0:
            return holidays[0]
        else:
            return None

    # noinspection PyUnresolvedReferences
    def disable(self) -> bool:
        # pylint: disable=no-member
        if self.user is not None:
            self.user.is_active = False
            self.user.save()
            logger.info("'%s' account has been disabled", self.user.username)
            return True
        else:
            logger.warning("Cannot disable account for user '%s %s'", self.first_name, self.last_name)
            return False

    # noinspection PyUnresolvedReferences
    def enable(self) -> bool:
        # pylint: disable=no-member
        if self.user is not None:
            self.user.is_active = True
            self.user.save()
            logger.info("'%s' account has been enabled", self.user.username)
            return True
        else:
            logger.warning("Cannot enable account for user '%s %s'", self.first_name, self.last_name)
            return False

    # noinspection PyUnresolvedReferences
    def is_active(self) -> bool:
        # pylint: disable=no-member
        if self.user is not None:
            return self.user.is_active
        else:
            return False

    def availability_percentage(self, start_date: datetime = None, end_date: datetime = None):
        """
            start_date: defaults to None and then is set to today's midnight date
            end_date: defaults to None and then is set to today's midnight date + 24 hours
        """
        today_midnight = get_today_midnight_date()

        if start_date is None:
            start_date = today_midnight
        if end_date is None:
            start_date = start_date.replace(hour=0, minute=0, second=0)
            end_date = start_date + datetime.timedelta(days=1)

        office_availability = OfficeAvailability(f'{self.first_name} {self.last_name}', start=start_date,
                                                 end=end_date)

        # Subject Appointments
        old_events = Q(date_when__gt=start_date) & Q(date_when__gt=end_date)
        future_events = Q(date_when__lt=start_date) & Q(date_when__lt=end_date)
        non_overlap_events = old_events | future_events
        overlap_events = ~non_overlap_events
        query = Q(worker=self.id) & overlap_events
        subject_appointments = AppointmentTypeLink.objects.filter(query)

        # General Appointments
        old_events = Q(datetime_when__gt=start_date) & Q(datetime_when__gt=end_date)
        future_events = Q(datetime_when__lt=start_date) & Q(datetime_when__lt=end_date)
        non_overlap_events = old_events | future_events
        overlap_events = ~non_overlap_events
        query = Q(worker_assigned=self.id) & overlap_events
        general_appointments = Appointment.objects.filter(query)

        # Holidays and extra availabilities.
        old_events = Q(datetime_start__gt=start_date) & Q(datetime_start__gt=end_date)
        future_events = Q(datetime_end__lt=start_date) & Q(datetime_end__lt=end_date)
        non_overlap_events = old_events | future_events
        overlap_events = ~non_overlap_events
        holidays_and_extra_availabilities = self.holiday_set.filter(overlap_events).order_by('-datetime_start')

        # Availability
        weekdays = get_weekdays_in_period(start_date, end_date)
        weekdayQ = Q()  # create a filter for each weekday in the selected period
        for weekday in weekdays:
            weekdayQ = weekdayQ | Q(day_number=weekday)
        availabilities = self.availability_set.filter(person=self.id).filter(weekdayQ).order_by('day_number',
                                                                                                'available_from')

        things = []
        things.extend(availabilities)
        things.extend(holidays_and_extra_availabilities)
        things.extend(subject_appointments)
        things.extend(general_appointments)
        for thing in things:
            office_availability.consider_this(thing, only_working_hours=True)

        return office_availability.get_availability_percentage(only_working_hours=True)

    def get_permissions(self, study):
        # pylint: disable=no-member
        # noinspection PyUnresolvedReferences
        if self.user.is_superuser:
            return {p.codename for p in Permission.objects.all()}

        roles = self.roles.filter(study=study)
        if roles.count() == 0:
            return set()
        return {p.codename for p in roles[0].permissions.all()}

    def has_perm(self, codename, study):
        roles = self.roles.filter(study=study)
        if roles.count() == 0:
            return False

        if roles[0].permissions.filter(codename=codename).all().count() > 0:
            return True
        else:
            return False

    @property
    def role(self):
        roles = self.roles.filter(study=GLOBAL_STUDY_ID)
        if roles.count() == 0:
            return WORKER_STAFF
        role = roles[0].name
        if role not in [role_type for role_type, _ in ROLE_CHOICES]:
            raise TypeError("Unknown worker role")
        return role

    @staticmethod
    def get_by_user(the_user: Union[get_user_model(), 'Worker', AnonymousUser]) -> Optional['Worker']:
        if isinstance(the_user, get_user_model()):
            workers = Worker.objects.filter(user=the_user)
            if len(workers) > 0:
                return workers[0]
            else:
                return None
        elif isinstance(the_user, Worker):
            return the_user
        elif isinstance(the_user, AnonymousUser):
            return None
        elif the_user is not None:
            raise TypeError("Unknown class type: " + the_user.__class__.__name__)
        else:
            return None

    @staticmethod
    def get_workers_by_worker_type(worker_type, study_id=GLOBAL_STUDY_ID):
        return Worker.objects.filter(roles__study_id=study_id,
                                     roles__name__in=roles_by_worker_type(worker_type))

    def __str__(self):
        if self.name != '':
            if self.first_name == '':
                return f"{self.name} ({self.address}, {self.city})"
            else:
                return f"{self.name} {self.first_name} {self.last_name}"
        else:
            return f"{self.first_name} {self.last_name}"

    def initials(self):
        result = ""
        if len(self.first_name) > 0:
            result += self.first_name[0]
        if len(self.last_name) > 0:
            result += self.last_name[0]
        return result
