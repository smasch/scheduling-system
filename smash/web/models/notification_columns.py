# coding=utf-8
from django.db import models


class StudyNotificationParameters(models.Model):
    class Meta:
        app_label = 'web'

    exceeded_visits_visible = models.BooleanField(
        default=True,
        verbose_name='exceeded visit time',
    )

    missing_redcap_subject_visible = models.BooleanField(
        default=True,
        verbose_name='missing RED Cap subject',
    )

    inconsistent_redcap_subject_visible = models.BooleanField(
        default=True,
        verbose_name='inconsistent RED Cap subject',
    )

    subject_require_contact_visible = models.BooleanField(
        default=True,
        verbose_name='subject required contact',
    )

    subject_no_visits_visible = models.BooleanField(
        default=True,
        verbose_name='subject without visit',
    )

    subject_voucher_expiry_visible = models.BooleanField(
        default=False,
        verbose_name='subject vouchers almost expired',
    )

    unfinished_visits_visible = models.BooleanField(
        default=True,
        verbose_name='unfinished visits',
    )

    visits_with_missing_appointments_visible = models.BooleanField(
        default=True,
        verbose_name='visits with missing appointments',
    )

    approaching_visits_without_appointments_visible = models.BooleanField(
        default=True,
        verbose_name='approaching visits',
    )

    approaching_visits_for_mail_contact_visible = models.BooleanField(
        default=True,
        verbose_name='post mail for approaching visits',
    )

    unfinished_appointments_visible = models.BooleanField(
        default=True,
        verbose_name='unfinished appointments',
    )
