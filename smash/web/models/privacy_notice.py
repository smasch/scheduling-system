# coding=utf-8
import os

from django.db import models
from django.dispatch import receiver

from web.templatetags.filters import basename
from web import disable_for_loaddata


class PrivacyNotice(models.Model):
    name = models.CharField(max_length=255, verbose_name="Name")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created at")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Updated at")
    summary = models.CharField(max_length=255, verbose_name="Summary", blank=False, null=False)
    document = models.FileField(
        upload_to="privacy_notices/", verbose_name="Study Privacy Notice file", null=False, editable=True
    )

    def __str__(self):
        return f"{self.name} ({basename(self.document.url)})"

    @property
    def all_studies(self):
        return self.studies.all()


# These two auto-delete files from filesystem when they are unneeded:


@receiver(models.signals.post_delete, sender=PrivacyNotice)
@disable_for_loaddata
def auto_delete_file_on_delete(sender, instance: PrivacyNotice, **kwargs):  # pylint: disable=unused-argument
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.document:
        if os.path.isfile(instance.document.path):
            os.remove(instance.document.path)


@receiver(models.signals.pre_save, sender=PrivacyNotice)
@disable_for_loaddata
def auto_delete_file_on_change(sender, instance: PrivacyNotice, **kwargs):  # pylint: disable=unused-argument
    """
    Deletes old file from filesystem
    when corresponding `PrivacyNotice` object is updated
    with new file.
    """
    if not instance.pk:
        return False
    try:
        old_file = PrivacyNotice.objects.get(pk=instance.pk).document
    except PrivacyNotice.DoesNotExist:
        return False

    new_file = instance.document
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)
