# coding=utf-8
import re

from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import models

from web.utils import strtobool
from web.models.constants import (
    CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE,
    NO_SHOW_APPOINTMENT_COLOR_CONFIGURATION_TYPE,
    KIT_EMAIL_HOUR_CONFIGURATION_TYPE,
    KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE,
    KIT_DAILY_EMAIL_TIME_FORMAT_TYPE,
    KIT_DAILY_EMAIL_DAYS_PERIOD_TYPE,
    VALUE_TYPE_CHOICES,
    VALUE_TYPE_TEXT,
    DEFAULT_FROM_EMAIL,
    VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE,
    KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE,
    VISIT_SHOW_VISIT_NUMBER_FROM_ZERO,
)


def is_valid_email(value: str) -> bool:
    try:
        validate_email(value=value)
        return True
    except ValidationError:
        return False


class ConfigurationItem(models.Model):
    class Meta:
        app_label = "web"

    type = models.CharField(max_length=50, verbose_name="Type", editable=False)
    name = models.CharField(max_length=255, verbose_name="Name", editable=False)

    value = models.TextField(
        max_length=1024,
        verbose_name="Value",
    )
    value_type = models.CharField(
        max_length=32, choices=VALUE_TYPE_CHOICES, verbose_name="Value type", default=VALUE_TYPE_TEXT
    )

    def __str__(self):
        return f"{self.name} {self.value}"

    @staticmethod
    def is_valid(item: "ConfigurationItem") -> bool:
        message = ConfigurationItem.validation_error(item)
        return message == ""

    @staticmethod
    def validation_error(item: "ConfigurationItem") -> str:
        pattern = None
        if item.type in (CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE, NO_SHOW_APPOINTMENT_COLOR_CONFIGURATION_TYPE):
            pattern = "^#[0-9a-fA-F]+$"
        if item.type in (KIT_EMAIL_HOUR_CONFIGURATION_TYPE, VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE):
            pattern = "^[0-9]{2}:[0-9]{2}|$"
        if item.type == KIT_DAILY_EMAIL_DAYS_PERIOD_TYPE:
            pattern = "^[0-9]+$"
        if item.type == KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE:
            pattern = "^(MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY|SUNDAY)$"
        if item.type == KIT_DAILY_EMAIL_TIME_FORMAT_TYPE:
            pattern = "^(%Y-%m-%d|%Y-%m-%d %H:%M)$"
        if item.type == KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE:
            for email in item.value.split(";"):
                if email != "" and not is_valid_email(email):
                    return f"Email {email} address is invalid"

        if item.type == DEFAULT_FROM_EMAIL:
            if not is_valid_email(item.value):
                return "Email address is invalid"
        if item.type == VISIT_SHOW_VISIT_NUMBER_FROM_ZERO:
            try:
                strtobool(item.value)
            except ValueError:
                return "Invalid bool value of param: " + item.name

        if pattern is not None:
            if not re.compile(pattern).match(item.value):
                return "Invalid value of param: " + item.name + ". It should match regex pattern: " + pattern

        return ""
