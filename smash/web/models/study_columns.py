# coding=utf-8
from django.db import models

from web.models.custom_data import CustomStudySubjectField, CustomStudySubjectVisibility


class StudyColumns(models.Model):
    class Meta:
        app_label = 'web'

    postponed = models.BooleanField(
        verbose_name='Postponed',
        default=True
    )

    datetime_contact_reminder = models.BooleanField(
        default=True,
        verbose_name='Please make a contact on'
    )
    type = models.BooleanField(
        default=True,
        verbose_name='Type'
    )

    default_location = models.BooleanField(
        default=True,
        verbose_name='Default appointment location',
    )

    flying_team = models.BooleanField(
        default=True,
        verbose_name='Default flying team location (if applicable)',
    )

    screening_number = models.BooleanField(
        default=True,
        verbose_name='Screening number',
    )
    nd_number = models.BooleanField(
        default=True,
        verbose_name='Subject number',
    )
    comments = models.BooleanField(
        default=True,
        verbose_name='Comments'
    )
    referral = models.BooleanField(
        default=True,
        verbose_name='Referred by'
    )

    information_sent = models.BooleanField(
        default=True,
        verbose_name='Information sent',
    )
    resigned = models.BooleanField(
        default=True,
        verbose_name='Resigned',
    )
    resign_reason = models.BooleanField(
        default=True,
        verbose_name='Resign reason'
    )

    excluded = models.BooleanField(default=False, verbose_name='Excluded')

    exclude_reason = models.BooleanField(default=False, verbose_name='Excluded comments')

    endpoint_reached = models.BooleanField(default=True, verbose_name='Endpoint reached')

    endpoint_reached_reason = models.BooleanField(default=True, verbose_name='Endpoint reached comments')

    referral_letter = models.BooleanField(
        default=False,
        verbose_name='Referral letter'
    )

    health_partner = models.BooleanField(
        default=False,
        verbose_name='Health partner'
    )

    health_partner_feedback_agreement = models.BooleanField(
        default=False,
        verbose_name='Agrees to give information to referral'
    )

    vouchers = models.BooleanField(
        default=False,
        verbose_name='Vouchers',
    )

    @property
    def custom_fields_visibility(self):
        study = self.studysubjectlist_set.all()[0].study
        values = CustomStudySubjectVisibility.objects.filter(visible_subject_study_columns=self)
        fields = list(CustomStudySubjectField.objects.filter(study=study))
        for value in values:
            fields.remove(value.study_subject_field)
        for field in fields:
            CustomStudySubjectVisibility.objects.create(visible_subject_study_columns=self, study_subject_field=field)
        return CustomStudySubjectVisibility.objects.filter(visible_subject_study_columns=self)

    def set_custom_field_visibility(self, custom_study_subject_field: CustomStudySubjectField, visible: bool):
        for existing_value in self.custom_fields_visibility.all():
            if existing_value.study_subject_field == custom_study_subject_field:
                existing_value.visible = visible
                existing_value.save()

    def is_custom_field_visible(self, field: CustomStudySubjectField) -> bool:
        for existing_value in self.custom_fields_visibility.all():
            if existing_value.study_subject_field == field:
                return existing_value.visible
        return False
