from django.db import models


class AppointmentTypeLink(models.Model):
    class Meta:
        permissions = [
            ("view_daily_planning", "Can see daily planning"),
        ]
    appointment = models.ForeignKey("web.Appointment", on_delete=models.CASCADE)
    appointment_type = models.ForeignKey("web.AppointmentType", on_delete=models.CASCADE)
    date_when = models.DateTimeField(null=True, default=None)
    worker = models.ForeignKey("web.Worker", null=True, default=None, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if self.date_when is not None:
            self.date_when = self.date_when.replace(tzinfo=None)
        super().save(*args, **kwargs)
        return self
