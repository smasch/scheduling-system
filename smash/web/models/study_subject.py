# coding=utf-8
import logging
import re
from typing import Optional

from django.db import models
from django.db.models import FilteredRelation, Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from web.models import Appointment, Location, Provenance, Visit, VoucherType
from web.models.constants import BOOL_CHOICES, FILE_STORAGE
from web.models.custom_data import CustomStudySubjectField, CustomStudySubjectValue
from web import disable_for_loaddata

logger = logging.getLogger(__name__)


class StudySubject(models.Model):
    class Meta:
        app_label = "web"

    @property
    def provenances(self):
        return Provenance.objects.filter(
            modified_table=StudySubject._meta.db_table, modified_table_id=self.id
        ).order_by("-modification_date")

    def finish_all_visits(self):
        visits = Visit.objects.filter(subject=self, is_finished=False)
        for visit in visits:
            visit.is_finished = True
            visit.save()

    def finish_all_appointments(self):
        appointments = Appointment.objects.filter(visit__subject=self, status=Appointment.APPOINTMENT_STATUS_SCHEDULED)
        for appointment in appointments:
            appointment.status = Appointment.APPOINTMENT_STATUS_CANCELLED
            appointment.save()

    def mark_as_resigned(self):
        self.resigned = True
        self.finish_all_visits()
        self.finish_all_appointments()

    def mark_as_excluded(self):
        self.excluded = True
        self.finish_all_visits()
        self.finish_all_appointments()

    def mark_as_endpoint_reached(self):
        self.endpoint_reached = True
        self.finish_all_visits()
        self.finish_all_appointments()

    subject = models.ForeignKey(
        "web.Subject",
        verbose_name="Subject",
        editable=False,
        null=False,
        on_delete=models.CASCADE,
    )

    study = models.ForeignKey(
        "web.Study",
        verbose_name="Study",
        editable=False,
        null=False,
        on_delete=models.CASCADE,
    )

    postponed = models.BooleanField(choices=BOOL_CHOICES, verbose_name="Postponed", default=False)
    datetime_contact_reminder = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name="Please make a contact on",
    )

    type = models.ForeignKey(
        "web.SubjectType",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        verbose_name="Type",
    )

    visit_used_to_compute_followup_date = models.ForeignKey(
        "web.Visit",
        null=True,
        on_delete=models.SET_NULL,
        editable=False,
    )

    default_location = models.ForeignKey(
        Location,
        verbose_name="Default appointment location",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        limit_choices_to={"removed": False},
    )

    flying_team = models.ForeignKey(
        "web.FlyingTeam",
        verbose_name="Default flying team location (if applicable)",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    screening_number = models.CharField(max_length=50, verbose_name="Screening number", blank=True, null=True)
    nd_number = models.CharField(
        max_length=25,
        blank=True,
        verbose_name="Subject number",
    )
    comments = models.TextField(max_length=2000, blank=True, verbose_name="Comments")
    date_added = models.DateField(verbose_name="Added on", auto_now_add=True)
    referral = models.CharField(max_length=128, null=True, blank=True, verbose_name="Referred by")
    referral_letter = models.FileField(
        storage=FILE_STORAGE,
        upload_to="referral_letters",
        verbose_name="Referral letter",
        blank=True,
        null=True,
    )

    health_partner = models.ForeignKey(
        "web.Worker",
        verbose_name="Health partner",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )

    health_partner_feedback_agreement = models.BooleanField(
        verbose_name="Agrees to give information to referral",
        default=False,
    )

    voucher_types = models.ManyToManyField(VoucherType, blank=True, verbose_name="Voucher types")

    information_sent = models.BooleanField(verbose_name="Information sent", default=False)

    resigned = models.BooleanField(verbose_name="Resigned", default=False, editable=True)
    resign_reason = models.TextField(max_length=2000, blank=True, verbose_name="Resign reason")
    excluded = models.BooleanField(verbose_name="Excluded", default=False, editable=True)
    exclude_reason = models.TextField(max_length=2000, blank=True, verbose_name="Exclude reason")
    endpoint_reached = models.BooleanField(verbose_name="Endpoint Reached", default=False, editable=True)
    endpoint_reached_reason = models.TextField(max_length=2000, blank=True, verbose_name="Endpoint reached comments")

    def sort_matched_screening_first(self, pattern, reverse=False):
        if self.screening_number is None:
            return None

        parts = self.screening_number.split(";")
        matches, reminder = [], []

        try:
            for part in parts:
                chunks = part.strip().split("-")
                if len(chunks) == 2:
                    letter, number = chunks
                    try:
                        tupl = (letter, int(number))
                    except ValueError:  # better than isdigit because isdigit fails with negative numbers and others
                        tupl = (letter, number)
                else:
                    logger.warning(
                        "There are %d chunks in some parts of this screening_number: |%s|.",
                        len(chunks),
                        self.screening_number,
                    )
                    tupl = (part.strip(), None)
                if pattern is not None and pattern in part:
                    matches.append(tupl)
                else:
                    reminder.append(tupl)
        except BaseException:  # if the format is not the expected format
            matches = parts

        return matches + sorted(reminder, reverse=reverse)

    @staticmethod
    def check_nd_number_regex(regex_str, study):
        nd_numbers = (
            StudySubject.objects.filter(study=study)
            .exclude(nd_number__isnull=True)
            .exclude(nd_number__exact="")
            .all()
            .values_list("nd_number", flat=True)
        )
        regex = re.compile(regex_str)
        for nd_number in nd_numbers:
            if regex.match(nd_number) is None:
                return False
        return True

    def can_schedule(self):
        return not any([self.resigned, self.excluded, self.endpoint_reached, self.subject.dead])

    @property
    def status(self):
        if self.subject.dead:
            return "Deceased"
        elif self.resigned:
            return "Resigned"
        elif self.excluded:
            return "Excluded"
        elif self.endpoint_reached:
            return "Endpoint Reached"
        else:
            return "Normal"

    @property
    def custom_data_values(self):
        # find the custom fields that have not yet been populated into the study subject
        # https://docs.djangoproject.com/en/3.2/ref/models/querysets/#filteredrelation-objects
        fields = CustomStudySubjectField.objects.annotate(
            t=FilteredRelation(
                "customstudysubjectvalue",
                condition=Q(customstudysubjectvalue__study_subject=self),
            )
        ).filter(t__study_subject_field__isnull=True, study=self.study)

        for field in fields:
            CustomStudySubjectValue.objects.create(
                study_subject=self, value=field.default_value, study_subject_field=field
            )

        return CustomStudySubjectValue.objects.filter(study_subject=self).prefetch_related("study_subject_field")

    def set_custom_data_value(self, custom_study_subject_field: CustomStudySubjectField, value: str):
        found = False
        for existing_value in self.customstudysubjectvalue_set.all():
            if existing_value.study_subject_field == custom_study_subject_field:
                found = True
                existing_value.value = value
                existing_value.save()
        if not found:
            self.customstudysubjectvalue_set.add(
                CustomStudySubjectValue.objects.create(
                    study_subject=self,
                    value=value,
                    study_subject_field=custom_study_subject_field,
                )
            )

    def __str__(self):
        # pylint: disable-next=C0209
        return "%s %s" % (self.subject.first_name, self.subject.last_name)

    def get_custom_data_value(self, custom_field: CustomStudySubjectField) -> Optional[CustomStudySubjectValue]:
        for value in self.custom_data_values:
            if value.study_subject_field == custom_field:
                return value
        return None

    def get_custom_field_value(self, param: str) -> str:
        for value in self.custom_data_values.all():
            if value.study_subject_field.name == param:
                return value.value
        return ""

    def set_custom_field_value(self, param: str, value: str):
        for custom_value in self.custom_data_values.all():
            if custom_value.study_subject_field.name == param:
                custom_value.value = value
                custom_value.save()


# SIGNALS
@receiver(post_save, sender=StudySubject)
@disable_for_loaddata
def set_as_resigned_or_excluded_or_endpoint_reached(sender, instance, **kwargs):  # pylint: disable=unused-argument
    if instance.excluded:
        instance.mark_as_excluded()
    if instance.resigned:
        instance.mark_as_resigned()
    if instance.endpoint_reached:
        instance.mark_as_endpoint_reached()
