# coding=utf-8

from django.db import models

from .constants import WEEKDAY_CHOICES


class Availability(models.Model):
    class Meta:
        app_label = 'web'

    person = models.ForeignKey("web.Worker", on_delete=models.CASCADE,
                               verbose_name='Worker'
                               )
    day_number = models.IntegerField(
        verbose_name='Day of the week',
        choices=WEEKDAY_CHOICES
    )
    available_from = models.TimeField(
        verbose_name='Available from',
    )
    available_till = models.TimeField(
        verbose_name='Available until',
    )

    def __str__(self):
        day_of_week = self.get_day_of_week_as_string()
        return f"{day_of_week} {self.person.last_name} {self.person.first_name}"

    def get_day_of_week_as_string(self):
        day_of_week = "N/A"
        for row in WEEKDAY_CHOICES:
            if row[0] == self.day_number:
                day_of_week = row[1]
        return day_of_week
