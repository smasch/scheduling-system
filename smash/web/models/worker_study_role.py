# coding=utf-8
import logging

from django.db import models
from django.contrib.auth.models import Permission

logger = logging.getLogger(__name__)

ROLE_CHOICES_SECRETARY = "SECRETARY"
ROLE_CHOICES_DOCTOR = "DOCTOR"
ROLE_CHOICES_NURSE = "NURSE"
ROLE_CHOICES_PSYCHOLOGIST = "PSYCHOLOGIST"
ROLE_CHOICES_TECHNICIAN = "TECHNICIAN"
ROLE_CHOICES_PROJECT_MANAGER = "PROJECT MANAGER"

ROLE_CHOICES_HEALTH_PARTNER = "HEALTH_PARTNER"
ROLE_CHOICES_VOUCHER_PARTNER = "VOUCHER_PARTNER"

STUDY_ROLE_CHOICES = (
    (ROLE_CHOICES_DOCTOR, 'Doctor'),
    (ROLE_CHOICES_NURSE, 'Nurse'),
    (ROLE_CHOICES_PSYCHOLOGIST, 'Psychologist'),
    (ROLE_CHOICES_TECHNICIAN, 'Technician'),
    (ROLE_CHOICES_SECRETARY, 'Secretary'),
    (ROLE_CHOICES_PROJECT_MANAGER, 'Project Manager')
)

HEALTH_PARTNER_ROLE_CHOICES = (
    (ROLE_CHOICES_HEALTH_PARTNER, "Health Partner"),
)

VOUCHER_PARTNER_ROLE_CHOICES = (
    (ROLE_CHOICES_VOUCHER_PARTNER, "Voucher Partner"),
)

ROLE_CHOICES = STUDY_ROLE_CHOICES + HEALTH_PARTNER_ROLE_CHOICES + VOUCHER_PARTNER_ROLE_CHOICES

WORKER_STAFF = "STAFF"
WORKER_HEALTH_PARTNER = "HEALTH_PARTNER"
WORKER_VOUCHER_PARTNER = "VOUCHER_PARTNER"


class WorkerStudyRole(models.Model):
    class Meta:
        app_label = 'web'

    worker = models.ForeignKey("web.Worker",
                               related_name="roles",
                               on_delete=models.CASCADE
                               )

    study = models.ForeignKey("web.Study", on_delete=models.CASCADE)

    name = models.CharField(max_length=20, choices=STUDY_ROLE_CHOICES,
                            verbose_name='Role'
                            )
    permissions = models.ManyToManyField(Permission, verbose_name='Worker Study Permissions', blank=True)

    @property  # in case some code still calls .role property
    def role(self):
        return self.name
