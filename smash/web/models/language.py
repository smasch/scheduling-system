# coding=utf-8
import locale
import platform

from django.db import models

from .constants import LOCALE_CHOICES, DEFAULT_LOCALE_NAME


class Language(models.Model):
    class Meta:
        app_label = 'web'
        ordering = ["order"]

    name = models.CharField(max_length=20, unique=True)
    image = models.ImageField()
    order = models.IntegerField(default=0)
    locale = models.CharField(max_length=10, choices=LOCALE_CHOICES, null=False, blank=False,
                              default=DEFAULT_LOCALE_NAME)
    windows_locale_name = models.CharField(max_length=10, choices=LOCALE_CHOICES, null=False, blank=False,
                                           default="French")

    def __str__(self):
        return self.name

    def is_locale_installed(self) -> bool:
        if platform.system() == 'Windows':
            locale_name = self.windows_locale_name
        else:
            locale_name = self.locale
        old = locale.getlocale(locale.LC_TIME)

        try:
            locale.setlocale(locale.LC_TIME, locale_name)
        except locale.Error:
            return False
        finally:
            locale.setlocale(locale.LC_TIME, old)
        return True

    def image_img(self):
        if self.image:
            return f'<img class="flag-icon" src="{self.image.url}" />'
        else:
            return 'No image'

    image_img.short_description = 'Flag icon'
    image_img.allow_tags = True
