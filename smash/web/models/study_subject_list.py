# coding=utf-8
from django.db import models

from web.models import Study, SubjectColumns, StudyColumns

SUBJECT_LIST_GENERIC = "GENERIC"
SUBJECT_LIST_NO_VISIT = "NO_VISIT"
SUBJECT_LIST_REQUIRE_CONTACT = "REQUIRE_CONTACT"
SUBJECT_LIST_VOUCHER_EXPIRY = "VOUCHER_EXPIRY"

SUBJECT_LIST_CHOICES = {
    SUBJECT_LIST_GENERIC: 'Generic subject list',
    SUBJECT_LIST_NO_VISIT: 'Subjects without visit',
    SUBJECT_LIST_REQUIRE_CONTACT: 'Subjects required contact',
    SUBJECT_LIST_VOUCHER_EXPIRY: 'Subject with vouchers to be expired soon'
}


class StudySubjectList(models.Model):
    class Meta:
        app_label = 'web'

    study = models.ForeignKey(
        Study,
        on_delete=models.CASCADE,
        null=False,
        editable=False,
    )

    visible_subject_study_columns = models.ForeignKey(
        StudyColumns,
        on_delete=models.CASCADE,
        null=False,
        editable=False,
    )

    visible_subject_columns = models.ForeignKey(
        SubjectColumns,
        on_delete=models.CASCADE,
        null=False,
        editable=False,
    )

    last_contact_attempt = models.BooleanField(
        default=False,
        verbose_name='Last contact attempt'
    )

    visits = models.BooleanField(
        default=True,
        verbose_name='Visits summary'
    )

    edit = models.BooleanField(
        default=True,
        verbose_name='Edit'
    )

    type = models.CharField(max_length=50,
                            choices=list(SUBJECT_LIST_CHOICES.items()),
                            verbose_name='Type of list',
                            null=True,
                            blank=True,
                            editable=False,
                            )
