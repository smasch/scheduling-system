# coding=utf-8
from django.db import models


class SubjectColumns(models.Model):
    class Meta:
        app_label = 'web'

    sex = models.BooleanField(max_length=1,
                              default=False,
                              verbose_name='Sex',
                              )

    first_name = models.BooleanField(max_length=1,
                                     default=True,
                                     verbose_name='First name'
                                     )

    social_security_number = models.BooleanField(default=False,
                                                 verbose_name='Social security_number'
                                                 )

    last_name = models.BooleanField(max_length=1,
                                    default=True,
                                    verbose_name='Last name'
                                    )

    languages = models.BooleanField(max_length=1,
                                    default=False,
                                    verbose_name='Known languages'
                                    )

    default_written_communication_language = models.BooleanField(max_length=1,
                                                                 default=False,
                                                                 verbose_name='Default language for document generation'
                                                                 )
    phone_number = models.BooleanField(max_length=1,
                                       default=False,
                                       verbose_name='Phone number'
                                       )

    phone_number_2 = models.BooleanField(max_length=1,
                                         default=False,
                                         verbose_name='Phone number 2'
                                         )

    phone_number_3 = models.BooleanField(max_length=1,
                                         default=False,
                                         verbose_name='Phone number 3'
                                         )

    email = models.BooleanField(max_length=1,
                                default=False,
                                verbose_name='E-mail'
                                )

    date_born = models.BooleanField(max_length=1,
                                    default=False,
                                    verbose_name='Date of birth'
                                    )

    address = models.BooleanField(max_length=1,
                                  default=False,
                                  verbose_name='Address'
                                  )

    postal_code = models.BooleanField(max_length=1,
                                      default=False,
                                      verbose_name='Postal code'
                                      )

    city = models.BooleanField(max_length=1,
                               default=False,
                               verbose_name='City'
                               )

    country = models.BooleanField(max_length=1,
                                  default=False,
                                  verbose_name='Country'
                                  )

    dead = models.BooleanField(max_length=1,
                               default=True,
                               verbose_name='Deceased',
                               )

    next_of_kin_name = models.BooleanField(max_length=1,
                                           default=False,
                                           verbose_name='Next of kin',
                                           )

    next_of_kin_phone = models.BooleanField(max_length=1,
                                            default=False,
                                            verbose_name='Next of kin phone',
                                            )

    next_of_kin_address = models.BooleanField(max_length=1,
                                              default=False,
                                              verbose_name='Next of kin address',
                                              )
