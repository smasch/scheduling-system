# coding=utf-8
from django.db import models


class FlyingTeam(models.Model):
    class Meta:
        app_label = 'web'

    place = models.CharField(max_length=255, verbose_name='Place')

    def __str__(self):
        return f"{self.place}"
