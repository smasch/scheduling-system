# coding=utf-8
from django.db import models

from web.models import Study, SubjectColumns, VisitColumns, AppointmentColumns, StudyColumns

APPOINTMENT_LIST_GENERIC = "GENERIC"
APPOINTMENT_LIST_UNFINISHED = "UNFINISHED"
APPOINTMENT_LIST_APPROACHING = "APPROACHING"

APPOINTMENT_LIST_CHOICES = {
    APPOINTMENT_LIST_GENERIC: 'Generic',
    APPOINTMENT_LIST_UNFINISHED: 'Unfinished',
    APPOINTMENT_LIST_APPROACHING: 'Approaching',
}


class AppointmentList(models.Model):
    class Meta:
        app_label = 'web'

    study = models.ForeignKey(
        Study,
        on_delete=models.CASCADE,
        null=False,
    )

    visible_visit_columns = models.ForeignKey(
        VisitColumns,
        on_delete=models.CASCADE,
        null=False,
    )

    visible_subject_columns = models.ForeignKey(
        SubjectColumns,
        on_delete=models.CASCADE,
        null=False,
    )
    visible_study_subject_columns = models.ForeignKey(
        StudyColumns,
        on_delete=models.CASCADE,
        null=False,
    )
    visible_appointment_columns = models.ForeignKey(
        AppointmentColumns,
        on_delete=models.CASCADE,
        null=False,
    )

    type = models.CharField(max_length=50,
                            choices=list(APPOINTMENT_LIST_CHOICES.items()),
                            verbose_name='Type of list',
                            )
