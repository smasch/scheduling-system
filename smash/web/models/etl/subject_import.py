# coding=utf-8
import logging

from django.db import models
from django.db.models import Field, Q

from web.models import Language, SubjectType, Country
from web.models.etl.etl_import_data import EtlImportData

logger = logging.getLogger(__name__)


class SubjectImportData(EtlImportData):
    location = models.ForeignKey("web.Location",
                                 verbose_name='Default location',
                                 blank=True,
                                 null=True,
                                 on_delete=models.SET_NULL
                                 )
    country = models.ForeignKey("web.Country",
                                verbose_name='Default country',
                                blank=True,
                                null=True,
                                on_delete=models.SET_NULL,
                                limit_choices_to=~Q(name="---")
                                )


def field_can_be_imported(field: Field) -> bool:
    if field.get_internal_type() in ("CharField", "DateField", "TextField"):
        return True
    if field.get_internal_type() == "ForeignKey" and field.related_model in (Language, SubjectType, Country):
        return True
    return False
