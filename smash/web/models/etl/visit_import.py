# coding=utf-8
import logging

from django.db import models

from web.models.etl.etl_import_data import EtlImportData

logger = logging.getLogger(__name__)


class VisitImportData(EtlImportData):
    appointment_type = models.ForeignKey("web.AppointmentType",
                                         verbose_name='Default appointment type',
                                         blank=True,
                                         null=True,
                                         on_delete=models.CASCADE
                                         )

    subject_id_column_name = models.CharField(max_length=128,
                                              verbose_name='Subject id column name',
                                              default='donor_id',
                                              null=False,
                                              blank=False
                                              )

    visit_date_column_name = models.CharField(max_length=128,
                                              verbose_name='Visit date column name',
                                              default='dateofvisit',
                                              null=False,
                                              blank=False
                                              )

    location_column_name = models.CharField(max_length=128,
                                            verbose_name='Location column name',
                                            default='adressofvisit',
                                            null=False,
                                            blank=False
                                            )

    visit_number_column_name = models.CharField(max_length=128,
                                                verbose_name='Visit number column name',
                                                default='visit_id',
                                                null=False,
                                                blank=False
                                                )
