# coding=utf-8
import logging

from django.db import models

from web.models.etl.etl_export_data import EtlExportData

logger = logging.getLogger(__name__)


class VisitExportData(EtlExportData):
    subject_id_column_name = models.CharField(max_length=128,
                                              verbose_name='Subject id column name',
                                              default='donor_id',
                                              null=False,
                                              blank=False
                                              )
