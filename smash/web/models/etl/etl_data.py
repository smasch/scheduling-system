# coding=utf-8
import logging
import os
from typing import Type

from django.conf import settings
from django.db import models

from .etl_column_mapping import EtlColumnMapping

logger = logging.getLogger(__name__)


class EtlData(models.Model):

    study = models.ForeignKey("web.Study", verbose_name="Study", editable=False, null=False, on_delete=models.CASCADE)

    filename = models.CharField(
        max_length=128, verbose_name="File used for automatic import", default="", null=False, blank=True
    )
    run_at_times = models.TextField(
        max_length=1024, verbose_name="At what time automatic import should run", default="", null=False, blank=True
    )

    csv_delimiter = models.CharField(max_length=1, verbose_name="CSV delimiter", default=",", null=False, blank=False)
    date_format = models.CharField(
        max_length=20, verbose_name="Date format", default="%Y-%m-%d", null=False, blank=False
    )

    def set_column_mapping(self, object_type: Type[models.Model], column_name: str, csv_column_name: str):
        for entry in self.column_mappings.all():
            if entry.table_name == object_type._meta.db_table and entry.column_name == column_name:
                entry.csv_column_name = csv_column_name
                entry.save()
                return
        EtlColumnMapping.objects.create(
            etl_data=self,
            table_name=object_type._meta.db_table,
            column_name=column_name,
            csv_column_name=csv_column_name,
        )

    def get_absolute_file_path(self) -> str:
        return os.path.join(settings.ETL_ROOT, self.filename)
