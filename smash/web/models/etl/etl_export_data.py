# coding=utf-8
import logging

from django.db import models
from django.db.models import Field

from web.models import Language, SubjectType, Country
from web.models.etl.etl_data import EtlData

logger = logging.getLogger(__name__)


class EtlExportData(EtlData):
    class Meta:
        abstract = True

    collection_delimiter = models.CharField(max_length=1,
                                            verbose_name='delimiter used to separate multiple values in single cell',
                                            default=';',
                                            null=False,
                                            blank=False
                                            )
    export_object_as_id = models.BooleanField(
        default=True,
        verbose_name="Should references to objects be exported as id")


def field_can_be_exported(field: Field) -> bool:
    if field.get_internal_type() in ("CharField", "DateField", "TextField", "IntegerField", "DateTimeField"):
        return True
    if field.get_internal_type() == "ForeignKey" and field.related_model in (Language, SubjectType, Country):
        return True
    if field.get_internal_type() == "ManyToManyField" and field.related_model in (Language,):
        return True
    return False
