# coding=utf-8
import logging
import os

from django.conf import settings
from django.db import models

from .etl_data import EtlData

logger = logging.getLogger(__name__)


class EtlImportData(EtlData):

    class Meta:
        abstract = True

    import_worker = models.ForeignKey("web.Worker",
                                      verbose_name='Worker used by importer',
                                      blank=True,
                                      null=True,
                                      on_delete=models.CASCADE
                                      )

    def file_available(self) -> bool:
        if self.filename is None or self.filename == '':
            return False
        absolute_path = self.get_absolute_file_path()
        if os.path.basename(absolute_path) != self.filename:
            logger.warning('File "%s" outside defined ETL_ROOT: %s', self.filename, settings.ETL_ROOT)
            return False
        return os.path.isfile(absolute_path)
