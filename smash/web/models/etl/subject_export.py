# coding=utf-8
import logging

from web.models.etl.etl_export_data import EtlExportData

logger = logging.getLogger(__name__)


class SubjectExportData(EtlExportData):
    pass
