# coding=utf-8
import logging

from django.db import models

logger = logging.getLogger(__name__)


class EtlColumnMapping(models.Model):
    etl_data = models.ForeignKey(
        "web.EtlData",
        verbose_name="Importer",
        editable=False,
        null=False,
        on_delete=models.CASCADE,
        related_name="column_mappings",
    )

    table_name = models.CharField(max_length=128, default="", null=False, blank=True)

    column_name = models.CharField(max_length=128, default="", null=False, blank=True)

    csv_column_name = models.TextField(max_length=1024, default="", null=False, blank=True)

    enabled = models.BooleanField(default=True, null=False, blank=False)
