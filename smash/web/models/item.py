# coding=utf-8
from django.db import models


class Item(models.Model):
    class Meta:
        app_label = 'web'

    is_fixed = models.BooleanField(
        default=False,
        verbose_name='Is the item fixed?'
    )

    disposable = models.BooleanField(
        default=False,
        verbose_name='Disposable set'
    )

    name = models.CharField(max_length=255,
                            verbose_name='Name'
                            )

    def __str__(self):
        return self.name
