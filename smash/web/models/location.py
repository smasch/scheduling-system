# coding=utf-8
from django.db import models


class Location(models.Model):
    class Meta:
        app_label = 'web'

    name = models.CharField(max_length=256)

    color = models.CharField(max_length=20,
                             verbose_name='Calendar appointment color',
                             blank=True,
                             default="")

    prefix = models.CharField(max_length=1, blank=True)

    removed = models.BooleanField(default=False,
                                  editable=False,
                                  )

    def __str__(self):
        return f"{self.name}"
