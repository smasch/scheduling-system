# coding=utf-8

from django.db import models

from web.models import VoucherType, StudySubject, Worker
from web.models.constants import VOUCHER_STATUS_CHOICES, VOUCHER_STATUS_NEW


class Voucher(models.Model):
    class Meta:
        app_label = 'web'

    number = models.CharField(
        max_length=50,
        verbose_name='Number',
        blank=False,
        null=False,
        unique=True
    )

    issue_date = models.DateField(verbose_name='Issue date', null=False)
    expiry_date = models.DateField(verbose_name='Expiry date', null=False)

    issue_worker = models.ForeignKey(Worker,
                                     verbose_name='Issued by',
                                     null=False,
                                     related_name='issued_vouchers',
                                     on_delete=models.CASCADE)

    hours = models.IntegerField(
        verbose_name='Hours',
        default=0,
        null=False
    )

    voucher_type = models.ForeignKey(
        VoucherType,
        on_delete=models.CASCADE,
        null=False,
    )

    study_subject = models.ForeignKey(
        StudySubject,
        on_delete=models.CASCADE,
        null=False,
        related_name="vouchers",
        editable=False
    )

    status = models.CharField(max_length=20, choices=VOUCHER_STATUS_CHOICES,
                              verbose_name='Status',
                              default=VOUCHER_STATUS_NEW
                              )

    activity_type = models.CharField(max_length=40,
                                     verbose_name='Activity type',
                                     null=False,
                                     blank=True,
                                     default=''
                                     )

    feedback = models.TextField(max_length=2000,
                                blank=True,
                                verbose_name='Feedback'
                                )

    usage_partner = models.ForeignKey(
        Worker,
        on_delete=models.CASCADE,
        null=False
    )

    def __str__(self):
        return f"{self.number} - {self.study_subject.subject.first_name} {self.study_subject.subject.last_name}"
