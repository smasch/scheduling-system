# coding=utf-8
from django.db import models

from web.models import Study, SubjectColumns, VisitColumns
from web.models import StudyColumns

VISIT_LIST_GENERIC = "GENERIC"
VISIT_LIST_EXCEEDED_TIME = "EXCEEDED_TIME"
VISIT_LIST_UNFINISHED = "UNFINISHED"
VISIT_LIST_MISSING_APPOINTMENTS = "MISSING_APPOINTMENTS"
VISIT_LIST_APPROACHING_WITHOUT_APPOINTMENTS = "APPROACHING_WITHOUT_APPOINTMENTS"
VISIT_LIST_APPROACHING_FOR_MAIL_CONTACT = "APPROACHING_FOR_MAIL_CONTACT"

VISIT_LIST_CHOICES = {
    VISIT_LIST_GENERIC: 'Generic visit list',
    VISIT_LIST_EXCEEDED_TIME: 'Exceeded visit time',
    VISIT_LIST_UNFINISHED: 'Unfinished visits',
    VISIT_LIST_MISSING_APPOINTMENTS: 'Visits with missing appointments',
    VISIT_LIST_APPROACHING_WITHOUT_APPOINTMENTS: 'Approaching visits',
    VISIT_LIST_APPROACHING_FOR_MAIL_CONTACT: 'Post mail for approaching visits',
}


class StudyVisitList(models.Model):
    class Meta:
        app_label = 'web'

    study = models.ForeignKey(
        Study,
        on_delete=models.CASCADE,
        null=False,
    )

    visible_visit_columns = models.ForeignKey(
        VisitColumns,
        on_delete=models.CASCADE,
        null=False,
    )

    visible_subject_columns = models.ForeignKey(
        SubjectColumns,
        on_delete=models.CASCADE,
        null=False,
    )
    visible_study_subject_columns = models.ForeignKey(
        StudyColumns,
        on_delete=models.CASCADE,
        null=False,
    )

    visible_appointment_types_in_progress = models.BooleanField(default=False,
                                                                verbose_name='Appointments in progress'
                                                                )
    visible_appointment_types_done = models.BooleanField(default=False,
                                                         verbose_name='Done appointments'
                                                         )
    visible_appointment_types_missing = models.BooleanField(default=False,
                                                            verbose_name='Missing appointments'
                                                            )

    type = models.CharField(max_length=50,
                            choices=list(VISIT_LIST_CHOICES.items()),
                            verbose_name='Type of list',
                            )
