# coding=utf-8

from django.db import models


class Country(models.Model):
    class Meta:
        app_label = 'web'
        ordering = ["order"]

    name = models.CharField(max_length=50)
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.name
