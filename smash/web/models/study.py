# coding=utf-8
import re

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from web.models import StudyColumns, StudyNotificationParameters, StudyRedCapColumns

FOLLOW_UP_INCREMENT_IN_YEARS = 'years'
FOLLOW_UP_INCREMENT_IN_MONTHS = 'months'
FOLLOW_UP_INCREMENT_IN_WEEKS = 'weeks'
FOLLOW_UP_INCREMENT_IN_DAYS = 'days'
FOLLOW_UP_INCREMENT_UNIT_CHOICE = {
    FOLLOW_UP_INCREMENT_IN_YEARS: 'Years',
    FOLLOW_UP_INCREMENT_IN_DAYS: 'Days'
}


class Study(models.Model):
    class Meta:
        app_label = 'web'

    name = models.CharField(max_length=255, verbose_name='Name')

    nd_number_study_subject_regex = models.CharField(
        max_length=255, verbose_name='Study Subject ND Number Regex', default=r'^ND\d{4}$',
        help_text='Defines the regex to check the ID used for each study subject. '
                  'Keep in mind that this regex should be valid for all previous study subjects in the database.')

    columns = models.OneToOneField(
        StudyColumns,
        on_delete=models.CASCADE,
    )
    notification_parameters = models.OneToOneField(
        StudyNotificationParameters,
        on_delete=models.CASCADE,
    )

    redcap_columns = models.OneToOneField(
        StudyRedCapColumns,
        on_delete=models.CASCADE,
    )

    redcap_first_visit_number = models.IntegerField(
        default=1,
        verbose_name="Number of the first visit in redcap system"
    )

    sample_mail_statistics = models.BooleanField(
        default=False,
        verbose_name="Email with sample collections should use statistics"
    )

    visits_to_show_in_subject_list = models.IntegerField(
        verbose_name='Number of visits to show in the subject list',
        default=5,
        validators=[MaxValueValidator(100), MinValueValidator(1)]
    )

    default_voucher_expiration_in_months = models.IntegerField(
        verbose_name='Duration of the vouchers in months',
        default=3,
        validators=[MinValueValidator(1)]
    )

    default_visit_duration_in_months = models.IntegerField(
        verbose_name='Duration of the visits in months',
        help_text='Duration of the visit, this is, the time interval, in months, when the appointments may take place',
        default=6,
        validators=[MinValueValidator(1)]
    )

    study_privacy_notice = models.ForeignKey("web.PrivacyNotice",
                                             verbose_name='Study Privacy Note',
                                             editable=True,
                                             blank=True,
                                             null=True,
                                             on_delete=models.SET_NULL,
                                             related_name='studies'
                                             )

    acceptance_of_study_privacy_notice_required = models.BooleanField(
        default=False,
        verbose_name="Is privacy notice acceptance required?"
    )

    def check_nd_number(self, nd_number):
        regex = re.compile(self.nd_number_study_subject_regex)
        return regex.match(nd_number) is not None

    def __str__(self):
        return f"{self.name}"

    @property
    def has_vouchers(self):
        return self.columns.vouchers

    @staticmethod
    def get_by_id(study_id):
        study = Study.objects.filter(id=study_id)
        if len(study) > 0:
            return study[0]
        else:
            return None
