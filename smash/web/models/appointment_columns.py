# coding=utf-8
from django.db import models


class AppointmentColumns(models.Model):
    class Meta:
        app_label = 'web'

    flying_team = models.BooleanField(default=False,
                                      verbose_name='Flying team',
                                      )

    worker_assigned = models.BooleanField(default=False,
                                          verbose_name='Worker conducting the assessment',
                                          )

    appointment_types = models.BooleanField(default=True,
                                            verbose_name='Appointment types',
                                            )

    room = models.BooleanField(default=False,
                               verbose_name='Room',
                               )

    location = models.BooleanField(default=False,
                                   verbose_name='Location',
                                   )

    comment = models.BooleanField(default=False,
                                  verbose_name='Comment',
                                  )

    datetime_when = models.BooleanField(default=True,
                                        verbose_name='Comment',
                                        )

    length = models.BooleanField(default=False,
                                 verbose_name='Appointment length',
                                 )

    status = models.BooleanField(default=False,
                                 verbose_name='Status',
                                 )

    post_mail_sent = models.BooleanField(default=False,
                                         verbose_name='Post mail sent',
                                         )
