# coding=utf-8
from django.db import models


class Provenance(models.Model):
    class Meta:
        app_label = "web"
        index_together = ["modified_table", "modified_table_id", "modification_date"]

    modified_table = models.CharField(max_length=128, verbose_name="Modified table", blank=False, null=True)

    modified_table_id = models.IntegerField(default=0, verbose_name="Modified table row", blank=False, null=True)

    modification_date = models.DateTimeField(verbose_name="Modified on", null=False, blank=False, auto_now_add=True)

    modification_author = models.ForeignKey(
        "web.Worker",
        verbose_name="Worker who modified the row",
        null=True,
        blank=False,
        on_delete=models.deletion.CASCADE,
    )

    modified_field = models.CharField(max_length=128, verbose_name="Modified field", blank="", null=False)

    previous_value = models.TextField(max_length=2048, verbose_name="Previous Value", blank=True, null=True)

    new_value = models.TextField(max_length=2048, verbose_name="New Value", blank=True, null=True)

    modification_description = models.TextField(max_length=20480, verbose_name="Description", blank=False, null=False)

    request_path = models.TextField(max_length=20480, verbose_name="Request Path", blank=True, null=True)

    request_ip_addr = models.GenericIPAddressField(verbose_name="Request IP Address", null=True)
