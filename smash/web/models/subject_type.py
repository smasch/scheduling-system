# coding=utf-8

from django.core.validators import MinValueValidator
from django.db import models

from web.models.study import FOLLOW_UP_INCREMENT_UNIT_CHOICE, FOLLOW_UP_INCREMENT_IN_YEARS
from . import Study


class SubjectType(models.Model):
    class Meta:
        app_label = 'web'

    name = models.CharField(
        max_length=50,
        verbose_name='Name',
        blank=False,
        null=False
    )

    screening_number_prefix = models.CharField(max_length=5)

    follow_up_delta_time = models.IntegerField(
        verbose_name='Time difference between subject visits',
        help_text='Time difference between visits used to automatically create follow up visits',
        default=1,
        null=True,
        validators=[MinValueValidator(1)]
    )

    follow_up_delta_units = models.CharField(max_length=10,
                                             choices=list(FOLLOW_UP_INCREMENT_UNIT_CHOICE.items()),
                                             verbose_name='Units for the follow up time difference',
                                             help_text='Units for the number of days between visits',
                                             default=FOLLOW_UP_INCREMENT_IN_YEARS,
                                             blank=False
                                             )

    study = models.ForeignKey(
        Study,
        null=False,
        editable=False,
        on_delete=models.CASCADE,
    )

    auto_create_follow_up = models.BooleanField(
        default=True,
        verbose_name="Auto create follow up visit"
    )

    def __str__(self):
        return f"{self.name}"
