# coding=utf-8
from django.db import models


class StudyRedCapColumns(models.Model):
    class Meta:
        app_label = 'web'

    sex = models.BooleanField(
        verbose_name='Sex',
        default=True
    )

    date_born = models.BooleanField(
        verbose_name='Date of birth',
        default=True
    )

    dead = models.BooleanField(
        verbose_name='Dead',
        default=True
    )

    mpower_id = models.BooleanField(
        verbose_name='MPower ID',
        default=True
    )

    languages = models.BooleanField(
        verbose_name='Languages',
        default=True
    )
