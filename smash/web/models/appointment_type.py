# coding=utf-8
from django.db import models

from .constants import APPOINTMENT_TYPE_DEFAULT_COLOR, APPOINTMENT_TYPE_DEFAULT_FONT_COLOR


class AppointmentType(models.Model):
    class Meta:
        app_label = "web"
        ordering = ["description"]

    required_equipment = models.ManyToManyField("web.Item", verbose_name="Required equipment", blank=True)
    code = models.CharField(max_length=20, verbose_name="Appointment code")
    description = models.TextField(max_length=2000, verbose_name="Appointment description")
    default_duration = models.IntegerField(verbose_name="Default duration (in minutes)")
    calendar_color_priority = models.IntegerField(verbose_name="Calendar color priority", default=1)
    calendar_color = models.TextField(
        max_length=2000, verbose_name="Calendar color", default=APPOINTMENT_TYPE_DEFAULT_COLOR
    )
    calendar_font_color = models.TextField(
        max_length=2000, verbose_name="Calendar font color", default=APPOINTMENT_TYPE_DEFAULT_FONT_COLOR
    )
    rest_time = models.IntegerField(verbose_name="Suggested rest time", default=0)
    can_be_parallelized = models.BooleanField(verbose_name="Can be parallelized", default=False)
    REQ_ROLE_CHOICES = (("DOCTOR", "Doctor"), ("NURSE", "Nurse"), ("PSYCHOLOGIST", "Psychologist"), ("ANY", "Any"))
    required_worker = models.CharField(
        max_length=20, choices=REQ_ROLE_CHOICES, verbose_name="Type of worker required for appointment", default="ANY"
    )

    def __str__(self):
        return self.description
