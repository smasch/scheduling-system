# coding=utf-8
from django.db import models

from .constants import AVAILABILITY_CHOICES, AVAILABILITY_HOLIDAY


class Holiday(models.Model):
    class Meta:
        app_label = 'web'

    person = models.ForeignKey("web.Worker", on_delete=models.CASCADE,
                               verbose_name='Worker'
                               )
    datetime_start = models.DateTimeField(
        verbose_name='On leave since'
    )
    datetime_end = models.DateTimeField(
        verbose_name='On leave until'
    )

    info = models.TextField(max_length=2000,
                            blank=True,
                            verbose_name='Comments'
                            )

    kind = models.CharField(max_length=1,
                            choices=AVAILABILITY_CHOICES,
                            default=AVAILABILITY_HOLIDAY,
                            help_text='Defines the kind of availability. Either Holiday or Extra Availability.')

    def __str__(self):
        return f"{self.person.first_name} {self.person.last_name}"
