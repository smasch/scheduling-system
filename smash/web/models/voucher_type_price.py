# coding=utf-8
from django.db import models

from web.models import VoucherType


class VoucherTypePrice(models.Model):
    class Meta:
        app_label = 'web'

    price = models.DecimalField(max_digits=6, decimal_places=2, verbose_name='Price', null=False, blank=False)
    start_date = models.DateField(verbose_name='Start date', null=False, blank=False)
    end_date = models.DateField(verbose_name='End date', null=False, blank=False)

    voucher_type = models.ForeignKey(
        VoucherType,
        on_delete=models.CASCADE,
        null=False,
        related_name="prices"
    )
