# coding=utf-8
from django.db import models

from .constants import CONTACT_TYPES_CHOICES, CONTACT_TYPES_PHONE

__author__ = 'Valentin Grouès'


class ContactAttempt(models.Model):
    subject = models.ForeignKey("web.StudySubject",
                                verbose_name='Subject', on_delete=models.CASCADE
                                )
    worker = models.ForeignKey("web.Worker", null=True,
                               verbose_name='Worker', on_delete=models.CASCADE
                               )
    type = models.CharField(max_length=2, default=CONTACT_TYPES_PHONE, choices=CONTACT_TYPES_CHOICES)

    datetime_when = models.DateTimeField(verbose_name="When", help_text='When did the contact occurred?')

    success = models.BooleanField(default=False)

    comment = models.TextField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return f"{self.subject} {self.worker}"
