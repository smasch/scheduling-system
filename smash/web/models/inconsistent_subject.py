# coding=utf-8
import logging

from django.db import models

logger = logging.getLogger(__name__)


class InconsistentField(models.Model):
    name = models.CharField(max_length=255,
                            verbose_name='Field name',
                            null=False,
                            blank=False
                            )
    smash_value = models.CharField(max_length=255,
                                   verbose_name='Smash value'
                                   )
    redcap_value = models.CharField(max_length=255,
                                    verbose_name='RED Cap value'
                                    )
    inconsistent_subject = models.ForeignKey("web.InconsistentSubject",
                                             verbose_name='Invalid fields',
                                             on_delete=models.CASCADE
                                             )

    @staticmethod
    def create(name, smash_value, redcap_value):
        result = InconsistentField()
        result.name = name
        result.smash_value = smash_value
        if result.smash_value is None:
            result.smash_value = "[None]"
        result.redcap_value = redcap_value
        if result.redcap_value is None:
            result.redcap_value = "[None]"
        return result

    def __str__(self):
        return self.name + ": " + self.smash_value + "; RED Cap: " + self.redcap_value


class InconsistentSubject(models.Model):
    class Meta:
        app_label = 'web'

    subject = models.ForeignKey("web.StudySubject",
                                verbose_name='Subject',
                                null=True,
                                blank=True,
                                on_delete=models.CASCADE
                                )

    redcap_url = models.CharField(max_length=255,
                                  verbose_name='URL to RED Cap subject',
                                  null=True,
                                  blank=True
                                  )

    def __str__(self):
        return "Subject: " + str(self.subject)
