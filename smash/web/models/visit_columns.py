# coding=utf-8
from django.db import models

from web.models.constants import BOOL_CHOICES


class VisitColumns(models.Model):
    class Meta:
        app_label = 'web'

    datetime_begin = models.BooleanField(choices=BOOL_CHOICES,
                                         verbose_name='Visit starts date',
                                         default=True
                                         )

    datetime_end = models.BooleanField(choices=BOOL_CHOICES,
                                       verbose_name='Visit ends date',
                                       default=True
                                       )

    is_finished = models.BooleanField(choices=BOOL_CHOICES,
                                      verbose_name='Is finished',
                                      default=True
                                      )

    post_mail_sent = models.BooleanField(choices=BOOL_CHOICES,
                                         verbose_name='Post mail sent',
                                         default=True
                                         )

    visit_number = models.BooleanField(choices=BOOL_CHOICES,
                                       verbose_name='Visit number',
                                       default=True
                                       )
    visible_appointment_types = models.BooleanField(default=False,
                                                    verbose_name='All appointments'
                                                    )
