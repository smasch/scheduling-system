# coding=utf-8
from django.db import models


class MissingSubject(models.Model):
    class Meta:
        app_label = 'web'

    ignore = models.BooleanField(
        default=False,
        verbose_name='Ignore missing subject'
    )

    subject = models.ForeignKey("web.StudySubject",
                                verbose_name='Subject',
                                null=True,
                                blank=True,
                                on_delete=models.CASCADE
                                )

    redcap_id = models.CharField(max_length=255,
                                 verbose_name='RED Cap id',
                                 null=True,
                                 blank=True
                                 )
    redcap_url = models.CharField(max_length=255,
                                  verbose_name='URL to RED Cap subject',
                                  null=True,
                                  blank=True
                                  )

    @staticmethod
    def create(red_cap_subject=None, smash_subject=None, url=None):
        result = MissingSubject()
        if red_cap_subject is not None:
            result.redcap_id = red_cap_subject.nd_number
        result.subject = smash_subject
        result.redcap_url = url
        return result

    def __str__(self):
        if self.subject is not None:
            return "Subject: " + str(self.subject)
        return "RED Cap subject: " + self.redcap_id
