# coding=utf-8
import logging

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from .constants import SEX_CHOICES, COUNTRY_OTHER_ID
from web.models import Country, Visit, Appointment, Provenance
from web import disable_for_loaddata
from . import Language

logger = logging.getLogger(__name__)


class Subject(models.Model):
    class Meta:
        app_label = "web"
        permissions = [
            ("export_subjects", "Can export subject data to excel/csv"),
        ]

    @property
    def provenances(self):
        return Provenance.objects.filter(modified_table=Subject._meta.db_table, modified_table_id=self.id).order_by(
            "-modification_date"
        )

    sex = models.CharField(max_length=1, choices=SEX_CHOICES, verbose_name="Sex")

    first_name = models.CharField(max_length=50, verbose_name="First name")

    social_security_number = models.CharField(
        max_length=50,
        verbose_name="Social security number",
        blank=True,
    )

    last_name = models.CharField(max_length=50, verbose_name="Last name")

    languages = models.ManyToManyField(Language, blank=True, verbose_name="Known languages")

    default_written_communication_language = models.ForeignKey(
        Language,
        null=True,
        blank=True,
        related_name="subjects_written_communication",
        verbose_name="Default language for document generation",
        on_delete=models.SET_NULL,
    )
    phone_number = models.CharField(max_length=64, null=True, blank=True, verbose_name="Phone number")

    phone_number_2 = models.CharField(max_length=64, null=True, blank=True, verbose_name="Phone number 2")

    phone_number_3 = models.CharField(max_length=64, null=True, blank=True, verbose_name="Phone number 3")

    email = models.EmailField(null=True, blank=True, verbose_name="E-mail")

    date_born = models.DateField(null=True, blank=True, verbose_name="Date of birth (YYYY-MM-DD)")

    address = models.CharField(max_length=255, blank=True, verbose_name="Address")

    postal_code = models.CharField(max_length=7, blank=True, verbose_name="Postal code")

    city = models.CharField(max_length=50, blank=True, verbose_name="City")

    country = models.ForeignKey(
        Country, null=False, blank=False, default=COUNTRY_OTHER_ID, verbose_name="Country", on_delete=models.CASCADE
    )

    next_of_kin_name = models.CharField(max_length=255, blank=True, verbose_name="Next of kin")

    next_of_kin_phone = models.CharField(max_length=50, blank=True, verbose_name="Next of kin phone")

    next_of_kin_address = models.TextField(max_length=2000, blank=True, verbose_name="Next of kin address")

    dead = models.BooleanField(verbose_name="Deceased", default=False, editable=True)

    def pretty_address(self):
        return f"{self.address} ({self.postal_code}), {self.city}. {self.country}"

    def mark_as_dead(self):
        self.dead = True
        self.finish_all_visits()
        self.finish_all_appointments()

    def finish_all_visits(self):
        visits = Visit.objects.filter(subject__subject=self, is_finished=False)
        for visit in visits:
            visit.is_finished = True
            visit.save()

    def finish_all_appointments(self):
        appointments = Appointment.objects.filter(
            visit__subject__subject=self, status=Appointment.APPOINTMENT_STATUS_SCHEDULED
        )
        for appointment in appointments:
            appointment.status = Appointment.APPOINTMENT_STATUS_CANCELLED
            appointment.save()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


# SIGNALS
@receiver(post_save, sender=Subject)
@disable_for_loaddata
def set_as_deceased(sender, instance, **kwargs):  # pylint: disable=unused-argument
    if instance.dead:
        p = Provenance(
            modified_table=Subject._meta.db_table,
            modified_table_id=instance.id,
            modification_author=None,
            previous_value=instance.dead,
            new_value=True,
            modification_description=f'Subject "{instance}" marked as dead',
            modified_field="dead",
        )
        instance.mark_as_dead()
        p.save()
