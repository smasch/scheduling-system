# coding=utf-8

from django.db import models

from web.models import Voucher


class VoucherPartnerSession(models.Model):
    class Meta:
        app_label = 'web'

    length = models.IntegerField(
        verbose_name='Length (minutes)',
        null=False
    )

    date = models.DateTimeField(verbose_name='Issue date', null=False)

    voucher = models.ForeignKey(
        Voucher,
        on_delete=models.CASCADE,
        null=False,
        related_name="voucher_partner_sessions",
        editable=False
    )
