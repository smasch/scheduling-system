from functools import wraps


def disable_for_loaddata(signal_handler):
    """
    Decorator that turns off signal handlers when loading fixture data.
    https://docs.djangoproject.com/en/3.2/ref/django-admin/#what-s-a-fixture
    """

    @wraps(signal_handler)
    def wrapper(*args, **kwargs):
        if "raw" in kwargs and kwargs["raw"]:
            return
        signal_handler(*args, **kwargs)

    return wrapper
