from django.conf import settings

DB_NAME = 'default'
DB_BACKEND = settings.DATABASES[DB_NAME]['ENGINE'].split('.')[-1]


def is_sqlite_db():
    return DB_BACKEND == 'sqlite3'
