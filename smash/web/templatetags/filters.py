# See: http://stackoverflow.com/a/18962481
import datetime
import os
from functools import reduce

from django import template
from django.forms import CheckboxSelectMultiple, CheckboxInput
from django.utils.safestring import mark_safe

from web.utils import strtobool
from web.models import ConfigurationItem
from web.models.constants import VISIT_SHOW_VISIT_NUMBER_FROM_ZERO

register = template.Library()


@register.filter(name="add_class")
def add_class(value, arg):
    # Get all the field's initial css-classes
    if isinstance(value.field.widget, CheckboxSelectMultiple):
        return value
    if isinstance(value.field.widget, CheckboxInput):
        arg = "checkbox"
    css_classes = value.field.widget.attrs.get("class", " ").split(" ")
    # Filter out zero-length class names ('')
    css_classes = [x for x in css_classes if len(x) > 0]
    # Convert list to string
    css_classes = reduce(lambda a, x: f"{a} {x}", css_classes, "")
    css_classes = f"{css_classes} {arg}"
    # Return the widget with freshly crafted classes
    return value.as_widget(attrs={"class": css_classes})


@register.filter(name="disable")
def disable(value):
    value.field.widget.attrs["disabled"] = "disabled"
    return value


@register.filter(name="is_checkbox")
def is_checkbox(value):
    return isinstance(value.field.widget, CheckboxSelectMultiple)


@register.filter(name="render_appointments")
def render_appointments(statistics, appointment_type):
    html = ""
    for status_count in statistics.get(appointment_type, []):
        html += f"<td>{status_count}</td>"
    return mark_safe(html)


@register.filter(name="timestamp")
def timestamp(value):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (value.replace(tzinfo=None) - epoch).total_seconds()


@register.filter(name="display_visit_number")
def display_visit_number(visit_number):
    visit_from_zero = ConfigurationItem.objects.get(type=VISIT_SHOW_VISIT_NUMBER_FROM_ZERO).value
    # True values are y, yes, t, true, on and 1; false values are n, no, f, false, off and 0.
    if strtobool(visit_from_zero):
        return visit_number - 1
    else:
        return visit_number


@register.filter(name="basename")
def basename(path):
    return os.path.basename(path)
