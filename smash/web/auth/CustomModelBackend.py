import logging

from django.contrib.auth.backends import ModelBackend

from web.models import Worker

logger = logging.getLogger(__name__)


class CustomModelBackend(ModelBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):

        count = Worker.objects.filter(user__username=username, user__is_active=True, ldap_user=False).count()

        if count == 1:
            return super().authenticate(request, username, password, **kwargs)
        else:
            return None
