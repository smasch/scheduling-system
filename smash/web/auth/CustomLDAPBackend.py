import logging

from django_auth_ldap.backend import LDAPBackend

from web.models import Worker

logger = logging.getLogger(__name__)


class CustomLDAPBackend(LDAPBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        count = Worker.objects.filter(user__username=username, user__is_active=True, ldap_user=True).count()
        if count == 1:
            logger.debug("Trying LDAP auth with: %s", username)
            return super().authenticate(request, username, password, **kwargs)
        else:
            return None
