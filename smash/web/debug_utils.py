# coding=utf-8
import time


def timeit(method):
    """
    Debug decorator to measure the execution time of some method or function
    """

    def timed(*args, **kw):
        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((end_time - start_time) * 1000)
        else:
            print(f'{method.__name__!r}  {(end_time - start_time) * 1000:2.2f} ms')
        return result

    return timed
