# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-10-03 09:11


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0119_auto_20181002_0908'),
    ]

    operations = [
        migrations.AddField(
            model_name='holiday',
            name='kind',
            field=models.CharField(choices=[('H', 'Holiday'), ('X', 'Extra Availability')], default='H', max_length=16),
        ),
    ]
