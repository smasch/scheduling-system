# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-04-26 06:58


from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('web', '0037_appointment_types_descriptions_changes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactattempt',
            name='datetime_when',
            field=models.DateTimeField(help_text='When did the contact occurred?', verbose_name='When'),
        ),
        migrations.AlterField(
            model_name='subject',
            name='pd_in_family',
            field=models.NullBooleanField(default=False, verbose_name='PD in family'),
        ),
    ]
