# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-04-05 07:39


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0030_subject_information_sent'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='post_mail_sent',
            field=models.BooleanField(default=False, verbose_name='Post mail sent'),
        ),
    ]
