# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-09-12 14:54


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0050_configurationitem_redcap_items"),
    ]

    operations = [
        migrations.CreateModel(
            name="MissingSubject",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("ignore", models.BooleanField(default=False, verbose_name="Ignore missing subject")),
                ("redcap_id", models.CharField(blank=True, max_length=255, null=True, verbose_name="RED Cap id")),
                (
                    "redcap_url",
                    models.CharField(blank=True, max_length=255, null=True, verbose_name="URL to RED Cap subject"),
                ),
                (
                    "subject",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="web.Subject",
                        verbose_name="Subject",
                    ),
                ),
            ],
        ),
    ]
