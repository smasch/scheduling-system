# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-01 10:13


from django.db import migrations, models
import django.db.models.deletion


# noinspection PyUnusedLocal
# noinspection PyPep8Naming
def create_default_study_columns(apps, schema_editor):  # pylint: disable=unused-argument
    # We can't import the Study model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    StudyColumns = apps.get_model("web", "StudyColumns")
    study_columns = StudyColumns.objects.create()
    study_columns.save()


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0071_auto_20171130_1607"),
    ]

    operations = [
        migrations.CreateModel(
            name="StudyColumns",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                (
                    "postponed",
                    models.BooleanField(choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Postponed"),
                ),
                (
                    "datetime_contact_reminder",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Please make a contact on"
                    ),
                ),
                (
                    "type",
                    models.BooleanField(choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Type"),
                ),
                (
                    "default_location",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")],
                        default=True,
                        verbose_name="Default appointment location",
                    ),
                ),
                (
                    "flying_team",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")],
                        default=True,
                        verbose_name="Default flying team location (if applicable)",
                    ),
                ),
                (
                    "screening_number",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Screening number"
                    ),
                ),
                (
                    "nd_number",
                    models.BooleanField(choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="ND number"),
                ),
                (
                    "mpower_id",
                    models.BooleanField(choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="MPower ID"),
                ),
                (
                    "comments",
                    models.BooleanField(choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Comments"),
                ),
                (
                    "referral",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Referred by"
                    ),
                ),
                (
                    "diagnosis",
                    models.BooleanField(choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Diagnosis"),
                ),
                (
                    "year_of_diagnosis",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Year of diagnosis (YYYY)"
                    ),
                ),
                (
                    "information_sent",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Information sent"
                    ),
                ),
                (
                    "pd_in_family",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="PD in family"
                    ),
                ),
                (
                    "resigned",
                    models.BooleanField(choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Resigned"),
                ),
                (
                    "resign_reason",
                    models.BooleanField(
                        choices=[(True, "Yes"), (False, "No")], default=True, verbose_name="Resign reason"
                    ),
                ),
            ],
        ),
        migrations.RunPython(create_default_study_columns),
        migrations.AddField(
            model_name="study",
            name="columns",
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, to="web.StudyColumns"),
            preserve_default=False,
        ),
    ]
