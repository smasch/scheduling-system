from django.db import migrations

from web.models.constants import REDCAP_TOKEN_CONFIGURATION_TYPE, REDCAP_BASE_URL_CONFIGURATION_TYPE


def configuration_item_color_fields(apps, schema_editor):  # pylint: disable=unused-argument
    # We can't import the ConfigurationItem model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    ConfigurationItem = apps.get_model("web", "ConfigurationItem")
    cancelled_appointment_color = ConfigurationItem.objects.create()
    cancelled_appointment_color.type = REDCAP_TOKEN_CONFIGURATION_TYPE
    cancelled_appointment_color.value = ""
    cancelled_appointment_color.name = "API Token for RED Cap integration"
    cancelled_appointment_color.save()

    cancelled_appointment_color = ConfigurationItem.objects.create()
    cancelled_appointment_color.type = REDCAP_BASE_URL_CONFIGURATION_TYPE
    cancelled_appointment_color.value = ""
    cancelled_appointment_color.name = "Base url of RED Cap (ie  'https://pd-redcap.uni.lu/redcap/')"
    cancelled_appointment_color.save()


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0049_auto_20170912_1438"),
    ]

    operations = [
        migrations.RunPython(configuration_item_color_fields),
    ]
