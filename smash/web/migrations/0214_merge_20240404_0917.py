# Generated by Django 3.2.22 on 2024-04-04 09:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0212_customstudysubjectfield_tracked'),
        ('web', '0213_alter_studysubject_referral_letter'),
    ]

    operations = [
    ]
