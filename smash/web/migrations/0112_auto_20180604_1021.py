# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-06-04 10:21


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0111_auto_20180601_1318"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mailtemplate",
            name="context",
            field=models.CharField(
                choices=[("A", "Appointment"), ("S", "Subject"), ("V", "Visit"), ("C", "Voucher")], max_length=1
            ),
        ),
        migrations.AlterField(
            model_name="mailtemplate",
            name="language",
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to="web.Language"),
        ),
    ]
