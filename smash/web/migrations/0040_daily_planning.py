# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-05-09 11:49


import django.db.models.deletion
from django.db import migrations, models

from ..models import AppointmentTypeLink


def convert_records(apps, schema_editor):  # pylint: disable=unused-argument
    Appointment = apps.get_model("web", "Appointment")
    for appointment in Appointment.objects.all():
        for appointment_type_id in appointment.appointment_types.values_list("id", flat=True):
            new_link = AppointmentTypeLink(appointment_id=appointment.id, appointment_type_id=appointment_type_id)
            new_link.save()


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0039_pd_family_default_unknown"),
    ]

    operations = [
        migrations.CreateModel(
            name="AppointmentTypeLink",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("date_when", models.DateTimeField(null=True, default=None)),
                ("appointment", models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="web.Appointment")),
                (
                    "appointment_type",
                    models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="web.AppointmentType"),
                ),
                (
                    "worker",
                    models.ForeignKey(
                        default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to="web.Worker"
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="appointment",
            name="appointment_types_new",
            field=models.ManyToManyField(
                blank=True,
                related_name="new_appointment",
                through="web.AppointmentTypeLink",
                to="web.AppointmentType",
                verbose_name="Appointment types",
            ),
        ),
        migrations.RunPython(convert_records),
        migrations.RunSQL("DROP TABLE web_appointment_appointment_types"),
        migrations.RenameField(
            model_name="appointment", old_name="appointment_types_new", new_name="appointment_types"
        ),
    ]
