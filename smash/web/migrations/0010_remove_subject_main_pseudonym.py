# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-02-08 11:55


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0009_remove_subject_visit_count'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subject',
            name='main_pseudonym',
        ),
    ]
