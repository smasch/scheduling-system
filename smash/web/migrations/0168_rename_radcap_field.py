# -*- coding: utf-8 -*-


from django.db import migrations

from web.models.constants import RED_CAP_SAMPLE_DATE_FIELD_TYPE, RED_CAP_KIT_ID_FIELD_TYPE


def create_item(apps, _type, value, name):
    # We can't import the ConfigurationItem model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    ConfigurationItem = apps.get_model("web", "ConfigurationItem")
    item = ConfigurationItem.objects.create()
    item.type = _type
    item.value = value
    item.name = name
    item.save()


def configuration_items(apps, schema_editor):  # pylint: disable=unused-argument
    create_item(apps, RED_CAP_SAMPLE_DATE_FIELD_TYPE, "", "Redcap field for sample date in the visit")


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0167_auto_20200428_1110"),
    ]

    operations = [
        migrations.RunSQL(
            "update web_configurationitem, set type = '"
            + RED_CAP_KIT_ID_FIELD_TYPE
            + "' where type = '"
            + RED_CAP_SAMPLE_DATE_FIELD_TYPE
            + "';"
        ),
        migrations.RunSQL(
            "update web_configurationitem, set name = 'Redcap field for sample kit id in the visit' where type = '"
            + RED_CAP_KIT_ID_FIELD_TYPE
            + "';"
        ),
        migrations.RunPython(configuration_items),
    ]
