# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import migrations

from web.models.constants import (
    DEFAULT_FROM_EMAIL,
    IMPORTER_USER,
    IMPORT_APPOINTMENT_TYPE,
    NEXMO_API_KEY,
    NEXMO_API_SECRET,
    NEXMO_DEFAULT_FROM,
    LOGIN_PAGE_BACKGROUND_IMAGE,
)


def create_item(apps, item_type, value, name):
    # We can't import the ConfigurationItem model directly as it may be a newer
    # version than this migration expects. We use the historical version.

    # noinspection PyPep8Naming
    ConfigurationItem = apps.get_model("web", "ConfigurationItem")
    item = ConfigurationItem.objects.create()
    item.type = item_type
    item.value = value
    item.name = name
    item.save()


# noinspection PyUnusedLocal
def configuration_items(apps, schema_editor):  # pylint: disable=unused-argument
    email_from = getattr(settings, "DEFAULT_FROM_EMAIL", "")
    create_item(
        apps, DEFAULT_FROM_EMAIL, email_from, "Default email address used in the from field when sending emails"
    )

    subject_import_file = getattr(settings, "DAILY_SUBJECT_IMPORT_FILE", "")
    create_item(apps, "DAILY_SUBJECT_IMPORT_FILE", subject_import_file, "File used to import subjects automatically")

    subject_export_file = getattr(settings, "DAILY_SUBJECT_EXPORT_FILE", "")
    create_item(apps, "DAILY_SUBJECT_EXPORT_FILE", subject_export_file, "File used to export subjects automatically")

    visit_import_file = getattr(settings, "DAILY_VISIT_IMPORT_FILE", "")
    create_item(apps, "DAILY_VISIT_IMPORT_FILE", visit_import_file, "File used to import visits automatically")

    visit_export_file = getattr(settings, "DAILY_VISIT_EXPORT_FILE", "")
    create_item(apps, "DAILY_VISIT_EXPORT_FILE", visit_export_file, "File used to export visits automatically")

    subject_import_run_at_times = getattr(settings, "SUBJECT_IMPORT_RUN_AT", ["23:45"])
    create_item(
        apps,
        "SUBJECT_IMPORT_RUN_AT",
        ";".join(subject_import_run_at_times),
        "At what times should the subject importer run",
    )

    export_run_at_times = getattr(settings, "EXPORT_RUN_AT", ["23:55"])
    create_item(
        apps, "SUBJECT_EXPORT_RUN_AT", ";".join(export_run_at_times), "At what times should the subject exporter run"
    )
    create_item(
        apps, "VISIT_EXPORT_RUN_AT", ";".join(export_run_at_times), "At what times should the visit exporter run"
    )

    visit_import_run_at_times = getattr(settings, "VISIT_IMPORT_RUN_AT", ["23:55"])
    create_item(
        apps, "VISIT_IMPORT_RUN_AT", ";".join(visit_import_run_at_times), "At what times should the visit importer run"
    )

    importer_user_name = getattr(settings, "IMPORTER_USER", "")
    create_item(
        apps, IMPORTER_USER, importer_user_name, "User that should be assigned to changes introduced by importer"
    )

    appointment_code = getattr(settings, "IMPORT_APPOINTMENT_TYPE", "SAMPLES")
    create_item(apps, IMPORT_APPOINTMENT_TYPE, appointment_code, "Type of appointment assigned to imported visits")

    nexmo_api_key = getattr(settings, "NEXMO_API_KEY", "")
    create_item(apps, NEXMO_API_KEY, nexmo_api_key, "NEXMO API KEY")

    nexmo_api_secret = getattr(settings, "NEXMO_API_SECRET", "")
    create_item(apps, NEXMO_API_SECRET, nexmo_api_secret, "NEXMO API SECRET")

    nexmo_api_from = getattr(settings, "NEXMO_DEFAULT_FROM", "")
    if nexmo_api_from is None or nexmo_api_from == "":
        nexmo_api_from = "SMASCH"
    create_item(apps, NEXMO_DEFAULT_FROM, nexmo_api_from, "The sender of the message from NEXMO (phone number or text)")

    background_image = getattr(settings, "LOGIN_PAGE_BACKGROUND_IMAGE", "background.jpg")
    create_item(
        apps,
        LOGIN_PAGE_BACKGROUND_IMAGE,
        background_image,
        "Path to a static file containing background image, used in login.html",
    )


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0175_auto_20201109_1404"),
    ]

    operations = [
        migrations.RunPython(configuration_items),
    ]
