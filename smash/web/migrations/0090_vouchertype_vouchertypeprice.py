# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-08 09:00


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("web", "0089_unfinshed_appointment_list"),
    ]

    operations = [
        migrations.CreateModel(
            name="VoucherType",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("code", models.CharField(max_length=20, verbose_name="Code")),
                ("description", models.TextField(blank=True, max_length=1024, verbose_name="Description")),
                ("study", models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="web.Study")),
            ],
        ),
        migrations.CreateModel(
            name="VoucherTypePrice",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("price", models.DecimalField(decimal_places=2, max_digits=6, verbose_name="Price")),
                ("start_date", models.DateField(verbose_name="Start date")),
                ("end_date", models.DateField(verbose_name="End date")),
                ("voucher_type", models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="web.VoucherType")),
            ],
        ),
    ]
