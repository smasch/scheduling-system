# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-04-20 14:35


from django.db import migrations, models


def add_prefixes(apps, schema_editor):  # pylint: disable=unused-argument
    Location = apps.get_model("web", "Location")
    updates_to_perform = [
        ("LIH", "L"),
        ("PRC", "P"),
    ]
    for update_to_perform in updates_to_perform:
        name, prefix = update_to_perform
        location = Location.objects.filter(name=name).first()
        if location is not None:
            location.prefix = prefix
            location.save()


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0034_mail_templates"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="worker",
            name="screening_number_prefix",
        ),
        migrations.AddField(
            model_name="location",
            name="prefix",
            field=models.CharField(max_length=1, blank=True),
        ),
        migrations.RunPython(add_prefixes),
    ]
