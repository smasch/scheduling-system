# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-04-04 14:16


from django.db import migrations

from ..migration_functions import is_sqlite_db


class Migration(migrations.Migration):
    dependencies = [
        ('web', '0029_auto_20170404_1616'),
    ]

    if is_sqlite_db():
        operations = [
            migrations.RunSQL(
                "update web_subject set information_sent=1 where id in" +
                "(select web_subject.id from web_appointment  " +
                " left join web_visit on visit_id = web_visit.id " +
                " left join web_subject on web_visit.subject_id = web_subject.id where status = 'FINISHED');"),
        ]
    else:
        operations = [
            migrations.RunSQL(
                "update web_subject set information_sent=true where id in" +
                "(select web_subject.id from web_appointment  " +
                " left join web_visit on visit_id = web_visit.id " +
                " left join web_subject on web_visit.subject_id = web_subject.id where status = 'FINISHED');"),
        ]
