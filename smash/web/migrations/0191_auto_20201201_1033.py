# Generated by Django 3.1.3 on 2020-12-01 10:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0190_remove_study_related_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='studycolumns',
            name='mpower_id',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='diagnosis',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='screening',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='brain_donation_agreement',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='pd_in_family',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='previously_in_study',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='year_of_diagnosis',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_1_collection_date',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_1_iga_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_1_igg_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_1_result',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_1_updated',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_2_collection_date',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_2_iga_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_2_igg_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_2_result',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_2_updated',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_3_collection_date',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_3_iga_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_3_igg_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_3_result',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_3_updated',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_4_collection_date',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_4_iga_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_4_igg_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_4_result',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_4_updated',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_5_collection_date',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_5_iga_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_5_igg_status',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_5_result',
        ),
        migrations.RemoveField(
            model_name='studycolumns',
            name='virus_test_5_updated',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_1_collection_date',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_1_iga_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_1_igg_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_1_result',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_1_updated',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_2_collection_date',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_2_iga_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_2_igg_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_2_result',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_2_updated',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_3_collection_date',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_3_iga_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_3_igg_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_3_result',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_3_updated',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_4_collection_date',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_4_iga_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_4_igg_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_4_result',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_4_updated',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_5_collection_date',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_5_iga_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_5_igg_status',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_5_result',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='virus_test_5_updated',

        ),


        migrations.RemoveField(
            model_name='studysubject',
            name='mpower_id',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='diagnosis',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='screening',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='brain_donation_agreement',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='pd_in_family',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='previously_in_study',
        ),
        migrations.RemoveField(
            model_name='studysubject',
            name='year_of_diagnosis',
        ),
        migrations.AlterField(
            model_name='studycolumns',
            name='nd_number',
            field=models.BooleanField(default=True, verbose_name='Subject number'),
        ),
        migrations.AlterField(
            model_name='studysubject',
            name='nd_number',
            field=models.CharField(blank=True, max_length=25, verbose_name='Subject number'),
        ),
    ]
