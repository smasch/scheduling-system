# -*- coding: utf-8 -*-
# assigns default flying team location for subjects that had in the past finished appointment at flying team location


from django.db import migrations

from ..migration_functions import is_sqlite_db


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0046_subject_flying_team"),
    ]
    if is_sqlite_db():
        operations = [
            migrations.RunSQL(
                "update web_subject set flying_team_id = (select flying_team_id from web_visit, web_appointment "
                + "where web_subject.id=web_visit.subject_id and "
                + "web_visit.id = web_appointment.visit_id and "
                + "web_appointment.flying_team_id is not null and status = 'FINISHED') "
                "where web_subject.flying_team_id is null"
            ),
        ]
    else:
        operations = [
            migrations.RunSQL(
                "update web_subject set flying_team_id = web_appointment.flying_team_id from web_visit, web_appointment"
                + " where web_subject.id=web_visit.subject_id and "
                + "web_subject.flying_team_id is null and "
                + "web_visit.id = web_appointment.visit_id and "
                + "web_appointment.flying_team_id is not null and status = 'FINISHED';"
            ),
        ]
