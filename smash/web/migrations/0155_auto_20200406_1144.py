# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2020-04-06 11:44


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0154_add_permission_to_existing_workers'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='provenance',
            index_together=set([('modified_table', 'modified_table_id')]),
        ),
    ]
