from django.db import migrations

from web.models.constants import GLOBAL_STUDY_ID


# noinspection PyUnusedLocal
def create_visit_export_data(apps, schema_editor):  # pylint: disable=unused-argument
    # noinspection PyPep8Naming
    SubjectExportData = apps.get_model("web", "VisitExportData")
    # noinspection PyPep8Naming
    Study = apps.get_model("web", "Study")

    entry = SubjectExportData()
    entry.study = Study.objects.get(pk=GLOBAL_STUDY_ID)

    entry.filename = ""
    entry.run_at_times = ""
    entry.delimiter = ","
    entry.save()


# noinspection PyUnusedLocal
def create_subject_import_data(apps, schema_editor):  # pylint: disable=unused-argument
    # noinspection PyPep8Naming
    SubjectExportData = apps.get_model("web", "SubjectExportData")
    # noinspection PyPep8Naming
    Study = apps.get_model("web", "Study")

    entry = SubjectExportData()
    entry.study = Study.objects.get(pk=GLOBAL_STUDY_ID)

    entry.filename = ""
    entry.run_at_times = ""
    entry.delimiter = ","
    entry.save()


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0210_auto_20220331_1150"),
    ]

    operations = [
        migrations.RunPython(create_visit_export_data),
        migrations.RunPython(create_subject_import_data),
    ]
