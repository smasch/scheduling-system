# Generated by Django 3.1.4 on 2021-10-25 09:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0204_merge_20211020_0805"),
    ]

    operations = [
        migrations.AlterField(
            model_name="appointment",
            name="flying_team",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="web.flyingteam",
                verbose_name="Flying team (if applicable)",
            ),
        ),
        migrations.AlterField(
            model_name="appointment",
            name="location",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT, to="web.location", verbose_name="Location"
            ),
        ),
        migrations.AlterField(
            model_name="appointment",
            name="room",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="web.room",
                verbose_name="Room ID",
            ),
        ),
        migrations.AlterField(
            model_name="subject",
            name="default_written_communication_language",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="subjects_written_communication",
                to="web.language",
                verbose_name="Default language for document generation",
            ),
        ),
    ]
