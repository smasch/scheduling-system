# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-10-24 18:59


from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0120_study_nd_number_study_subject_regex"),
    ]

    operations = [
        migrations.AlterField(
            model_name="study",
            name="nd_number_study_subject_regex",
            field=models.CharField(
                default="^ND\\d{4}$",
                help_text="Defines the regex to check the ID used for each study subject."
                + " Keep in mind that this regex should be valid for all previous study subjects in the database.",
                max_length=255,
                verbose_name="Study Subject ND Number Regex",
            ),
        ),
    ]
