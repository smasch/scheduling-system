# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-11-13 15:50


import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0132_study_default_visit_duration_in_months"),
    ]

    operations = [
        migrations.AddField(
            model_name="study",
            name="default_delta_time_for_control_follow_up",
            field=models.IntegerField(
                default=4,
                help_text="Time difference between visits used to automatically create follow up visits",
                validators=[django.core.validators.MinValueValidator(1)],
                verbose_name="Time difference between control visits",
            ),
        ),
        migrations.AddField(
            model_name="study",
            name="default_delta_time_for_follow_up_units",
            field=models.CharField(
                choices=[("days", "Days"), ("years", "Years")],
                default="years",
                help_text="Units for the number of days between visits for both patients and controls",
                max_length=10,
                verbose_name="Units for the follow up incrementals",
            ),
        ),
        migrations.AddField(
            model_name="study",
            name="default_delta_time_for_patient_follow_up",
            field=models.IntegerField(
                default=1,
                help_text="Time difference between visits used to automatically create follow up visits",
                validators=[django.core.validators.MinValueValidator(1)],
                verbose_name="Time difference between patient visits",
            ),
        ),
        migrations.AlterField(
            model_name="study",
            name="default_visit_duration_in_months",
            field=models.IntegerField(
                default=6,
                help_text="Duration of the visit, this is, the time interval,"
                + " in months, when the appointments may take place",
                validators=[django.core.validators.MinValueValidator(1)],
                verbose_name="Duration of the visits in months",
            ),
        ),
        migrations.AlterField(
            model_name="study",
            name="default_voucher_expiration_in_months",
            field=models.IntegerField(
                default=3,
                validators=[django.core.validators.MinValueValidator(1)],
                verbose_name="Duration of the vouchers in months",
            ),
        ),
    ]
