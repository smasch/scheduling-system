# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-04-04 09:43


from django.db import migrations

from web.models.constants import KIT_DAILY_EMAIL_DAYS_PERIOD_TYPE, KIT_DAILY_EMAIL_TIME_FORMAT_TYPE


def create_item(apps, _type, value, name):
    # We can't import the ConfigurationItem model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    ConfigurationItem = apps.get_model("web", "ConfigurationItem")
    item = ConfigurationItem.objects.create()
    item.type = _type
    item.value = value
    item.name = name
    item.save()


def configuration_items(apps, schema_editor):  # pylint: disable=unused-argument
    create_item(apps, KIT_DAILY_EMAIL_DAYS_PERIOD_TYPE, "7", "Number of days with sample kits included in email")
    create_item(
        apps, KIT_DAILY_EMAIL_TIME_FORMAT_TYPE, "%Y-%m-%d %H:%M", "Appointment date format used in sample kits email"
    )


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0157_auto_20200414_0909"),
    ]

    operations = [
        migrations.RunPython(configuration_items),
    ]
