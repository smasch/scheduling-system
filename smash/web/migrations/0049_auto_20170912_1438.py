# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-09-12 12:38


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0048_auto_20170911_1504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configurationitem',
            name='name',
            field=models.CharField(editable=False, max_length=255, verbose_name='Name'),
        ),
    ]
