# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-10-10 12:29


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0121_auto_20181003_1256'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='worker',
            name='appointments',
        ),
    ]
