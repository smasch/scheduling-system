# Generated by Django 3.1.4 on 2021-09-27 13:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0196_merge_20210824_0809'),
    ]

    operations = [
        migrations.AddField(
            model_name='worker',
            name='ldap_user',
            field=models.BooleanField(default=False, verbose_name='Use LDAP authentication'),
        ),
    ]
