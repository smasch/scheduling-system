# Generated by Django 2.0.13 on 2020-11-09 14:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0174_auto_20201105_1157'),
    ]

    operations = [
        migrations.AddField(
            model_name='provenance',
            name='request_ip_addr',
            field=models.GenericIPAddressField(null=True, verbose_name='Request IP Address'),
        )
    ]
