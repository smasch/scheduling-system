from django.db import migrations

from web.models.constants import GLOBAL_STUDY_ID


def get_val(apps, item_type: str):
    # noinspection PyPep8Naming
    ConfigurationItem = apps.get_model("web", "ConfigurationItem")
    items = ConfigurationItem.objects.filter(type=item_type)
    if len(items) == 0:
        return None
    return items[0].value


def delete_config_option(apps, item_type: str):
    # noinspection PyPep8Naming
    ConfigurationItem = apps.get_model("web", "ConfigurationItem")
    ConfigurationItem.objects.filter(type=item_type).delete()


# noinspection PyUnusedLocal
def create_visit_import_data(apps, schema_editor):  # pylint: disable=unused-argument
    # noinspection PyPep8Naming
    VisitImportData = apps.get_model("web", "VisitImportData")
    # noinspection PyPep8Naming
    Study = apps.get_model("web", "Study")
    # noinspection PyPep8Naming
    Worker = apps.get_model("web", "Worker")
    # noinspection PyPep8Naming
    AppointmentType = apps.get_model("web", "AppointmentType")

    entry = VisitImportData()
    entry.study = Study.objects.get(pk=GLOBAL_STUDY_ID)
    importer_user_name = get_val(apps, "IMPORTER_USER")
    appointment_type_name = get_val(apps, "IMPORT_APPOINTMENT_TYPE")
    import_file = get_val(apps, "DAILY_VISIT_IMPORT_FILE")
    import_file_run_at = get_val(apps, "VISIT_IMPORT_RUN_AT")

    if importer_user_name is not None:
        workers = Worker.objects.filter(user__username=importer_user_name)

        if len(workers) > 0:
            entry.worker = workers[0]

    if appointment_type_name is not None:
        appointment_type = AppointmentType.objects.filter(code=appointment_type_name)

        if len(appointment_type) > 0:
            entry.appointment_type = appointment_type[0]
    entry.filename = import_file
    entry.run_at_times = import_file_run_at
    entry.delimiter = ";"
    entry.save()


# noinspection PyUnusedLocal
def create_subject_import_data(apps, schema_editor):  # pylint: disable=unused-argument
    # noinspection PyPep8Naming
    SubjectImportData = apps.get_model("web", "SubjectImportData")
    # noinspection PyPep8Naming
    Study = apps.get_model("web", "Study")
    # noinspection PyPep8Naming
    Worker = apps.get_model("web", "Worker")

    entry = SubjectImportData()
    entry.study = Study.objects.get(pk=GLOBAL_STUDY_ID)
    importer_user_name = get_val(apps, "IMPORTER_USER")
    import_file = get_val(apps, "DAILY_SUBJECT_IMPORT_FILE")
    import_file_run_at = get_val(apps, "SUBJECT_IMPORT_RUN_AT")

    if importer_user_name is not None:
        workers = Worker.objects.filter(user__username=importer_user_name)

        if len(workers) > 0:
            entry.worker = workers[0]

    entry.filename = import_file
    entry.run_at_times = import_file_run_at
    entry.delimiter = ";"
    entry.save()


def remove_old_config(apps, schema_editor):  # pylint: disable=unused-argument
    delete_config_option(apps, "DAILY_SUBJECT_IMPORT_FILE")
    delete_config_option(apps, "DAILY_SUBJECT_EXPORT_FILE")
    delete_config_option(apps, "DAILY_VISIT_IMPORT_FILE")
    delete_config_option(apps, "DAILY_VISIT_EXPORT_FILE")
    delete_config_option(apps, "SUBJECT_IMPORT_RUN_AT")
    delete_config_option(apps, "SUBJECT_EXPORT_RUN_AT")
    delete_config_option(apps, "VISIT_EXPORT_RUN_AT")
    delete_config_option(apps, "VISIT_IMPORT_RUN_AT")


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0184_visitimportdata"),
    ]

    operations = [
        migrations.RunPython(create_visit_import_data),
        migrations.RunPython(create_subject_import_data),
        migrations.RunPython(remove_old_config),
    ]
