# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2020-04-16 12:36


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0162_auto_20200416_1212'),
    ]

    operations = [
        migrations.AddField(
            model_name='study',
            name='redcap_first_visit_number',
            field=models.IntegerField(default=1, verbose_name='Number of the first visit in redcap system'),
        ),
    ]
