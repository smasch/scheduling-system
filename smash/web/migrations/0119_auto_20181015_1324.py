# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-10-15 13:24


from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0118_voucher_activity_type"),
    ]

    operations = [
        migrations.AlterField(
            model_name="studyvisitlist",
            name="type",
            field=models.CharField(
                choices=[
                    ("UNFINISHED", "Unfinished visits"),
                    ("APPROACHING_WITHOUT_APPOINTMENTS", "Approaching visits"),
                    ("APPROACHING_FOR_MAIL_CONTACT", "Post mail for approaching visits"),
                    ("GENERIC", "Generic visit list"),
                    ("MISSING_APPOINTMENTS", "Visits with missing appointments"),
                    ("EXCEEDED_TIME", "Exceeded visit time"),
                ],
                max_length=50,
                verbose_name="Type of list",
            ),
        ),
        migrations.AlterField(
            model_name="workerstudyrole",
            name="role",
            field=models.CharField(
                choices=[
                    ("DOCTOR", "Doctor"),
                    ("NURSE", "Nurse"),
                    ("PSYCHOLOGIST", "Psychologist"),
                    ("TECHNICIAN", "Technician"),
                    ("SECRETARY", "Secretary"),
                    ("PROJECT MANAGER", "Project Manager"),
                ],
                max_length=20,
                verbose_name="Role",
            ),
        ),
    ]
