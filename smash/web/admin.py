from django.contrib import admin
from django.utils.html import format_html

from .models import StudySubject, Item, Room, AppointmentType, Language, Location, Worker, FlyingTeam, Availability, \
    Holiday, Visit, Appointment, StudyColumns, StudySubjectList, StudyVisitList, VisitColumns, SubjectColumns, \
    Voucher, VoucherType, Provenance, Subject


# pylint: disable=no-self-use

# good tutorial
# https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Admin_site

class LanguageAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('name', 'image_img')


class VisitAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('change_button', 'id', 'get_first_name', 'get_last_name', 'visit_number', 'get_start_date',
                    'get_end_date', 'get_status')
    ordering = ('subject__subject__first_name', 'subject__subject__last_name', 'datetime_begin', 'datetime_end',
                'visit_number')
    list_filter = ('is_finished', 'visit_number', 'datetime_begin', 'datetime_end', 'appointment_types')
    search_fields = ('subject__subject__first_name', 'subject__subject__last_name')

    def get_first_name(self, obj):
        return obj.subject.subject.first_name

    def get_last_name(self, obj):
        return obj.subject.subject.last_name

    def get_start_date(self, obj):
        return obj.datetime_begin.strftime('%Y-%m-%d')

    def get_end_date(self, obj):
        return obj.datetime_end.strftime('%Y-%m-%d')

    def get_status(self, obj):
        return '✓' if obj.is_finished else ''

    def change_button(self, obj):
        return format_html('<a class="changelink" href="/admin/web/visit/{}/change/">Change</a>', obj.id)
    change_button.short_description = ''

    get_first_name.short_description = 'Subject First Name'
    get_first_name.admin_order_field = 'subject__subject__first_name'

    get_last_name.short_description = 'Subject Last Name'
    get_last_name.admin_order_field = 'subject__subject__last_name'

    get_start_date.short_description = 'Start Date'
    get_start_date.admin_order_field = 'datetime_begin'

    get_end_date.short_description = 'End Date'
    get_end_date.admin_order_field = 'datetime_end'

    get_status.short_description = 'Visit Finished'
    get_status.admin_order_field = 'is_finished'


class GeneralAppointmentFilter(admin.SimpleListFilter):
    title = 'appointment type'
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'visit__isnull'

    def lookups(self, request, model_admin):
        return (
            ('True', 'General Appointment'),
            ('False', 'Visit Appointment'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'False':
            return queryset.filter(visit__isnull=False)
        if self.value() == 'True':
            return queryset.filter(visit__isnull=True)


class AppointmentAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('change_button', 'id', 'get_datetime_when', 'get_length', 'location', 'status', 'get_first_name',
                    'get_last_name', 'get_visit_number', 'get_start_date', 'get_end_date', 'get_status',
                    'is_general_appointment')
    ordering = ('datetime_when', 'length', 'location', 'visit__subject__subject__first_name', 'status',
                'visit__subject__subject__last_name', 'visit__datetime_begin', 'visit__datetime_end',
                'visit__visit_number')
    list_filter = ('datetime_when', 'length', 'location', 'visit__is_finished', 'visit__visit_number',
                   'visit__datetime_begin', 'visit__datetime_end', 'appointment_types', 'status',
                   GeneralAppointmentFilter)
    search_fields = ('visit__subject__subject__first_name', 'visit__subject__subject__last_name')

    def get_first_name(self, obj):
        if obj.visit is not None:
            return obj.visit.subject.subject.first_name
        return None

    def get_last_name(self, obj):
        if obj.visit is not None:
            return obj.visit.subject.subject.last_name
        return None

    def get_datetime_when(self, obj):
        return obj.datetime_when.strftime('%Y-%m-%d %H:%M')

    def get_length(self, obj):
        return obj.length

    def get_start_date(self, obj):
        if obj.visit is not None:
            return obj.visit.datetime_begin.strftime('%Y-%m-%d')
        return None

    def get_end_date(self, obj):
        if obj.visit is not None:
            return obj.visit.datetime_end.strftime('%Y-%m-%d')
        return None

    def get_status(self, obj):
        if obj.visit is not None:
            return '✓' if obj.visit.is_finished else ''
        return None

    def get_visit_number(self, obj):
        if obj.visit is not None:
            return obj.visit.visit_number
        return None

    def is_general_appointment(self, obj):
        return '✓' if obj.visit is None else ''

    def change_button(self, obj):
        return format_html('<a class="changelink" href="/admin/web/appointment/{}/change/">Change</a>', obj.id)
    change_button.short_description = ''

    get_first_name.short_description = 'Subject First Name'
    get_first_name.admin_order_field = 'visit__subject__subject__first_name'

    get_datetime_when.short_description = 'Appointment Date'
    get_datetime_when.admin_order_field = 'datetime_when'

    get_length.short_description = 'Appointment Duration (mins)'
    get_length.admin_order_field = 'length'

    get_start_date.short_description = 'Visit Start Date'
    get_start_date.admin_order_field = 'visit__datetime_begin'

    get_start_date.short_description = 'Visit Start Date'
    get_start_date.admin_order_field = 'visit__datetime_begin'

    get_end_date.short_description = 'Visit End Date'
    get_end_date.admin_order_field = 'visit__datetime_end'

    get_status.short_description = 'Visit Finished'
    get_status.admin_order_field = 'visit__is_finished'

    get_visit_number.short_description = 'Visit Number'
    get_visit_number.admin_order_field = 'visit__visit_number'

    is_general_appointment.short_description = 'General Appointment'


class ProvenanceAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('change_button', 'id', 'modified_table', 'modified_table_id', 'modification_date',
                    'modification_author', 'modified_field', 'previous_value', 'new_value', 'modification_description')
    list_filter = ('modified_table', 'modification_date', 'modification_author', 'modified_field')
    ordering = ('modified_table', 'modification_date', 'modification_author', 'modified_field')

    def change_button(self, obj):
        return format_html('<a class="changelink" href="/admin/web/provenance/{}/change/">Change</a>', obj.id)

    change_button.short_description = ''


# Register your models here.
admin.site.register(StudySubject)
admin.site.register(Subject)
admin.site.register(Visit, VisitAdmin)
admin.site.register(Item)
admin.site.register(Room)
admin.site.register(AppointmentType)
admin.site.register(Language, LanguageAdmin)
admin.site.register(Location)
admin.site.register(Worker)
admin.site.register(FlyingTeam)
admin.site.register(Availability)
admin.site.register(Holiday)
admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(SubjectColumns)
admin.site.register(StudyColumns)
admin.site.register(StudySubjectList)
admin.site.register(StudyVisitList)
admin.site.register(VisitColumns)
admin.site.register(Voucher)
admin.site.register(VoucherType)
admin.site.register(Provenance, ProvenanceAdmin)
