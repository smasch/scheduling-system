import logging

from django.forms import ModelForm

from web.models import Study, StudyNotificationParameters, StudyColumns, StudySubject, StudyRedCapColumns

logger = logging.getLogger(__name__)


class StudyEditForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()

        # check regex
        nd_number_study_subject_regex = cleaned_data.get('nd_number_study_subject_regex')

        instance = getattr(self, 'instance', None)

        if nd_number_study_subject_regex is None or not StudySubject.check_nd_number_regex(
                nd_number_study_subject_regex, instance):
            self.add_error('nd_number_study_subject_regex', 'Please enter a valid nd_number_study_subject_regex regex.')

        return cleaned_data

    class Meta:
        model = Study
        fields = '__all__'
        exclude = ['columns', 'notification_parameters', 'redcap_columns']


class StudyNotificationParametersEditForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = StudyNotificationParameters
        fields = '__all__'


class StudyColumnsEditForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = StudyColumns
        fields = '__all__'


class StudyRedCapColumnsEditForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = StudyRedCapColumns
        fields = '__all__'
