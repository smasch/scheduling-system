import datetime
import logging
from collections import OrderedDict

from dateutil.relativedelta import relativedelta
from django import forms
from django.db.models.query import QuerySet
from django.forms import ModelForm

from web.forms.forms import DATETIMEPICKER_DATE_ATTRS, APPOINTMENT_TYPES_FIELD_POSITION, DATEPICKER_DATE_ATTRS
from web.models import Appointment, Worker, AppointmentTypeLink, AppointmentType, Provenance, Visit
from web.models.study import FOLLOW_UP_INCREMENT_IN_WEEKS, FOLLOW_UP_INCREMENT_IN_MONTHS
from web.models.worker_study_role import WORKER_STAFF
from web.views.notifications import get_filter_locations

logger = logging.getLogger(__name__)

RECURRING_EVENT_UNIT_CHOICE = (
    (FOLLOW_UP_INCREMENT_IN_WEEKS, 'Weeks'),
    (FOLLOW_UP_INCREMENT_IN_MONTHS, 'Months'),
)


class AppointmentForm(ModelForm):
    datetime_when = forms.DateTimeField(label='Appointment on',
                                        widget=forms.DateTimeInput(DATETIMEPICKER_DATE_ATTRS)
                                        )
    length = forms.IntegerField(label='Appointment length (in minutes)', min_value=1)
    user: Worker

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        if user is None:
            raise TypeError("User not defined")
        self.user = Worker.get_by_user(user)
        if self.user is None:
            raise TypeError("Worker not defined for: " + user.username)

        self.fields['worker_assigned'].queryset = Worker.get_workers_by_worker_type(WORKER_STAFF).filter(
            locations__in=get_filter_locations(self.user)).distinct().order_by('first_name', 'last_name')
        self.changes = []

    def save_changes(self):
        for change in self.changes:
            if change.modified_table_id is None:
                change.modified_table_id = self.instance.id
            change.save()

    def register_changes(self):
        self.changes = []
        for field in self.changed_data:
            new_value = self.cleaned_data[field] or self.data.get(field)
            if isinstance(new_value, QuerySet):
                new_human_values = '; '.join([str(element) for element in new_value])
                new_value = ','.join([str(element.id) for element in new_value])  # overwrite variable
                # old value
                if self.instance.id:  # update instance
                    list_of_values = getattr(self.instance, field).all()
                    old_human_values = '; '.join([str(element) for element in list_of_values])
                    previous_value = ','.join([str(element.id) for element in list_of_values])
                else:  # new instance
                    old_human_values = ''
                    previous_value = ''
                # description
                description = f'{field} changed from "{old_human_values}" to "{new_human_values}"'
            else:
                if self.instance.id:  # update instance
                    previous_value = getattr(self.instance, field)
                else:
                    previous_value = ''
                if isinstance(new_value, QuerySet):
                    new_value = ','.join([str(element.id) for element in new_value])  # overwrite variable
                description = f'{field} changed from "{previous_value}" to "{new_value}"'

            p = Provenance(modified_table=Appointment._meta.db_table,
                           modified_table_id=self.instance.id,
                           modification_author=self.user,
                           previous_value=previous_value,
                           new_value=new_value,
                           modification_description=description,
                           modified_field=field,
                           )
            self.changes.append(p)


class AppointmentEditForm(AppointmentForm):
    class Meta:
        model = Appointment
        fields = '__all__'
        exclude = ['appointment_types']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs:
            initial_appointment_types = AppointmentTypeLink.objects.filter(appointment=kwargs['instance']).values_list(
                'appointment_type', flat=True)
        else:
            initial_appointment_types = []
        fields = OrderedDict()
        for i, field_tuple in enumerate(self.fields.items()):
            key, value = field_tuple
            fields[key] = value
            if i == APPOINTMENT_TYPES_FIELD_POSITION:
                fields['appointment_types'] = forms.ModelMultipleChoiceField(required=False,
                                                                             widget=forms.CheckboxSelectMultiple,
                                                                             queryset=AppointmentType.objects.all(),
                                                                             initial=initial_appointment_types)
        self.fields = fields
        self.fields['location'].queryset = get_filter_locations(self.user)

    def clean_location(self):
        location = self.cleaned_data['location']
        if self.user.locations.filter(id=location.id).count() == 0:
            self.add_error('location', "You cannot create appointment for this location")
            raise forms.ValidationError("You cannot create appointment for this location")
        else:
            return location

    def clean(self):
        if len(self.errors) == 0:
            self.register_changes()  # right before instance is changed

    def save(self, commit=True):

        appointment = super().save(commit)
        # if appointment date change, remove appointment_type links
        if 'datetime_when' in self.changed_data:
            AppointmentTypeLink.objects.filter(appointment=appointment).delete()
            appointment_type_links = []
        else:
            appointment_type_links = AppointmentTypeLink.objects.filter(appointment=appointment).all()
        appointment_types_from_links = []
        appointment_types = self.cleaned_data['appointment_types']
        for appointment_type_link in appointment_type_links:
            if appointment_type_link.appointment_type not in appointment_types:
                # we delete appointment links for appointments types that have been removed
                appointment_type_link.delete()
            else:
                appointment_types_from_links.append(appointment_type_link.appointment_type)
        for appointment_type in appointment_types:
            if appointment_type not in appointment_types_from_links:
                # we create new appointment links for appointments types that have been added
                appointment_type_link = AppointmentTypeLink(appointment=appointment,
                                                            appointment_type=appointment_type)
                appointment_type_link.save()

        self.save_changes()
        return appointment


class AppointmentAddForm(AppointmentForm):
    visit: Visit

    class Meta:
        model = Appointment
        exclude = ['status', 'appointment_types']

    def __init__(self, *args, **kwargs):
        self.visit = kwargs.pop('visit', None)
        super().__init__(*args, **kwargs)

        fields = OrderedDict()
        for i, field_tuple in enumerate(self.fields.items()):
            key, value = field_tuple
            fields[key] = value
            if i == APPOINTMENT_TYPES_FIELD_POSITION:
                fields['appointment_types'] = forms.ModelMultipleChoiceField(required=False,
                                                                             widget=forms.CheckboxSelectMultiple,
                                                                             queryset=AppointmentType.objects.all(),
                                                                             )
        if self.visit is None:
            fields['is_recurring'] = forms.BooleanField(initial=False,
                                                        label='Is recurring event',
                                                        required=False,
                                                        )
            fields['until_recurring'] = forms.DateTimeField(label='Recurring until',
                                                            required=False,
                                                            widget=forms.DateInput(DATEPICKER_DATE_ATTRS, "%Y-%m-%d"))
            fields['until_recurring'].widget.attrs = {'class': 'start_date', 'placeholder': 'yyyy-mm-dd',
                                                      'pattern': '[0-9]{4}-[0-9]{2}-[0-9]{2}'}

            fields['time_unit_recurring'] = forms.ChoiceField(label='How often',
                                                              initial=FOLLOW_UP_INCREMENT_IN_WEEKS,
                                                              required=False,
                                                              choices=RECURRING_EVENT_UNIT_CHOICE)

        fields['worker_assigned'].widget.attrs = {'class': 'search_worker_availability'}
        fields['datetime_when'].widget.attrs = {'class': 'start_date', 'placeholder': 'yyyy-mm-dd HH:MM',
                                                'pattern': '[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}'}

        self.fields = fields
        self.fields['location'].queryset = get_filter_locations(self.user)

    def clean(self):
        if self.visit is None:
            if self.cleaned_data['is_recurring']:
                if not self.cleaned_data['until_recurring']:
                    self.add_error('until_recurring', "Please introduce a valid date")
                    return

                days = (self.cleaned_data['until_recurring'] - self.cleaned_data['datetime_when']).days
                if self.cleaned_data['time_unit_recurring'] == FOLLOW_UP_INCREMENT_IN_WEEKS:
                    count = days / 7
                else:
                    count = days / 30  # it's estimate
                if count > 100:
                    self.add_error('until_recurring', "You cannot create so many recurring appointments (max 100)")

        if len(self.errors) == 0:
            self.register_changes()  # right before instance is changed

    def clean_location(self):
        location = self.cleaned_data['location']
        if self.user.locations.filter(id=location.id).count() == 0:
            self.add_error('location', "You cannot create appointment for this location")
        else:
            return location

    def save(self, commit=True) -> Appointment:
        appointment = super().save(commit)
        self.add_appointment_types(appointment)
        self.save_changes()

        first_id = appointment.id
        if self.visit is None:
            if self.cleaned_data['is_recurring']:

                date = appointment.datetime_when
                if self.cleaned_data['time_unit_recurring'] == FOLLOW_UP_INCREMENT_IN_WEEKS:
                    date = date + datetime.timedelta(days=7)
                else:
                    date = date + relativedelta(months=1)

                while date < self.cleaned_data['until_recurring']:
                    appointment.id = None
                    appointment.datetime_when = date
                    appointment.save()
                    self.add_appointment_types(appointment)
                    if self.cleaned_data['time_unit_recurring'] == FOLLOW_UP_INCREMENT_IN_WEEKS:
                        date = date + datetime.timedelta(days=7)
                    else:
                        date = date + relativedelta(months=1)

        return Appointment.objects.get(id=first_id)

    def add_appointment_types(self, appointment: Appointment):
        appointment_types = self.cleaned_data['appointment_types']
        for appointment_type in appointment_types:
            appointment_type_link = AppointmentTypeLink(appointment=appointment, appointment_type=appointment_type)
            appointment_type_link.save()
