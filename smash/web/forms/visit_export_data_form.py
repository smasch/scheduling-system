import re
from typing import Type

from django import forms
from django.db import models
from django.forms import ModelForm

from web.models import Appointment, Visit
from web.models.etl.etl_export_data import field_can_be_exported
from web.models.etl.subject_export import SubjectExportData
from web.models.etl.visit_export import VisitExportData


class VisitExportDataEditForm(ModelForm):
    class Meta:
        model = VisitExportData
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        self.fields['filename'].label = 'File used for automatic export'
        self.fields['run_at_times'].label = 'At what time automatic export should run'
        if instance:
            self.create_column_fields_for_class(instance, Appointment)
            self.create_column_fields_for_class(instance, Visit)

    def create_column_fields_for_class(self, instance: SubjectExportData, clazz: Type[models.Model]):
        for field in clazz._meta.get_fields():
            if field_can_be_exported(field):
                field_id = clazz._meta.db_table + " - " + field.name
                value = field.name
                for mapping in instance.column_mappings.all():
                    if mapping.table_name == clazz._meta.db_table and field.name == mapping.column_name:
                        value = mapping.csv_column_name
                self.fields[field_id] = forms.CharField(label=field.verbose_name + " column name ", initial=value)

    def save(self, commit=True) -> SubjectExportData:
        instance = super().save(commit)
        # we can add custom values only after object exists in the database
        self.update_field_data_for_class(instance, Appointment)
        self.update_field_data_for_class(instance, Visit)

        return instance

    def update_field_data_for_class(self, instance: SubjectExportData, clazz: Type[models.Model]):
        for field in clazz._meta.get_fields():
            if field_can_be_exported(field):
                field_id = clazz._meta.db_table + " - " + field.name
                value = self[field_id].value()
                instance.set_column_mapping(clazz, field.name, value)

    def clean_run_at_times(self):
        run_at_times = self.cleaned_data['run_at_times']
        pattern = re.compile("^[0-9]{2}:[0-9]{2}$")
        for entry in run_at_times.split(";"):
            if entry != '' and not pattern.match(entry):
                self.add_error('run_at_times', "Hours must be semicolon separated HH:MM values")
        return run_at_times
