import datetime
import logging

from django import forms
from django.forms import ModelForm

from web.forms.forms import DATETIMEPICKER_DATE_ATTRS
from web.models.voucher_partner_session import VoucherPartnerSession

logger = logging.getLogger(__name__)


class VoucherPartnerSessionForm(ModelForm):
    date = forms.DateTimeField(widget=forms.DateTimeInput(DATETIMEPICKER_DATE_ATTRS),
                               initial=datetime.datetime.now().replace(hour=8, minute=0, second=0))

    class Meta:
        model = VoucherPartnerSession
        exclude = ['voucher']
