import logging

from django import forms
from django.forms import ModelForm

from web.forms.forms import DATEPICKER_DATE_ATTRS
from web.models import Subject
from web.models.constants import COUNTRY_OTHER_ID
from web.utils import is_valid_social_security_number

logger = logging.getLogger(__name__)


def validate_subject_country(self, cleaned_data):
    if 'country' not in cleaned_data or cleaned_data['country'].id == COUNTRY_OTHER_ID:
        self.add_error('country', "Select valid country")


def validate_social_security_number(self, number):
    if not is_valid_social_security_number(number):
        self.add_error('social_security_number', "Social security number is invalid")


FIELD_ORDER = ["first_name", "last_name", "sex", "date_born", "social_security_number",
               "default_written_communication_language", "languages", "phone_number", "phone_number_2",
               "phone_number_3", "address", "city", "postal_code", "country"]


class SubjectAddForm(ModelForm):
    date_born = forms.DateField(label="Date of birth",
                                widget=forms.DateInput(DATEPICKER_DATE_ATTRS, "%Y-%m-%d"), required=False)

    field_order = FIELD_ORDER

    class Meta:
        model = Subject
        fields = '__all__'
        exclude = ['dead']

    def clean(self):
        cleaned_data = super().clean()
        validate_subject_country(self, cleaned_data)
        validate_social_security_number(self, cleaned_data["social_security_number"])
        return cleaned_data


class SubjectEditForm(ModelForm):
    date_born = forms.DateField(label="Date of birth",
                                widget=forms.DateInput(DATEPICKER_DATE_ATTRS, "%Y-%m-%d"), required=False)

    field_order = FIELD_ORDER

    def __init__(self, *args, **kwargs):
        was_dead = kwargs.get('was_dead', False)
        if 'was_dead' in kwargs:
            kwargs.pop('was_dead')
        super().__init__(*args, **kwargs)
        if was_dead:
            self.fields['dead'].disabled = True

    def clean(self):
        validate_subject_country(self, self.cleaned_data)
        validate_social_security_number(self, self.cleaned_data["social_security_number"])

    class Meta:
        model = Subject
        fields = '__all__'


class SubjectDetailForm(ModelForm):
    field_order = FIELD_ORDER

    class Meta:
        model = Subject
        fields = '__all__'
