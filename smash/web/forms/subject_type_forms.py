from django.forms import ModelForm

from web.models import SubjectType, Study


class SubjectTypeForm(ModelForm):
    study: Study

    class Meta:
        model = SubjectType
        fields = "__all__"

    def save(self, commit: bool = True) -> SubjectType:
        self.instance.study_id = self.study.id
        return super().save(commit)


class SubjectTypeAddForm(SubjectTypeForm):
    def __init__(self, *args, **kwargs):
        self.study = get_study_from_args(kwargs)
        super().__init__(*args, **kwargs)


class SubjectTypeEditForm(SubjectTypeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        self.study = get_study_from_instance(instance)


def get_study_from_args(kwargs) -> Study:
    study = kwargs.pop('study', None)
    if study is None:
        raise TypeError("Study not defined")
    return study


def get_study_from_instance(subject_type: SubjectType) -> Study:
    if subject_type is not None:
        return subject_type.study
    else:
        raise TypeError("SubjectType not defined")
