from typing import Type

from django import forms
from django.db import models
from django.forms import ModelForm

from web.models import SubjectImportData, Subject, StudySubject
from web.models.custom_data import CustomStudySubjectField
from web.models.custom_data.custom_study_subject_field import get_study_subject_field_id
from web.models.etl.subject_import import field_can_be_imported


class SubjectImportDataEditForm(ModelForm):
    class Meta:
        model = SubjectImportData
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance:
            self.create_column_fields_for_class(instance, Subject)
            self.create_column_fields_for_class(instance, StudySubject)
            self.create_column_fields_for_custom_fields(instance)

    def create_column_fields_for_class(self, instance: SubjectImportData, clazz: Type[models.Model]):
        for field in clazz._meta.get_fields():
            if field_can_be_imported(field):
                field_id = clazz._meta.db_table + " - " + field.name
                value = field.name
                for mapping in instance.column_mappings.all():
                    if mapping.table_name == clazz._meta.db_table and field.name == mapping.column_name:
                        value = mapping.csv_column_name
                self.fields[field_id] = forms.CharField(label=field.verbose_name + " column name ", initial=value)

    def create_column_fields_for_custom_fields(self, instance: SubjectImportData):
        for field_type in CustomStudySubjectField.objects.filter(study=instance.study):
            field_id = get_study_subject_field_id(field_type)
            value = field_type.name.replace(" ", "_")
            for mapping in instance.column_mappings.all():
                if mapping.table_name == CustomStudySubjectField._meta.db_table and field_id == mapping.column_name:
                    value = mapping.csv_column_name
            self.fields[field_id] = forms.CharField(label=field_type.name + " column name ", initial=value)

    def save(self, commit=True) -> StudySubject:
        instance = super().save(commit)
        # we can add custom values only after object exists in the database
        self.update_field_data_for_class(instance, Subject)
        self.update_field_data_for_class(instance, StudySubject)
        self.update_field_data_for_custom_fields(instance)

        return instance

    def update_field_data_for_class(self, instance: SubjectImportData, clazz: Type[models.Model]):
        for field in clazz._meta.get_fields():
            if field_can_be_imported(field):
                field_id = clazz._meta.db_table + " - " + field.name
                value = self[field_id].value()
                instance.set_column_mapping(clazz, field.name, value)

    def update_field_data_for_custom_fields(self, instance: SubjectImportData):
        for field_type in CustomStudySubjectField.objects.filter(study=instance.study):
            field_id = get_study_subject_field_id(field_type)
            value = self[field_id].value()
            instance.set_column_mapping(CustomStudySubjectField, field_id, value)
