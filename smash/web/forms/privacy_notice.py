import logging

from django.forms import ModelForm

from web.models import PrivacyNotice

logger = logging.getLogger(__name__)


class PrivacyNoticeForm(ModelForm):
    class Meta:
        model = PrivacyNotice
        fields = '__all__'

    def clean(self):
        cleaned_data = super().clean()
        return cleaned_data
