import re

from django import forms

from web.models.constants import CUSTOM_FIELD_TYPE_BOOLEAN, CUSTOM_FIELD_TYPE_INTEGER, CUSTOM_FIELD_TYPE_DOUBLE, \
    CUSTOM_FIELD_TYPE_DATE, CUSTOM_FIELD_TYPE_SELECT_LIST, CUSTOM_FIELD_TYPE_FILE
from web.models.custom_data import CustomStudySubjectField


class CustomStudySubjectFieldForm(forms.ModelForm):
    possible_values = forms.CharField(label='Possible values',
                                      widget=forms.TextInput(attrs={'placeholder': "values separated by ';'"}),
                                      required=False)

    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data['default_value'] != '' \
                and cleaned_data['default_value'] is not None:
            if cleaned_data['type'] == CUSTOM_FIELD_TYPE_BOOLEAN \
                    and cleaned_data['default_value'].lower() != 'false' \
                    and cleaned_data['default_value'].lower() != 'true':
                self.add_error('default_value',
                               "Default value can be one of the following: 'true', 'false', ''.")
            elif cleaned_data['type'] == CUSTOM_FIELD_TYPE_INTEGER \
                    and re.match(r"[-+]?\d+$", cleaned_data['default_value']) is None:
                self.add_error('default_value',
                               "Default value must be integer (or empty).")
            elif cleaned_data['type'] == CUSTOM_FIELD_TYPE_DOUBLE \
                    and re.match(r"[-+]?\d+?\.\d+?$", cleaned_data['default_value']) is None:
                self.add_error('default_value',
                               "Default value must be numeric (or empty).")
            elif cleaned_data['type'] == CUSTOM_FIELD_TYPE_DATE \
                    and re.match(r"\d\d\d\d-\d\d-\d\d?$", cleaned_data['default_value']) is None:
                self.add_error('default_value',
                               "Default value must be in format YYYY-MM-DD (or empty).")
            elif cleaned_data['type'] == CUSTOM_FIELD_TYPE_SELECT_LIST \
                    and not cleaned_data['default_value'] in cleaned_data['possible_values'].split(';'):
                self.add_error('default_value',
                               "Default value must be listed in possible values (semicolon separated list)")
            elif cleaned_data['type'] == CUSTOM_FIELD_TYPE_FILE \
                    and cleaned_data['default_value'] is not None \
                    and cleaned_data['default_value'] != '':
                self.add_error('default_value',
                               "Default value is not available for files")
        return cleaned_data


class CustomStudySubjectFieldAddForm(CustomStudySubjectFieldForm):
    class Meta:
        model = CustomStudySubjectField
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.study = kwargs.pop('study', None)
        super().__init__(*args, **kwargs)

    def save(self, commit=True) -> CustomStudySubjectField:
        self.instance.study_id = self.study.id
        return super().save(commit)


class CustomStudySubjectFieldEditForm(CustomStudySubjectFieldForm):
    class Meta:
        model = CustomStudySubjectField
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
