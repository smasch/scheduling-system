import logging

from django import forms
from django.forms import ModelForm

from web.models import MailTemplate, Language

logger = logging.getLogger(__name__)


class MailTemplateForm(ModelForm):
    class Meta:
        model = MailTemplate
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['Multilingual'] = \
            forms.BooleanField(label='Multilingual', disabled=True, required=False,
                               help_text='Only for voucher context. '
                                         'Check this option if the voucher template is multilingual.',
                               widget=forms.CheckboxInput(
                                   attrs={'class': 'hidden_form_field', 'id': 'multilingual_checkbox'}))

        languages = [(language.id, language.name)
                     for language in Language.objects.all() if language.is_locale_installed()]
        languages.insert(0, (None, '---'))
        self.fields['language'].choices = languages

    def clean(self):
        cleaned_data = super().clean()
        return cleaned_data
