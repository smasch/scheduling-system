import datetime
from dateutil.relativedelta import relativedelta
import logging

from django import forms
from django.forms import ModelForm
from django.utils import timezone

from web.algorithm import VerhoeffAlgorithm
from web.forms.forms import DATEPICKER_DATE_ATTRS
from web.models import VoucherType, VoucherTypePrice, Voucher, Worker
from web.models.constants import VOUCHER_STATUS_USED, VOUCHER_STATUS_EXPIRED, VOUCHER_STATUS_REMOVED
from web.models.worker_study_role import WORKER_VOUCHER_PARTNER

logger = logging.getLogger(__name__)


class VoucherTypeForm(ModelForm):
    class Meta:
        model = VoucherType
        exclude = ["study"]


class VoucherTypePriceForm(ModelForm):
    start_date = forms.DateField(label="Start date", widget=forms.DateInput(DATEPICKER_DATE_ATTRS, "%Y-%m-%d"))
    end_date = forms.DateField(label="End date", widget=forms.DateInput(DATEPICKER_DATE_ATTRS, "%Y-%m-%d"))

    class Meta:
        model = VoucherTypePrice
        exclude = ["voucher_type"]


class VoucherForm(ModelForm):
    class Meta:
        model = Voucher
        fields = "__all__"

    @staticmethod
    def _voucher_type_optgroup(subject_voucher_types, all_voucher_types):
        subject_voucher_types = {(vt.id, str(vt)) for vt in subject_voucher_types}
        all_voucher_types = {(vt.id, str(vt)) for vt in all_voucher_types}
        if subject_voucher_types == all_voucher_types:
            return [(None, "Please select an option")] + list(all_voucher_types)
        elif len(subject_voucher_types) == 0:
            return [(None, "Please select an option")] + list(all_voucher_types)

        rest_voucher_types = all_voucher_types - subject_voucher_types
        return (
            (None, "Please select an option"),
            ("Subject Voucher Types", tuple(subject_voucher_types)),
            ("Rest of Voucher Types", tuple(rest_voucher_types)),
        )

    def __init__(self, *args, **kwargs):
        voucher_types = kwargs.pop("voucher_types", VoucherType.objects.all())
        super().__init__(*args, **kwargs)

        self.fields["voucher_type"].choices = VoucherForm._voucher_type_optgroup(
            voucher_types, VoucherType.objects.all()
        )
        self.fields["voucher_type"].default = None
        self.fields["usage_partner"].queryset = Worker.get_workers_by_worker_type(WORKER_VOUCHER_PARTNER)

        self.fields["number"].widget.attrs["readonly"] = True
        self.fields["number"].required = False
        self.fields["issue_worker"].widget.attrs["readonly"] = True
        # issue date
        self.fields["issue_date"].input_formats = ("%Y-%m-%d",)
        self.fields["issue_date"].required = False
        self.fields["issue_date"].widget.attrs.update({"class": "datepicker"})
        self.fields["issue_date"].widget.attrs.update({"readonly": False})
        self.fields["issue_date"].widget.attrs.update({"placeholder": "yyyy-mm-dd (optional)"})
        self.fields["issue_date"].widget.attrs.update({"pattern": "[0-9]{4}-[0-9]{2}-[0-9]{2}"})
        self.fields["issue_date"].widget.attrs.update({"required": False})
        self.fields["issue_date"].initial = timezone.now()
        # hours
        self.fields["hours"].widget.attrs.update({"min": 0})
        # expiry date
        self.fields["expiry_date"].widget.attrs["readonly"] = True
        self.fields["expiry_date"].required = False
        instance = getattr(self, "instance", None)
        if instance and instance.pk:
            self.fields["issue_date"].widget.attrs["readonly"] = True
            self.fields["voucher_type"].widget.attrs["readonly"] = True
            self.fields["hours"].widget.attrs["readonly"] = True
            self.fields["activity_type"].widget.attrs["readonly"] = True
            self.fields["usage_partner"].widget.attrs["readonly"] = True

            if instance.status in [VOUCHER_STATUS_USED, VOUCHER_STATUS_EXPIRED, VOUCHER_STATUS_REMOVED]:
                self.fields["status"].widget.attrs["readonly"] = True
                self.fields["feedback"].widget.attrs["readonly"] = True

    def clean(self):
        # if empty, add a datetime
        if self.cleaned_data["issue_date"] is None:
            self.cleaned_data["issue_date"] = timezone.now()

    def save(self, commit=True):
        instance = super().save(commit=False)
        if not instance.id:
            instance.expiry_date = instance.issue_date + relativedelta(
                months=+instance.study_subject.study.default_voucher_expiration_in_months
            )
            max_id = str(1).zfill(5)
            if Voucher.objects.all().count() > 0:
                max_id = str(Voucher.objects.latest("id").id + 1).zfill(4)
            # number in format  {ND_NUMBER}-{DATE}-{VOUCHER_PARTNER_CODE}-{VOUCHER_TYPE}-{SEQ_NUMBER}{CHECKSUM}
            # pylint: disable-next=consider-using-f-string
            instance.number = "{}-{}-{}-{}-{}{}".format(
                instance.study_subject.nd_number,
                datetime.datetime.now().strftime("%Y%m%d"),
                instance.usage_partner.voucher_partner_code,
                instance.voucher_type.code,
                max_id,
                VerhoeffAlgorithm.calculate_verhoeff_check_sum(max_id),
            )
        if commit:
            instance.save()
