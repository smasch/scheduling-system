from django import forms
from django.forms import ModelForm

from web.models import StudySubjectList, SubjectColumns, StudyColumns
from web.models.custom_data.custom_study_subject_field import get_study_subject_field_id


class StudySubjectListEditForm(ModelForm):
    class Meta:
        model = StudySubjectList
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class SubjectColumnsEditForm(ModelForm):
    class Meta:
        model = SubjectColumns
        exclude = ['sex', 'postal_code', 'city', 'country', 'email', 'languages']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class StudySubjectColumnsEditForm(ModelForm):
    class Meta:
        model = StudyColumns
        exclude = ['comments', 'referral_letter', 'resign_reason',
                   'health_partner_feedback_agreement', 'vouchers']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance: StudyColumns = kwargs.get('instance')
        for visibility in instance.custom_fields_visibility:
            field = visibility.study_subject_field
            field_id = get_study_subject_field_id(field)
            self.fields[field_id] = forms.BooleanField(label=field.name, initial=visibility.visible, required=False)

    def save(self, commit=True) -> StudyColumns:
        instance = super().save(commit)
        # we can add custom values only after object exists in the database
        for visibility in instance.custom_fields_visibility:
            field = self[get_study_subject_field_id(visibility.study_subject_field)]
            visibility.visible = str(field.value()).lower() == "true"
            visibility.save()
        return instance
