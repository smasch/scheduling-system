from django import forms
from django.forms import ModelForm

from web.forms.forms import DATETIMEPICKER_DATE_ATTRS
from web.models import ContactAttempt, Worker
from web.models.worker_study_role import WORKER_STAFF


class ContactAttemptForm(ModelForm):
    datetime_when = forms.DateTimeField(label='When? (YYYY-MM-DD HH:MM)',
                                        widget=forms.DateTimeInput(DATETIMEPICKER_DATE_ATTRS)
                                        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        if user is None:
            raise TypeError("User not defined")
        self.user = Worker.get_by_user(user)
        if self.user is None:
            raise TypeError("Worker not defined for: " + user.username)
        super().__init__(*args, **kwargs)
        self.fields['subject'].disabled = True
        self.fields['worker'].queryset = Worker.get_workers_by_worker_type(WORKER_STAFF).distinct().order_by(
            'first_name', 'last_name')


class ContactAttemptAddForm(ContactAttemptForm):
    class Meta:
        model = ContactAttempt
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        subject = kwargs.pop('subject', None)
        super().__init__(*args, **kwargs)
        self.fields['subject'].initial = subject.id
        self.fields['worker'].initial = self.user


class ContactAttemptEditForm(ContactAttemptForm):
    class Meta:
        model = ContactAttempt
        fields = '__all__'
