import re

from django.forms import ModelForm

from web.models import VisitImportData


class VisitImportDataEditForm(ModelForm):
    class Meta:
        model = VisitImportData
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_run_at_times(self):
        run_at_times = self.cleaned_data['run_at_times']
        pattern = re.compile("^[0-9]{2}:[0-9]{2}$")
        for entry in run_at_times.split(";"):
            if entry != '' and not pattern.match(entry):
                self.add_error('run_at_times', "Hours must be semicolon separated HH:MM values")
        return run_at_times
