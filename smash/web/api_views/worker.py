import datetime
import logging
import json
from django.http import JsonResponse, HttpResponse
from django.utils import timezone

from django.shortcuts import get_object_or_404
from web.models.constants import GLOBAL_STUDY_ID
from web.api_views.daily_planning import (
    get_workers_for_daily_planning,
    get_availabilities,
    prepare_worker_availabilities,
    prepare_remove_holidays,
)
from ..models.worker_study_role import (
    WORKER_STAFF,
    WORKER_HEALTH_PARTNER,
    WORKER_VOUCHER_PARTNER,
)
from ..models import Worker, Availability, Holiday
from web.models.constants import AVAILABILITY_EXTRA
from web.tests.functions import datetimeify_date

logger = logging.getLogger(__name__)


def specializations(request):
    workers = (
        Worker.objects.filter(specialization__isnull=False)
        .values_list("specialization")
        .distinct()
    )
    return JsonResponse({"specializations": [x[0] for x in workers]})


def units(request):
    workers = Worker.objects.filter(unit__isnull=False).values_list("unit").distinct()
    return JsonResponse({"units": [x[0] for x in workers]})


def accept_privacy_notice(request):
    worker = Worker.get_by_user(request.user)
    worker.privacy_notice_accepted = True
    worker.save()
    return JsonResponse({"status": "ok"})


def workers_for_daily_planning(request):
    start_date = request.GET.get("start_date")
    workers = get_workers_for_daily_planning(request)
    workers_list_for_json = []
    if start_date is not None:
        today = timezone.now()
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").replace(
            tzinfo=today.tzinfo
        )
    for worker in workers:
        role = str(worker.roles.filter(study_id=GLOBAL_STUDY_ID)[0].role)
        worker_dict_for_json = {
            "id": worker.id,
            "flags": [
                language.image.url
                for language in worker.languages.all()
                if language.image
            ],
            "availability": worker.availability_percentage(start_date=start_date),
            "title": f"{str(worker)} ({role[:1].upper()})",
            "role": role,
        }
        workers_list_for_json.append(worker_dict_for_json)
    return JsonResponse(workers_list_for_json, safe=False)


def availabilities(request):
    result = []
    min_date = request.GET.get("start_date")
    max_date = request.GET.get("end_date")

    workers = get_workers_for_daily_planning(request)

    if min_date is None or max_date is None:
        return JsonResponse(
            {
                "status": False,
                "message": "Parameters missing",
                "availabilities": result,
            },
            status=422,
        )

    min_date = datetime.datetime.strptime(min_date, "%Y-%m-%d").replace(
        tzinfo=timezone.now().tzinfo
    )
    max_date = datetime.datetime.strptime(max_date, "%Y-%m-%d").replace(
        tzinfo=timezone.now().tzinfo
    )

    availability_map = prepare_worker_availabilities(min_date, max_date, workers)

    while min_date <= max_date:
        str_date = min_date.strftime("%Y-%m-%d")
        date_result = {"date": str_date, "workers": []}
        today = datetimeify_date(str_date)
        weekday = today.weekday() + 1

        workers_timestamps_map, directions_map = prepare_remove_holidays(workers, today)
        for worker in workers:
            filtered_availabilities = availability_map[weekday][worker.id]
            holiday_timestamps = workers_timestamps_map[worker.id]
            direction = directions_map[worker.id]
            if (
                len(
                    get_availabilities(
                        worker,
                        today,
                        filtered_availabilities,
                        holiday_timestamps,
                        direction,
                    )
                )
                > 0
            ):
                date_result["workers"].append(
                    {
                        "worker_id": worker.id,
                        "initials": worker.initials(),
                    }
                )
        result.append(date_result)
        min_date += datetime.timedelta(days=1)

    return JsonResponse(
        {
            "availabilities": result,
        }
    )


def get_workers(request, worker_role):
    if worker_role not in [WORKER_STAFF, WORKER_HEALTH_PARTNER, WORKER_VOUCHER_PARTNER]:
        return JsonResponse(
            {
                "status": False,
                "message": "invalid worker role",
                "recordsTotal": 0,
                "recordsFiltered": 0,
                "data": [],
            },
            status=400,
        )

    all_workers = Worker.get_workers_by_worker_type(worker_role).distinct()
    count = all_workers.count()

    data = []
    for voucher_type in all_workers:
        data.append(serialize_worker(voucher_type))

    return JsonResponse(
        {
            "recordsTotal": count,
            "recordsFiltered": count,
            "data": data,
        }
    )


def serialize_worker(worker):
    result = {
        "first_name": worker.first_name,
        "last_name": worker.last_name,
        "name": str(worker),
        "id": worker.id,
    }
    return result


def add_worker_availability(request, worker_id, weekday, start_hour, end_hour):
    if worker_id is None or weekday is None or start_hour is None or end_hour is None:
        context = {
            "status": "400",
            "reason": "Either worker_id, weekday, start_hour, end_hour or all of them are invalid.",
        }
        response = HttpResponse(json.dumps(context), content_type="application/json")
        response.status_code = 400
        return response

    worker = get_object_or_404(Worker, id=int(worker_id))

    availability, _ = Availability.objects.update_or_create(
        person=worker,
        day_number=weekday,
        defaults={"available_from": start_hour, "available_till": end_hour},
    )
    availability.save()

    return JsonResponse({}, status=200)


def add_worker_extra_availability(request, worker_id, start_str_date, end_str_date):
    if start_str_date is None or end_str_date is None or worker_id is None:
        context = {
            "status": "400",
            "reason": "Either start_date, end_str_date, worker_id or all of them are invalid.",
        }
        response = HttpResponse(json.dumps(context), content_type="application/json")
        response.status_code = 400
        return response

    start_date = datetime.datetime.strptime(start_str_date, "%Y-%m-%d-%H-%M").replace(
        tzinfo=timezone.now().tzinfo
    )
    end_date = datetime.datetime.strptime(end_str_date, "%Y-%m-%d-%H-%M").replace(
        tzinfo=timezone.now().tzinfo
    )
    worker = get_object_or_404(Worker, id=int(worker_id))

    extra_availability, _ = Holiday.objects.update_or_create(
        person=worker,
        datetime_start=start_date,
        datetime_end=end_date,
        kind=AVAILABILITY_EXTRA,
    )
    extra_availability.save()

    return JsonResponse({}, status=200)


def get_worker_availability(request):
    start_str_date = request.GET.get("start_date")
    end_str_date = request.GET.get("end_date")
    worker_id = request.GET.get("worker_id")

    if start_str_date is None or worker_id is None:
        context = {
            "status": "400",
            "reason": "Either start_date, worker_id or both are invalid.",
        }
        response = HttpResponse(json.dumps(context), content_type="application/json")
        response.status_code = 400
        return response

    start_date = datetime.datetime.strptime(start_str_date, "%Y-%m-%d-%H-%M").replace(
        tzinfo=timezone.now().tzinfo
    )
    if end_str_date is None or end_str_date == start_str_date:
        start_date = start_date.replace(hour=0, minute=0, second=0)
        end_date = start_date + datetime.timedelta(days=1)
    else:
        end_date = datetime.datetime.strptime(end_str_date, "%Y-%m-%d-%H-%M").replace(
            tzinfo=timezone.now().tzinfo
        )
    worker = get_object_or_404(Worker, id=int(worker_id))

    result = {
        "start_date": start_date,
        "end_date": end_date,
        "availability": round(
            worker.availability_percentage(start_date=start_date, end_date=end_date), 0
        ),
    }
    return JsonResponse(result)
