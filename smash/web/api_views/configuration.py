from django.core.handlers.wsgi import WSGIRequest
from django.http import JsonResponse, HttpResponseNotAllowed

from web.models import ConfigurationItem
from web.models.constants import VALUE_TYPE_PASSWORD


def configuration_items(request: WSGIRequest):
    if request.method == "GET":
        request_data = request.GET
    elif request.method == "POST":
        request_data = request.POST
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])

    # id of the query from dataTable: https://datatables.net/manual/server-side
    draw = int(request_data.get("draw", "-1"))

    start = int(request_data.get("start", "0"))
    length = int(request_data.get("length", "10"))

    items = ConfigurationItem.objects.all()
    count = items.count()
    count_filtered = count

    sliced_items = items[start:(start + length)]

    data = []
    for configuration_item in sliced_items:
        value = configuration_item.value
        if configuration_item.value_type == VALUE_TYPE_PASSWORD:
            value = ''
        data.append({
            "id": configuration_item.id,
            "name": configuration_item.name,
            "value": value,
            "value_type": configuration_item.value_type,
        })
    return JsonResponse({
        "draw": draw,
        "recordsTotal": count,
        "recordsFiltered": count_filtered,
        "data": data
    })


def update_configuration_item(request):
    item_id = int(request.GET.get("id", "-1"))
    value = request.GET.get("value", None)

    if (item_id is None) or (value is None):
        return JsonResponse({
            "status": "error",
            "message": "id and value are obligatory"
        })

    items = ConfigurationItem.objects.filter(id=item_id)
    if len(items) == 0:
        return JsonResponse({
            "status": "error",
            "message": "item with given id doesn't exist"
        })
    item = items[0]
    item.value = value
    if ConfigurationItem.is_valid(item):
        item.save()
        return JsonResponse({
            "status": "ok",
        })
    else:
        return JsonResponse({
            "status": "error",
            "message": ConfigurationItem.validation_error(item)
        })
