from django.http import JsonResponse

from web.models import AppointmentType


def appointment_types(request):
    appointments = AppointmentType.objects.filter().all()
    result = []
    for appointment in appointments:
        result.append({
            "id": appointment.id,
            "type": appointment.code,
            "default_duration": appointment.default_duration,
            "can_be_parallelized": appointment.can_be_parallelized,
        })
    return JsonResponse({
        "appointment_types": result
    })
