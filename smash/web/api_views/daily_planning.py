import datetime
import json
import logging
from operator import itemgetter
from collections import defaultdict

from django.http import JsonResponse, HttpRequest
from django.shortcuts import get_object_or_404
from django.db.models import Count

from web.models import Appointment, AppointmentTypeLink, Worker, Availability, Holiday
from web.models.worker_study_role import WORKER_STAFF
from web.tests.functions import datetimeify_date
from web.views.view_utils import e500_error
from web.views.notifications import get_filter_locations

logger = logging.getLogger(__name__)

RANDOM_COLORS = [
    f"#{8:02X}{218:02X}{217:02X}",
    f"#{8:02X}{8:02X}{101:02X}",
    f"#{8:02X}{218:02X}{3:02X}",
    f"#{247:02X}{137:02X}{156:02X}",
    f"#{183:02X}{96:02X}{2:02X}",
    f"#{20:02X}{106:02X}{55:02X}",
    f"#{8:02X}{119:02X}{217:02X}",
    f"#{16:02X}{76:02X}{27:02X}",
    f"#{85:02X}{7:02X}{162:02X}",
    f"#{157:02X}{7:02X}{2:02X}",
    f"#{49:02X}{65:02X}{68:02X}",
    f"#{112:02X}{124:02X}{98:02X}",
    f"#{8:02X}{8:02X}{215:02X}",
    f"#{85:02X}{7:02X}{98:02X}",
    f"#{16:02X}{76:02X}{100:02X}",
    f"#{85:02X}{7:02X}{50:02X}",
    f"#{183:02X}{5:02X}{2:02X}",
    f"#{183:02X}{156:02X}{2:02X}",
    f"#{112:02X}{82:02X}{38:02X}",
]


def build_duration(duration):
    number_of_hours = duration // 60
    minutes = duration - number_of_hours * 60
    return f"{number_of_hours:02d}:{minutes:02d}"


def get_holidays(worker, date):
    date_datetime = datetimeify_date(date)

    today_start = date_datetime.replace(hour=0, minute=0)
    today_end = date_datetime.replace(hour=23, minute=59)
    holidays = Holiday.objects.filter(
        person=worker, datetime_start__lte=today_end, datetime_end__gte=today_start
    )
    result = []
    for holiday in holidays:
        minutes = int(
            (holiday.datetime_end - holiday.datetime_start).total_seconds() / 60
        )
        # hack for time zones
        if holiday.datetime_start < today_start:
            # if the start date of the holiday is before than today, we put the link_start today 00.00
            start_date = today_start
        else:
            start_date = datetime.datetime.combine(
                today_start, holiday.datetime_start.time()
            )

        if holiday.datetime_end > today_end:
            # if the end date of the holiday is later than today, we put the link_end today 23.59
            end_date = today_end
        else:
            # truncates the end link to today at the time of the holiday end date
            end_date = datetime.datetime.combine(
                today_start, holiday.datetime_end.time()
            )

        event = {
            "duration": build_duration(minutes),
            "link_when": start_date,
            "link_who": worker.id,
            "link_end": end_date,
            "kind": holiday.kind,
        }
        result.append(event)

    return result


def prepare_remove_holidays(workers, date):
    today_start = date.replace(hour=0, minute=0)
    today_end = date.replace(hour=23, minute=59)
    holidays_starting_before = Holiday.objects.filter(
        person__in=workers,
        datetime_start__lte=today_start,
        datetime_end__gte=today_start,
    )
    holidays_starting_today = Holiday.objects.filter(
        person__in=workers,
        datetime_start__lte=today_end,
        datetime_start__gte=today_start,
    )
    holidays_ending_today = Holiday.objects.filter(
        person__in=workers, datetime_end__lte=today_end, datetime_end__gte=today_start
    )

    timestamp_worker_map = defaultdict(list)

    for holiday in holidays_starting_today:
        timestamp_worker_map[holiday.person_id].append(
            {"time": holiday.datetime_start.time(), "type": "stop"}
        )
    for holiday in holidays_ending_today:
        timestamp_worker_map[holiday.person_id].append(
            {"time": holiday.datetime_end.time(), "type": "start"}
        )

    directions_map = defaultdict(int)
    directions = holidays_starting_before.annotate(count=Count("person_id"))
    for direction in directions:
        directions_map[direction.person_id] = -direction.count

    return timestamp_worker_map, directions_map


def remove_holidays(availability, worker, today, holidays_timestamps, direction):
    today_end = today.replace(hour=23, minute=59)
    result = []
    timestamps = holidays_timestamps + [
        {"time": availability.available_from, "type": "start"},
        {"time": availability.available_till, "type": "stop"},
    ]

    start_date = None
    stop_date = None

    # sort first by time then by type (reverse) (stop goes before start)
    # to do so we first sort the secondary key
    sorted_array = sorted(timestamps, key=itemgetter("type"), reverse=True)
    sorted_array = sorted(sorted_array, key=itemgetter("time"))

    for timestamp in sorted_array:
        timestamp_type = timestamp.get("type", None)
        if timestamp_type == "stop":
            direction -= 1
            stop_date = timestamp["time"]
            if direction == 0:
                result.append(
                    Availability(
                        person=worker,
                        day_number=availability.day_number,
                        available_from=start_date,
                        available_till=stop_date,
                    )
                )
        elif timestamp_type == "start":
            direction += 1
            start_date = timestamp["time"]
        else:
            raise TypeError("Unknown type: " + str(timestamp_type))
    if direction > 0:
        result.append(
            Availability(
                person=worker,
                day_number=availability.day_number,
                available_from=start_date,
                available_till=today_end.time(),
            )
        )

    return result


def prepare_worker_availabilities(
    min_date: datetime.datetime, max_date: datetime.datetime, workers
):
    weekdays = []
    while min_date <= max_date:
        weekday = min_date.weekday() + 1
        weekdays.append(weekday)
        min_date += datetime.timedelta(days=1)

    list_of_availabilities = Availability.objects.filter(
        person__in=workers, day_number__in=weekdays
    )
    availability_map = defaultdict(lambda: defaultdict(list))

    for availability in list_of_availabilities:
        availability_map[availability.day_number][availability.person_id].append(
            availability
        )

    return availability_map


def get_availabilities(
    worker: Worker,
    today: datetime.datetime,
    filtered_availabilities: list,
    timestamps: list,
    direction: int,
):
    result = []

    for availability in filtered_availabilities:
        availabilities_without_holidays = remove_holidays(
            availability, worker, today, timestamps, direction
        )
        for availability_without_holiday in availabilities_without_holidays:
            start_date = datetime.datetime.combine(
                today, availability_without_holiday.available_from
            )
            end_date = datetime.datetime.combine(
                today, availability_without_holiday.available_till
            )
            delta = end_date - start_date

            minutes = int(delta.total_seconds() / 60)
            event = {
                "duration": build_duration(minutes),
                "link_when": start_date,
                "link_who": worker.id,
                "link_end": end_date,
            }
            result.append(event)
    return result


def get_conflicts(worker, date):
    result = []

    appointments = Appointment.objects.filter(
        datetime_when__date=date,
        location__in=get_filter_locations(worker),
        status=Appointment.APPOINTMENT_STATUS_SCHEDULED,
        visit__isnull=False,
    ).all()
    for _, appointment in enumerate(appointments):
        if appointment.worker_assigned is not None:
            link_when = appointment.datetime_when
            link_when = link_when.replace(tzinfo=None)
            link_end = link_when + datetime.timedelta(minutes=appointment.length)
            event = {
                "title": appointment.title(),
                "duration": appointment.length,
                "appointment_id": appointment.id,
                "link_when": link_when,
                "link_who": appointment.worker_assigned.id,
                "link_end": link_end,
                "location": str(appointment.location),
            }
            result.append(event)

        links = AppointmentTypeLink.objects.filter(appointment=appointment).all()
        for _, link in enumerate(links):
            link_when = link.date_when
            if link_when is not None:
                link_when = link_when.replace(tzinfo=None)
                link_end = link_when + datetime.timedelta(
                    minutes=link.appointment_type.default_duration
                )
                event = {
                    "title": link.appointment_type.description,
                    "duration": build_duration(link.appointment_type.default_duration),
                    "appointment_id": appointment.id,
                    "link_when": link_when,
                    "link_who": link.worker_id,
                    "link_end": link_end,
                    "location": str(appointment.location),
                }
                result.append(event)
    return result


def get_generic_appointment_events(
    request: HttpRequest, date: str, include_all: bool = False
):
    result = {}

    date = datetimeify_date(date)

    if include_all:
        appointments = Appointment.objects.filter(
            datetime_when__date=date,
            location__in=get_filter_locations(request.user),
            visit__isnull=True,
        ).all()
    else:
        appointments = Appointment.objects.filter(
            datetime_when__date=date,
            location__in=get_filter_locations(request.user),
            status=Appointment.APPOINTMENT_STATUS_SCHEDULED,
            visit__isnull=True,
        ).all()

    for appointment in appointments:
        if appointment.location_id not in result:
            appointment_data = {
                "name": "GENERAL",
                "id": appointment.id,
                "status": appointment.status,
                "color": RANDOM_COLORS[len(RANDOM_COLORS) - 1],
                "start": "",
                "location": str(appointment.location),
                "events": [],
            }
            result[appointment.location_id] = appointment_data

        link_when = appointment.datetime_when
        link_end = None
        if link_when is not None:
            link_when = link_when.replace(tzinfo=None)
            link_end = link_when + datetime.timedelta(minutes=appointment.length)
        event = {
            "title": appointment.title()
            + " ("
            + appointment.datetime_when.replace(tzinfo=None).strftime("%H:%M")
            + ")",
            "short_title": appointment.title(),
            "status": appointment.status,
            "duration": build_duration(appointment.length),
            "id": f"app-{appointment.id}",
            "appointment_id": appointment.id,
            "link_when": link_when,
            "link_who": appointment.worker_assigned_id,
            "link_end": link_end,
            "location": str(appointment.location),
            "flying_team_location": str(appointment.flying_team),
            "appointment_start": appointment.datetime_when.replace(tzinfo=None),
            "appointment_end": appointment.datetime_when.replace(
                tzinfo=None, hour=19, minute=0
            ),
        }
        appointment_events = result[appointment.location_id]["events"]
        appointment_events.append(event)

    return list(result.values())


def events(request: HttpRequest, date: str, include_all=False):
    date = datetimeify_date(date)

    # fetch appointments
    if include_all:
        appointments = Appointment.objects.filter(
            datetime_when__date=date,
            location__in=get_filter_locations(request.user),
            visit__isnull=False,
        ).all()
    else:
        appointments = Appointment.objects.filter(
            datetime_when__date=date,
            location__in=get_filter_locations(request.user),
            status=Appointment.APPOINTMENT_STATUS_SCHEDULED,
            visit__isnull=False,
        ).all()
    # store subjects
    subjects = {}
    # for each appointment
    for i, appointment in enumerate(appointments):
        # get its subject
        appointment_subject = appointment.visit.subject
        # if there is a subject we add it to the subjects dict
        if appointment_subject.id not in subjects:
            # create subject
            flag_set = {
                language.image.url
                for language in appointment_subject.subject.languages.all()
                if language.image is not None
            }
            default_language = (
                appointment_subject.subject.default_written_communication_language
            )
            if default_language is not None and default_language.image is not None:
                flag_set.add(default_language.image.url)

            subject = {
                "name": str(appointment_subject),
                "id": appointment_subject.id,
                "appointment_id": appointment.id,
                "color": RANDOM_COLORS[i],
                "start": appointment.datetime_when.replace(tzinfo=None).strftime(
                    "%H:%M"
                ),
                "flags": list(flag_set),
                # this indicates only location of the first appointment
                # (there is small chance to have two appointments in two different places at the same day)
                "location": str(appointment.location),
                "events": [],
            }
            subjects[appointment_subject.id] = subject

        # get appointment type link of the appointment
        links = AppointmentTypeLink.objects.filter(appointment=appointment).all()
        # for the AppointmentTypeLink, get all the info and create the event object
        for j, link in enumerate(links):
            link_when = link.date_when
            link_end = None
            if link_when is not None:
                link_when = link_when.replace(tzinfo=None)
                link_end = link_when + datetime.timedelta(
                    minutes=link.appointment_type.default_duration
                )
            event = {
                "title": link.appointment_type.description,
                "status": link.appointment.status,
                "short_title": link.appointment_type.code,
                "duration": build_duration(link.appointment_type.default_duration),
                "subject": str(appointment_subject),
                "id": f"{i}-{j}",
                "link_id": link.id,
                "link_when": link_when,
                "link_who": link.worker_id,
                "link_end": link_end,
                "location": str(appointment.location),
                "flying_team_location": str(appointment.flying_team),
                "appointment_start": appointment.datetime_when.replace(tzinfo=None),
                "appointment_end": appointment.datetime_when.replace(
                    tzinfo=None, hour=19, minute=0
                ),
            }
            subject_events = subjects[appointment_subject.id]["events"]
            subject_events.append(event)

    generic_appointment_events = get_generic_appointment_events(
        request, date, include_all=include_all
    )

    result_availabilities = []
    holidays = []
    workers = get_workers_for_daily_planning(request)

    availability_map = prepare_worker_availabilities(date, date, workers)
    workers_timestamps_map, directions_map = prepare_remove_holidays(workers, date)
    weekday = date.weekday() + 1

    for worker in workers:
        filtered_availabilities = availability_map[weekday][worker.id]
        holiday_timestamps = workers_timestamps_map[worker.id]
        direction = directions_map[worker.id]
        result_availabilities = result_availabilities + get_availabilities(
            worker, date, filtered_availabilities, holiday_timestamps, direction
        )
        holidays = holidays + get_holidays(worker, date)

    return JsonResponse(
        {
            "data": list(subjects.values()),
            "generic": generic_appointment_events,
            "availabilities": result_availabilities,
            "holidays": holidays,
        }
    )


def get_workers_for_daily_planning(request: HttpRequest):
    result = (
        Worker.get_workers_by_worker_type(WORKER_STAFF)
        .filter(locations__in=get_filter_locations(request.user))
        .filter(user__is_active=True)
        .distinct()
    )
    return result


def availabilities(request, date):
    date = datetimeify_date(date)
    result_availabilities = []
    holidays = []
    conflicts = []
    workers = get_workers_for_daily_planning(request)
    availability_map = prepare_worker_availabilities(date, date, workers)
    workers_timestamps_map, directions_map = prepare_remove_holidays(workers, date)
    weekday = date.weekday() + 1

    for worker in workers:
        filtered_availabilities = availability_map[weekday][worker.id]
        holiday_timestamps = workers_timestamps_map[worker.id]
        direction = directions_map[worker.id]
        result_availabilities = result_availabilities + get_availabilities(
            worker, date, filtered_availabilities, holiday_timestamps, direction
        )
        holidays = holidays + get_holidays(worker, date)
        conflicts = conflicts + get_conflicts(worker, date)

    return JsonResponse(
        {
            "conflicts": conflicts,
            "availabilities": result_availabilities,
            "holidays": holidays,
        }
    )


def events_persist(request):
    try:
        event_links = json.loads(request.POST.get("events_to_persist"))
        for event_link in event_links:
            try:
                when = datetime.datetime.strptime(
                    event_link["start"], "%Y-%m-%dT%H:%M:00"
                )
            except ValueError:
                when = datetime.datetime.strptime(
                    event_link["start"][0:-6], "%Y-%m-%dT%H:%M:00"
                )
            worker_id = event_link["link_who"]
            if "link_id" in event_link:
                link_id = event_link["link_id"]
                appointment_type_link = get_object_or_404(
                    AppointmentTypeLink, pk=link_id
                )
                appointment_type_link.worker_id = worker_id
                appointment_type_link.date_when = when
                appointment_type_link.save()
            else:
                logger.info(event_link)
                appointment_id = event_link["appointment_id"]
                appointment = get_object_or_404(Appointment, pk=appointment_id)
                appointment.worker_assigned_id = worker_id
                appointment.datetime_when = when
                appointment.save()
        events_to_clear = json.loads(request.POST.get("events_to_clear"))
        for link_id in events_to_clear:
            appointment_type_link = get_object_or_404(AppointmentTypeLink, pk=link_id)
            appointment_type_link.worker_id = None
            appointment_type_link.date_when = None
            appointment_type_link.save()
        appointments_to_clear = json.loads(request.POST.get("appointments_to_clear"))
        for appointment_id in appointments_to_clear:
            appointment = get_object_or_404(Appointment, pk=appointment_id)
            appointment.worker_assigned_id = None
            appointment.save()
        return JsonResponse({}, status=201)
    except Exception as e:
        logger.error(e, exc_info=True)
        return e500_error(request)
