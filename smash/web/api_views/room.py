from django.http import HttpRequest, JsonResponse

from web.models import Room, Appointment, Location
from web.tests.functions import datetimeify_date


def rooms_for_location(request: HttpRequest, location_id: int):
    location = Location.objects.filter(id=location_id).first()
    if location is None:
        return JsonResponse({})
    rooms = Appointment.objects.filter(location=location).values_list('room', flat=True).distinct()
    room_ids = [room for room in rooms if room is not None]
    room_objs = Room.objects.filter(id__in=room_ids)

    data = []
    for room in room_objs:
        data.append({"id": room.id, "title": str(room)})
    data.append({"id": -1, "title": "No Room"})
    return JsonResponse(data, safe=False)


def appointments_for_location_and_date(request: HttpRequest, location_id: int):
    start_date = request.GET.get('start')
    location = Location.objects.filter(id=location_id).first()
    if location is None:
        return JsonResponse({})

    start_date = datetimeify_date(start_date)

    appointments = Appointment.objects.filter(
            datetime_when__date=start_date, location=location).all()

    data = []
    for appointment in appointments:

        until = appointment.datetime_until()
        if until is not None:
            until = until.replace(tzinfo=None)

        room = appointment.room
        if room is None:
            resourceId = "-1"
        else:
            resourceId = str(room.id)

        event = {
            "resourceId": resourceId,
            "title": appointment.title(),
            "start": appointment.datetime_when.replace(tzinfo=None),
            "end": until,
            "appointment_id": appointment.id
        }
        data.append(event)

    return JsonResponse(data, safe=False)
