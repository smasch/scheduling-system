import logging

from django.http import JsonResponse

from web.models import VoucherType

logger = logging.getLogger(__name__)


def get_voucher_types(request):
    all_vouchers = VoucherType.objects.all()

    count = all_vouchers.count()

    data = []
    for voucher_type in all_vouchers:
        data.append(serialize_voucher_type(voucher_type))

    return JsonResponse({
        "recordsTotal": count,
        "recordsFiltered": count,
        "data": data,
    })


def serialize_voucher_type(voucher_type):
    result = {
        "code": voucher_type.code,
        "description": voucher_type.description,
        "id": voucher_type.id,
    }
    return result
