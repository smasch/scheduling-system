from django.http import JsonResponse

from web.models import FlyingTeam


def flying_teams(request):
    all_flying_teams = FlyingTeam.objects.all()
    data = []
    for flying_team in all_flying_teams:
        data.append({"id": flying_team.id, "name": flying_team.place})
    return JsonResponse({
        "flying_teams": data
    })
