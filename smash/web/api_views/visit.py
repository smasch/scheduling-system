import logging

from django.core.handlers.wsgi import WSGIRequest
from django.db.models import Q
from django.http import JsonResponse, HttpResponseNotAllowed

from web.utils import strtobool
from web.api_views.serialization_utils import bool_to_yes_no, flying_team_to_str, location_to_str, add_column, \
    serialize_date, get_filters_for_data_table_request
from web.models import AppointmentType, Appointment
from web.models import ConfigurationItem
from web.models import SubjectColumns
from web.models import Visit, Study, VisitColumns, StudyVisitList, StudyColumns
from web.models.constants import GLOBAL_STUDY_ID, VISIT_SHOW_VISIT_NUMBER_FROM_ZERO
from web.models.study_visit_list import VISIT_LIST_GENERIC, VISIT_LIST_EXCEEDED_TIME, VISIT_LIST_UNFINISHED, \
    VISIT_LIST_MISSING_APPOINTMENTS, VISIT_LIST_APPROACHING_WITHOUT_APPOINTMENTS, \
    VISIT_LIST_APPROACHING_FOR_MAIL_CONTACT
from web.templatetags.filters import display_visit_number
from web.views.notifications import get_unfinished_visits, get_active_visits_with_missing_appointments, \
    get_approaching_visits_without_appointments, get_approaching_visits_for_mail_contact, get_exceeded_visits

logger = logging.getLogger(__name__)


# noinspection PyUnusedLocal
def get_visit_columns(request, visit_list_type):
    study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
    study_visit_lists = StudyVisitList.objects.filter(study=study, type=visit_list_type)
    if len(study_visit_lists) > 0:
        visit_list = study_visit_lists[0]
        visit_columns = visit_list.visible_visit_columns
        visit_subject_columns = visit_list.visible_subject_columns
        visit_subject_study_columns = visit_list.visible_study_subject_columns
    else:
        visit_list = StudyVisitList()
        visit_columns = VisitColumns()
        visit_subject_columns = SubjectColumns()
        visit_subject_study_columns = StudyColumns()

    result = []
    add_column(result, "First name", "first_name", visit_subject_columns, "string_filter")
    add_column(result, "Last name", "last_name", visit_subject_columns, "string_filter")
    add_column(result, "Subject number", "nd_number", visit_subject_study_columns, "string_filter", study.columns)
    add_column(result, "Location", "default_location", visit_subject_study_columns, "location_filter", study.columns)
    add_column(result, "Flying team location", "flying_team", visit_subject_study_columns, "flying_team_filter",
               study.columns)
    add_column(result, "Visit begins", "datetime_begin", visit_columns, None)
    add_column(result, "Visit ends", "datetime_end", visit_columns, None)
    add_column(result, "Finished", "is_finished", visit_columns, "yes_no_filter")
    add_column(result, "Post mail sent", "post_mail_sent", visit_columns, "yes_no_filter")

    visit_from_zero = ConfigurationItem.objects.get(type=VISIT_SHOW_VISIT_NUMBER_FROM_ZERO).value
    # True values are y, yes, t, true, on and 1; false values are n, no, f, false, off and 0.
    if strtobool(visit_from_zero):
        add_column(result, "Visit number", "display_visit_number", None, "from_zero_integer_filter")
    else:
        add_column(result, "Visit number", "visit_number", visit_columns, "integer_filter")

    add_column(result, "Appointments in progress", "visible_appointment_types_in_progress", visit_list,
               "appointment_type_filter", sortable=False)
    add_column(result, "Done appointments", "visible_appointment_types_done", visit_list, "appointment_type_filter",
               sortable=False)
    add_column(result, "Missing appointments", "visible_appointment_types_missing", visit_list,
               "appointment_type_filter", sortable=False)
    add_column(result, "All appointments", "visible_appointment_types", visit_columns, "appointment_type_filter",
               sortable=False)
    add_column(result, "Edit", "edit", None, None, sortable=False)

    return JsonResponse({"columns": result})


# noinspection PyUnusedLocal
def get_visits(request, visit_type):
    if visit_type == VISIT_LIST_GENERIC:
        return Visit.objects.all()
    elif visit_type == VISIT_LIST_EXCEEDED_TIME:
        return get_exceeded_visits(request.user).all()
    elif visit_type == VISIT_LIST_UNFINISHED:
        return get_unfinished_visits(request.user).all()
    elif visit_type == VISIT_LIST_MISSING_APPOINTMENTS:
        return get_active_visits_with_missing_appointments(request.user).all()
    elif visit_type == VISIT_LIST_APPROACHING_WITHOUT_APPOINTMENTS:
        return get_approaching_visits_without_appointments(request.user).all()
    elif visit_type == VISIT_LIST_APPROACHING_FOR_MAIL_CONTACT:
        return get_approaching_visits_for_mail_contact(request.user).all()
    else:
        raise TypeError("Unknown query type: " + visit_type)


def get_visits_order(visits_to_be_ordered, order_column, order_direction):
    result = visits_to_be_ordered
    if order_direction == "asc":
        order_direction = ""
    else:
        order_direction = "-"
    if order_column == "first_name":
        result = visits_to_be_ordered.order_by(order_direction + 'subject__subject__first_name')
    elif order_column == "last_name":
        result = visits_to_be_ordered.order_by(order_direction + 'subject__subject__last_name')
    elif order_column == "nd_number":
        result = visits_to_be_ordered.order_by(order_direction + 'subject__nd_number')
    elif order_column == "default_location":
        result = visits_to_be_ordered.order_by(order_direction + 'subject__default_location')
    elif order_column == "flying_team":
        result = visits_to_be_ordered.order_by(order_direction + 'subject__flying_team')
    elif order_column == "datetime_begin":
        result = visits_to_be_ordered.order_by(order_direction + 'datetime_begin')
    elif order_column == "datetime_end":
        result = visits_to_be_ordered.order_by(order_direction + 'datetime_end')
    elif order_column == "is_finished":
        result = visits_to_be_ordered.order_by(order_direction + 'is_finished')
    elif order_column == "post_mail_sent":
        result = visits_to_be_ordered.order_by(order_direction + 'post_mail_sent')
    elif order_column == "visit_number":
        result = visits_to_be_ordered.order_by(order_direction + 'visit_number')
    elif order_column == "display_visit_number":
        result = visits_to_be_ordered.order_by(order_direction + 'visit_number')
    else:
        logger.warning("Unknown sort column: %s", str(order_column))
    return result


def filter_by_appointment_in_progress(visits_to_filter, appointment_type):
    result = visits_to_filter.filter(Q(appointment__appointment_types=appointment_type) & Q(
        appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED))
    return result


def filter_by_appointment_done(visits_to_filter, appointment_type):
    result = visits_to_filter.filter(Q(appointment__appointment_types=appointment_type) & Q(
        appointment__status=Appointment.APPOINTMENT_STATUS_FINISHED))
    return result


def filter_by_appointment_missing(visits_to_filter, appointment_type):
    result = filter_by_appointment(visits_to_filter, appointment_type).exclude(
        Q(appointment__appointment_types=appointment_type), Q(
            appointment__status=Appointment.APPOINTMENT_STATUS_FINISHED) | Q(
            appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED))
    return result


def filter_by_appointment(visits_to_filter, appointment_type):
    result = visits_to_filter.filter(appointment_types=appointment_type)
    return result


def get_visits_filtered(visits_to_be_filtered, filters):
    result = visits_to_be_filtered
    for row in filters:
        column = row[0]
        value = row[1]
        if column == "first_name":
            result = result.filter(subject__subject__first_name__icontains=value)
        elif column == "last_name":
            result = result.filter(subject__subject__last_name__icontains=value)
        elif column == "nd_number":
            result = result.filter(subject__nd_number__icontains=value)
        elif column == "flying_team":
            result = result.filter(subject__flying_team=value)
        elif column == "default_location":
            result = result.filter(subject__default_location=value)
        elif column == "is_finished":
            result = result.filter(is_finished=(value == "true"))
        elif column == "post_mail_sent":
            result = result.filter(post_mail_sent=(value == "true"))
        elif column == "visit_number":
            result = result.filter(visit_number=int(value))
        elif column == "display_visit_number":
            result = result.filter(visit_number=int(value) + 1)
        elif column == "visible_appointment_types_in_progress":
            result = filter_by_appointment_in_progress(result, value)
        elif column == "visible_appointment_types_done":
            result = filter_by_appointment_done(result, value)
        elif column == "visible_appointment_types_missing":
            result = filter_by_appointment_missing(result, value)
        elif column == "visible_appointment_types":
            result = filter_by_appointment(result, value)
        else:
            message = "UNKNOWN filter: "
            if column is None:
                message += "[None]"
            else:
                message += str(column)
            logger.warning(message)

    return result


def visits(request: WSGIRequest, visit_list_type):
    if request.method == "GET":
        request_data = request.GET
    elif request.method == "POST":
        request_data = request.POST
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])
    # id of the query from dataTable: https://datatables.net/manual/server-side
    draw = int(request_data.get("draw", "-1"))

    start = int(request_data.get("start", "0"))
    length = int(request_data.get("length", "10"))
    order = int(request_data.get("order[0][column]", "0"))
    order_dir = request_data.get("order[0][dir]", "asc")
    order_column = request_data.get("columns[" + str(order) + "][data]", "last_name")

    filters = get_filters_for_data_table_request(request_data)

    all_visits = get_visits(request, visit_list_type)

    count = all_visits.count()

    ordered_visits = get_visits_order(all_visits, order_column, order_dir)
    filtered_visits = get_visits_filtered(ordered_visits, filters)
    sliced_visits = filtered_visits[start:(start + length)]

    result_visits = sliced_visits

    count_filtered = filtered_visits.count()

    data = []
    for visit in result_visits:
        data.append(serialize_visit(visit))

    return JsonResponse({
        "draw": draw,
        "recordsTotal": count,
        "recordsFiltered": count_filtered,
        "data": data,
    })


def appointment_types_to_str(appointment_types):
    result = ""
    for appointment_type in appointment_types:
        result += str(appointment_type.code) + ", "
    return result


def serialize_visit(visit):
    appointment_types = visit.appointment_types.all()
    appointment_types_in_progress = AppointmentType.objects.filter(Q(appointmenttypelink__appointment__visit=visit) & Q(
        appointmenttypelink__appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED)).distinct().all()
    appointment_types_done = AppointmentType.objects.filter(Q(appointmenttypelink__appointment__visit=visit) & Q(
        appointmenttypelink__appointment__status=Appointment.APPOINTMENT_STATUS_FINISHED)).distinct().all()

    appointment_types_missing = []
    for appointment_type in appointment_types:
        if appointment_types_in_progress.filter(id=appointment_type.id).count() == 0 and appointment_types_done.filter(
                id=appointment_type.id).count() == 0:
            appointment_types_missing.append(appointment_type)

    result = {
        "first_name": visit.subject.subject.first_name,
        "last_name": visit.subject.subject.last_name,
        "nd_number": visit.subject.nd_number,
        "datetime_begin": serialize_date(visit.datetime_begin),
        "datetime_end": serialize_date(visit.datetime_end),
        "flying_team": flying_team_to_str(visit.subject.flying_team),
        "default_location": location_to_str(visit.subject.default_location),
        "is_finished": bool_to_yes_no(visit.is_finished),
        "post_mail_sent": bool_to_yes_no(visit.post_mail_sent),
        "visit_number": visit.visit_number,
        "id": visit.id,
        "visible_appointment_types_in_progress": appointment_types_to_str(appointment_types_in_progress),
        "visible_appointment_types_done": appointment_types_to_str(appointment_types_done),
        "visible_appointment_types_missing": appointment_types_to_str(appointment_types_missing),
        "visible_appointment_types": appointment_types_to_str(appointment_types),
    }

    visit_from_zero = ConfigurationItem.objects.get(type=VISIT_SHOW_VISIT_NUMBER_FROM_ZERO).value
    # True values are y, yes, t, true, on and 1; false values are n, no, f, false, off and 0.
    if strtobool(visit_from_zero):
        result["display_visit_number"] = display_visit_number(visit.visit_number)

    return result
