import logging
from typing import List, Optional

logger = logging.getLogger(__name__)


def bool_to_yes_no(val: bool):
    if val:
        return "YES"
    else:
        return "NO"


def bool_to_yes_no_null(val: bool):
    if val is None:
        return "N/A"
    if val:
        return "YES"
    else:
        return "NO"


def str_to_yes_no(val: str):
    if val.lower() == "true":
        return "YES"
    else:
        return "NO"


def str_to_yes_no_null(val: str):
    if val is None:
        return None
    if val.lower() == "true":
        return "YES"
    else:
        return "NO"


def flying_team_to_str(flying_team):
    result = ""
    if flying_team is not None:
        result = str(flying_team)
    return result


def location_to_str(location):
    result = ""
    if location is not None:
        result = str(location.name)
    return result


def serialize_date(date):
    if date is not None:
        result = date.strftime("%Y-%m-%d")
    else:
        result = ""
    return result


def serialize_datetime(date):
    if date is not None:
        result = date.strftime("%Y-%m-%d %H:%M")
    else:
        result = ""
    return result


def add_column(
    result: List,
    name: str,
    field_name: str,
    column_list: object,
    param_filter: Optional[str],
    columns_used_in_study: Optional[object] = None,
    visible_param: Optional[bool] = None,
    sortable: Optional[bool] = True,
    add_param: Optional[bool] = True,
):
    add = add_param
    if columns_used_in_study:
        add = getattr(columns_used_in_study, field_name)
    if add:
        if visible_param is not None:
            visible = visible_param
        elif column_list is None:
            visible = True
        else:
            visible = getattr(column_list, field_name)
        result.append(
            {
                "type": field_name,
                "name": name,
                "filter": param_filter,
                "visible": visible,
                "sortable": sortable,
            }
        )


def get_filters_for_data_table_request(request_data):
    filters = []
    column_id = 0
    while (
        request_data.get("columns[" + str(column_id) + "][search][value]", "unknown")
        != "unknown"
    ):
        val = request_data.get(
            "columns[" + str(column_id) + "][search][value]", "unknown"
        )
        if val != "":
            filters.append(
                [request_data.get("columns[" + str(column_id) + "][data]"), val]
            )
        column_id += 1
    return filters
