import logging

from django.core.handlers.wsgi import WSGIRequest
from django.http import JsonResponse, HttpRequest, HttpResponseNotAllowed

from web.api_views.serialization_utils import get_filters_for_data_table_request, add_column, \
    serialize_date
from web.models import Voucher

logger = logging.getLogger(__name__)


def get_vouchers_order(vouchers_to_be_ordered, order_column, order_direction):
    result = vouchers_to_be_ordered
    if order_direction == "asc":
        order_direction = ""
    else:
        order_direction = "-"
    if order_column == "first_name":
        result = vouchers_to_be_ordered.order_by(order_direction + 'study_subject__subject__first_name')
    elif order_column == "last_name":
        result = vouchers_to_be_ordered.order_by(order_direction + 'study_subject__subject__last_name')
    elif order_column == "number":
        result = vouchers_to_be_ordered.order_by(order_direction + 'number')
    elif order_column == "expiry_date":
        result = vouchers_to_be_ordered.order_by(order_direction + 'expiry_date')
    elif order_column == "issue_date":
        result = vouchers_to_be_ordered.order_by(order_direction + 'issue_date')
    elif order_column == "id":
        result = vouchers_to_be_ordered.order_by(order_direction + 'id')
    elif order_column == "type":
        result = vouchers_to_be_ordered.order_by(order_direction + 'voucher_type__code')
    elif order_column == "status":
        result = vouchers_to_be_ordered.order_by(order_direction + 'status')
    else:
        logger.warning("Unknown sort column: %s", str(order_column))
    return result


def get_vouchers_filtered(vouchers_to_be_filtered, filters):
    result = vouchers_to_be_filtered
    for row in filters:
        column = row[0]
        value = row[1]
        if column == "first_name":
            result = result.filter(study_subject__subject__first_name__icontains=value)
        elif column == "last_name":
            result = result.filter(study_subject__subject__last_name__icontains=value)
        elif column == "number":
            result = result.filter(number__icontains=value)
        elif column == "type":
            result = result.filter(voucher_type=value)
        elif column == "status":
            result = result.filter(status=value)
        elif column == "voucher_partner":
            result = result.filter(usage_partner=value)
        elif column == "feedback":
            result = result.filter(feedback__icontains=value)
        elif column == "":
            pass
        else:
            message = "UNKNOWN filter: "
            if column is None:
                message += "[None]"
            else:
                message += str(column)
            logger.warning(message)
    return result


# noinspection PyUnusedLocal
def get_voucher_columns(request: HttpRequest) -> JsonResponse:
    result = []
    add_column(result, "First name", "first_name", None, "string_filter")
    add_column(result, "Last name", "last_name", None, "string_filter")
    add_column(result, "Number", "number", None, "string_filter")
    add_column(result, "Type", "type", None, "voucher_type_filter")
    add_column(result, "Status", "status", None, "voucher_status_filter")
    add_column(result, "Voucher partner", "voucher_partner", None, "voucher_partner_filter")
    add_column(result, "Issue date", "issue_date", None, None)
    add_column(result, "Expiry date", "expiry_date", None, None)
    add_column(result, "Edit", "edit", None, None, sortable=False)

    return JsonResponse({"columns": result})


def get_vouchers(request: WSGIRequest):
    # id of the query from dataTable: https://datatables.net/manual/server-side
    if request.method == "GET":
        request_data = request.GET
    elif request.method == "POST":
        request_data = request.POST
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])
    draw = int(request_data.get("draw", "-1"))

    start = int(request_data.get("start", "0"))
    length = int(request_data.get("length", "10"))
    order = int(request_data.get("order[0][column]", "0"))
    order_dir = request_data.get("order[0][dir]", "asc")
    order_column = request_data.get("columns[" + str(order) + "][data]", "last_name")

    filters = get_filters_for_data_table_request(request_data)

    all_vouchers = Voucher.objects.all()

    count = all_vouchers.count()

    ordered_vouchers = get_vouchers_order(all_vouchers, order_column, order_dir)
    filtered_vouchers = get_vouchers_filtered(ordered_vouchers, filters)
    sliced_vouchers = filtered_vouchers[start:(start + length)]

    result_vouchers = sliced_vouchers

    count_filtered = filtered_vouchers.count()

    data = []
    for voucher in result_vouchers:
        data.append(serialize_voucher(voucher))

    return JsonResponse({
        "draw": draw,
        "recordsTotal": count,
        "recordsFiltered": count_filtered,
        "data": data,
    })


def serialize_voucher(voucher: Voucher) -> dict:
    issue_date = serialize_date(voucher.issue_date)
    expiry_date = serialize_date(voucher.expiry_date)
    result = {
        "first_name": voucher.study_subject.subject.first_name,
        "last_name": voucher.study_subject.subject.last_name,
        "issue_date": issue_date,
        "type": voucher.voucher_type.code,
        "status": voucher.get_status_display(),
        "voucher_partner": str(voucher.usage_partner),
        "feedback": voucher.feedback,
        "expiry_date": expiry_date,
        "number": voucher.number,
        "id": voucher.id,
    }
    return result
