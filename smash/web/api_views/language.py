from django.http import JsonResponse

from web.models import Language


def languages(request):
    langs = Language.objects.all().order_by('name')
    result = []
    for lang in langs:
        result.append({
            "id": lang.id,
            "name": lang.name,
            "image": lang.image.url if lang.image else "",
            "locale": lang.locale,
            "order": lang.order,
            "windows_locale_name": lang.windows_locale_name
        })
    return JsonResponse({
        "languages": result
    })
