from ..models import Provenance, Worker
from django_datatables_view.base_datatable_view import BaseDatatableView


class ExportLog(BaseDatatableView):
    model = Provenance
    columns = [
        'modification_date',
        'modification_author',
        'modification_description',
        'request_path',
        'request_ip_addr'
    ]

    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = [
        'modification_date',
        'modification_author',
        'modification_description',
        'request_path',
        'request_ip_addr'
    ]

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 10

    def filter_queryset(self, qs):
        # simple example:
        search = self.request.GET.get('search[value]', None)
        if search != '' and search is not None:
            inner_qs = Worker.objects.filter(first_name__icontains=search) | Worker.objects.filter(
                last_name__icontains=search)
            qs = qs.filter(
                modification_author__in=inner_qs) | qs.filter(
                modification_description__icontains=search) | qs.filter(
                request_path__icontains=search) | qs.filter(
                request_ip_addr__icontains=search)

        qs = qs & qs.filter(modified_table__isnull=True,
                            previous_value__isnull=True,
                            new_value__isnull=True,
                            modified_field='',
                            request_path__startswith='/export/')

        return qs


class CompleteLog(BaseDatatableView):
    model = Provenance
    columns = ['modified_table',
               'modified_table_id',
               'modification_date',
               'modification_author',
               'modified_field',
               'previous_value',
               'new_value',
               'modification_description',
               'request_path',
               'request_ip_addr'
               ]

    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['modified_table',
                     'modified_table_id',
                     'modification_date',
                     'modification_author',
                     'modified_field',
                     'previous_value',
                     'new_value',
                     'modification_description',
                     'request_path',
                     'request_ip_addr'
                     ]

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 10

    def filter_queryset(self, qs):
        # simple example:
        search = self.request.GET.get('search[value]', None)
        if search != '' and search is not None:
            inner_qs = Worker.objects.filter(first_name__icontains=search) | Worker.objects.filter(
                last_name__icontains=search)
            qs = qs.filter(
                modification_author__in=inner_qs) | qs.filter(
                modification_description__icontains=search) | qs.filter(
                request_path__icontains=search) | qs.filter(
                request_ip_addr__icontains=search)

        return qs
