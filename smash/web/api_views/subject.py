from django.db.models import F
import logging
import re

from django.core.handlers.wsgi import WSGIRequest
from django.db.models import Count, Case, When, Min, Max, QuerySet
from django.db.models import Q
from django.http import JsonResponse, HttpRequest, HttpResponseNotAllowed, HttpResponse
from django.urls import reverse

from web.utils import strtobool
from web.api_views.serialization_utils import (
    str_to_yes_no_null,
    bool_to_yes_no,
    flying_team_to_str,
    location_to_str,
    add_column,
    serialize_date,
    serialize_datetime,
    get_filters_for_data_table_request,
)
from web.models import (
    ConfigurationItem,
    StudySubject,
    Visit,
    Appointment,
    Subject,
    SubjectColumns,
    StudyColumns,
    Study,
    ContactAttempt,
    SubjectType,
)
from web.models.constants import (
    GLOBAL_STUDY_ID,
    VISIT_SHOW_VISIT_NUMBER_FROM_ZERO,
    CUSTOM_FIELD_TYPE_TEXT,
    CUSTOM_FIELD_TYPE_BOOLEAN,
    CUSTOM_FIELD_TYPE_INTEGER,
    CUSTOM_FIELD_TYPE_DOUBLE,
    CUSTOM_FIELD_TYPE_DATE,
    CUSTOM_FIELD_TYPE_SELECT_LIST,
    CUSTOM_FIELD_TYPE_FILE,
)
from web.models.custom_data.custom_study_subject_field import (
    get_study_subject_field_id,
    CustomStudySubjectField,
)
from web.models.study_subject_list import (
    SUBJECT_LIST_GENERIC,
    SUBJECT_LIST_NO_VISIT,
    SUBJECT_LIST_REQUIRE_CONTACT,
    StudySubjectList,
    SUBJECT_LIST_VOUCHER_EXPIRY,
)
from web.views.notifications import (
    get_subjects_with_no_visit,
    get_subjects_with_reminder,
    get_today_midnight_date,
    get_subjects_with_almost_expired_vouchers,
)
from web.views.view_utils import e500_error

logger = logging.getLogger(__name__)


# noinspection PyUnusedLocal
def cities(request):
    result_subjects = (
        Subject.objects.filter(city__isnull=False).values_list("city").distinct()
    )
    return JsonResponse({"cities": [x[0] for x in result_subjects]})


# noinspection PyUnusedLocal
def referrals(request):
    result_subjects = (
        StudySubject.objects.filter(referral__isnull=False)
        .values_list("referral")
        .distinct()
    )
    return JsonResponse({"referrals": [x[0] for x in result_subjects]})


# noinspection PyUnusedLocal
def get_subject_columns(request: HttpRequest, subject_list_type: str) -> JsonResponse:
    study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
    study_subject_lists = StudySubjectList.objects.filter(
        study=study, type=subject_list_type
    )
    if len(study_subject_lists) > 0:
        study_subject_list = study_subject_lists[0]
        subject_columns = study_subject_list.visible_subject_columns
        study_subject_columns = study_subject_list.visible_subject_study_columns
    else:
        study_subject_list = StudySubjectList()
        subject_columns = SubjectColumns()
        study_subject_columns = StudyColumns()

    result = []
    add_column(
        result,
        "Subject number",
        "nd_number",
        study_subject_columns,
        "string_filter",
        study.columns,
    )
    add_column(
        result,
        "Screening",
        "screening_number",
        study_subject_columns,
        "string_filter",
        study.columns,
    )
    add_column(result, "First name", "first_name", subject_columns, "string_filter")
    add_column(result, "Last name", "last_name", subject_columns, "string_filter")
    add_column(result, "Address", "address", subject_columns, "string_filter")
    add_column(
        result,
        "Social Security Number",
        "social_security_number",
        subject_columns,
        "string_filter",
    )
    add_column(result, "Phone number", "phone_number", subject_columns, "string_filter")
    add_column(
        result, "Phone number 2", "phone_number_2", subject_columns, "string_filter"
    )
    add_column(
        result, "Phone number 3", "phone_number_3", subject_columns, "string_filter"
    )
    add_column(result, "Date of birth", "date_born", subject_columns, None)
    add_column(
        result,
        "Contact on",
        "datetime_contact_reminder",
        study_subject_columns,
        None,
        study.columns,
    )
    add_column(
        result, "Last contact attempt", "last_contact_attempt", study_subject_list, None
    )
    add_column(
        result,
        "Referred by",
        "referral",
        study_subject_columns,
        "string_filter",
        study.columns,
    )
    add_column(
        result,
        "Health partner name",
        "health_partner_first_name",
        None,
        "string_filter",
        add_param=study.columns.health_partner,
        visible_param=study_subject_columns.health_partner,
    )
    add_column(
        result,
        "Health partner last name",
        "health_partner_last_name",
        None,
        "string_filter",
        add_param=study.columns.health_partner,
        visible_param=study_subject_columns.health_partner,
    )
    add_column(
        result,
        "Location",
        "default_location",
        study_subject_columns,
        "location_filter",
        study.columns,
    )
    add_column(
        result,
        "Flying team location",
        "flying_team",
        study_subject_columns,
        "flying_team_filter",
        study.columns,
    )
    add_column(result, "Deceased", "dead", subject_columns, "yes_no_filter")
    add_column(
        result,
        "Resigned",
        "resigned",
        study_subject_columns,
        "yes_no_filter",
        study.columns,
    )
    add_column(
        result,
        "Endpoint Reached",
        "endpoint_reached",
        study_subject_columns,
        "yes_no_filter",
        study.columns,
    )
    add_column(
        result,
        "Postponed",
        "postponed",
        study_subject_columns,
        "yes_no_filter",
        study.columns,
    )
    add_column(
        result, "Next of kin", "next_of_kin_name", subject_columns, "string_filter"
    )
    add_column(
        result,
        "Next of kin phone",
        "next_of_kin_phone",
        subject_columns,
        "string_filter",
    )
    add_column(
        result,
        "Next of kin address",
        "next_of_kin_address",
        subject_columns,
        "string_filter",
    )
    add_column(
        result,
        "Excluded",
        "excluded",
        study_subject_columns,
        "yes_no_filter",
        study.columns,
    )
    add_column(
        result,
        "Info sent",
        "information_sent",
        study_subject_columns,
        "yes_no_filter",
        study.columns,
    )
    add_column(
        result,
        "Default Written Communication Language",
        "default_written_communication_language",
        subject_columns,
        "language_filter",
    )

    visit_from_zero = ConfigurationItem.objects.get(
        type=VISIT_SHOW_VISIT_NUMBER_FROM_ZERO
    ).value
    # True values are y, yes, t, true, on and 1; false values are n, no, f, false, off and 0.
    if strtobool(visit_from_zero):
        visit_numbers = list(range(0, study.visits_to_show_in_subject_list + 0))
    else:
        visit_numbers = list(range(1, study.visits_to_show_in_subject_list + 1))

    add_column(
        result, "Type", "type", study_subject_columns, "type_filter", study.columns
    )
    for custom_study_subject_field in study.customstudysubjectfield_set.all():
        visible = study_subject_columns.is_custom_field_visible(
            custom_study_subject_field
        )
        if custom_study_subject_field.type == CUSTOM_FIELD_TYPE_TEXT:
            add_column(
                result,
                custom_study_subject_field.name,
                get_study_subject_field_id(custom_study_subject_field),
                study_subject_columns,
                "string_filter",
                visible_param=visible,
            )
        elif custom_study_subject_field.type == CUSTOM_FIELD_TYPE_BOOLEAN:
            add_column(
                result,
                custom_study_subject_field.name,
                get_study_subject_field_id(custom_study_subject_field),
                study_subject_columns,
                "yes_no_filter",
                visible_param=visible,
            )
        elif custom_study_subject_field.type == CUSTOM_FIELD_TYPE_INTEGER:
            add_column(
                result,
                custom_study_subject_field.name,
                get_study_subject_field_id(custom_study_subject_field),
                study_subject_columns,
                None,
                sortable=False,
                visible_param=visible,
            )
        elif custom_study_subject_field.type == CUSTOM_FIELD_TYPE_DOUBLE:
            add_column(
                result,
                custom_study_subject_field.name,
                get_study_subject_field_id(custom_study_subject_field),
                study_subject_columns,
                None,
                sortable=False,
                visible_param=visible,
            )
        elif custom_study_subject_field.type == CUSTOM_FIELD_TYPE_DATE:
            add_column(
                result,
                custom_study_subject_field.name,
                get_study_subject_field_id(custom_study_subject_field),
                study_subject_columns,
                None,
                visible_param=visible,
            )
        elif custom_study_subject_field.type == CUSTOM_FIELD_TYPE_SELECT_LIST:
            add_column(
                result,
                custom_study_subject_field.name,
                get_study_subject_field_id(custom_study_subject_field),
                study_subject_columns,
                "select_filter:" + custom_study_subject_field.possible_values,
                visible_param=visible,
            )
        elif custom_study_subject_field.type == CUSTOM_FIELD_TYPE_FILE:
            add_column(
                result,
                custom_study_subject_field.name,
                get_study_subject_field_id(custom_study_subject_field),
                study_subject_columns,
                "select_filter:N/A;Available",
                visible_param=visible,
            )
        else:
            raise NotImplementedError

    add_column(result, "Edit", "edit", study_subject_list, None, sortable=False)

    for one_based_idx, visit_number in enumerate(visit_numbers, 1):
        visit_key = f"visit_{one_based_idx}"  # always starts in 1
        add_column(
            result,
            f"Visit {visit_number}",
            visit_key,
            None,
            "visit_filter",
            visible_param=study_subject_list.visits,
        )

    return JsonResponse({"columns": result})


def get_subjects(request, list_type):
    if list_type == SUBJECT_LIST_GENERIC:
        return StudySubject.objects.all()
    elif list_type == SUBJECT_LIST_NO_VISIT:
        return get_subjects_with_no_visit(request.user)
    elif list_type == SUBJECT_LIST_REQUIRE_CONTACT:
        return get_subjects_with_reminder(request.user)
    elif list_type == SUBJECT_LIST_VOUCHER_EXPIRY:
        return get_subjects_with_almost_expired_vouchers(request.user)
    else:
        raise TypeError("Unknown query type: " + list_type)


def order_by_visit(subjects_to_be_ordered, order_f, visit_number):
    return subjects_to_be_ordered.annotate(
        sort_visit_date=Min(
            Case(When(visit__visit_number=visit_number, then="visit__datetime_begin"))
        )
    ).order_by(order_f("sort_visit_date"))


def get_subjects_order(
    subjects_to_be_ordered: QuerySet, order_column, order_direction, column_filters=None
):
    if column_filters is None:
        column_filters = {}
    result = subjects_to_be_ordered
    if order_direction == "asc":

        def order_f(x):
            return F(x).asc(nulls_first=True)

    else:

        def order_f(x):
            return F(x).desc(nulls_last=True)

    if order_column is None:
        logger.warning("Column cannot be null")
    elif order_column == "first_name":
        result = subjects_to_be_ordered.order_by(order_f("subject__first_name"))
    elif order_column == "last_name":
        result = subjects_to_be_ordered.order_by(order_f("subject__last_name"))
    elif order_column == "address":
        result = subjects_to_be_ordered.order_by(order_f("subject__address"))
    elif order_column == "next_of_kin_name":
        result = subjects_to_be_ordered.order_by(order_f("subject__next_of_kin_name"))
    elif order_column == "next_of_kin_phone":
        result = subjects_to_be_ordered.order_by(order_f("subject__next_of_kin_phone"))
    elif order_column == "next_of_kin_address":
        result = subjects_to_be_ordered.order_by(
            order_f("subject__next_of_kin_address")
        )
    elif order_column == "nd_number":
        result = subjects_to_be_ordered.order_by(order_f("nd_number"))
    elif order_column == "referral":
        result = subjects_to_be_ordered.order_by(order_f("referral"))
    elif order_column == "default_written_communication_language":
        result = subjects_to_be_ordered.order_by(
            order_f("subject__default_written_communication_language__name")
        )
    elif order_column == "phone_number":
        result = subjects_to_be_ordered.order_by(order_f("subject__phone_number"))
    elif order_column == "phone_number_2":
        result = subjects_to_be_ordered.order_by(order_f("subject__phone_number_2"))
    elif order_column == "phone_number_3":
        result = subjects_to_be_ordered.order_by(order_f("subject__phone_number_3"))
    elif order_column == "screening_number":
        if "screening_number" not in column_filters:
            pattern = None
        else:
            pattern = column_filters["screening_number"]
        result = subjects_to_be_ordered.all()
        result = sorted(
            result,
            key=lambda t: t.sort_matched_screening_first(
                pattern, reverse=order_direction != "asc"
            ),
            reverse=order_direction != "asc",
        )
    elif order_column == "default_location":
        result = subjects_to_be_ordered.order_by(order_f("default_location__name"))
    elif order_column == "flying_team":
        result = subjects_to_be_ordered.order_by(order_f("flying_team"))
    elif order_column == "dead":
        result = subjects_to_be_ordered.order_by(order_f("subject__dead"))
    elif order_column == "resigned":
        result = subjects_to_be_ordered.order_by(order_f("resigned"))
    elif order_column == "endpoint_reached":
        result = subjects_to_be_ordered.order_by(order_f("endpoint_reached"))
    elif order_column == "information_sent":
        result = subjects_to_be_ordered.order_by(order_f("information_sent"))
    elif order_column == "health_partner_first_name":
        result = subjects_to_be_ordered.order_by(order_f("health_partner__first_name"))
    elif order_column == "health_partner_last_name":
        result = subjects_to_be_ordered.order_by(order_f("health_partner__last_name"))
    elif order_column == "social_security_number":
        result = subjects_to_be_ordered.order_by(
            order_f("subject__social_security_number")
        )
    elif order_column == "postponed":
        result = subjects_to_be_ordered.order_by(order_f("postponed"))
    elif order_column == "excluded":
        result = subjects_to_be_ordered.order_by(order_f("excluded"))
    elif order_column == "type":
        result = subjects_to_be_ordered.order_by(order_f("type__name"))
    elif order_column == "id":
        result = subjects_to_be_ordered.order_by(order_f("id"))
    elif order_column == "date_born":
        result = subjects_to_be_ordered.order_by(order_f("subject__date_born"))
    elif order_column == "datetime_contact_reminder":
        result = subjects_to_be_ordered.order_by(order_f("datetime_contact_reminder"))
    elif order_column == "last_contact_attempt":
        # noinspection SpellCheckingInspection
        result = subjects_to_be_ordered.annotate(
            sort_contact_attempt=Max("contactattempt__datetime_when")
        ).order_by(order_f("sort_contact_attempt"))
    elif str(order_column).startswith("visit_"):
        visit_number = get_visit_number_from_visit_x_string(order_column)
        result = order_by_visit(subjects_to_be_ordered, order_f, visit_number)
    elif re.search(r"^custom_field-[0-9]+$", order_column):
        field_id = int(order_column.replace("custom_field-", ""))
        result = subjects_to_be_ordered.annotate(
            custom_field_value=Min(
                Case(
                    When(
                        customstudysubjectvalue__study_subject_field__id=field_id,
                        then="customstudysubjectvalue__value",
                    )
                )
            )
        ).order_by(order_f("custom_field_value"))
    else:
        logger.warning("Unknown sort column: %s", str(order_column))
    return result


def get_visit_number_from_visit_x_string(order_column):
    return int(str(order_column).split("_")[1])


def filter_by_visit(result, visit_number, visit_type):
    # we need to give custom names for filtering params that contain visit_number in it
    # because we might want to filter by few visits  and they shouldn't collide
    datetime_begin_filter = "visit_" + str(visit_number) + "_datetime_begin"
    datetime_end_filter = "visit_" + str(visit_number) + "_datetime_end"
    is_finished_filter = "visit_" + str(visit_number) + "_is_finished"
    finished_appointments_filter = (
        "visit_" + str(visit_number) + "_finished_appointments"
    )
    scheduled_appointments_filter = (
        "visit_" + str(visit_number) + "_scheduled_appointments"
    )

    # this is hack... instead of providing True/False value this field contain 1/0 value, the problem is that we need
    # to provide aggregate function for the interacting parameter
    # If we try to assign it with pure Case(When...) (without Count/Min/Max/Avg) we obtain duplicates
    # of the results which are later on messing up with the subject list
    result = result.annotate(
        **{
            is_finished_filter: Count(
                Case(
                    When(
                        Q(visit__is_finished=True)
                        & Q(visit__visit_number=visit_number),
                        then=1,
                    )
                )
            )
        }
    )

    # number of finished appointments
    result = result.annotate(
        **{
            finished_appointments_filter: Count(
                Case(
                    When(
                        Q(
                            visit__appointment__status=Appointment.APPOINTMENT_STATUS_FINISHED
                        )
                        & Q(visit__visit_number=visit_number),
                        then=1,
                    )
                )
            )
        }
    )
    # number of scheduled appointments
    result = result.annotate(
        **{
            scheduled_appointments_filter: Count(
                Case(
                    When(
                        Q(
                            visit__appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED
                        )
                        & Q(visit__visit_number=visit_number),
                        then=1,
                    )
                )
            )
        }
    )

    # when visit starts
    result = result.annotate(
        **{
            datetime_begin_filter: Min(
                Case(
                    When(visit__visit_number=visit_number, then="visit__datetime_begin")
                )
            )
        }
    )
    # when visit finish
    result = result.annotate(
        **{
            datetime_end_filter: Min(
                Case(When(visit__visit_number=visit_number, then="visit__datetime_end"))
            )
        }
    )

    if visit_type == "DONE":
        result = (
            result.filter(**{datetime_begin_filter + "__lt": get_today_midnight_date()})
            .filter(**{is_finished_filter + "__gt": 0})
            .filter(**{finished_appointments_filter + "__gt": 0})
        )
    elif visit_type == "MISSED":
        result = (
            result.filter(**{datetime_begin_filter + "__lt": get_today_midnight_date()})
            .filter(**{is_finished_filter + "__gt": 0})
            .filter(**{finished_appointments_filter: 0})
        )
    elif visit_type == "EXCEED":
        result = (
            result.filter(**{datetime_begin_filter + "__lt": get_today_midnight_date()})
            .filter(**{is_finished_filter: 0})
            .filter(**{scheduled_appointments_filter: 0})
            .filter(**{datetime_end_filter + "__lt": get_today_midnight_date()})
        )
    elif visit_type == "IN_PROGRESS":
        result = (
            result.filter(**{datetime_begin_filter + "__lt": get_today_midnight_date()})
            .filter(**{is_finished_filter: 0})
            .filter(**{scheduled_appointments_filter + "__gt": 0})
        )
    elif visit_type == "SHOULD_BE_IN_PROGRESS":
        result = (
            result.filter(**{datetime_begin_filter + "__lt": get_today_midnight_date()})
            .filter(**{is_finished_filter: 0})
            .filter(**{datetime_end_filter + "__gt": get_today_midnight_date()})
            .filter(**{scheduled_appointments_filter: 0})
        )
    elif visit_type == "UPCOMING":
        result = result.filter(
            **{datetime_begin_filter + "__gt": get_today_midnight_date()}
        )

    return result


def get_subjects_filtered(subjects_to_be_filtered: QuerySet, filters) -> QuerySet:
    result = subjects_to_be_filtered
    for row in filters:
        column = row[0]
        value = row[1]
        if column is None:
            logger.warning("Filter column cannot be null")
        elif column == "first_name":
            result = result.filter(subject__first_name__icontains=value)
        elif column == "last_name":
            result = result.filter(subject__last_name__icontains=value)
        elif column == "address":
            # pylint: disable-next=E1131
            result = result.filter(
                Q(subject__address__icontains=value)
                | Q(subject__city__icontains=value)
                | Q(subject__country__name__icontains=value)
            )
        elif column == "next_of_kin_name":
            result = result.filter(subject__next_of_kin_name__icontains=value)
        elif column == "next_of_kin_phone":
            result = result.filter(subject__next_of_kin_phone__icontains=value)
        elif column == "next_of_kin_address":
            result = result.filter(subject__next_of_kin_address__icontains=value)
        elif column == "nd_number":
            result = result.filter(nd_number__icontains=value)
        elif column == "referral":
            result = result.filter(referral__icontains=value)
        elif column == "default_written_communication_language":
            result = result.filter(
                subject__default_written_communication_language__id=value
            )
        elif column == "phone_number":
            result = result.filter(subject__phone_number__icontains=value)
        elif column == "phone_number_2":
            result = result.filter(subject__phone_number_2__icontains=value)
        elif column == "phone_number_3":
            result = result.filter(subject__phone_number_3__icontains=value)
        elif column == "screening_number":
            result = result.filter(screening_number__icontains=value)
        elif column == "dead":
            result = result.filter(subject__dead=(value == "true"))
        elif column == "resigned":
            result = result.filter(resigned=(value == "true"))
        elif column == "endpoint_reached":
            result = result.filter(endpoint_reached=(value == "true"))
        elif column == "postponed":
            result = result.filter(postponed=(value == "true"))
        elif column == "excluded":
            result = result.filter(excluded=(value == "true"))
        elif column == "information_sent":
            result = result.filter(information_sent=(value == "true"))
        elif column == "health_partner_first_name":
            result = result.filter(health_partner__first_name__icontains=value)
        elif column == "health_partner_last_name":
            result = result.filter(health_partner__last_name__icontains=value)
        elif column == "social_security_number":
            result = result.filter(subject__social_security_number__icontains=value)
        elif column == "default_location":
            result = result.filter(default_location=value)
        elif column == "flying_team":
            result = result.filter(flying_team=value)
        elif column == "type":
            result = result.filter(type_id=value)
        elif str(column).startswith("visit_"):
            visit_number = get_visit_number_from_visit_x_string(column)
            result = filter_by_visit(result, visit_number, value)
        elif re.search(r"^custom_field-[0-9]+$", column):
            field_id = int(column.replace("custom_field-", ""))
            field = CustomStudySubjectField.objects.get(pk=field_id)
            if field.type == CUSTOM_FIELD_TYPE_TEXT:
                result = result.filter(
                    customstudysubjectvalue__study_subject_field__id=field_id,
                    customstudysubjectvalue__value__icontains=value,
                )
            elif field.type == CUSTOM_FIELD_TYPE_BOOLEAN:
                if value.lower() == "true" or value.lower() == "false":
                    result = result.filter(
                        customstudysubjectvalue__study_subject_field__id=field_id,
                        customstudysubjectvalue__value__icontains=value,
                    )
                else:
                    result = result.filter(
                        customstudysubjectvalue__study_subject_field__id=field_id,
                        customstudysubjectvalue__value="",
                    )
            elif field.type in (CUSTOM_FIELD_TYPE_INTEGER, CUSTOM_FIELD_TYPE_DOUBLE):
                result = result.filter(
                    customstudysubjectvalue__study_subject_field__id=field_id,
                    customstudysubjectvalue__value=value,
                )
            elif field.type == CUSTOM_FIELD_TYPE_DATE:
                result = result.filter(
                    customstudysubjectvalue__study_subject_field__id=field_id,
                    customstudysubjectvalue__value=value,
                )
            elif field.type in (
                CUSTOM_FIELD_TYPE_INTEGER,
                CUSTOM_FIELD_TYPE_SELECT_LIST,
            ):
                result = result.filter(
                    customstudysubjectvalue__study_subject_field__id=field_id,
                    customstudysubjectvalue__value=value,
                )
            elif field.type == CUSTOM_FIELD_TYPE_FILE:
                result = result.filter(
                    customstudysubjectvalue__study_subject_field__id=field_id,
                    customstudysubjectvalue__value__isnull=(value == "N/A"),
                )
            else:
                raise NotImplementedError
        elif column == "":
            pass
        else:
            message = "UNKNOWN filter: "
            if column is None:
                message += "[None]"
            else:
                message += str(column)
            logger.warning(message)
    return result


def subjects(request: WSGIRequest, subject_list_type: str) -> HttpResponse:
    try:
        if request.method == "GET":
            request_data = request.GET
        elif request.method == "POST":
            request_data = request.POST
        else:
            return HttpResponseNotAllowed(["GET", "POST"])

        # id of the query from dataTable: https://datatables.net/manual/server-side
        draw = int(request_data.get("draw", "-1"))

        start = int(request_data.get("start", "0"))
        length = int(request_data.get("length", "10"))
        order = int(request_data.get("order[0][column]", "0"))
        order_dir = request_data.get("order[0][dir]", "asc")
        order_column = request_data.get(
            "columns[" + str(order) + "][data]", "last_name"
        )

        filters = get_filters_for_data_table_request(request_data)

        all_subjects = get_subjects(request, subject_list_type)

        count = all_subjects.count()

        filtered_subjects = get_subjects_filtered(all_subjects, filters)
        ordered_subjects = get_subjects_order(
            filtered_subjects, order_column, order_dir, column_filters=dict(filters)
        )
        if length == -1:
            sliced_subjects = ordered_subjects
        else:
            sliced_subjects = ordered_subjects[start: (start + length)]

        result_subjects = sliced_subjects

        count_filtered = filtered_subjects.count()

        data = []
        for subject in result_subjects:
            data.append(serialize_subject(subject))

        return JsonResponse(
            {
                "draw": draw,
                "recordsTotal": count,
                "recordsFiltered": count_filtered,
                "data": data,
            }
        )
    except Exception as e:
        logger.error(e, exc_info=True)
        return e500_error(request)


# noinspection PyUnusedLocal
def types(request):
    data = []
    for subject_type in SubjectType.objects.all():
        data.append({"id": subject_type.id, "name": subject_type.name})
    return JsonResponse({"types": data})


def serialize_subject(study_subject: StudySubject):
    location = location_to_str(study_subject.default_location)
    flying_team = flying_team_to_str(study_subject.flying_team)
    visits = (
        Visit.objects.filter(subject=study_subject)
        .order_by("visit_number")
        .prefetch_related("appointment_set", "appointment_types")
    )
    serialized_visits = []
    for visit in visits:
        if visit.datetime_begin < get_today_midnight_date():
            if visit.is_finished:
                finished_appointments_count = visit.appointment_set.filter(
                    status=Appointment.APPOINTMENT_STATUS_FINISHED
                ).count()
                if finished_appointments_count > 0:
                    status = "DONE"
                else:
                    status = "MISSED"
            elif visit.datetime_end < get_today_midnight_date():
                scheduled_appointments_count = visit.appointment_set.filter(
                    status=Appointment.APPOINTMENT_STATUS_SCHEDULED
                ).count()
                if scheduled_appointments_count > 0:
                    status = "IN_PROGRESS"
                else:
                    status = "EXCEEDED"
            else:
                scheduled_appointments_count = visit.appointment_set.filter(
                    status=Appointment.APPOINTMENT_STATUS_SCHEDULED
                ).count()
                if scheduled_appointments_count > 0:
                    status = "IN_PROGRESS"
                else:
                    status = "SHOULD_BE_IN_PROGRESS"
        else:
            status = "UPCOMING"

        appointment_types = [
            f"{at.code} ({at.description})" for at in visit.appointment_types.all()
        ]
        if len(appointment_types) == 0:
            appointment_types = ["No appointment types set."]

        serialized_visits.append(
            {
                "status": status,
                "appointment_types": appointment_types,
                "edit_visit_url": reverse("web.views.visit_details", args=(visit.id,)),
                "add_appointment_url": reverse(
                    "web.views.appointment_add", args=(visit.id,)
                ),
                "datetime_start": serialize_date(visit.datetime_begin),
                "datetime_end": serialize_date(visit.datetime_end),
                "is_finished": visit.is_finished,
            }
        )
    contact_reminder = serialize_datetime(study_subject.datetime_contact_reminder)
    contact_attempts = ContactAttempt.objects.filter(subject=study_subject).order_by(
        "-datetime_when"
    )
    if len(contact_attempts) > 0:
        last_contact_attempt = contact_attempts[0]
        last_contact_attempt_string = (
            serialize_datetime(last_contact_attempt.datetime_when)
            + "<br/>"
            + str(last_contact_attempt.worker)
            + "<br/> Success: "
            + bool_to_yes_no(last_contact_attempt.success)
            + "<br/>"
            + last_contact_attempt.comment
        )

    else:
        last_contact_attempt_string = ""
    health_partner_first_name = ""
    if study_subject.health_partner:
        health_partner_first_name = study_subject.health_partner.first_name
    health_partner_last_name = ""
    if study_subject.health_partner:
        health_partner_last_name = study_subject.health_partner.last_name
    default_written_communication_language = ""
    if study_subject.subject.default_written_communication_language:
        default_written_communication_language = (
            study_subject.subject.default_written_communication_language.name
        )

    result = {
        "first_name": study_subject.subject.first_name,
        "last_name": study_subject.subject.last_name,
        "address": study_subject.subject.pretty_address(),
        "next_of_kin_name": study_subject.subject.next_of_kin_name,
        "next_of_kin_phone": study_subject.subject.next_of_kin_phone,
        "next_of_kin_address": study_subject.subject.next_of_kin_address,
        "date_born": study_subject.subject.date_born,
        "datetime_contact_reminder": contact_reminder,
        "last_contact_attempt": last_contact_attempt_string,
        "nd_number": study_subject.nd_number,
        "screening_number": study_subject.screening_number,
        "referral": study_subject.referral,
        "phone_number": study_subject.subject.phone_number,
        "phone_number_2": study_subject.subject.phone_number_2,
        "phone_number_3": study_subject.subject.phone_number_3,
        "default_location": location,
        "flying_team": flying_team,
        "dead": bool_to_yes_no(study_subject.subject.dead),
        "resigned": bool_to_yes_no(study_subject.resigned),
        "endpoint_reached": bool_to_yes_no(study_subject.endpoint_reached),
        "postponed": bool_to_yes_no(study_subject.postponed),
        "excluded": bool_to_yes_no(study_subject.excluded),
        "information_sent": bool_to_yes_no(study_subject.information_sent),
        "health_partner_first_name": health_partner_first_name,
        "health_partner_last_name": health_partner_last_name,
        "social_security_number": study_subject.subject.social_security_number,
        "type": study_subject.type.name,
        "id": study_subject.id,
        "visits": serialized_visits,
        "default_written_communication_language": default_written_communication_language,
    }

    for field_value in study_subject.custom_data_values:
        if field_value.study_subject_field.type in (
            CUSTOM_FIELD_TYPE_TEXT,
            CUSTOM_FIELD_TYPE_INTEGER,
            CUSTOM_FIELD_TYPE_DOUBLE,
        ):
            val = field_value.value
            if val is None:
                val = ""
            result[get_study_subject_field_id(field_value.study_subject_field)] = val
        elif field_value.study_subject_field.type == CUSTOM_FIELD_TYPE_BOOLEAN:
            result[get_study_subject_field_id(field_value.study_subject_field)] = (
                str_to_yes_no_null(field_value.value)
            )
        elif field_value.study_subject_field.type == CUSTOM_FIELD_TYPE_DATE:
            result[get_study_subject_field_id(field_value.study_subject_field)] = (
                field_value.value
            )
        elif field_value.study_subject_field.type == CUSTOM_FIELD_TYPE_SELECT_LIST:
            result[get_study_subject_field_id(field_value.study_subject_field)] = (
                field_value.value
            )
        elif field_value.study_subject_field.type == CUSTOM_FIELD_TYPE_FILE:
            if field_value.value is None:
                result[get_study_subject_field_id(field_value.study_subject_field)] = ""
            else:
                result[get_study_subject_field_id(field_value.study_subject_field)] = (
                    reverse("web.views.uploaded_files")
                    + "?file="
                    + str(field_value.value)
                )
        else:
            raise NotImplementedError

    return result
