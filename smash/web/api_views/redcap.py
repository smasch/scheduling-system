from django.http import JsonResponse

from web.models import MissingSubject


def ignore_missing_subject(request, missing_subject_id):
    missing_subjects = MissingSubject.objects.filter(id=missing_subject_id)
    for missing_subject in missing_subjects:
        missing_subject.ignore = True
        missing_subject.save()
    return JsonResponse({
        "status": "ok"
    })


def unignore_missing_subject(request, missing_subject_id):
    missing_subjects = MissingSubject.objects.filter(id=missing_subject_id)
    for missing_subject in missing_subjects:
        missing_subject.ignore = False
        missing_subject.save()
    return JsonResponse({
        "status": "ok"
    })
