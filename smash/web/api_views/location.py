from django.http import JsonResponse

from web.models import Location


def locations(request):
    data = []
    for location in Location.objects.all():
        data.append({"id": location.id, "name": location.name})
    return JsonResponse({
        "locations": data
    })
