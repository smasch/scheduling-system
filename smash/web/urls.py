"""smash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include
from django.urls import re_path
from django.contrib.auth.views import LogoutView
from django.views.defaults import page_not_found

from web import views
from web.views import subject_types
from web.views.daily_planning import TemplateDailyPlannerView

urlpatterns = [
    # make sure that users cannot disable two factors authentication
    re_path(r"^account/two_factor/disable/$", page_not_found, {"exception": Exception("Not Found")}),
    re_path(r"^change_password/$", views.password.change_password, name="change_password"),
    re_path(
        r"^appointment_types/$",
        views.appointment_type.AppointmentTypeListView.as_view(),
        name="web.views.appointment_types",
    ),
    re_path(
        r"^appointment_types/add$",
        views.appointment_type.AppointmentTypeCreateView.as_view(),
        name="web.views.appointment_type_add",
    ),
    re_path(
        r"^appointment_types/(?P<pk>\d+)/edit$",
        views.appointment_type.AppointmentTypeEditView.as_view(),
        name="web.views.appointment_type_edit",
    ),
    re_path(
        r"^appointment_types/(?P<pk>\d+)/delete$",
        views.appointment_type.AppointmentTypeDeleteView.as_view(),
        name="web.views.appointment_type_delete",
    ),
    ####################
    #   APPOINTMENTS   #
    ####################
    re_path(r"^appointments$", views.appointment.appointments, name="web.views.appointments"),
    re_path(
        r"^appointments/unfinished$",
        views.appointment.unfinished_appointments,
        name="web.views.unfinished_appointments",
    ),
    re_path(
        r"^appointments/add/(?P<visit_id>\d+)$", views.appointment.appointment_add, name="web.views.appointment_add"
    ),
    re_path(r"^appointments/add/general$", views.appointment.appointment_add, name="web.views.appointment_add_general"),
    re_path(
        r"^appointments/edit/(?P<appointment_id>\d+)$",
        views.appointment.appointment_edit,
        name="web.views.appointment_edit",
    ),
    re_path(
        r"^appointments/delete/(?P<pk>\d+)$",
        views.appointment.AppointmentDeleteView.as_view(),
        name="web.views.appointment_delete",
    ),
    ####################
    #      VISITS      #
    ####################
    re_path(r"^visits$", views.visit.visits, name="web.views.visits"),
    re_path(r"^visits/exceeded$", views.visit.exceeded_visits, name="web.views.exceeded_visits"),
    re_path(r"^visits/unfinished$", views.visit.unfinished_visits, name="web.views.unfinished_visits"),
    re_path(
        r"^visits/approaching$",
        views.visit.approaching_visits_without_appointments,
        name="web.views.approaching_visits_without_appointments",
    ),
    re_path(
        r"^visits/approaching_post_mail$",
        views.visit.approaching_visits_for_mail_contact,
        name="web.views.approaching_visits_for_mail_contact",
    ),
    re_path(
        r"^visits/missing_appointments$",
        views.visit.visits_with_missing_appointments,
        name="web.views.visits_with_missing_appointments",
    ),
    re_path(r"^visits/details/(?P<visit_id>\d+)$", views.visit.visit_details, name="web.views.visit_details"),
    re_path(r"^visits/unfinish/(?P<visit_id>\d+)$", views.visit.visit_unfinish, name="web.views.visit_unfinish"),
    re_path(r"^visits/add$", views.visit.visit_add, name="web.views.visit_add"),
    re_path(r"^visits/add/(?P<subject_id>\d+)$", views.visit.visit_add, name="web.views.visit_add"),
    re_path(r"^visit/mark/(?P<visit_id>\d+)/(?P<as_what>[A-z]+)$", views.visit.visit_mark, name="web.views.visit_mark"),
    ####################
    #    SUBJECTS      #
    ####################
    re_path(r"^subjects$", views.subject.subjects, name="web.views.subjects"),
    re_path(r"^subjects/no_visit$", views.subject.subject_no_visits, name="web.views.subject_no_visits"),
    re_path(
        r"^subjects/require_contact$", views.subject.subject_require_contact, name="web.views.subject_require_contact"
    ),
    re_path(r"^subjects/voucher_expiry", views.subject.subject_voucher_expiry, name="web.views.subject_voucher_expiry"),
    re_path(r"^study/(?P<study_id>\d+)/subjects/add$", views.subject.subject_add, name="web.views.subject_add"),
    re_path(
        r"^subjects/subject_visit_details/(?P<subject_id>\d+)$",
        views.subject.subject_visit_details,
        name="web.views.subject_visit_details",
    ),
    re_path(r"^subjects/edit/(?P<subject_id>\d+)$", views.subject.subject_edit, name="web.views.subject_edit"),
    re_path(
        r"^subjects/(?P<pk>\d+)/delete$", views.subject.SubjectDeleteView.as_view(), name="web.views.subject_delete"
    ),
    re_path(
        r"^studysubjects/(?P<pk>\d+)/delete$",
        views.subject.StudySubjectDeleteView.as_view(),
        name="web.views.study_subject_delete",
    ),
    #########################
    # RED CAP NOTIFICATIONS #
    #########################
    re_path(r"^redcap/missing_subjects$", views.redcap.missing_subjects, name="web.views.missing_redcap_subject"),
    re_path(
        r"^redcap/inconsistent_subjects$",
        views.redcap.inconsistent_subjects,
        name="web.views.inconsistent_redcap_subject",
    ),
    ####################
    #    CONTACTS      #
    ####################
    re_path(
        r"^subjects/(?P<subject_id>\d+)/contacts/add$", views.contact_attempt.contact_add, name="web.views.contact_add"
    ),
    re_path(
        r"^subjects/(?P<subject_id>\d+)/contacts/(?P<contact_attempt_id>\d+)/edit",
        views.contact_attempt.contact_edit,
        name="web.views.contact_edit",
    ),
    ####################
    #     DOCTORS      #
    ####################
    re_path(r"^doctors/$", views.worker.worker_list, name="web.views.workers"),
    re_path(r"^doctors/(?P<worker_type>[A-z]+)$", views.worker.worker_list, name="web.views.workers"),
    re_path(r"^doctors/add/(?P<worker_type>[A-z]+)$", views.worker.worker_add, name="web.views.worker_add"),
    re_path(r"^doctors/edit/(?P<worker_id>\d+)$", views.worker.worker_edit, name="web.views.worker_edit"),
    re_path(r"^doctors/disable/(?P<doctor_id>\d+)$", views.worker.worker_disable, name="web.views.worker_disable"),
    re_path(r"^doctors/enable/(?P<doctor_id>\d+)$", views.worker.worker_enable, name="web.views.worker_enable"),
    re_path(
        r"^doctors/(?P<doctor_id>\d+)/availability/add$",
        views.worker.worker_availability_add,
        name="web.views.worker_availability_add",
    ),
    re_path(
        r"^doctors/availability/(?P<availability_id>\d+)/delete$",
        views.worker.worker_availability_delete,
        name="web.views.worker_availability_delete",
    ),
    re_path(
        r"^doctors/availability/(?P<availability_id>\d+)/edit",
        views.worker.worker_availability_edit,
        name="web.views.worker_availability_edit",
    ),
    re_path(
        r"^doctors/(?P<doctor_id>\d+)/holiday/add$",
        views.worker.worker_holiday_add,
        name="web.views.worker_holiday_add",
    ),
    re_path(
        r"^doctors/holiday/(?P<holiday_id>\d+)/delete$",
        views.worker.worker_holiday_delete,
        name="web.views.worker_holiday_delete",
    ),
    ####################
    #    EQUIPMENT     #
    ####################
    re_path(
        r"^equipment_and_rooms$", views.equipment_and_rooms.equipment_and_rooms, name="web.views.equipment_and_rooms"
    ),
    re_path(r"^equipment_and_rooms/equipment$", views.equipment.equipment, name="web.views.equipment"),
    re_path(r"^equipment_and_rooms/equipment/add$", views.equipment.equipment_add, name="web.views.equipment_add"),
    re_path(
        r"^equipment_and_rooms/equipment/edit/(?P<equipment_id>\d+)$",
        views.equipment.equipment_edit,
        name="web.views.equipment_edit",
    ),
    re_path(
        r"^equipment_and_rooms/equipment/delete/(?P<equipment_id>\d+)$",
        views.equipment.equipment_delete,
        name="web.views.equipment_delete",
    ),
    re_path(r"^equipment_and_rooms/kit_requests$", views.kit.kit_requests, name="web.views.kit_requests"),
    re_path(
        r"^equipment_and_rooms/kit_requests/(?P<start_date>[\w-]+)/$",
        views.kit.kit_requests_send_mail,
        name="web.views.kit_requests_send_mail",
    ),
    re_path(
        r"^equipment_and_rooms/kit_requests/(?P<start_date>[\w-]+)/(?P<end_date>[\w-]+)/$",
        views.kit.kit_requests_send_mail,
        name="web.views.kit_requests_send_mail",
    ),
    re_path(
        r"^equipment_and_rooms/flying_teams$",
        views.flying_teams.flying_teams,
        name="web.views.equipment_and_rooms.flying_teams",
    ),
    re_path(
        r"^equipment_and_rooms/flying_teams/add$",
        views.flying_teams.flying_teams_add,
        name="web.views.equipment_and_rooms.flying_teams_add",
    ),
    re_path(
        r"^equipment_and_rooms/flying_teams/edit/(?P<flying_team_id>\d+)$",
        views.flying_teams.flying_teams_edit,
        name="web.views.equipment_and_rooms.flying_teams_edit",
    ),
    re_path(r"^equipment_and_rooms/rooms$", views.rooms.rooms, name="web.views.equipment_and_rooms.rooms"),
    re_path(
        r"^equipment_and_rooms/rooms/planning$",
        views.rooms.rooms_planning,
        name="web.views.equipment_and_rooms.rooms_planning",
    ),
    re_path(r"^equipment_and_rooms/rooms/add$", views.rooms.rooms_add, name="web.views.equipment_and_rooms.rooms_add"),
    re_path(
        r"^equipment_and_rooms/rooms/edit/(?P<room_id>\d+)$",
        views.rooms.rooms_edit,
        name="web.views.equipment_and_rooms.rooms_edit",
    ),
    re_path(
        r"^equipment_and_rooms/rooms/delete/(?P<room_id>\d+)$",
        views.rooms.rooms_delete,
        name="web.views.equipment_and_rooms.rooms_delete",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/subject_types/(?P<subject_type_id>\d+)$",
        subject_types.subject_type_edit,
        name="web.views.subject_type_edit",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/subject_types/add$",
        subject_types.subject_type_add,
        name="web.views.subject_type_add",
    ),
    re_path(r"^study/(?P<study_id>\d+)/subject_types$", subject_types.subject_types, name="web.views.subject_types"),
    re_path(
        r"^study/(?P<study_id>\d+)/subject_types/(?P<subject_type_id>\d+)/delete$",
        subject_types.subject_type_delete,
        name="web.views.subject_type_delete",
    ),
    ####################
    #  PRIVACY NOTICE  #
    ####################
    re_path(
        r"^privacy_notices$", views.privacy_notice.PrivacyNoticeListView.as_view(), name="web.views.privacy_notices"
    ),
    re_path(
        r"^accept_privacy_notice/(?P<pk>\d+)$",
        views.privacy_notice.privacy_notice_accept,
        name="web.views.accept_privacy_notice",
    ),
    re_path(r"^privacy_notices/add$", views.privacy_notice.privacy_notice_add, name="web.views.privacy_notice_add"),
    re_path(
        r"^privacy_notices/(?P<pk>\d+)/edit$",
        views.privacy_notice.privacy_notice_edit,
        name="web.views.privacy_notice_edit",
    ),
    re_path(
        r"^privacy_notices/(?P<pk>\d+)/delete$",
        views.privacy_notice.PrivacyNoticeDeleteView.as_view(),
        name="web.views.privacy_notice_delete",
    ),
    ####################
    #       MAIL       #
    ####################
    re_path(r"^mail_templates$", views.mails.MailTemplatesListView.as_view(), name="web.views.mail_templates"),
    re_path(r"^mail_templates/add$", views.mails.mail_template_add, name="web.views.mail_template_add"),
    re_path(r"^mail_templates/(?P<pk>\d+)/edit$", views.mails.mail_template_edit, name="web.views.mail_template_edit"),
    re_path(
        r"^mail_templates/(?P<pk>\d+)/delete$",
        views.mails.MailTemplatesDeleteView.as_view(),
        name="web.views.mail_template_delete",
    ),
    re_path(
        r"^mail_templates/(?P<mail_template_id>\d+)/generate/(?P<instance_id>\d+)$",
        views.mails.generate,
        name="web.views.mail_template_generate",
    ),
    re_path(
        r"^mail_templates/print_vouchers$",
        views.mails.generate_for_vouchers,
        name="web.views.mail_template_generate_for_vouchers",
    ),
    ####################
    # DAILY PLANNING   #
    ####################
    re_path(
        r"^daily_planning$",
        TemplateDailyPlannerView.as_view(template_name="daily_planning/index.html"),
        name="web.views.daily_planning",
    ),
    ####################
    #     LANGUAGES    #
    ####################
    re_path(r"^languages$", views.language.LanguageListView.as_view(), name="web.views.languages"),
    re_path(r"^languages/add$", views.language.LanguageCreateView.as_view(), name="web.views.language_add"),
    re_path(
        r"^languages/(?P<pk>\d+)/delete$", views.language.LanguageDeleteView.as_view(), name="web.views.language_delete"
    ),
    re_path(r"^languages/(?P<pk>\d+)/edit$", views.language.LanguageEditView.as_view(), name="web.views.language_edit"),
    ####################
    #     LOCATIONS    #
    ####################
    re_path(r"^locations$", views.location.LocationListView.as_view(), name="web.views.locations"),
    re_path(r"^locations/add$", views.location.LocationCreateView.as_view(), name="web.views.location_add"),
    re_path(
        r"^locations/(?P<pk>\d+)/delete$", views.location.LocationDeleteView.as_view(), name="web.views.location_delete"
    ),
    re_path(r"^locations/(?P<pk>\d+)/edit$", views.location.LocationEditView.as_view(), name="web.views.location_edit"),
    ####################
    #   VOUCHER TYPES  #
    ####################
    re_path(r"^voucher_types$", views.voucher_type.VoucherTypeListView.as_view(), name="web.views.voucher_types"),
    re_path(
        r"^voucher_types/add$", views.voucher_type.VoucherTypeCreateView.as_view(), name="web.views.voucher_type_add"
    ),
    re_path(
        r"^voucher_types/(?P<pk>\d+)/edit$",
        views.voucher_type.VoucherTypeEditView.as_view(),
        name="web.views.voucher_type_edit",
    ),
    #######################
    # VOUCHER TYPE PRICES #
    #######################
    re_path(
        r"^voucher_types/(?P<voucher_type_id>\d+)/prices/add$",
        views.voucher_type_price.VoucherTypePriceCreateView.as_view(),
        name="web.views.voucher_type_price_add",
    ),
    re_path(
        r"^voucher_types/(?P<voucher_type_id>\d+)/prices/(?P<pk>\d+)/edit$",
        views.voucher_type_price.VoucherTypePriceEditView.as_view(),
        name="web.views.voucher_type_price_edit",
    ),
    ####################
    #     VOUCHERS     #
    ####################
    re_path(r"^vouchers$", views.voucher.VoucherListView.as_view(), name="web.views.vouchers"),
    re_path(r"^vouchers/add$", views.voucher.VoucherCreateView.as_view(), name="web.views.voucher_add"),
    re_path(r"^vouchers/(?P<pk>\d+)/edit$", views.voucher.VoucherEditView.as_view(), name="web.views.voucher_edit"),
    ####################################
    #     VOUCHER PARTNER SESSIONS     #
    ####################################
    re_path(
        r"^vouchers/(?P<pk>\d+)/voucher_partner_session/add$",
        views.voucher_partner_session.VoucherPartnerSessionCreateView.as_view(),
        name="web.views.voucher_partner_sessions_add",
    ),
    ####################
    #    STATISTICS    #
    ####################
    re_path(r"^statistics$", views.statistics.statistics, name="web.views.statistics"),
    re_path(r"^provenance$", views.provenance.ProvenanceListView.as_view(), name="web.views.provenance"),
    ####################
    #    STUDY         #
    ####################
    re_path(r"^study/(?P<study_id>\d+)/edit", views.study.study_edit, name="web.views.edit_study"),
    re_path(
        r"^study/(?P<study_id>\d+)/custom_study_subject_field/add",
        views.study.custom_study_subject_field_add,
        name="web.views.custom_study_subject_field_add",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/custom_study_subject_field/(?P<field_id>\d+)/edit",
        views.study.custom_study_subject_field_edit,
        name="web.views.custom_study_subject_field_edit",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/custom_study_subject_field/(?P<field_id>\d+)/delete",
        views.study.custom_study_subject_field_delete,
        name="web.views.custom_study_subject_field_delete",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/import_visit_edit/(?P<import_id>\d+)/edit",
        views.etl.import_visit_edit,
        name="web.views.import_visit_edit",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/import_visit_edit/(?P<import_id>\d+)/execute",
        views.etl.import_visit_execute,
        name="web.views.import_visit_execute",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/import_subject_edit/(?P<import_id>\d+)/edit",
        views.etl.import_subject_edit,
        name="web.views.import_subject_edit",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/import_subject_edit/(?P<import_id>\d+)/execute",
        views.etl.import_subject_execute,
        name="web.views.import_subject_execute",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/export_subject_edit/(?P<export_id>\d+)/edit",
        views.etl.export_subject_edit,
        name="web.views.export_subject_edit",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/export_subject_edit/(?P<export_id>\d+)/execute",
        views.etl.export_subject_execute,
        name="web.views.export_subject_execute",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/export_visit_edit/(?P<export_id>\d+)/edit",
        views.etl.export_visit_edit,
        name="web.views.export_visit_edit",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/export_visit_edit/(?P<export_id>\d+)/execute",
        views.etl.export_visit_execute,
        name="web.views.export_visit_execute",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)/subject_list/(?P<study_subject_list_id>\d+)/edit",
        views.study_subject_list.study_subject_list_edit,
        name="web.views.study_subject_list_edit",
    ),
    ####################
    #       EXPORT     #
    ####################
    re_path(r"^study/(?P<study_id>\d+)export$", views.export.export, name="web.views.export"),
    re_path(
        r"^study/(?P<study_id>\d+)export/csv/(?P<data_type>[A-z]+)$",
        views.export.export_to_csv,
        name="web.views.export_to_csv",
    ),
    re_path(
        r"^study/(?P<study_id>\d+)export/xls/(?P<data_type>[A-z]+)$",
        views.export.export_to_excel,
        name="web.views.export_to_excel",
    ),
    ####################
    #       CONFIGURATION     #
    ####################
    re_path(r"^configuration$", views.configuration_item.configuration_items, name="web.views.configuration"),
    ####################
    #       FILES      #
    ####################
    re_path(r"^files/", views.uploaded_files.download, name="web.views.uploaded_files"),
    ####################
    #       AUTH       #
    ####################
    # re_path(r'^login$', views.auth.login, name='web.views.login'),
    # re_path(r'^logout$', views.auth.logout, name='web.views.logout'),
    re_path(r"^logout$", LogoutView.as_view(), name="logout"),
    re_path(r"^$", views.index, name="web.views.index"),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        re_path(r"^__debug__/", include(debug_toolbar.urls)),
    ]
