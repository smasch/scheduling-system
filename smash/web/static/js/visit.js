function visit_dates_behaviour(startDateInput, endDateInput, default_visit_duration) {
    $(startDateInput).change(function () {
        var object = $(this);
        try {
            var date = new Date(object.val());
            date.setMonth(date.getMonth() + default_visit_duration);
            if($(endDateInput).val() == ""){
                $(endDateInput).val(date.toISOString().substring(0, 10));
            }
        } catch (err) {
            //there was a problematic date to process
        }
    });
}


function createVisitsTable(params) {
    return createTable(params);
}
