function appointment_type_behaviour(checkboxes, outObject, api_call) {
    var appointment_types_data = null;

    var global_sequential_time = 0;
    var global_time = 0;
    var global_parallel_time = 0;

    function recompute_parallel_time(object) {
        var result = 0;
        var object_id = -1;
        if (object) {
            object_id = object.attr('id');
        }
        $.each(checkboxes, function (index, value) {
            var checkbox = $(value);
            var val = parseInt(checkbox.attr('value'));
            if ((checkbox.is(":checked") && checkbox.attr('id') != object_id) || (!checkbox.is(":checked") && checkbox.attr('id') == object_id)) {
                var appointment_type = appointment_types_data[val];
                if (appointment_type.can_be_parallelized) {
                    result = Math.max(result, appointment_type.default_duration);
                }
            }
        });
        return result;
    }

    function recompute_sequential_time(object) {
        var result = 0;
        var object_id = -1;
        if (object) {
            object_id = object.attr('id');
        }
        $.each(checkboxes, function (index, value) {
            var checkbox = $(value);
            var val = parseInt(checkbox.attr('value'));
            if ((checkbox.is(":checked") && checkbox.attr('id') != object_id) || (!checkbox.is(":checked") && checkbox.attr('id') == object_id)) {
                var appointment_type = appointment_types_data[val];
                if (!appointment_type.can_be_parallelized) {
                    result += appointment_type.default_duration;
                }
            }
        });
        return result;
    }

    function compute_time(object) {
        var val = parseInt(object.attr('value'));
        var time = 0;
        var appointment_type = appointment_types_data[val];
        if (appointment_type == null) {
            console.log("Cannot find appointment type with id: " + val);
        } else {
            time = appointment_type.default_duration;
            if (appointment_type.can_be_parallelized) {
                if (object.is(":checked")) {
                    global_parallel_time = Math.max(global_parallel_time, time);
                } else {
                    global_parallel_time = recompute_parallel_time();
                }
            } else {
                if (object.is(":checked")) {
                    global_sequential_time += time;
                } else {
                    global_sequential_time -= time;
                }
            }
            global_time = Math.max(global_parallel_time, global_sequential_time);
            $(outObject).val(global_time + "");
            $(outObject).trigger("change");
        }

    }


    $(checkboxes).change(function () {
        var object = $(this);
        if (appointment_types_data === null) {
            $.get(api_call, function (data) {
                appointment_types_data = {};
                $.each(data.appointment_types, function (index, appointment_type) {
                    appointment_types_data[appointment_type.id] = appointment_type;
                });
                global_parallel_time = recompute_parallel_time(object);
                global_sequential_time = recompute_sequential_time(object);
                compute_time(object)
            });
        } else {
            compute_time(object);
        }

    });
}

/**
 * When any option is selected in flying_team_select, location value is set to flying team.
 *
 * @param flying_team_select
 * @param location_select
 */
function appointment_flying_team_place_behaviour(flying_team_select, location_select) {
    $(flying_team_select).change(function () {
        if ($(this).val() !== "") {
            $(location_select).find('option').each(function () {
                if ("Flying Team" === $(this).html()) {
                    $(location_select).val($(this).val());
                }
            });
        }
    });
}


function appointment_date_change_behaviour(datetime_picker, worker_select, minutes_input, appointment_id) {
    function match_availability(id, availabilities, partialMatch, date_start, date_end) {
        if (id === undefined) {
            return false;
        }
        id = parseInt(id);
        for (var i = 0; i < availabilities.length; i++) {
            var availability = availabilities[i];
            if (availability.link_who === id && (appointment_id !== availability.appointment_id || appointment_id === undefined)) {
                var event_start = Date.parse(availability.link_when);
                var event_end = Date.parse(availability.link_end);
                if (partialMatch && event_start >= date_start && event_start <= date_end) {
                    return true;
                }
                if (partialMatch && event_end >= date_start && event_end <= date_end) {
                    return true;
                }
                if (partialMatch && date_start >= event_start && date_start <= event_end) {
                    return true;
                }
                if (event_start <= date_start && date_end <= event_end) {
                    return true;
                }
            }
        }
        return false;
    }

    var getSelectedClass = function () {
        return $(worker_select).find(":selected")[0].className;
    };

    var previousClass = getSelectedClass();

    $(worker_select).addClass(previousClass);

    function workerChanged() {
        // Do something with the previous value after the change

        $(worker_select).removeClass(previousClass);
        // Make sure the previous value is updated
        previousClass = getSelectedClass();
        $(worker_select).addClass(previousClass);
    }

    $(worker_select).on('focus', function () {
        // Store the current value on focus and on change
        previousClass = getSelectedClass();
    }).change(workerChanged);

    function timeChange() {
        if ($(datetime_picker).val() !== "") {
            var datetime_start = moment($(datetime_picker).val()).toDate();
            var datetime_end = datetime_start + 60 * 1000 * parseInt(minutes_input.val());
            if (isNaN(datetime_end)) {
                datetime_end = datetime_start;
            }
            var date = $(datetime_picker).val().substr(0, 10);
            $.get('/api/availabilities/' + date, function (data) {
                var options = $("option", worker_select);
                for (var i = 0; i < options.length; i++) {
                    var option = options[i];
                    if (match_availability(option.value, data.holidays, true, datetime_start, datetime_end)) {
                        // console.log(data.holidays);
                        // console.log("HOLIDAY");
                        option.className = "worker-option-holiday";
                    } else if (match_availability(option.value, data.conflicts, true, datetime_start, datetime_end)) {
                        // console.log(data.conflicts);
                        // console.log("CONFLICT");
                        option.className = "worker-option-conflict";
                    } else if (match_availability(option.value, data.availabilities, false, datetime_start, datetime_end)) {
                        // console.log(data.availabilities);
                        // console.log("MATCH");
                        option.className = "worker-option-match";
                    } else if (match_availability(option.value, data.availabilities, true, datetime_start, datetime_end)) {
                        // console.log(data.availabilities);
                        // console.log("PARTIAL_MATCH");
                        option.className = "worker-option-partial-match";
                    } else {
                        option.className = "worker-option-no-match";
                    }
                }
                workerChanged();
            });
        }
    }

    $(datetime_picker).on("dp.change", timeChange);
    $(datetime_picker).on("change", timeChange);
    $(datetime_picker).on("keyup", timeChange);
    $(minutes_input).on("change", timeChange);
    $(minutes_input).on("keyup", timeChange);

}

function get_calendar_events_function(source, allow_url_redirection, day_headers, workerAvailabilitiesUrl) {
    if (allow_url_redirection === undefined) {
        allow_url_redirection = false;
    }
    return function (start, end, timezone, callback) {
        if (day_headers !== undefined) {
            $.ajax({
                data: {
                    start_date: start.format(),
                    end_date: end.format()
                },
                url: workerAvailabilitiesUrl,
                success: function (doc) {
                    for (var i = 0; i < doc.availabilities.length; i++) {
                        const entry = doc.availabilities[i];
                        var initials = '';
                        for (var j = 0; j < entry.workers.length; j++) {
                            initials += entry.workers[j].initials + ", ";
                        }
                        var element = day_headers[entry.date];
                        if (element !== undefined) {
                            element.innerHTML = initials;
                        }
                    }
                }
            });

        }
        $.ajax({
            data: {
                // our hypothetical feed requires UNIX timestamps
                start_date: start.format(),
                end_date: end.format()
            },
            url: source,
            success: function (doc) {
                var events = [];
                for (var i = 0; i < doc.data.length; i++) {

                    const entry = doc.data[i];
                    var title = entry.subject;
                    if (title !== "") {
                        title += " (" + entry.nd_number + "); type: " + entry.appointment_types;
                    } else {
                        title = entry.title
                    }
                    var event = {
                        title: title,
                        status: entry.status,
                        start: entry.datetime_when,
                        end: entry.datetime_until,
                        id: entry.id,
                        color: entry.color,
                        nd_number: entry.nd_number,
                        location: entry.location,
                        flying_team: entry.flying_team,
                        screening_number: entry.screening_number,
                        phone_number: entry.phone_number,
                        worker_assigned: entry.worker_assigned,
                        appointment_types: entry.appointment_types,
                        appointment_type_names: entry.appointment_type_names
                    };
                    if (allow_url_redirection) {
                        event["url"] = entry.url;
                    }
                    events.push(event)
                }
                callback(events);
            }
        });
    }
}

function createAppointmentsTable(params) {
    return createTable(params);
}