function ignore_missing_subject(id, url) {
    $.ajax({
        url: url,
        success: function () {
            $("#ignore_" + id).hide();
            $("#unignore_" + id).show();
            $("#row_" + id).find('td').addClass("ignore-row");
        }
    });
}

function unignore_missing_subject(id, url) {

    $.ajax({
        url: url,
        success: function () {
            $("#unignore_" + id).hide();
            $("#ignore_" + id).show();
            $("#row_" + id).find('td').removeClass("ignore-row");
        }
    });
}
