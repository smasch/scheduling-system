Icons come from an open source project: https://github.com/googlei18n/region-flags 
To add another ones, pick them from png folder, and resize to 300x180 to maintain consistency.
Then, add them in administrative panel.
