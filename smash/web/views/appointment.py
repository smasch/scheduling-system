# coding=utf-8
import datetime
import logging

from django.contrib import messages
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.decorators.cache import never_cache
from django.views.generic import DeleteView

from web.decorators import PermissionDecorator
from web.forms import AppointmentEditForm, AppointmentAddForm, SubjectEditForm, StudySubjectEditForm
from web.models.appointment_list import (
    APPOINTMENT_LIST_APPROACHING,
    APPOINTMENT_LIST_GENERIC,
    APPOINTMENT_LIST_UNFINISHED,
)
from web.models.constants import GLOBAL_STUDY_ID
from .view_utils import WrappedView, wrap_response
from ..models import Appointment, StudySubject, MailTemplate, Visit, Study, Provenance, Worker

logger = logging.getLogger(__name__)


@never_cache  # https://docs.djangoproject.com/en/1.8/topics/cache/#controlling-cache-using-other-headers
def appointments(request):
    context = {"approaching_list": APPOINTMENT_LIST_APPROACHING, "full_list": APPOINTMENT_LIST_GENERIC}
    return wrap_response(request, "appointments/index.html", context)


def unfinished_appointments(request):
    context = {"list_type": APPOINTMENT_LIST_UNFINISHED, "list_description": "List of unfinished appointments"}

    return wrap_response(request, "appointments/list.html", context)


def appointment_add(request: HttpRequest, visit_id: int = None) -> HttpResponse:
    visit = None
    if visit_id is not None:
        visit = get_object_or_404(Visit, id=visit_id)
        visit_start = visit.datetime_begin.strftime("%Y-%m-%d")
        visit_end = visit.datetime_end.strftime("%Y-%m-%d")
        if visit.is_finished:
            messages.error(request, "Appointment cannot be added because the visit is finished")
            return redirect("web.views.subject_visit_details", subject_id=visit.subject.id)
        if not visit.subject.can_schedule():
            messages.error(
                request, f"Appointment cannot be added because the subject status is: {visit.subject.status}"
            )
            return redirect("web.views.subject_visit_details", subject_id=visit.subject.id)
    else:
        visit_start = datetime.datetime.today().strftime("%Y-%m-%d")
        visit_end = datetime.datetime.today().strftime("%Y-%m-%d")
    if request.method == "POST":
        form = AppointmentAddForm(request.POST, request.FILES, user=request.user, visit=visit)
        if form.is_valid():
            form.instance.visit_id = visit_id
            form.save()
            if visit_id is None:
                return redirect("web.views.appointments")
            else:
                return redirect("web.views.visit_details", visit_id)
        else:
            messages.add_message(request, messages.ERROR, f"{form.non_field_errors()}")
    else:
        form = AppointmentAddForm(user=request.user, visit=visit)

    return wrap_response(
        request,
        "appointments/add.html",
        {
            "form": form,
            "visitID": visit_id,
            "isGeneral": visit_id is None,
            "visit_start": visit_start,
            "visit_end": visit_end,
            "full_list": APPOINTMENT_LIST_GENERIC,
        },
    )


def appointment_edit(request, appointment_id: int):
    the_appointment = get_object_or_404(Appointment, id=appointment_id)
    study_subject_form = None
    subject_form = None
    contact_attempts = None
    Study.get_by_id(GLOBAL_STUDY_ID)

    if the_appointment.visit is not None and the_appointment.visit.subject is not None:
        if not the_appointment.visit.subject.can_schedule():
            messages.error(
                request,
                f"Appointment cannot be edited because the subject status is: {the_appointment.visit.subject.status}",
            )
            return redirect("web.views.subject_visit_details", subject_id=the_appointment.visit.subject.id)

    if request.method == "POST":
        appointment_form = AppointmentEditForm(
            request.POST, request.FILES, instance=the_appointment, user=request.user, prefix="appointment"
        )
        is_valid_form = True
        if the_appointment.visit is not None:
            study_subject_form = StudySubjectEditForm(
                request.POST, instance=the_appointment.visit.subject, prefix="study-subject"
            )
            if not study_subject_form.is_valid():
                is_valid_form = False
            subject_form = SubjectEditForm(
                request.POST, instance=the_appointment.visit.subject.subject, prefix="subject"
            )
            if not subject_form.is_valid():
                is_valid_form = False

        if not appointment_form.is_valid():
            print(appointment_form.errors)
            is_valid_form = False

        adjust_date = False
        if the_appointment.visit is not None:
            if (
                the_appointment.visit.visit_number == 1
                and Appointment.objects.filter(
                    visit=the_appointment.visit, status=Appointment.APPOINTMENT_STATUS_FINISHED
                ).count()
                == 0
            ):
                adjust_date = True

        if is_valid_form:
            appointment_form.save()
            if study_subject_form is not None:
                study_subject_form.save()
                subject_form.save()
            the_appointment = get_object_or_404(Appointment, id=appointment_id)

            if the_appointment.status == Appointment.APPOINTMENT_STATUS_FINISHED and the_appointment.visit is not None:
                subject = StudySubject.objects.get(id=the_appointment.visit.subject.id)
                subject.information_sent = True
                if the_appointment.flying_team is not None and subject.flying_team is None:
                    subject.flying_team = the_appointment.flying_team
                subject.save()
                if adjust_date:
                    time_difference = the_appointment.datetime_when - the_appointment.visit.datetime_begin
                    the_appointment.visit.datetime_end = the_appointment.visit.datetime_end + time_difference
                    the_appointment.visit.datetime_begin = the_appointment.datetime_when
                    the_appointment.visit.save()

            messages.success(request, "Modifications saved")
            if "_continue" in request.POST:
                return redirect("web.views.appointment_edit", the_appointment.id)
            if "from_visit" in request.GET:
                return redirect("web.views.visit_details", the_appointment.visit.id)
            else:
                return redirect("web.views.appointments")
        else:
            messages.add_message(request, messages.ERROR, "Invalid data. Please fix data and try again.")

    else:
        appointment_form = AppointmentEditForm(instance=the_appointment, user=request.user, prefix="appointment")

        if the_appointment.visit is not None:
            study_subject_form = StudySubjectEditForm(instance=the_appointment.visit.subject, prefix="study-subject")
            subject_form = SubjectEditForm(instance=the_appointment.visit.subject.subject, prefix="subject")
            contact_attempts = the_appointment.visit.subject.contactattempt_set.order_by("-datetime_when").all()

    languages = []
    if the_appointment.visit is not None:
        subject = the_appointment.visit.subject.subject
        if subject.default_written_communication_language:
            languages.append(subject.default_written_communication_language)
        languages.extend(subject.languages.all())
    return wrap_response(
        request,
        "appointments/edit.html",
        {
            "appointment_form": appointment_form,
            "study_subject_form": study_subject_form,
            "subject_form": subject_form,
            "appointment": the_appointment,
            "contact_attempts": contact_attempts,
            "mail_templates": MailTemplate.get_appointment_mail_templates(languages),
        },
    )


class AppointmentDeleteView(DeleteView, WrappedView):
    model = Appointment
    success_url = reverse_lazy("web.views.appointments")
    template_name = "appointments/confirm_delete.html"

    def delete(self, request, *args, **kwargs):
        appointment = self.get_object()
        if appointment.status != Appointment.APPOINTMENT_STATUS_SCHEDULED:
            messages.error(request, "Appointment cannot be deleted because is already finished")
            return redirect("web.views.appointment_edit", appointment_id=appointment.id)
        else:
            messages.success(request, "Appointment deleted")
            worker = Worker.get_by_user(request.user)
            p = Provenance(
                modified_table=Appointment._meta.db_table,
                modified_table_id=appointment.id,
                modification_author=worker,
                previous_value="",
                new_value="",
                modification_description="Appointment deleted",
                modified_field="",
            )
            p.save()
            return super().delete(request, *args, **kwargs)

    @PermissionDecorator("delete_appointment", "configuration")
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
