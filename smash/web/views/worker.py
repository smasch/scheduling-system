# coding=utf-8
import logging

from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect, get_object_or_404

from web.forms import AvailabilityAddForm, AvailabilityEditForm, HolidayAddForm
from web.forms import WorkerForm
from web.models import Worker, Availability, Holiday
from web.models.constants import WEEKDAY_CHOICES, GLOBAL_STUDY_ID, AVAILABILITY_CHOICES
from web.models.worker import worker_type_by_worker
from web.models.worker_study_role import WORKER_STAFF
from .view_utils import wrap_response
from web.decorators import PermissionDecorator

logger = logging.getLogger(__name__)


@PermissionDecorator('change_worker', 'configuration')
def worker_list(request, worker_type=WORKER_STAFF):
    doctors_list = Worker.get_workers_by_worker_type(worker_type, study_id=GLOBAL_STUDY_ID, ).order_by('-last_name')
    context = {
        'doctors_list': doctors_list,
        'worker_type': worker_type
    }

    return wrap_response(request, "doctors/index.html", context)


@PermissionDecorator('add_worker', 'configuration')
def worker_add(request, worker_type):
    if request.method == 'POST':
        form = WorkerForm(request.POST, request.FILES, worker_type=worker_type)
        if form.is_valid():
            form.save()
            return redirect('web.views.workers', worker_type=worker_type)
    else:
        form = WorkerForm(worker_type=worker_type)

    return wrap_response(request, 'doctors/add.html', {'form': form, "worker_type": worker_type})


@PermissionDecorator('change_worker', 'configuration')
def worker_edit(request, worker_id):
    worker = get_object_or_404(Worker, id=worker_id)
    worker_type = worker_type_by_worker(worker)
    if request.method == 'POST':
        form = WorkerForm(request.POST, request.FILES, instance=worker, worker_type=worker_type)
        if form.is_valid():
            form.save()
            return redirect('web.views.workers', worker_type=worker_type)
    else:
        form = WorkerForm(instance=worker)
    availabilities = Availability.objects.filter(person=worker_id).order_by('day_number', 'available_from')
    holidays = Holiday.objects.filter(person=worker_id).order_by('-datetime_start')
    return wrap_response(request, 'doctors/edit.html',
                         {
                             'form': form,
                             'availabilities': availabilities,
                             'holidays': holidays,
                             'doctor_id': worker_id,
                             'weekdays': WEEKDAY_CHOICES,
                             'availability_choices': AVAILABILITY_CHOICES,
                             "worker_type": worker_type
                         })


@PermissionDecorator('delete_worker')
def worker_disable(request: HttpRequest, doctor_id: int) -> HttpResponse:
    the_doctor = get_object_or_404(Worker, id=doctor_id)
    the_doctor.disable()
    return worker_list(request)


@PermissionDecorator('add_worker')
def worker_enable(request: HttpRequest, doctor_id: int) -> HttpResponse:
    the_doctor = get_object_or_404(Worker, id=doctor_id)
    the_doctor.enable()
    return worker_list(request)


@PermissionDecorator('change_worker', 'configuration')
def worker_availability_delete(request, availability_id):
    availability = Availability.objects.filter(id=availability_id)
    doctor_id = availability[0].person.id
    availability.delete()
    return redirect(worker_edit, worker_id=doctor_id)


@PermissionDecorator('change_worker', 'configuration')
def worker_availability_add(request, doctor_id):
    worker = get_object_or_404(Worker, id=doctor_id)
    if request.method == 'POST':
        form = AvailabilityAddForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect(worker_edit, worker_id=doctor_id)
    else:
        form = AvailabilityAddForm(initial={'person': worker})
    return wrap_response(request, 'doctors/add_availability.html',
                         {
                             'form': form,
                             'doctor_id': doctor_id,
                             'doctor_name': str(worker)
                         })


@PermissionDecorator('change_worker', 'configuration')
def worker_availability_edit(request, availability_id):
    availability = get_object_or_404(Availability, id=availability_id)
    if request.method == 'POST':
        form = AvailabilityEditForm(request.POST, request.FILES, instance=availability)
        if form.is_valid():
            form.save()
            return redirect(worker_edit, worker_id=availability.person_id)
    else:
        form = AvailabilityEditForm(instance=availability)
    return wrap_response(request, 'doctors/edit_availability.html',
                         {
                             'form': form,
                             'availability_id': availability_id,
                             'doctor_id': availability.person_id,
                         })


@PermissionDecorator('change_worker', 'configuration')
def worker_holiday_delete(request, holiday_id):
    holiday = Holiday.objects.filter(id=holiday_id)
    doctor_id = holiday[0].person.id
    holiday.delete()
    return redirect(worker_edit, worker_id=doctor_id)


@PermissionDecorator('change_worker', 'configuration')
def worker_holiday_add(request, doctor_id):
    doctors = Worker.objects.filter(id=doctor_id)
    doctor = None
    if len(doctors) > 0:
        doctor = doctors[0]
    if request.method == 'POST':
        form = HolidayAddForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            return redirect(worker_edit, worker_id=doctor_id)
    else:
        form = HolidayAddForm(initial={'person': doctor})
        return wrap_response(request, 'doctors/add_holiday.html',
                             {
                                 'form': form,
                                 'doctor_id': doctor_id,
                                 'doctor_name': str(doctor)
                             })
