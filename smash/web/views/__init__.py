# coding=utf-8
import logging

from django.conf import settings
from django.shortcuts import redirect

from web.views.notifications import get_notifications
from . import appointment
from . import appointment_type
from . import configuration_item
from . import contact_attempt
from . import equipment
from . import equipment_and_rooms
from . import etl
from . import export
from . import flying_teams
from . import kit
from . import language
from . import location
from . import mails
from . import password
from . import privacy_notice
from . import provenance
from . import redcap
from . import rooms
from . import statistics
from . import study
from . import study_subject_list
from . import subject
from . import uploaded_files
from . import visit
from . import voucher
from . import voucher_partner_session
from . import voucher_type
from . import voucher_type_price
from . import worker
from .view_utils import e400_bad_request, e500_error, e403_permission_denied, e404_page_not_found

handler404 = e404_page_not_found
handler500 = e500_error
handler403 = e403_permission_denied
handler400 = e400_bad_request

logger = logging.getLogger(__name__)


def index(request):
    if request.user.is_authenticated:
        return redirect('web.views.appointments')
    return redirect(getattr(settings, "LOGIN_URL"))
