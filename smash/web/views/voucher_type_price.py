# coding=utf-8
import logging

from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import UpdateView

from web.forms import VoucherTypePriceForm
from web.models import VoucherTypePrice
from .view_utils import WrappedView

logger = logging.getLogger(__name__)


class VoucherTypePriceCreateView(CreateView, WrappedView):
    form_class = VoucherTypePriceForm
    model = VoucherTypePrice

    template_name = "voucher_type_prices/add.html"
    success_message = "Voucher type created"

    def form_valid(self, form):
        # noinspection PyUnresolvedReferences
        form.instance.voucher_type_id = self.kwargs['voucher_type_id']
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        # noinspection PyUnresolvedReferences
        return reverse_lazy('web.views.voucher_type_edit', kwargs={'pk': self.kwargs['voucher_type_id']})


class VoucherTypePriceEditView(UpdateView, WrappedView):
    form_class = VoucherTypePriceForm
    model = VoucherTypePrice

    success_message = "Voucher type edited"
    template_name = "voucher_type_prices/edit.html"
    context_object_name = "voucher_type"

    def get_success_url(self, **kwargs):
        # noinspection PyUnresolvedReferences
        return reverse_lazy('web.views.voucher_type_edit', kwargs={'pk': self.kwargs['voucher_type_id']})
