# coding=utf-8
from django.http import HttpRequest
from django.shortcuts import redirect, get_object_or_404

from web.decorators import PermissionDecorator
from web.forms.study_subject_list_form import StudySubjectListEditForm, StudySubjectColumnsEditForm, \
    SubjectColumnsEditForm
from web.models import StudySubjectList
from .view_utils import wrap_response
from ..models.study_subject_list import SUBJECT_LIST_CHOICES


@PermissionDecorator('change_study', 'configuration')
def study_subject_list_edit(request: HttpRequest, study_id: int, study_subject_list_id: int):
    the_list = get_object_or_404(StudySubjectList, id=study_subject_list_id)
    if request.method == 'POST':
        list_form = StudySubjectListEditForm(request.POST, instance=the_list, prefix="list")
        study_subject_columns_form = StudySubjectColumnsEditForm(request.POST,
                                                                 instance=the_list.visible_subject_study_columns,
                                                                 prefix="study_subject")
        subject_columns_form = SubjectColumnsEditForm(request.POST,
                                                      instance=the_list.visible_subject_columns,
                                                      prefix="subject")
        if list_form.is_valid() and study_subject_columns_form.is_valid() and subject_columns_form.is_valid():
            list_form.save()
            study_subject_columns_form.save()
            subject_columns_form.save()
            if '_continue' in request.POST:
                return redirect('web.views.study_subject_list_edit', study_id=study_id,
                                study_subject_list_id=the_list.id)
            return redirect('web.views.edit_study', study_id=study_id)
    else:
        list_form = StudySubjectListEditForm(instance=the_list, prefix="list")
        study_subject_columns_form = StudySubjectColumnsEditForm(instance=the_list.visible_subject_study_columns,
                                                                 prefix="study_subject")
        subject_columns_form = SubjectColumnsEditForm(instance=the_list.visible_subject_columns,
                                                      prefix="subject")

    return wrap_response(request, 'study_subject_list/edit.html', {'list_form': list_form,
                                                                   'study_subject_form': study_subject_columns_form,
                                                                   'subject_columns_form': subject_columns_form,
                                                                   'type': SUBJECT_LIST_CHOICES.get(the_list.type,
                                                                                                    the_list.type)})
