# coding=utf-8
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from .view_utils import WrappedView
from ..models import AppointmentType
from django.urls import reverse_lazy
from django.contrib import messages
from web.decorators import PermissionDecorator


class AppointmentTypeListView(ListView, WrappedView):
    model = AppointmentType
    template_name = 'appointment_types/index.html'
    context_object_name = "appointment_types"

    @PermissionDecorator('change_appointmenttype', 'equipment')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class AppointmentTypeCreateView(CreateView, WrappedView):
    model = AppointmentType
    template_name = "appointment_types/add.html"
    fields = '__all__'
    success_url = reverse_lazy('web.views.appointment_types')
    success_message = "Appointment type created"

    @PermissionDecorator('change_appointmenttype', 'equipment')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class AppointmentTypeEditView(UpdateView, WrappedView):
    model = AppointmentType
    success_url = reverse_lazy('web.views.appointment_types')
    fields = '__all__'
    success_message = "Appointment Type edited"
    template_name = "appointment_types/edit.html"
    context_object_name = "appointment_types"

    @PermissionDecorator('change_appointmenttype', 'equipment')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class AppointmentTypeDeleteView(DeleteView, WrappedView):
    model = AppointmentType
    success_url = reverse_lazy('web.views.appointment_types')
    template_name = 'appointment_types/confirm_delete.html'

    def delete(self, request, *args, **kwargs):
        messages.success(request, "Appointment Type deleted")
        return super().delete(request, *args, **kwargs)

    @PermissionDecorator('change_appointmenttype', 'equipment')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
