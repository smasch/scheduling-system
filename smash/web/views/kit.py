# coding=utf-8
import datetime
import locale
import logging
import platform
import time
import traceback
from typing import Union

import timeout_decorator
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.http import HttpRequest, HttpResponse
from django.utils.timezone import make_aware
from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog

from web.decorators import PermissionDecorator
from web.models import ConfigurationItem, Language, Worker, Study, Location
from web.models.constants import KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE, KIT_DAILY_EMAIL_DAYS_PERIOD_TYPE, \
    KIT_EMAIL_HOUR_CONFIGURATION_TYPE, KIT_DAILY_EMAIL_TIME_FORMAT_TYPE, \
    KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE, CRON_JOB_TIMEOUT, GLOBAL_STUDY_ID
from .view_utils import wrap_response
from .notifications import get_filter_locations, get_today_midnight_date
from ..forms import KitRequestForm
from ..models import AppointmentType, Appointment
from ..smash_email import EmailSender

logger = logging.getLogger(__name__)


def get_kit_requests(user: get_user_model(), start_date: Union[str, datetime.datetime] = None,
                     end_date: Union[str, datetime.datetime] = None):
    if start_date is None:
        days = ConfigurationItem.objects.get(type=KIT_DAILY_EMAIL_DAYS_PERIOD_TYPE).value
        if days.isdigit():
            days = int(days)
        else:
            days = 7
        start_date = get_today_midnight_date() + datetime.timedelta(days=1)
        end_date = start_date + datetime.timedelta(days=days)
    else:
        if isinstance(start_date, str):
            start_date = make_aware(datetime.datetime.strptime(start_date, '%Y-%m-%d'))
        if (end_date is not None) and (isinstance(end_date, str)):
            end_date = make_aware(datetime.datetime.strptime(end_date, '%Y-%m-%d'))

    appointment_types = AppointmentType.objects.filter(
        required_equipment__disposable=True)

    appointments = Appointment.objects.filter(
        appointment_types__in=appointment_types,
        datetime_when__gt=start_date,
        location__in=get_filter_locations(user),
        status=Appointment.APPOINTMENT_STATUS_SCHEDULED,
    ).distinct().order_by('datetime_when')

    if end_date is not None:
        appointments = appointments.filter(datetime_when__lt=end_date)

    result = {
        'start_date': start_date,
        'end_date': end_date,
        'appointments': appointments,
    }
    return result


@PermissionDecorator('send_sample_mail_for_appointments', 'kit')
def get_kit_requests_data(request: HttpRequest, start_date: str = None, end_date: str = None):
    form = KitRequestForm()
    if request.method == 'POST':
        form = KitRequestForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            start_date = form_data.get('start_date')
            end_date = form_data.get('end_date')

    params = get_kit_requests(request.user, start_date, end_date)
    params.update({
        'form': form
    })
    return params


@PermissionDecorator('send_sample_mail_for_appointments', 'kit')
def kit_requests(request: HttpRequest) -> HttpResponse:
    return wrap_response(request, 'equipment_and_rooms/kit_requests/kit_requests.html', get_kit_requests_data(request))


def create_detailed_email_content(data, title):
    time_format = ConfigurationItem.objects.get(type=KIT_DAILY_EMAIL_TIME_FORMAT_TYPE).value
    cell_style = "padding: 8px; line-height: 1.42857143; vertical-align: top; " \
                 "font-size: 14px; font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;"

    email_body = "<h1>" + title + "</h1>"
    email_body += '<table style="border: 1px solid #f4f4f4;border-spacing: 0;border-collapse: collapse;">' \
                  '<thead><tr>' \
                  '<th>Date</th>' \
                  '<th>ND number</th>' \
                  '<th>Samples</th>' \
                  '<th>Location</th>' \
                  '<th>Person responsible</th>' \
                  '</tr></thead>'
    email_body += "<tbody>"

    even = True
    for appointment in data["appointments"]:
        row_style = ""
        even = not even
        if even:
            row_style = ' background-color: #f9f9f9;'
        email_body += "<tr style='" + row_style + "'>"
        email_body += "<td style='" + cell_style + "'>" + \
                      appointment.datetime_when.strftime(time_format) + "</td>"
        if appointment.visit is not None and appointment.visit.subject is not None:
            email_body += "<td style='" + cell_style + "'>" + \
                          appointment.visit.subject.nd_number + "</td>"
        else:
            email_body += "<td style='" + cell_style + "'>" + '-' + "</td>"
        email_body += "<td style='" + cell_style + "'>"
        for appointment_type in appointment.appointment_types.all():
            for item in appointment_type.required_equipment.all():
                if item.disposable:
                    email_body += item.name + ", "
        email_body += "</td>"
        location = str(appointment.location)
        if appointment.flying_team is not None:
            location += " (" + str(appointment.flying_team) + ")"
        email_body += "<td style='" + cell_style + "'>" + location + "</td>"
        email_body += "<td style='" + cell_style + "'>" + \
                      str(appointment.worker_assigned) + "</td>"
        email_body += "</tr>"
    email_body += "</tbody></table>"
    return email_body


def create_statistic_email_content(data, title):
    time_format = ConfigurationItem.objects.get(type=KIT_DAILY_EMAIL_TIME_FORMAT_TYPE).value
    cell_style = "padding: 8px; line-height: 1.42857143; vertical-align: top; " \
                 "font-size: 14px; font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;"

    email_body = "<h1>" + title + "</h1>"

    location_summary = {}

    table = {}
    for appointment in data["appointments"]:

        appointment_date = appointment.datetime_when.strftime(time_format)
        location = str(appointment.location)
        if appointment.flying_team is not None:
            location += " (" + str(appointment.flying_team) + ")"
        simple_location = location.split(",")[0]
        if location_summary.get(simple_location) is None:
            location_summary[simple_location] = 0
        location_summary[simple_location] += 1

        if table.get(appointment_date) is None:
            table[appointment_date] = {}
        if table[appointment_date].get(location) is None:
            table[appointment_date][location] = 0
        table[appointment_date][location] += 1

    email_body += 'Total number of donors scheduled: ' + str(len(data["appointments"])) + "</br></br>"
    for location in location_summary:
        email_body += 'Total number of donors scheduled at ' + location + ': ' + str(
            location_summary[location]) + "</br>"

    email_body += "</br>"

    email_body += '<table style="border: 1px solid #f4f4f4;border-spacing: 0;border-collapse: collapse;">' \
                  '<thead><tr>' \
                  '<th>Date</th>' \
                  '<th>Location</th>' \
                  '<th>No-of-Donors</th>' \
                  '</tr></thead>'
    email_body += "<tbody>"

    even = True
    for appointment_date in table:
        for location in table[appointment_date]:
            subjects = table[appointment_date][location]
            row_style = ""
            even = not even
            if even:
                row_style = ' background-color: #f9f9f9;'
            email_body += "<tr style='" + row_style + "'>"
            email_body += "<td style='" + cell_style + "'>" + appointment_date + "</td>"
            email_body += "<td style='" + cell_style + "'>" + location + "</td>"
            email_body += "<td style='" + cell_style + "'>" + str(subjects) + "</td>"
            email_body += "</tr>"
    email_body += "</tbody></table>"
    return email_body


def send_mail(data):
    end_date_str = " end of time"
    title = None
    if data["end_date"] is not None:
        if (data["end_date"] - data["start_date"]).days < 2:
            title = "Details of Donors scheduled for bio-sampling on " + data["start_date"].strftime('%Y-%m-%d')
        end_date_str = data["end_date"].strftime('%Y-%m-%d')
    if title is None:
        title = "Samples between " + \
                data["start_date"].strftime('%Y-%m-%d') + " and " + end_date_str

    if Study.objects.get(id=GLOBAL_STUDY_ID).sample_mail_statistics:
        email_body = create_statistic_email_content(data, title)
    else:
        email_body = create_detailed_email_content(data, title)

    recipients = ConfigurationItem.objects.get(
        type=KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE).value
    cc_recipients = []
    logger.warning('Calling to send email')
    EmailSender.send_email(title, email_body, recipients, cc_recipients)


@PermissionDecorator('send_sample_mail_for_appointments', 'kit')
def kit_requests_send_mail(request: HttpRequest, start_date: str, end_date: str = None) -> HttpResponse:
    data = get_kit_requests_data(request, start_date, end_date)
    try:
        send_mail(data)
        messages.add_message(request, messages.SUCCESS, 'Mail sent')
    except Exception as e:
        traceback.print_exc()

        logger.warning('Kit Request Send Mail Failed: |%s|\n%s', str(e), e.args)
        messages.add_message(request, messages.ERROR, 'There was problem with sending email')
    return wrap_response(request, 'equipment_and_rooms/kit_requests/kit_requests.html', get_kit_requests_data(request))


class KitRequestEmailSendJob(CronJobBase):
    RUN_AT = []

    # noinspection PyBroadException
    try:
        times = ConfigurationItem.objects.get(
            type=KIT_EMAIL_HOUR_CONFIGURATION_TYPE).value.split(";")
        for entry in times:
            if entry != "":
                # it's a hack assuming that we are in CEST
                text = str((int(entry.split(":")[0]) + 22) % 24) + ":" + entry.split(":")[1]
                RUN_AT.append(text)
    except BaseException:
        logger.debug("Cannot fetch data about email hour")
    schedule = Schedule(run_at_times=RUN_AT)
    code = 'web.kit_request_weekly_email'  # a unique code

    @timeout_decorator.timeout(CRON_JOB_TIMEOUT)
    def do(self):
        jobs = CronJobLog.objects.filter(
            code=KitRequestEmailSendJob.code, message="mail sent", start_time__gte=datetime.datetime.utcnow())

        if jobs.count() == 0:
            if self.match_day_of_week():
                worker = Worker.objects.create()
                worker.locations.set(Location.objects.all())
                worker.save()
                data = get_kit_requests(worker)
                send_mail(data)
                worker.delete()
                return "mail sent"
            else:
                return "day of week doesn't match"
        else:
            return "mail already sent"

    @staticmethod
    def match_day_of_week():
        user_day_of_week = ConfigurationItem.objects.get(
            type=KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE).value
        if user_day_of_week == "*":
            return True
        language = Language.objects.get(name="English")
        locale_name = language.locale
        if platform.system() == 'Windows':
            locale_name = language.windows_locale_name
        try:
            locale.setlocale(locale.LC_TIME, locale_name)
        except BaseException:
            logger.exception("Problem with setting locale: %s", locale_name)

        try:
            user_day_of_week_int = int(time.strptime(
                user_day_of_week, '%A').tm_wday) + 1
            current_day_of_week_int = int(datetime.datetime.now().strftime("%w"))

            return user_day_of_week_int == current_day_of_week_int
        except ValueError:
            logger.exception("Invalid day of week: %s", user_day_of_week)
            return False
