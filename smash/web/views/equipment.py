# coding=utf-8
from django.shortcuts import redirect, get_object_or_404

from web.decorators import PermissionDecorator
from .view_utils import wrap_response
from ..forms.forms import ItemForm
from ..models import Item


@PermissionDecorator('change_item', 'equipment')
def equipment(request):
    equipment_list = Item.objects.order_by('-name')
    context = {
        'equipment_list': equipment_list
    }

    return wrap_response(request, "equipment_and_rooms/equipment/index.html", context)


@PermissionDecorator('change_item', 'equipment')
def equipment_add(request):
    if request.method == 'POST':
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('web.views.equipment')
    else:
        form = ItemForm()

    return wrap_response(request, 'equipment_and_rooms/equipment/add.html', {'form': form})


@PermissionDecorator('change_item', 'equipment')
def equipment_edit(request, equipment_id):
    the_item = get_object_or_404(Item, id=equipment_id)
    if request.method == 'POST':
        form = ItemForm(request.POST, instance=the_item)
        if form.is_valid():
            form.save()
            return redirect('web.views.equipment')
    else:
        form = ItemForm(instance=the_item)

    return wrap_response(request, 'equipment_and_rooms/equipment/edit.html', {'form': form})


@PermissionDecorator('change_item', 'equipment')
def equipment_delete(request, equipment_id):
    the_item = get_object_or_404(Item, id=equipment_id)
    the_item.delete()

    return redirect('web.views.equipment')
