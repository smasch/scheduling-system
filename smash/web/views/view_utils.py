import logging

from django.conf import settings
from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import render
from django.views.generic.base import ContextMixin

from web.decorators import PermissionDecorator
from web.models import Study, Worker
from web.models.constants import GLOBAL_STUDY_ID
from web.views import get_notifications

logger = logging.getLogger(__name__)


class WrappedView(ContextMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return extend_context(context, self.request)  # pylint: disable=no-member


def wrap_response(request: HttpRequest, template, params) -> HttpResponse:
    final_params = extend_context(params, request)
    return render(request, template, final_params)


def extend_context(params, request: HttpRequest):
    global_study = Study.get_by_id(GLOBAL_STUDY_ID)
    person = Worker.get_by_user(request.user)  # None if AnonymousUser or no Worker associated
    permissions = set()
    show_notice = True
    copyright_note = getattr(settings, 'COPYRIGHT_NOTE', '')
    if person is not None:
        role = person.role
        permissions = person.get_permissions(global_study)
        show_notice = global_study.study_privacy_notice and not person.privacy_notice_accepted
        person = str(person)
    else:
        # use full name if available, username otherwise
        if len(request.user.get_full_name()) > 1:
            person = request.user.get_full_name()
        else:
            person = request.user.get_username()
        role = '<No worker information>'
    if request.resolver_match is not None and request.resolver_match.url_name == 'web.views.accept_privacy_notice':
        show_notice = False
    notifications = get_notifications(request.user)
    final_params = params.copy()
    final_params.update({
        'copyright_note': copyright_note,
        'permissions': permissions,
        'conf_perms': permissions & PermissionDecorator.codename_groups['configuration'],
        'equipment_perms': permissions & PermissionDecorator.codename_groups['equipment'],

        # at least one permission must be present and study should have vouchers
        'voucher_should_shown': global_study.has_vouchers and bool(
            {'change_voucher', 'change_vouchertype', 'change_worker'} | permissions),
        'person': person,
        'role': role,
        'notifications': notifications,
        'show_notice': show_notice,
        'study_id': GLOBAL_STUDY_ID,
        'study': global_study
    })
    return final_params


def e404_page_not_found(request, context=None, exception=None):
    if exception is not None and not isinstance(exception, Http404):
        logger.exception("When handling %s request for url '%s' exception occurred", request.method,
                         request.get_full_path_info())

    return render(request, "errors/404.html", context, status=404)


def e500_error(request, context=None):
    logger.exception("When handling %s request for url '%s' exception occurred", request.method,
                     request.get_full_path_info())
    return render(request, "errors/500.html", context, status=500)


def e403_permission_denied(request, context=None):
    return render(request, "errors/403.html", context, status=403)


def e400_bad_request(request, context=None):
    return render(request, "errors/400.html", context, status=400)
