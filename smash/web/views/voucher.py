# coding=utf-8
import logging

import timeout_decorator
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django_cron import CronJobBase, Schedule

from web.decorators import PermissionDecorator
from web.forms import VoucherForm
from web.models import Voucher, StudySubject, MailTemplate, Worker
from web.models.constants import GLOBAL_STUDY_ID, VOUCHER_STATUS_NEW, VOUCHER_STATUS_EXPIRED, CRON_JOB_TIMEOUT
from web.views.notifications import get_today_midnight_date
from .view_utils import WrappedView

logger = logging.getLogger(__name__)


class VoucherListView(ListView, WrappedView):
    model = Voucher
    context_object_name = "vouchers"
    template_name = 'vouchers/list.html'

    @PermissionDecorator('change_voucher', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


def voucher_types_for_study_subject(study_subject_id):
    return StudySubject.objects.get(id=study_subject_id).voucher_types.all()


class VoucherCreateView(CreateView, WrappedView):
    form_class = VoucherForm
    model = Voucher

    template_name = "vouchers/add.html"
    success_url = reverse_lazy('web.views.vouchers')
    success_message = "Voucher type created"

    def get_initial(self):
        worker = Worker.get_by_user(self.request.user)
        worker_id = None
        if worker is not None:
            worker_id = worker.id
        return {
            'issue_worker': worker_id,
        }

    def form_valid(self, form):
        form.instance.study_id = GLOBAL_STUDY_ID
        # noinspection PyUnresolvedReferences
        form.instance.study_subject_id = self.request.GET.get("study_subject_id", -1)
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        # noinspection PyUnresolvedReferences
        return reverse_lazy('web.views.subject_edit',
                            kwargs={'subject_id': self.request.GET.get("study_subject_id", -1)})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['voucher_types'] = voucher_types_for_study_subject(self.request.GET.get("study_subject_id", -1))
        return kwargs

    @PermissionDecorator('change_voucher', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class VoucherEditView(SuccessMessageMixin, UpdateView, WrappedView):
    form_class = VoucherForm
    model = Voucher

    success_url = reverse_lazy('web.views.vouchers')
    success_message = "Voucher saved successfully"
    template_name = "vouchers/edit.html"
    context_object_name = "voucher"

    def get_success_url(self, **kwargs):
        # noinspection PyUnresolvedReferences
        return reverse_lazy('web.views.subject_edit', kwargs={'subject_id': self.get_study_subject_id()})

    def get_study_subject_id(self):
        return Voucher.objects.get(id=self.kwargs['pk']).study_subject.id

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['mail_templates'] = MailTemplate.get_voucher_mail_templates([])
        return context

    @PermissionDecorator('change_voucher', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class ExpireVouchersJob(CronJobBase):
    RUN_EVERY_MINUTES = 120
    schedule = Schedule(run_every_mins=RUN_EVERY_MINUTES)
    code = 'web.voucher_expiry_job'  # a unique code

    # noinspection PyMethodMayBeStatic
    # pylint: disable=no-self-use
    @timeout_decorator.timeout(CRON_JOB_TIMEOUT)
    def do(self):
        due_date = get_today_midnight_date()
        vouchers = Voucher.objects.filter(status=VOUCHER_STATUS_NEW, expiry_date__lte=due_date)
        count = vouchers.count()
        if count > 0:
            vouchers.update(status=VOUCHER_STATUS_EXPIRED)
        return str(count) + " vouchers expired"
