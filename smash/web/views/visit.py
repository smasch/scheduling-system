# coding=utf-8
import logging
from ..utils import get_client_ip
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
from .notifications import waiting_for_appointment
from web.models.study_visit_list import VISIT_LIST_GENERIC, VISIT_LIST_MISSING_APPOINTMENTS, \
    VISIT_LIST_APPROACHING_WITHOUT_APPOINTMENTS, VISIT_LIST_APPROACHING_FOR_MAIL_CONTACT, VISIT_LIST_EXCEEDED_TIME, \
    VISIT_LIST_UNFINISHED, VISIT_LIST_CHOICES
from .view_utils import wrap_response
from ..forms import VisitDetailForm, VisitAddForm, SubjectDetailForm, StudySubjectDetailForm
from ..models import Visit, Appointment, StudySubject, MailTemplate, Worker, Provenance
from web.decorators import PermissionDecorator

logger = logging.getLogger(__name__)


def show_visits(request, visit_list_type):
    context = {
        'visit_list_type': visit_list_type,
        'list_description': VISIT_LIST_CHOICES[visit_list_type]
    }
    return wrap_response(request, 'visits/index.html', context)


def visits(request):
    return show_visits(request, VISIT_LIST_GENERIC)


def visits_with_missing_appointments(request):
    return show_visits(request, VISIT_LIST_MISSING_APPOINTMENTS)


def approaching_visits_without_appointments(request):
    return show_visits(request, VISIT_LIST_APPROACHING_WITHOUT_APPOINTMENTS)


def approaching_visits_for_mail_contact(request):
    return show_visits(request, VISIT_LIST_APPROACHING_FOR_MAIL_CONTACT)


def exceeded_visits(request):
    return show_visits(request, VISIT_LIST_EXCEEDED_TIME)


def unfinished_visits(request):
    return show_visits(request, VISIT_LIST_UNFINISHED)


def visit_details(request, visit_id: int):
    displayed_visit = get_object_or_404(Visit, id=visit_id)
    if request.method == 'POST':
        visit_form = VisitDetailForm(request.POST, request.FILES, instance=displayed_visit)
        if visit_form.is_valid():
            visit_form.save()
    else:
        visit_form = VisitDetailForm(instance=displayed_visit)

    visit_form.fields['subject'].choices = [(displayed_visit.subject.id, displayed_visit.subject)]
    visit_form.fields['subject'].initial = (displayed_visit.subject.id, displayed_visit.subject)
    visit_form.fields['subject'].widget.attrs['readonly'] = True

    visit_finished = displayed_visit.is_finished
    visit_id = displayed_visit.id
    study_subject = displayed_visit.subject
    list_of_appointments = displayed_visit.appointment_set.all()

    can_finish = not waiting_for_appointment(displayed_visit)

    for appointment in list_of_appointments:
        if appointment.status == Appointment.APPOINTMENT_STATUS_SCHEDULED:
            can_finish = False

    study_subject_form = StudySubjectDetailForm(instance=study_subject)
    subject_form = SubjectDetailForm(instance=study_subject.subject)
    languages = []
    if study_subject.subject.default_written_communication_language:
        languages.append(study_subject.subject.default_written_communication_language)
    languages.extend(study_subject.subject.languages.all())

    next_visit_appointments = []
    if displayed_visit.next_visit is not None:
        next_visit_appointments = displayed_visit.next_visit.appointment_set.all()

    return wrap_response(request, 'visits/details.html', {
        'default_visit_duration': study_subject.study.default_visit_duration_in_months,
        'visit_form': visit_form,
        'study_subject_form': study_subject_form,
        'subject_form': subject_form,
        'loApp': list_of_appointments,
        'visFinished': visit_finished,
        'canFinish': can_finish,
        'vid': visit_id,
        'visit': displayed_visit,
        'mail_templates': MailTemplate.get_visit_mail_templates(languages),
        'next_visit': displayed_visit.next_visit,
        'next_visit_appointments': next_visit_appointments,
    })


@PermissionDecorator('delete_visit', 'visit')
def visit_unfinish(request, visit_id):
    visit = get_object_or_404(Visit, id=visit_id)
    try:
        visit.unfinish()
        messages.add_message(request, messages.SUCCESS, 'Visit has been unfinished.')
    except ValueError as error:
        messages.add_message(request, messages.ERROR, str(error))
    return redirect('web.views.visit_details', visit_id)


def visit_mark(request, visit_id: int, as_what: str):
    visit = get_object_or_404(Visit, id=visit_id)
    ip = get_client_ip(request)
    if as_what == 'finished':
        worker = Worker.get_by_user(request.user)
        p = Provenance(modified_table=Visit._meta.db_table,
                       modified_table_id=visit_id,
                       modification_author=worker,
                       previous_value=visit.is_finished,
                       new_value=True,
                       modification_description=f'Worker "{worker}" marked visit from "{visit.subject}" as finished',
                       modified_field='is_finished',
                       request_path=request.path,
                       request_ip_addr=ip
                       )
        visit.mark_as_finished()
        p.save()

    return redirect('web.views.visit_details', visit_id)


def visit_add(request, subject_id=-1):
    subjects = StudySubject.objects.filter(id=subject_id)
    subject = None
    if len(subjects) > 0:
        subject = subjects[0]

    if Visit.objects.filter(subject=subject, is_finished=False).count() > 0:
        messages.add_message(request, messages.WARNING,
                             'The subject has unfinished visits, please, finish them before adding new visits.')
        return redirect('web.views.subject_visit_details', subject_id=subject_id)

    if not subject.can_schedule():
        messages.error(request, f"Visit cannot be added because the subject status is: {subject.status}")
        return redirect('web.views.subject_visit_details', subject_id=subject_id)

    if request.method == 'POST':
        form = VisitAddForm(request.POST, request.FILES)
        args = {'form': form}
        if request.POST['subject'] != subject_id:
            messages.add_message(request, messages.WARNING, f'The subject is invalid. Must be {subject}')
            return wrap_response(request, 'visits/add.html', args)
        if form.is_valid():
            visit = form.save()
            return redirect('web.views.visit_details', visit.id)
    else:
        form = VisitAddForm(initial={'subject': subject})
        form.fields['subject'].choices = [(subject_id, subject)]
        form.fields['subject'].initial = (subject_id, subject)
        form.fields['subject'].widget.attrs['readonly'] = True
        args = {'form': form, 'default_visit_duration': subject.study.default_visit_duration_in_months}

    return wrap_response(request, 'visits/add.html', args)
