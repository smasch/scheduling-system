from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import redirect

from web.models.worker import Worker
from .view_utils import wrap_response


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, f'The password for {request.user} was successfully updated!')
            return redirect('web.views.workers')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)

    worker = Worker.get_by_user(request.user)
    return wrap_response(request, 'doctors/change_password.html', {'form': form, 'worker': worker})
