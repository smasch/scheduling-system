# coding=utf-8
from django.shortcuts import redirect, get_object_or_404

from web.decorators import PermissionDecorator
from .view_utils import wrap_response
from ..forms.forms import RoomForm
from ..models import Room
from web.views.notifications import get_filter_locations


@PermissionDecorator('change_room', 'equipment')
def rooms_planning(request):
    locations = get_filter_locations(request.user)
    context = {
        'locations': locations
    }

    return wrap_response(request,
                         "equipment_and_rooms/rooms/planning.html",
                         context)


@PermissionDecorator('change_room', 'equipment')
def rooms(request):
    rooms_list = Room.objects.filter(removed=False).order_by('-city')
    context = {
        'rooms_list': rooms_list
    }

    return wrap_response(request,
                         "equipment_and_rooms/rooms/index.html",
                         context)


@PermissionDecorator('change_room', 'equipment')
def rooms_add(request):
    if request.method == 'POST':
        form = RoomForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('web.views.equipment_and_rooms.rooms')
    else:
        form = RoomForm()

    return wrap_response(request, 'equipment_and_rooms/rooms/add.html', {'form': form})


@PermissionDecorator('change_room', 'equipment')
def rooms_edit(request, room_id):
    the_room = get_object_or_404(Room, id=room_id)
    if request.method == 'POST':
        form = RoomForm(request.POST, instance=the_room)
        if form.is_valid():
            form.save()
            return redirect('web.views.equipment_and_rooms.rooms')
    else:
        form = RoomForm(instance=the_room)

    return wrap_response(request, 'equipment_and_rooms/rooms/edit.html', {'form': form})


@PermissionDecorator('change_room', 'equipment')
def rooms_delete(request, room_id):
    the_room = get_object_or_404(Room, id=room_id)
    the_room.removed = True
    the_room.save()

    return redirect('web.views.equipment_and_rooms.rooms')
