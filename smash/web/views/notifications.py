# coding=utf-8
import datetime
import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.db.models import Count, Case, When, Q, F, Max
from django.utils import timezone

from web.models import Study, Worker, StudySubject, Visit, Appointment, Location, MissingSubject, InconsistentSubject
from web.models.constants import GLOBAL_STUDY_ID, VOUCHER_STATUS_NEW
from web.utils import get_today_midnight_date

logger = logging.getLogger(__name__)


class NotificationCount:
    title = ""
    count = 0
    style = ""
    type = ''

    def __init__(self, title="Unknown", count=0, style="fa fa-users text-aqua",
                 notification_type='web.views.appointments'):
        self.title = title
        self.count = count
        self.style = style
        self.type = notification_type


def get_exceeded_visit_notifications_count(user):
    notification = NotificationCount(
        title="exceeded visit time",
        count=get_exceeded_visits(user).count(),
        style="fa fa-thermometer-4 text-red",
        notification_type='web.views.exceeded_visits')
    return notification


def get_missing_redcap_subject_notification_count(user):
    notification = NotificationCount(
        title="missing RED Cap subject",
        count=get_missing_redcap_subjects(user, False).count(),
        style="fa fa-user text-red",
        notification_type='web.views.missing_redcap_subject')
    return notification


def get_inconsistent_redcap_subject_notification_count(user):
    notification = NotificationCount(
        title="inconsistent RED Cap subject",
        count=get_inconsistent_redcap_subjects(user).count(),
        style="fa fa-user-circle text-yellow",
        notification_type='web.views.inconsistent_redcap_subject')
    return notification


def get_subjects_with_reminder_count(user):
    notification = NotificationCount(
        title="subject required contact",
        count=get_subjects_with_reminder(user).count(),
        style="fa fa-users text-aqua",
        notification_type='web.views.subject_require_contact')
    return notification


def get_subject_with_no_visit_notifications_count(user):
    notification = NotificationCount(
        title="subject without visit",
        count=get_subjects_with_no_visit(user).count(),
        style="fa fa-users text-aqua",
        notification_type='web.views.subject_no_visits')
    return notification


def get_subject_voucher_expiry_notifications_count(user):
    notification = NotificationCount(
        title="subject vouchers almost expired",
        count=get_subjects_with_almost_expired_vouchers(user).count(),
        style="fa fa-users text-aqua",
        notification_type='web.views.subject_voucher_expiry')
    return notification


def get_visits_without_appointments_count(user):
    notification = NotificationCount(
        title="unfinished visits",
        count=get_unfinished_visits(user).count(),
        style="fa fa-user-times text-yellow",
        notification_type='web.views.unfinished_visits')
    return notification


def get_visits_with_missing_appointments_count(user):
    notification = NotificationCount(
        title="visits with missing appointments",
        count=get_active_visits_with_missing_appointments(user).count(),
        style="fa fa-user-times text-yellow",
        notification_type='web.views.visits_with_missing_appointments')
    return notification


def get_approaching_visits_without_appointments_count(user):
    notification = NotificationCount(
        title="approaching visits",
        count=get_approaching_visits_without_appointments(user).count(),
        style="fa fa-users text-aqua",
        notification_type='web.views.approaching_visits_without_appointments')
    return notification


def get_approaching_visits_for_mail_contact_count(user):
    notification = NotificationCount(
        title="post mail for approaching visits",
        count=get_approaching_visits_for_mail_contact(user).count(),
        style="fa fa-users text-aqua",
        notification_type='web.views.approaching_visits_for_mail_contact')
    return notification


def get_unfinished_appointments_count(user):
    return NotificationCount(
        title="unfinished appointments",
        count=get_unfinished_appointments(user).count(),
        style="fa fa-history text-yellow",
        notification_type='web.views.unfinished_appointments')


def get_notifications(the_user):
    worker = Worker.get_by_user(the_user)
    notifications = []
    count = 0
    if worker is not None:
        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        if study.notification_parameters.exceeded_visits_visible:
            notifications.append(get_exceeded_visit_notifications_count(worker))
        if study.notification_parameters.unfinished_visits_visible:
            notifications.append(get_visits_without_appointments_count(worker))
        if study.notification_parameters.approaching_visits_without_appointments_visible:
            notifications.append(get_approaching_visits_without_appointments_count(worker))
        if study.notification_parameters.unfinished_appointments_visible:
            notifications.append(get_unfinished_appointments_count(worker))
        if study.notification_parameters.visits_with_missing_appointments_visible:
            notifications.append(get_visits_with_missing_appointments_count(worker))
        if study.notification_parameters.subject_no_visits_visible:
            notifications.append(get_subject_with_no_visit_notifications_count(worker))
        if study.notification_parameters.subject_voucher_expiry_visible:
            notifications.append(get_subject_voucher_expiry_notifications_count(worker))
        if study.notification_parameters.approaching_visits_for_mail_contact_visible:
            notifications.append(get_approaching_visits_for_mail_contact_count(worker))
        if study.notification_parameters.subject_require_contact_visible:
            notifications.append(get_subjects_with_reminder_count(worker))
        if study.notification_parameters.missing_redcap_subject_visible:
            notifications.append(get_missing_redcap_subject_notification_count(worker))
        if study.notification_parameters.inconsistent_redcap_subject_visible:
            notifications.append(get_inconsistent_redcap_subject_notification_count(worker))

        for notification in notifications:
            count += notification.count
    return count, notifications


def get_subjects_with_no_visit(user):
    result = StudySubject.objects.annotate(
        unfinished_visit_count=Count(Case(When(visit__is_finished=False, then=1)))).filter(
        subject__dead=False,
        resigned=False,
        endpoint_reached=False,
        unfinished_visit_count=0,
        default_location__in=get_filter_locations(user),
        postponed=False,
        excluded=False,
        datetime_contact_reminder__isnull=True,
    ).exclude(vouchers__status=VOUCHER_STATUS_NEW)
    return result


def get_subjects_with_almost_expired_vouchers(user):
    # if expiry date is in the next 7 days
    notification_min_date = get_today_midnight_date() + datetime.timedelta(days=7)
    # if last successful contact was over 7 days ago
    contact_attempt_min_date = get_today_midnight_date() - datetime.timedelta(days=7)
    result = StudySubject.objects.filter(
        subject__dead=False,
        resigned=False,
        endpoint_reached=False,
        excluded=False,
        default_location__in=get_filter_locations(user),
    ).annotate(last_contact=Max(Case(When(contactattempt__success=True, then="contactattempt__datetime_when")))).filter(
        Q(vouchers__status=VOUCHER_STATUS_NEW) & Q(vouchers__expiry_date__lte=notification_min_date)).filter(
        Q(last_contact__lt=contact_attempt_min_date) | Q(last_contact__isnull=True))
    return result


def get_subjects_with_reminder(user):
    tomorrow = timezone.now() + datetime.timedelta(hours=1)

    result = StudySubject.objects.filter(
        subject__dead=False,
        resigned=False,
        endpoint_reached=False,
        excluded=False,
        default_location__in=get_filter_locations(user),
        datetime_contact_reminder__lt=tomorrow,
    )
    return result


def get_active_visits_with_missing_appointments(user):
    visits_without_appointments = get_active_visits_without_appointments(user)
    return visits_without_appointments.filter(types_in_system_count__lt=F("types_expected_in_system_count"))


def get_unfinished_visits(user):
    visits_without_appointments = get_active_visits_without_appointments(user)
    return visits_without_appointments.filter(types_in_system_count__gte=F("types_expected_in_system_count"))


def get_approaching_visits_without_appointments(user):
    today = get_today_midnight_date()
    today_plus_two_months = today + datetime.timedelta(days=91)
    return Visit.objects.annotate(
        my_count=Count(Case(When(appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED, then=1)))).filter(
        subject__subject__dead=False,
        subject__resigned=False,
        subject__excluded=False,
        datetime_begin__gt=today,
        datetime_begin__lt=today_plus_two_months,
        is_finished=False,
        subject__default_location__in=get_filter_locations(user),
        my_count=0).order_by('datetime_begin')


def get_approaching_visits_for_mail_contact(user):
    today = get_today_midnight_date()
    today_plus_three_months = today + datetime.timedelta(days=91)
    today_plus_six_months = today + datetime.timedelta(days=183)
    return Visit.objects.annotate(
        my_count=Count(Case(When(appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED, then=1)))).filter(
        subject__subject__dead=False,
        subject__resigned=False,
        subject__excluded=False,
        datetime_begin__gt=today_plus_three_months,
        datetime_begin__lt=today_plus_six_months,
        is_finished=False,
        post_mail_sent=False,
        subject__default_location__in=get_filter_locations(user),
        my_count=0).order_by('datetime_begin')


def get_exceeded_visits(user):
    return Visit.objects.filter(datetime_end__lt=get_today_midnight_date(),
                                is_finished=False,
                                subject__default_location__in=get_filter_locations(user)
                                ).filter(
        # by default any visit where visit number is bigger than 1
        # or in case of first visit - visits that had some successful appointment
        Q(visit_number__gt=1) | Q(appointment__status=Appointment.APPOINTMENT_STATUS_FINISHED)).filter(
        # visits that have scheduled appointments should be excluded
        ~Q(appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED)).order_by(
        'datetime_begin').distinct()


def get_missing_redcap_subjects(user, include_ignored=True):  # pylint: disable=unused-argument
    if include_ignored:
        return MissingSubject.objects.order_by("ignore")
    else:
        return MissingSubject.objects.filter(ignore=False)


def get_inconsistent_redcap_subjects(user):  # pylint: disable=unused-argument
    return InconsistentSubject.objects.all()


def get_unfinished_appointments(user):
    return Appointment.objects.filter(
        datetime_when__lt=get_today_midnight_date(),
        status=Appointment.APPOINTMENT_STATUS_SCHEDULED,
        location__in=get_filter_locations(user),
    ).exclude(visit__isnull=True).order_by('datetime_when')


def waiting_for_appointment(visit):
    required_types = visit.appointment_types.all()
    appointment_types = []
    for appointment in visit.appointment_set.all():
        for appointment_type in appointment.appointment_types.all():
            if (appointment.status in [Appointment.APPOINTMENT_STATUS_FINISHED,
                                       Appointment.APPOINTMENT_STATUS_SCHEDULED]) and (
                    not (appointment_type in appointment_types)):
                appointment_types.append(appointment_type)
    result = False
    for appointment_type in required_types:
        if not (appointment_type in appointment_types):
            result = True
    return result


def get_active_visits_without_appointments(user):
    today = get_today_midnight_date()
    return Visit.objects.annotate(
        my_count=Count(Case(When(appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED, then=1)))).filter(
        datetime_begin__lt=today,
        datetime_end__gt=today,
        is_finished=False,
        subject__default_location__in=get_filter_locations(user),
        my_count=0).order_by('datetime_begin').annotate(
        # types_in_system_count annotation counts how many different appointment types were scheduled or already
        # performed
        types_in_system_count=Count(Case(When(
            Q(appointment__status=Appointment.APPOINTMENT_STATUS_SCHEDULED) | Q(
                appointment__status=Appointment.APPOINTMENT_STATUS_FINISHED),
            then="appointment__appointment_types")),
            distinct=True)).annotate(
        # types_expected_in_system_count annotation counts how many different appointment types should be performed in
        # the visit
        types_expected_in_system_count=Count('appointment_types', distinct=True))


def get_filter_locations(user):
    worker = None

    if isinstance(user, get_user_model()):
        worker = Worker.get_by_user(user)
    elif isinstance(user, Worker):
        worker = user
    elif isinstance(user, AnonymousUser):
        return Location.objects.filter(id=-1)
    elif user is not None:
        raise TypeError("Unknown class type: " + user.__class__.__name__)

    return worker.locations.filter(removed=False)
