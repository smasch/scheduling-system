from django.shortcuts import redirect, get_object_or_404

from web.forms import ContactAttemptAddForm, ContactAttemptEditForm
from web.models import StudySubject, ContactAttempt
from .view_utils import wrap_response


def contact_add(request, subject_id):
    subject = get_object_or_404(StudySubject, id=subject_id)
    if request.method == 'POST':
        form = ContactAttemptAddForm(request.POST, user=request.user, subject=subject)
        form.instance.subject_id = subject_id
        if form.is_valid():
            form.save()
            if 'from_appointment' in request.GET:
                return redirect('web.views.appointment_edit', appointment_id=request.GET.get('from_appointment', ''))
            else:
                return redirect('web.views.subject_edit', subject_id=subject_id)
    else:
        form = ContactAttemptAddForm(user=request.user, subject=subject)

    return wrap_response(request, 'contact_attempt/add.html',
                         {'form': form, 'subject_id': subject_id})


def contact_edit(request, subject_id, contact_attempt_id):
    contact_attempt = get_object_or_404(ContactAttempt, id=contact_attempt_id)
    if request.method == 'POST':
        form = ContactAttemptEditForm(request.POST, instance=contact_attempt, user=request.user, )
        if form.is_valid():
            form.save()
            if 'from_appointment' in request.GET:
                return redirect('web.views.appointment_edit', appointment_id=request.GET.get('from_appointment', ''))
            else:
                return redirect('web.views.subject_edit', subject_id=contact_attempt.subject.id)
    else:
        form = ContactAttemptEditForm(instance=contact_attempt, user=request.user)

    return wrap_response(request, 'contact_attempt/edit.html', {'form': form, 'subject_id': subject_id})
