# coding=utf-8
from django.shortcuts import redirect, get_object_or_404

from web.decorators import PermissionDecorator
from .view_utils import wrap_response
from ..forms.forms import FlyingTeamAddForm, FlyingTeamEditForm
from ..models import FlyingTeam


@PermissionDecorator('change_flyingteam', 'equipment')
def flying_teams(request):
    flying_team_list = FlyingTeam.objects.order_by('-place')
    context = {
        'flying_team_list': flying_team_list
    }

    return wrap_response(request,
                         "equipment_and_rooms/flying_teams/index.html",
                         context)


@PermissionDecorator('change_flyingteam', 'equipment')
def flying_teams_add(request):
    if request.method == 'POST':
        form = FlyingTeamAddForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('web.views.equipment_and_rooms.flying_teams')
    else:
        form = FlyingTeamAddForm()

    return wrap_response(request, 'equipment_and_rooms/flying_teams/add.html', {'form': form})


@PermissionDecorator('change_flyingteam', 'equipment')
def flying_teams_edit(request, flying_team_id):
    the_flying_team = get_object_or_404(FlyingTeam, id=flying_team_id)
    if request.method == 'POST':
        form = FlyingTeamEditForm(request.POST, instance=the_flying_team)
        if form.is_valid():
            form.save()
            return redirect('web.views.equipment_and_rooms.flying_teams')
    else:
        form = FlyingTeamEditForm(instance=the_flying_team)

    return wrap_response(request, 'equipment_and_rooms/flying_teams/edit.html', {'form': form})
