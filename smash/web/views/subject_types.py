# coding=utf-8
from django.contrib import messages
from django.http import HttpRequest
from django.shortcuts import redirect, get_object_or_404

from web.decorators import PermissionDecorator
from .view_utils import wrap_response
from ..forms.subject_type_forms import SubjectTypeAddForm, SubjectTypeEditForm
from ..models import SubjectType, Study, StudySubject


@PermissionDecorator('change_subjecttype')
def subject_types(request: HttpRequest, study_id: int):
    subject_type_list = SubjectType.objects.filter(study_id=study_id).order_by('-name')
    data = []
    for subject_type in subject_type_list:
        data.append({"id": subject_type.id, "name": subject_type.name,
                     "subject_count": StudySubject.objects.filter(type=subject_type).count()})

    context = {
        'subject_type_list': data
    }

    return wrap_response(request, "subject_types/list.html", context)


@PermissionDecorator('change_subjecttype')
def subject_type_add(request: HttpRequest, study_id: int):
    study = get_object_or_404(Study, id=study_id)
    if request.method == 'POST':
        form = SubjectTypeAddForm(request.POST, study=study)
        if form.is_valid():
            form.save()
            return redirect('web.views.subject_types', study_id=study_id)
    else:
        form = SubjectTypeAddForm(study=study)

    return wrap_response(request, 'subject_types/add.html', {'form': form})


@PermissionDecorator('change_subjecttype')
def subject_type_edit(request: HttpRequest, study_id: int, subject_type_id: int):
    subject_type = get_object_or_404(SubjectType, id=subject_type_id, study_id=study_id)
    if request.method == 'POST':
        form = SubjectTypeEditForm(request.POST, instance=subject_type)
        if form.is_valid():
            form.save()
            return redirect('web.views.subject_types', study_id=study_id)
    else:
        form = SubjectTypeEditForm(instance=subject_type)

    return wrap_response(request, 'subject_types/edit.html', {'form': form, "subject_type": subject_type})


@PermissionDecorator('change_subjecttype')
def subject_type_delete(request: HttpRequest, study_id: int, subject_type_id: int):
    subject_type = get_object_or_404(SubjectType, id=subject_type_id, study_id=study_id)
    if StudySubject.objects.filter(type=subject_type).count() > 0:
        messages.add_message(request, messages.ERROR,
                             'SubjectType cannot be removed - there are subjects with the type defined')
    else:
        subject_type.delete()
    return redirect('web.views.subject_types', study_id=study_id)
