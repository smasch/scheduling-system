# coding=utf-8
import csv

import django_excel as excel
from django.db.models.manager import BaseManager
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from web.decorators import PermissionDecorator
from web.models import Subject, StudySubject, Appointment, Study, Worker, Provenance
from web.models.custom_data import CustomStudySubjectField
from web.models.custom_data.custom_study_subject_field import get_study_subject_field_id
from .notifications import get_today_midnight_date
from .view_utils import wrap_response, e500_error
from ..utils import get_client_ip


@PermissionDecorator("export_subjects", "subject")
def export_to_csv(request, study_id, data_type="subjects"):
    study = get_object_or_404(Study, id=study_id)
    # Create the HttpResponse object with the appropriate CSV header.
    selected_fields = request.GET.get("fields", None)
    response = HttpResponse(content_type="text/csv; charset=utf-8")
    response["Content-Disposition"] = (
        'attachment; filename="' + data_type + "-" + get_today_midnight_date().strftime("%Y-%m-%d") + '.csv"'
    )

    if data_type == "subjects":
        data = get_subjects_as_array(study, selected_fields=selected_fields)
    elif data_type == "appointments":
        data = get_appointments_as_array(selected_fields=selected_fields)
    else:
        return e500_error(request)
    writer = csv.writer(response, quotechar=str('"'), quoting=csv.QUOTE_ALL)
    for row in data:
        writer.writerow(row)

    worker = Worker.get_by_user(request.user)
    ip = get_client_ip(request)
    p = Provenance(
        modification_author=worker,
        modification_description=f"Export {data_type} to csv",
        modified_field="",
        request_path=request.path,
        request_ip_addr=ip,
    )
    p.save()

    return response


@PermissionDecorator("export_subjects", "subject")
def export_to_excel(request, study_id, data_type="subjects"):
    study = get_object_or_404(Study, id=study_id)
    selected_fields = request.GET.get("fields", None)
    filename = data_type + "-" + get_today_midnight_date().strftime("%Y-%m-%d") + ".xls"
    if data_type == "subjects":
        data = get_subjects_as_array(study, selected_fields=selected_fields)
    elif data_type == "appointments":
        data = get_appointments_as_array(selected_fields=selected_fields)
    else:
        return e500_error(request)

    response = excel.make_response_from_array(data, "xls", file_name=filename)
    response["Content-Disposition"] = 'attachment; filename="' + filename + '"'

    worker = Worker.get_by_user(request.user)
    ip = get_client_ip(request)
    p = Provenance(
        modification_author=worker,
        modification_description=f"Export {data_type} to excel",
        modified_field="",
        request_path=request.path,
        request_ip_addr=ip,
    )
    p.save()

    return response


class CustomField:
    name = ""
    verbose_name = ""

    def __init__(self, dictionary):
        for k, v in list(dictionary.items()):
            setattr(self, k, v)


DROP_OUT_FIELD = CustomField({"verbose_name": "DROP OUT", "name": "custom-drop-out"})
APPOINTMENT_TYPE_FIELD = CustomField({"name": "appointment_types", "verbose_name": "Appointment Types"})
STUDY_SUBJECT_FIELDS = [CustomField({"name": "nd_number", "verbose_name": "Subject number"})]

SUBJECT_FIELDS = [
    CustomField({"name": "last_name", "verbose_name": "Family name"}),
    CustomField({"name": "first_name", "verbose_name": "Name"}),
]
VISIT_FIELDS = [CustomField({"name": "visit_number", "verbose_name": "Visit"})]


def filter_fields_from_selected_fields(fields, selected_fields):
    if selected_fields is None:
        return fields
    selected_fields = set(selected_fields.split(","))
    return [field for field in fields if field.name in selected_fields]


def get_default_subject_fields(study: Study):
    subject_fields = []
    for field in Subject._meta.fields:
        if field.name.upper() != "ID":
            subject_fields.append(field)
    for field in Subject._meta.many_to_many:
        subject_fields.append(field)
    for field in StudySubject._meta.fields:
        if field.name.upper() != "ID" and field.name.upper() != "SUBJECT":
            subject_fields.append(field)
    subject_fields.append(DROP_OUT_FIELD)
    for custom_field in CustomStudySubjectField.objects.filter(study=study).all():
        subject_fields.append(
            CustomField(
                {
                    "verbose_name": custom_field.name,
                    "name": get_study_subject_field_id(custom_field),
                }
            )
        )
    return subject_fields


def get_subjects_as_array(study: Study, selected_fields: str = None):
    result = []
    subject_fields = get_default_subject_fields(study)
    subject_fields = filter_fields_from_selected_fields(subject_fields, selected_fields)

    field_names = [field.verbose_name for field in subject_fields]  # faster than loop
    result.append(field_names)

    subjects = StudySubject.objects.order_by("-subject__last_name").prefetch_related(
        "subject",
        "study",
        "type",
        "flying_team",
        "default_location",
        "subject__languages",
        "subject__country",
        "subject__default_written_communication_language",
    )
    subject2row = subject_to_row_for_fields_processor(study, subject_fields)
    for subject in subjects:
        row = subject2row.subject_to_row_for_fields(subject)
        result.append([str(s).replace("\n", ";").replace("\r", ";") for s in row])
    return result


class subject_to_row_for_fields_processor:
    def __init__(self, study, subject_fields):
        self.custom_fields_map = self._get_custom_fields_map(study)
        self.subject_fields = subject_fields
        self.study_subject_fields = set()  # emtpy
        self.study_subject_subject_fields = set()  # emtpy

    # pylint: disable=R6301
    def _get_custom_fields_map(self, study):
        custom_fields = CustomStudySubjectField.objects.filter(study=study).all()
        custom_fields_map = {}
        for custom_field in custom_fields:
            custom_fields_map[get_study_subject_field_id(custom_field)] = custom_field
        return custom_fields_map

    def subject_to_row_for_fields(self, study_subject: StudySubject):

        # cache fields
        if len(self.study_subject_fields) == 0:
            for field in self.subject_fields:
                if hasattr(study_subject, field.name):
                    self.study_subject_fields.add(field.name)
                elif hasattr(study_subject.subject, field.name):
                    self.study_subject_subject_fields.add(field.name)

        # to avoid re-execution of this function (property) inside get_custom_data_value
        custom_data_values = study_subject.custom_data_values
        custom_data_values_map = {
            value.study_subject_field: value for value in custom_data_values
        }  # considerably faster than for loop

        row = []

        for field in self.subject_fields:
            cell = None

            if field.name in self.custom_fields_map:
                custom_field = self.custom_fields_map[field.name]
                if custom_field in custom_data_values_map:
                    cell = custom_data_values_map[custom_field].value

            if field == DROP_OUT_FIELD:
                if not study_subject.resigned:
                    cell = False
                else:
                    finished_appointments = (
                        Appointment.objects.filter(visit__subject=study_subject)
                        .filter(status=Appointment.APPOINTMENT_STATUS_FINISHED)
                        .count()
                    )
                    cell = finished_appointments > 0
            else:
                if field.name in self.study_subject_fields:
                    cell = getattr(study_subject, field.name)
                elif field.name in self.study_subject_subject_fields:
                    cell = getattr(study_subject.subject, field.name)
                if cell is None:
                    cell = ""
            if isinstance(cell, BaseManager):
                collection_value = ""
                for value in cell.all():
                    collection_value += str(value) + ","
                cell = collection_value
            row.append(cell)

        return row


def get_appointment_fields():
    appointments_fields = []
    for field in Appointment._meta.fields:
        if (
            field.name.upper() != "VISIT"
            and field.name.upper() != "ID"
            and field.name.upper() != "WORKER_ASSIGNED"
            and field.name.upper() != "APPOINTMENT_TYPES"
            and field.name.upper() != "ROOM"
            and field.name.upper() != "FLYING_TEAM"
        ):
            appointments_fields.append(field)

    all_fields = STUDY_SUBJECT_FIELDS + SUBJECT_FIELDS + VISIT_FIELDS + appointments_fields + [APPOINTMENT_TYPE_FIELD]

    return all_fields, appointments_fields


def get_appointments_as_array(selected_fields=None):
    result = []
    all_fields, appointments_fields = get_appointment_fields()
    all_fields = filter_fields_from_selected_fields(all_fields, selected_fields)
    appointments_fields = filter_fields_from_selected_fields(appointments_fields, selected_fields)

    field_names = [field.verbose_name for field in all_fields]  # faster than loop
    result.append(field_names)

    appointments = Appointment.objects.order_by("-datetime_when").prefetch_related(
        "visit", "visit__subject", "visit__subject__subject", "worker_assigned", "location", "appointment_types"
    )

    for appointment in appointments:
        # add field_names ['ND number', 'Family name', 'Name', 'Visit'] first
        row = []
        for field in STUDY_SUBJECT_FIELDS:
            if field.verbose_name in field_names:
                if appointment.visit is None:
                    row.append("---")
                else:
                    row.append(getattr(appointment.visit.subject, field.name))
        for field in SUBJECT_FIELDS:
            if field.verbose_name in field_names:
                if appointment.visit is None:
                    row.append("---")
                else:
                    row.append(getattr(appointment.visit.subject.subject, field.name))
        for field in VISIT_FIELDS:
            if field.verbose_name in field_names:
                if appointment.visit is None:
                    row.append("---")
                else:
                    row.append(getattr(appointment.visit, field.name))
        for field in appointments_fields:
            row.append(getattr(appointment, field.name))
        if APPOINTMENT_TYPE_FIELD.verbose_name in field_names:
            # avoid last comma in the list of appointment types
            type_string = ",".join([appointment_type.code for appointment_type in appointment.appointment_types.all()])
            row.append(type_string)
        result.append([str(s).replace("\n", ";").replace("\r", ";") for s in row])
    return result


@PermissionDecorator("export_subjects", "subject")
def export(request, study_id):
    study = get_object_or_404(Study, id=study_id)
    return wrap_response(
        request,
        "export/index.html",
        {
            "subject_fields": get_default_subject_fields(study),
            "appointment_fields": get_appointment_fields()[0],
            "study_id": study_id,
        },
    )
