# coding=utf-8
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import ListView
from django.views.generic import UpdateView

from web.decorators import PermissionDecorator
from .view_utils import WrappedView
from ..models import Location


class LocationListView(ListView, WrappedView):
    model = Location
    context_object_name = "locations"
    template_name = 'locations/list.html'

    @PermissionDecorator('change_location', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        return Location.objects.filter(removed=False)


class LocationCreateView(CreateView, WrappedView):
    model = Location
    template_name = "locations/add.html"
    fields = '__all__'
    success_url = reverse_lazy('web.views.locations')
    success_message = "Location created"

    @PermissionDecorator('change_location', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class LocationDeleteView(DeleteView, WrappedView):
    model = Location
    success_url = reverse_lazy('web.views.locations')
    template_name = 'locations/confirm_delete.html'

    def delete(self, request, *args, **kwargs):
        location_id = self.kwargs['pk']
        location = Location.objects.get(id=location_id)
        location.removed = True
        location.save()
        messages.success(request, "Location deleted")
        return HttpResponseRedirect(self.success_url)

    @PermissionDecorator('change_location', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class LocationEditView(UpdateView, WrappedView):
    model = Location
    success_url = reverse_lazy('web.views.locations')
    fields = '__all__'
    success_message = "Location edited"
    template_name = "locations/edit.html"
    context_object_name = "location"

    @PermissionDecorator('change_location', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
