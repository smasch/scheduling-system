# coding=utf-8
from django.views.generic import ListView

from .view_utils import WrappedView
from ..models import Provenance
from web.decorators import PermissionDecorator


class ProvenanceListView(ListView, WrappedView):
    model = Provenance
    context_object_name = "provenances"
    template_name = 'provenance/list.html'

    @PermissionDecorator('view_provenance', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
