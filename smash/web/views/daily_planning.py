# coding=utf-8

from django.views.generic import TemplateView

from web.decorators import PermissionDecorator
from web.models.worker_study_role import STUDY_ROLE_CHOICES
from .view_utils import wrap_response


class TemplateDailyPlannerView(TemplateView):
    @PermissionDecorator('view_daily_planning', 'daily_planning')
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['worker_study_roles'] = STUDY_ROLE_CHOICES
        return wrap_response(request, 'daily_planning/index.html', context)
