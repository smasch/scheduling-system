# coding=utf-8
import logging
from django.contrib import messages
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import DeleteView

from web.decorators import PermissionDecorator
from web.models.custom_data import CustomStudySubjectField
from web.utils import get_deleted_objects
from .view_utils import WrappedView, wrap_response
from ..forms import VisitDetailForm, SubjectAddForm, SubjectEditForm, StudySubjectAddForm, StudySubjectEditForm
from ..models import StudySubject, MailTemplate, Worker, Study, Provenance, Subject
from ..models.constants import GLOBAL_STUDY_ID, FILE_STORAGE
from ..models.custom_data import CustomStudySubjectValue
from ..models.custom_data.custom_study_subject_field import get_study_subject_field_id
from ..models.study_subject_list import SUBJECT_LIST_GENERIC, SUBJECT_LIST_NO_VISIT, SUBJECT_LIST_REQUIRE_CONTACT, \
    SUBJECT_LIST_VOUCHER_EXPIRY, SUBJECT_LIST_CHOICES
from ..utils import get_client_ip

logger = logging.getLogger(__name__)


def subject_list(request, subject_list_type):
    context = {
        "list_type": subject_list_type,
        "worker": Worker.get_by_user(request.user),
        "list_description": SUBJECT_LIST_CHOICES[subject_list_type],
        "study_id": GLOBAL_STUDY_ID,
    }
    return wrap_response(request, "subjects/index.html", context)


def subjects(request):
    return subject_list(request, SUBJECT_LIST_GENERIC)


@PermissionDecorator("add_subject", "subject")
def subject_add(request, study_id):
    study = get_object_or_404(Study, id=study_id)
    if request.method == "POST":
        study_subject_form = StudySubjectAddForm(
            request.POST, request.FILES, prefix="study_subject", user=request.user, study=study
        )
        subject_form = SubjectAddForm(request.POST, request.FILES, prefix="subject")
        if study_subject_form.is_valid() and subject_form.is_valid():
            subject = subject_form.save()
            study_subject_form.instance.subject_id = subject.id
            study_subject = study_subject_form.save()
            persist_custom_file_fields(request, study_subject)
            messages.add_message(request, messages.SUCCESS, "Subject created")
            return redirect("web.views.subject_edit", subject_id=study_subject_form.instance.id)
        else:
            messages.add_message(request, messages.ERROR, "Invalid data. Please fix data and try again.")

    else:
        study_subject_form = StudySubjectAddForm(user=request.user, prefix="study_subject", study=study)
        subject_form = SubjectAddForm(prefix="subject")

    return wrap_response(
        request, "subjects/add.html", {"study_subject_form": study_subject_form, "subject_form": subject_form}
    )


# delete subject (from all studies!)
class SubjectDeleteView(DeleteView, WrappedView):
    model = Subject
    success_url = reverse_lazy("web.views.subjects")
    template_name = "subjects/confirm_delete.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        deletable_objects, model_count, protected = get_deleted_objects([self.object])
        context["deletable_objects"] = deletable_objects
        context["model_count"] = dict(model_count).items()
        context["protected"] = protected
        return context

    @PermissionDecorator("delete_subject", "subject")
    def delete(self, request, *args, **kwargs):
        messages.success(request, "Subject deleted")
        try:
            return super().delete(request, *args, **kwargs)
        except BaseException:
            message = "There was a problem when deleting the subject. Contact system administrator."
            messages.add_message(request, messages.ERROR, message)
            logger.exception(message)
        return redirect("web.views.subjects")


# delete subject from study
class StudySubjectDeleteView(DeleteView, WrappedView):
    model = StudySubject
    success_url = reverse_lazy("web.views.subjects")
    template_name = "subjects/confirm_delete_study_subject.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        deletable_objects, model_count, protected = get_deleted_objects([self.object])
        context["deletable_objects"] = deletable_objects
        context["model_count"] = dict(model_count).items()
        context["protected"] = protected
        return context

    @PermissionDecorator("delete_studysubject", "studysubject")
    def delete(self, request, *args, **kwargs):
        messages.success(request, "Study Subject deleted")
        try:
            return super().delete(request, *args, **kwargs)
        except BaseException:
            message = "There was a problem when deleting the Study Subject. Contact system administrator."
            messages.add_message(request, messages.ERROR, message)
            logger.exception(message)
        return redirect("web.views.subjects")


def subject_no_visits(request):
    return subject_list(request, SUBJECT_LIST_NO_VISIT)


def subject_voucher_expiry(request):
    return subject_list(request, SUBJECT_LIST_VOUCHER_EXPIRY)


def subject_require_contact(request):
    return subject_list(request, SUBJECT_LIST_REQUIRE_CONTACT)


def subject_edit(request, subject_id):
    study_subject = get_object_or_404(StudySubject, id=subject_id)
    contact_attempts = study_subject.contactattempt_set.order_by("-datetime_when").all()
    was_dead = study_subject.subject.dead
    was_resigned = study_subject.resigned
    old_type = study_subject.type
    endpoint_was_reached = study_subject.endpoint_reached
    ip = get_client_ip(request)

    # save previous values of custom fields
    prev_value_list_custom_fields = []
    for field in CustomStudySubjectValue.objects.filter(study_subject=study_subject):
        prev_value_list_custom_fields.append(field.value)

    if request.method == 'POST':
        study_subject_form = StudySubjectEditForm(request.POST, request.FILES, instance=study_subject,
                                                  was_resigned=was_resigned, prefix="study_subject",
                                                  endpoint_was_reached=endpoint_was_reached)
        subject_form = SubjectEditForm(request.POST, request.FILES, instance=study_subject.subject,
                                       was_dead=was_dead, prefix="subject"
                                       )
        if study_subject_form.is_valid() and subject_form.is_valid():
            study_subject_form.save()
            subject_form.save()
            persist_custom_file_fields(request, study_subject)

            if "type" in study_subject_form.changed_data and old_type != study_subject_form.cleaned_data["type"]:
                worker = Worker.get_by_user(request.user)
                # old_value = old_type.name
                new_value = None
                if study_subject_form.cleaned_data["type"] is not None:
                    new_value = study_subject_form.cleaned_data["type"].name
                p = Provenance(
                    modified_table=StudySubject._meta.db_table,
                    modified_table_id=study_subject.id,
                    modification_author=worker,
                    previous_value=old_type,
                    new_value=study_subject_form.cleaned_data["type"],
                    modification_description=f'Worker "{worker}" changed study subject'
                    f'"{study_subject.subject}" from "{old_type}" to "{new_value}"',
                    modified_field="type",
                    request_path=request.path,
                    request_ip_addr=ip,
                )
                p.save()
            # check if subject was marked as dead or resigned
            if subject_form.cleaned_data["dead"] and not was_dead:
                worker = Worker.get_by_user(request.user)
                p = Provenance(
                    modified_table=Subject._meta.db_table,
                    modified_table_id=study_subject.subject.id,
                    modification_author=worker,
                    previous_value=was_dead,
                    new_value=True,
                    modification_description=f'Worker "{worker}" marks subject "{study_subject.subject}" as dead',
                    modified_field="dead",
                    request_path=request.path,
                    request_ip_addr=ip,
                )
                study_subject.subject.mark_as_dead()
                p.save()
            if (
                study_subject.study.columns.resigned
                and study_subject_form.cleaned_data["resigned"]
                and not was_resigned
            ):
                worker = Worker.get_by_user(request.user)
                p = Provenance(
                    modified_table=StudySubject._meta.db_table,
                    modified_table_id=study_subject.id,
                    modification_author=worker,
                    previous_value=was_resigned,
                    new_value=True,
                    modification_description=f'Worker "{worker}" marks study subject'
                    f' "{study_subject.nd_number}" as resigned '
                    f'from study "{study_subject.study}"',
                    modified_field="resigned",
                    request_path=request.path,
                    request_ip_addr=ip,
                )
                study_subject.mark_as_resigned()
                p.save()

            # loop over custom fields to add Provenance to tracked custom fields
            for field in CustomStudySubjectValue.objects.filter(study_subject=study_subject):
                field_id = get_study_subject_field_id(field.study_subject_field)
                prev_value = prev_value_list_custom_fields.pop(0)
                if field_id in study_subject_form.changed_data and field.study_subject_field.tracked:
                    curr_value = study_subject_form.cleaned_data[field_id]
                    if prev_value != curr_value:
                        worker = Worker.get_by_user(request.user)
                        mod_desc = (f'Worker "{worker}" changed study subject '
                                    f'"{study_subject.nd_number}" field '
                                    f'"{field.study_subject_field.name}"'
                                    f' value from study '
                                    f'"{study_subject.study}"')
                        p = Provenance(modified_table=StudySubject._meta.db_table,
                                       modified_table_id=study_subject.id,
                                       modification_author=worker,
                                       previous_value=prev_value,
                                       new_value=curr_value,
                                       modification_description=mod_desc,
                                       modified_field=field.study_subject_field.name,
                                       request_path=request.path,
                                       request_ip_addr=ip
                                       )
                        p.save()

            messages.success(request, "Modifications saved")
            if "_continue" in request.POST:
                return redirect("web.views.subject_edit", subject_id=study_subject.id)
            return redirect("web.views.subjects")
        else:
            messages.add_message(request, messages.ERROR, "Invalid data. Please fix data and try again.")
    else:
        study_subject_form = StudySubjectEditForm(
            instance=study_subject,
            was_resigned=was_resigned,
            prefix="study_subject",
            endpoint_was_reached=endpoint_was_reached,
        )
        subject_form = SubjectEditForm(instance=study_subject.subject, was_dead=was_dead, prefix="subject")

    languages = []
    if study_subject.subject.default_written_communication_language:
        languages.append(study_subject.subject.default_written_communication_language)
    languages.extend(study_subject.subject.languages.all())

    return wrap_response(
        request,
        "subjects/edit.html",
        {
            "n_studies": Study.objects.count(),
            "study_subject_form": study_subject_form,
            "subject_form": subject_form,
            "study_subject": study_subject,
            "contact_attempts": contact_attempts,
            "mail_templates": MailTemplate.get_subject_mail_templates(languages),
        },
    )


def persist_custom_file_fields(request, study_subject):
    for key in request.FILES:
        if key.startswith("study_subject-custom_field-"):
            file_folder = key.replace("study_subject-", "")
            field_id = int(file_folder.replace("custom_field-", ""))
            file = FILE_STORAGE.save(name=file_folder + "/" + request.FILES[key].name, content=request.FILES[key])
            study_subject.set_custom_data_value(CustomStudySubjectField.objects.get(pk=field_id), file)


def subject_visit_details(request: HttpRequest, subject_id: int) -> HttpResponse:
    study_subject_to_be_viewed = get_object_or_404(StudySubject, id=subject_id)
    visits = study_subject_to_be_viewed.visit_set.order_by("-visit_number").all()
    visits_data = []
    allow_add_visit = True
    for visit in visits:
        data = {
            "visit_form": VisitDetailForm(instance=visit),
            "appointments": visit.appointment_set.all(),
            "finished": visit.is_finished,
            "visit_id": visit.id,
            "visit_number": visit.visit_number,
        }
        visits_data.append(data)
        if not visit.is_finished:
            allow_add_visit = False
        if not study_subject_to_be_viewed.can_schedule():
            allow_add_visit = False

    return wrap_response(
        request,
        "subjects/visit_details.html",
        {"display": visits_data, "id": subject_id, "allow_add_visit": allow_add_visit},
    )
