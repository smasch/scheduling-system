# coding=utf-8

from web.models.subject_type import SubjectType
from web.decorators import PermissionDecorator
from .view_utils import wrap_response
from ..forms import StatisticsForm
from ..statistics import StatisticsManager, get_previous_year_and_month
from django.contrib import messages


@PermissionDecorator('view_statistics', 'appointment')
def statistics(request):
    statistics_manager = StatisticsManager()
    visit_choices = [("-1", "all")]
    visit_choices.extend([(rank, rank) for rank in statistics_manager.visits_ranks])
    year_previous_month, previous_month = get_previous_year_and_month()

    form = StatisticsForm(request.GET, visit_choices=visit_choices, month=previous_month, year=year_previous_month)
    if not form.is_valid():
        form.is_bound = False
    month = form.data.get('month', previous_month)
    year = form.data.get('year', year_previous_month)
    subject_type = form.data.get('subject_type', "-1")
    visit = form.data.get('visit', "-1")
    if subject_type == "-1":
        subject_type = None
    else:
        subject_type = SubjectType.objects.filter(id=subject_type).first()
        if subject_type is None:
            messages.add_message(request, messages.ERROR, 'Invalid subject type')
    if visit == "-1":
        visit = None
    monthly_statistics = statistics_manager.get_statistics_for_month(month, year, subject_type, visit)
    return wrap_response(request, 'statistics/index.html', {
        'form': form,
        'monthly_statistics': monthly_statistics
    })
