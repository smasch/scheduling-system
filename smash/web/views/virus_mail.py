# coding=utf-8
import datetime
import logging

import timeout_decorator
from django.db.models import When, Case, Min
from django_cron import CronJobBase, Schedule

from web.models import ConfigurationItem
from web.models.constants import VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE, CRON_JOB_TIMEOUT, GLOBAL_STUDY_ID
from ..models import StudySubject, Worker
from ..models.custom_data import CustomStudySubjectField
from ..smash_email import EmailSender

logger = logging.getLogger(__name__)


def count_subjects(date_when: datetime, status: str) -> int:
    result = 0
    str_date = date_when.strftime("%Y-%m-%d")
    for i in range(0, 10):
        fields = CustomStudySubjectField.objects.filter(
            study_id=GLOBAL_STUDY_ID, name=f"Visit {i} RT-PCR collection date"
        ).all()
        collect_field = None
        if len(fields) > 0:
            collect_field = fields[0]
        fields = CustomStudySubjectField.objects.filter(
            study_id=GLOBAL_STUDY_ID, name__contains=f"Virus {i} RT-PCR"
        ).all()
        status_field = None
        if len(fields) > 0:
            status_field = fields[0]
        if collect_field is not None and status_field is not None:
            print("exist")
            result += (
                StudySubject.objects.annotate(
                    custom_field_update_value=Min(
                        Case(
                            When(
                                customstudysubjectvalue__study_subject_field=collect_field,
                                then="customstudysubjectvalue__value",
                            )
                        )
                    )
                )
                .annotate(
                    custom_field_status_value=Min(
                        Case(
                            When(
                                customstudysubjectvalue__study_subject_field=status_field,
                                then="customstudysubjectvalue__value",
                            )
                        )
                    )
                )
                .filter(custom_field_update_value=str_date, custom_field_status_value=status)
                .count()
            )
    return result


def get_subject_statistics():
    date_from = datetime.datetime.now() - datetime.timedelta(days=1)

    subjects_positive_count = count_subjects(date_from, "Positive")
    subjects_negative_count = count_subjects(date_from, "Negative")
    subjects_inconclusive_count = count_subjects(date_from, "Inconclusive")

    return {
        "Positive": subjects_positive_count,
        "Negative": subjects_negative_count,
        "Inconclusive": subjects_inconclusive_count,
        "total": subjects_positive_count + subjects_negative_count + subjects_inconclusive_count,
    }


def create_statistic_email_content(data, title):
    email_body = "<h1>" + title + "</h1>"

    email_body += f"<b>Date: {datetime.datetime.now().strftime('%d.%m.%Y')}</b>" + "</br></br>"

    email_body += "In the past 24 hours " + str(data["total"]) + " donors were tested</br></br>"

    email_body += """
    <table style="border: 1px solid black; border-collapse: collapse;">
        <tr style="border: 1px solid black;">
            <th style="border: 1px solid black;">SARS-COV2 Virus</th>
            <th style="border: 1px solid black;">Number of Donors</th>
        </tr>
    """

    for status in data:
        if status != "total":
            # pylint: disable-next=consider-using-f-string
            email_body += """
            <tr style="border: 1px solid black;">
                <td style="border: 1px solid black;">{}</td>
                <td style="border: 1px solid black; text-align: right;">{}</td>
            </tr>
            """.format(
                status, data[status]
            )
    # pylint: disable-next=consider-using-f-string
    email_body += """
        <tr style="border: 1px solid black;">
            <td style="border: 1px solid black;">{}</td>
            <td style="border: 1px solid black; text-align: right;">{}</td>
        </tr>
    """.format(
        "Total", data["total"]
    )

    email_body += "</table>"

    # SARS-COV2 Virus  | Number of Donors
    # Inconclusive     |               5
    # Positive         |              15
    # Negative         |              45
    # Total            |              65

    return email_body


def send_mail(data):
    title = "SARS-COV2 Virus RT-PCR Assay Statistics"
    email_body = create_statistic_email_content(data, title)

    recipients = []
    workers = Worker.objects.all()
    for worker in workers:
        if worker.user is not None:
            if worker.user.is_active:
                if worker.email is not None and worker.email != "":
                    recipients.append(worker.email)
    cc_recipients = []
    logger.warning("Calling to send email")
    EmailSender.send_email(title, email_body, ";".join(recipients), cc_recipients)


class KitRequestEmailSendJob(CronJobBase):
    RUN_AT = []

    # noinspection PyBroadException
    try:
        times = ConfigurationItem.objects.get(type=VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE).value.split(";")
        for entry in times:
            if entry != "":
                # it's a hack assuming that we are in CEST
                text = str((int(entry.split(":")[0]) + 22) % 24) + ":" + entry.split(":")[1]
                RUN_AT.append(text)
    except BaseException as e:
        logger.debug("Cannot fetch data about email hour")
    schedule = Schedule(run_at_times=RUN_AT)
    code = "web.virus_daily_email"  # a unique code

    # pylint: disable=no-self-use
    @timeout_decorator.timeout(CRON_JOB_TIMEOUT)
    def do(self):
        data = get_subject_statistics()
        send_mail(data)
        return "mail sent"
