# coding=utf-8
import logging

from django.urls import reverse_lazy
from django.views.generic import CreateView

from web.forms.voucher_partner_session_forms import VoucherPartnerSessionForm
from web.models import Voucher
from web.models.constants import VOUCHER_STATUS_IN_USE, VOUCHER_STATUS_NEW, VOUCHER_STATUS_USED
from web.models.voucher_partner_session import VoucherPartnerSession
from .view_utils import WrappedView

logger = logging.getLogger(__name__)


class VoucherPartnerSessionCreateView(CreateView, WrappedView):
    form_class = VoucherPartnerSessionForm
    model = VoucherPartnerSession

    template_name = "voucher_partner_sessions/add.html"
    success_url = reverse_lazy('web.views.voucher_edit')
    success_message = "Voucher partner session added"

    def form_valid(self, form):
        form.instance.voucher_id = self.kwargs['pk']
        response = super().form_valid(form)
        voucher = Voucher.objects.get(pk=self.kwargs['pk'])
        if voucher.status == VOUCHER_STATUS_NEW:
            voucher.status = VOUCHER_STATUS_IN_USE
            voucher.save()
        session_time_used_in_minutes = 0
        for session in voucher.voucher_partner_sessions.all():
            session_time_used_in_minutes += session.length
        if session_time_used_in_minutes >= voucher.hours * 60:
            voucher.status = VOUCHER_STATUS_USED
            voucher.save()
        return response

    def get_success_url(self, **kwargs):
        return reverse_lazy('web.views.voucher_edit', kwargs={'pk': self.kwargs['pk']})
