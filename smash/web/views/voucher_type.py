# coding=utf-8
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import UpdateView

from web.forms import VoucherTypeForm
from web.models import VoucherType
from web.models.constants import GLOBAL_STUDY_ID
from .view_utils import WrappedView
from web.decorators import PermissionDecorator


class VoucherTypeListView(ListView, WrappedView):
    model = VoucherType
    context_object_name = "voucher_types"
    template_name = 'voucher_types/list.html'

    @PermissionDecorator('change_vouchertype', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class VoucherTypeCreateView(CreateView, WrappedView):
    form_class = VoucherTypeForm
    model = VoucherType

    template_name = "voucher_types/add.html"
    success_url = reverse_lazy('web.views.voucher_types')
    success_message = "Voucher type created"

    def form_valid(self, form):
        form.instance.study_id = GLOBAL_STUDY_ID
        return super().form_valid(form)

    @PermissionDecorator('change_vouchertype', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class VoucherTypeEditView(UpdateView, WrappedView):
    form_class = VoucherTypeForm
    model = VoucherType

    success_url = reverse_lazy('web.views.voucher_types')
    success_message = "Voucher type edited"
    template_name = "voucher_types/edit.html"
    context_object_name = "voucher_type"

    @PermissionDecorator('change_vouchertype', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
