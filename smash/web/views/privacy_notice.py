# coding=utf-8
import logging

from django.contrib import messages
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import DeleteView
from django.views.generic import ListView

from web.decorators import PermissionDecorator
from .view_utils import WrappedView, wrap_response
from ..forms.privacy_notice import PrivacyNoticeForm
from ..forms.worker_form import WorkerAcceptPrivacyNoticeForm
from ..models import PrivacyNotice, Worker

logger = logging.getLogger(__name__)


class PrivacyNoticeListView(ListView, WrappedView):
    model = PrivacyNotice
    context_object_name = "privacy_notices"
    template_name = 'privacy_notice/list.html'

    @PermissionDecorator('change_privacynotice', 'privacynotice')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


@PermissionDecorator('change_privacynotice', 'privacynotice')
def privacy_notice_add(request):
    if request.method == 'POST':
        form = PrivacyNoticeForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                form.save()
            except Exception as e:
                logger.error('Error at %s', 'division', exc_info=e)
                messages.add_message(request, messages.ERROR, 'There was a problem when saving privacy notice. '
                                                              'Contact system administrator.')
            return redirect('web.views.privacy_notices')
    else:
        form = PrivacyNoticeForm()

    return wrap_response(request, 'privacy_notice/add.html', {'form': form})


@PermissionDecorator('change_privacynotice', 'privacynotice')
def privacy_notice_edit(request, pk):
    privacy_notice = get_object_or_404(PrivacyNotice, pk=pk)
    if request.method == 'POST':
        form = PrivacyNoticeForm(request.POST, request.FILES, instance=privacy_notice)
        if form.is_valid():
            try:
                form.save()
                return redirect('web.views.privacy_notices')
            except Exception as e:
                logger.error('Error at %s', 'division', exc_info=e)
                messages.add_message(request, messages.ERROR, 'There was a problem when updating the privacy notice. '
                                                              'Contact system administrator.')
                return wrap_response(request, 'privacy_notice/edit.html',
                                     {'form': form, 'privacy_notice': privacy_notice})
    else:
        form = PrivacyNoticeForm(instance=privacy_notice)

    return wrap_response(request, 'privacy_notice/edit.html', {'form': form, 'privacy_notice': privacy_notice})


class PrivacyNoticeDeleteView(DeleteView, WrappedView):
    model = PrivacyNotice
    success_url = reverse_lazy('web.views.privacy_notices')
    template_name = 'privacy_notice/confirm_delete.html'

    @PermissionDecorator('change_privacynotice', 'privacynotice')
    def delete(self, request, *args, **kwargs):
        messages.success(request, "Privacy Notice deleted")
        return super().delete(request, *args, **kwargs)


def privacy_notice_accept(request, pk):
    privacy_notice = get_object_or_404(PrivacyNotice, pk=pk)
    worker = Worker.get_by_user(request.user)
    if request.method == 'POST':
        form = WorkerAcceptPrivacyNoticeForm(request.POST, instance=worker)
        if form.is_valid():
            # noinspection PyBroadException
            try:
                form.save()
                if form.cleaned_data['privacy_notice_accepted']:
                    messages.add_message(request, messages.SUCCESS, 'Privacy notice accepted')
                    if request.POST.get('next'):
                        return redirect(request.POST.get('next'))
                    return redirect('web.views.appointments')
                else:
                    return redirect('logout')
            except BaseException as e:
                logger.error('Error at %s', 'division', exc_info=e)
                messages.add_message(request, messages.ERROR, 'There was a problem when updating the privacy notice.'
                                                              'Contact system administrator.')
                return wrap_response(request, 'privacy_notice/acceptance_study_privacy_notice.html',
                                     {'form': form, 'privacy_notice': privacy_notice})
    else:
        form = WorkerAcceptPrivacyNoticeForm(instance=worker)

    return wrap_response(request, 'privacy_notice/acceptance_study_privacy_notice.html',
                         {'form': form, 'privacy_notice': privacy_notice})
