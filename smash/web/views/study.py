# coding=utf-8
import logging

from django.contrib import messages
from django.shortcuts import redirect, get_object_or_404

from web.decorators import PermissionDecorator
from web.forms import StudyColumnsEditForm, StudyEditForm, StudyNotificationParametersEditForm, \
    StudyRedCapColumnsEditForm
from web.forms.custom_study_subject_field_forms import CustomStudySubjectFieldAddForm, CustomStudySubjectFieldEditForm
from web.models import Study, VisitImportData, SubjectImportData
from web.models.custom_data import CustomStudySubjectField
from web.models.etl.subject_export import SubjectExportData
from web.models.etl.visit_export import VisitExportData
from web.models.study_subject_list import SUBJECT_LIST_CHOICES
from web.views.view_utils import wrap_response

logger = logging.getLogger(__name__)


@PermissionDecorator('change_study', 'configuration')
def study_edit(request, study_id):
    study = get_object_or_404(Study, id=study_id)
    if request.method == 'POST':
        study_form = StudyEditForm(request.POST, request.FILES, instance=study, prefix="study")
        notifications_form = StudyNotificationParametersEditForm(request.POST, request.FILES,
                                                                 instance=study.notification_parameters,
                                                                 prefix="notifications")
        study_columns_form = StudyColumnsEditForm(request.POST, request.FILES, instance=study.columns,
                                                  prefix="columns")
        redcap_columns_form = StudyRedCapColumnsEditForm(request.POST, request.FILES, instance=study.redcap_columns,
                                                         prefix="redcap")
        if study_form.is_valid() \
                and notifications_form.is_valid() \
                and study_columns_form.is_valid() \
                and redcap_columns_form.is_valid():
            study_form.save()
            notifications_form.save()
            study_columns_form.save()
            redcap_columns_form.save()
            messages.success(request, "Modifications saved")
            if '_continue' in request.POST:
                return redirect('web.views.edit_study', study_id=study.id)
            return redirect('web.views.appointments')
        else:
            messages.add_message(request, messages.ERROR, 'Invalid data. Please fix data and try again.')
    else:
        study_form = StudyEditForm(instance=study, prefix="study")
        notifications_form = StudyNotificationParametersEditForm(instance=study.notification_parameters,
                                                                 prefix="notifications")
        study_columns_form = StudyColumnsEditForm(instance=study.columns,
                                                  prefix="columns")

        redcap_columns_form = StudyRedCapColumnsEditForm(instance=study.redcap_columns,
                                                         prefix="redcap")

    etl_entries = get_info_about_etl_entries(study)
    return wrap_response(request, 'study/edit.html', {
        'study_form': study_form,
        'privacy_notice': study.study_privacy_notice,
        'notifications_form': notifications_form,
        'study_columns_form': study_columns_form,
        'redcap_columns_form': redcap_columns_form,
        'etl_entries': etl_entries,
        'visible_column_lists': get_info_about_visible_columns_subject_list(study),
    })


def get_info_about_etl_entries(study: Study):
    etl_entries = []
    for import_data in VisitImportData.objects.filter(study=study).all():
        etl_entries.append({'type': 'Import visit',
                            'file': import_data.filename,
                            'filetype': 'CSV',
                            'run_at': import_data.run_at_times,
                            'id': import_data.id,
                            'available': import_data.filename != '' and import_data.filename is not None,
                            'worker': str(import_data.import_worker)
                            })
    for import_data in SubjectImportData.objects.filter(study=study).all():
        etl_entries.append({'type': 'Import subject',
                            'file': import_data.filename,
                            'filetype': 'CSV',
                            'run_at': import_data.run_at_times,
                            'id': import_data.id,
                            'available': import_data.filename != '' and import_data.filename is not None,
                            'worker': str(import_data.import_worker)
                            })

    for export_data in SubjectExportData.objects.filter(study=study).all():
        etl_entries.append({'type': 'Export subject',
                            'file': export_data.filename,
                            'filetype': 'CSV',
                            'run_at': export_data.run_at_times,
                            'id': export_data.id,
                            'available': export_data.filename != '' and export_data.filename is not None,
                            'worker': 'N/A'
                            })
    for export_data in VisitExportData.objects.filter(study=study).all():
        etl_entries.append({'type': 'Export visit',
                            'file': export_data.filename,
                            'filetype': 'CSV',
                            'run_at': export_data.run_at_times,
                            'id': export_data.id,
                            'available': export_data.filename != '' and export_data.filename is not None,
                            'worker': 'N/A'
                            })
    return etl_entries


def get_info_about_visible_columns_subject_list(study: Study):
    result = []

    for entry in study.studysubjectlist_set.all():
        result.append({'type': SUBJECT_LIST_CHOICES.get(entry.type, entry.type),
                       'id': entry.id,
                       })
    result.sort(key=lambda x: x['type'])
    return result


@PermissionDecorator('change_study', 'configuration')
def custom_study_subject_field_add(request, study_id):
    study = get_object_or_404(Study, id=study_id)
    if request.method == 'POST':
        field_form = CustomStudySubjectFieldAddForm(request.POST, study=study)
        field_form.instance.study = study
        if field_form.is_valid():
            field_form.save()
            return redirect('web.views.edit_study', study_id=study.id)
    else:
        field_form = CustomStudySubjectFieldAddForm(study=study)

    return wrap_response(request, 'custom_study_subject_field/add.html', {
        'form': field_form,
        'study_id': study.id
    })


@PermissionDecorator('change_study', 'configuration')
def custom_study_subject_field_edit(request, study_id, field_id):
    study = get_object_or_404(Study, id=study_id)
    field = get_object_or_404(CustomStudySubjectField, id=field_id)
    if request.method == 'POST':
        field_form = CustomStudySubjectFieldEditForm(request.POST, instance=field)
        field_form.instance.study = study
        if field_form.is_valid():
            field_form.save()
            return redirect('web.views.edit_study', study_id=study.id)
    else:
        field_form = CustomStudySubjectFieldEditForm(instance=field)

    return wrap_response(request, 'custom_study_subject_field/edit.html', {
        'form': field_form,
        'study_id': study.id
    })


# noinspection PyUnusedLocal
@PermissionDecorator('change_study', 'configuration')
def custom_study_subject_field_delete(request, study_id, field_id):
    study = get_object_or_404(Study, id=study_id)
    field = get_object_or_404(CustomStudySubjectField, id=field_id)
    field.delete()
    return redirect('web.views.edit_study', study_id=study.id)
