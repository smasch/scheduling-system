# coding=utf-8
from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import ListView
from django.views.generic import UpdateView

from .view_utils import WrappedView
from ..models import Language
from web.decorators import PermissionDecorator
from ..utils import get_deleted_objects


class LanguageListView(ListView, WrappedView):
    model = Language
    context_object_name = "languages"
    template_name = 'languages/list.html'

    @PermissionDecorator('change_language', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class LanguageCreateView(CreateView, WrappedView):
    model = Language
    template_name = "languages/add.html"
    fields = '__all__'
    success_url = reverse_lazy('web.views.languages')
    success_message = "Language created"

    @PermissionDecorator('change_language', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class LanguageDeleteView(DeleteView, WrappedView):
    model = Language
    success_url = reverse_lazy('web.views.languages')
    template_name = 'languages/confirm_delete.html'

    def delete(self, request, *args, **kwargs):
        messages.success(request, "Language deleted")
        return super().delete(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        deletable_objects, model_count, protected = get_deleted_objects([self.get_object()])
        context['deletable_objects'] = deletable_objects
        context['model_count'] = dict(model_count).items()
        context['protected'] = protected
        return context

    @PermissionDecorator('change_language', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class LanguageEditView(UpdateView, WrappedView):
    model = Language
    success_url = reverse_lazy('web.views.languages')
    fields = '__all__'
    success_message = "Language edited"
    template_name = "languages/edit.html"
    context_object_name = "language"

    @PermissionDecorator('change_language', 'configuration')
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
