# coding=utf-8

from django.contrib import messages

from .notifications import get_missing_redcap_subjects, get_inconsistent_redcap_subjects
from .view_utils import wrap_response
from ..redcap_connector import RedcapConnector


def missing_subjects(request):
    connector = RedcapConnector()
    if connector.is_valid():
        connector.refresh_missing()
    else:
        messages.add_message(request, messages.ERROR, 'There is a problem with RED Cap connection')

    context = {
        'missing_subjects': get_missing_redcap_subjects(request.user)
    }
    return wrap_response(request, 'redcap/missing_subjects.html', context)


def inconsistent_subjects(request):
    connector = RedcapConnector()
    if connector.is_valid():
        connector.refresh_inconsistent()
    else:
        messages.add_message(request, messages.ERROR, 'There is a problem with RED Cap connection')

    context = {
        'inconsistent_subjects': get_inconsistent_redcap_subjects(request.user)
    }
    return wrap_response(request, 'redcap/inconsistent_subjects.html', context)
