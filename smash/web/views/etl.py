# coding=utf-8
import logging

from django.contrib import messages
from django.shortcuts import redirect, get_object_or_404

from web.decorators import PermissionDecorator
from web.forms.subject_export_data_form import SubjectExportDataEditForm
from web.forms.subject_import_data_form import SubjectImportDataEditForm
from web.forms.visit_export_data_form import VisitExportDataEditForm
from web.forms.visit_import_data_form import VisitImportDataEditForm
from web.importer import Importer, CsvSubjectImportReader, CsvVisitImportReader
from web.importer.csv_subject_exporter import CsvSubjectExporter
from web.importer.csv_visit_exporter import CsvVisitExporter
from web.importer.log_storage import LogStorageHandler
from web.models import Study, VisitImportData, SubjectImportData
from web.models.etl.subject_export import SubjectExportData
from web.models.etl.visit_export import VisitExportData
from web.views.view_utils import wrap_response

logger = logging.getLogger(__name__)


@PermissionDecorator('change_study', 'configuration')
def import_visit_edit(request, study_id, import_id):
    study = get_object_or_404(Study, id=study_id)
    import_data = get_object_or_404(VisitImportData, id=import_id)
    if request.method == 'POST':
        visit_import_data_form = VisitImportDataEditForm(request.POST, instance=import_data)
        if visit_import_data_form.is_valid():
            visit_import_data_form.save()
            return redirect('web.views.edit_study', study_id=study.id)
    else:
        visit_import_data_form = VisitImportDataEditForm(instance=import_data)

    return wrap_response(request, 'visit_import_data/edit.html', {
        'form': visit_import_data_form,
        'study_id': study.id
    })


@PermissionDecorator('change_study', 'configuration')
def import_visit_execute(request, study_id, import_id):
    study = get_object_or_404(Study, id=study_id)
    import_data = get_object_or_404(VisitImportData, id=import_id)
    if import_data.file_available():
        reader = CsvVisitImportReader(import_data)
        log_storage = LogStorageHandler()
        logging.getLogger('').addHandler(log_storage)
        reader.load_data()
        logging.getLogger('').removeHandler(log_storage)
        messages.add_message(request, messages.SUCCESS,
                             str(reader.processed_count) + ' appointment(s) were added/updated successfully.')
        if reader.problematic_count > 0:
            messages.add_message(request, messages.ERROR,
                                 str(reader.problematic_count) + ' problematic entries encountered.')
        if "WARNING" in log_storage.level_messages:
            for entry in log_storage.level_messages["WARNING"]:
                messages.add_message(request, messages.WARNING, entry)
    else:
        messages.add_message(request, messages.ERROR, import_data.get_absolute_file_path() + ' is not available.')

    return redirect('web.views.edit_study', study_id=study.id)


@PermissionDecorator('change_study', 'configuration')
def import_subject_edit(request, study_id, import_id):
    study = get_object_or_404(Study, id=study_id)
    import_data = get_object_or_404(SubjectImportData, id=import_id)
    if request.method == 'POST':
        subject_import_data_form = SubjectImportDataEditForm(request.POST, instance=import_data)
        if subject_import_data_form.is_valid():
            subject_import_data_form.save()
            return redirect('web.views.edit_study', study_id=study.id)
    else:
        subject_import_data_form = SubjectImportDataEditForm(instance=import_data)

    return wrap_response(request, 'subject_import_data/edit.html', {
        'form': subject_import_data_form,
        'study_id': study.id
    })


@PermissionDecorator('change_study', 'configuration')
def export_subject_edit(request, study_id: int, export_id: int):
    study = get_object_or_404(Study, id=study_id)
    export_data = get_object_or_404(SubjectExportData, id=export_id)
    if request.method == 'POST':
        subject_export_data_form = SubjectExportDataEditForm(request.POST, instance=export_data)
        if subject_export_data_form.is_valid():
            subject_export_data_form.save()
            return redirect('web.views.edit_study', study_id=study.id)
    else:
        subject_export_data_form = SubjectExportDataEditForm(instance=export_data)

    return wrap_response(request, 'subject_export_data/edit.html', {
        'form': subject_export_data_form,
        'study_id': study.id
    })


@PermissionDecorator('change_study', 'configuration')
def import_subject_execute(request, study_id, import_id):
    study = get_object_or_404(Study, id=study_id)
    import_data = get_object_or_404(SubjectImportData, id=import_id)
    if import_data.file_available():
        reader = Importer(CsvSubjectImportReader(import_data))
        log_storage = LogStorageHandler()
        logging.getLogger('').addHandler(log_storage)
        reader.execute()
        logging.getLogger('').removeHandler(log_storage)
        if reader.added_count > 0:
            messages.add_message(request, messages.SUCCESS,
                                 str(reader.added_count) + ' subject(s) were added successfully.')
        if reader.merged_count > 0:
            messages.add_message(request, messages.SUCCESS,
                                 str(reader.merged_count) + ' subject(s) were updated successfully.')
        if reader.problematic_count > 0:
            messages.add_message(request, messages.ERROR,
                                 str(reader.problematic_count) + ' problematic entries encountered.')
        if "WARNING" in log_storage.level_messages:
            for entry in log_storage.level_messages["WARNING"]:
                messages.add_message(request, messages.WARNING, entry)
    else:
        messages.add_message(request, messages.ERROR, import_data.get_absolute_file_path() + ' is not available.')

    return redirect('web.views.edit_study', study_id=study.id)


@PermissionDecorator('change_study', 'configuration')
def export_subject_execute(request, study_id: int, export_id: int):
    study = get_object_or_404(Study, id=study_id)
    export_data = get_object_or_404(SubjectExportData, id=export_id)
    if export_data.filename != '' and export_data.filename is not None:
        exporter = CsvSubjectExporter(export_data)
        exporter.execute()
    else:
        messages.add_message(request, messages.ERROR, export_data.get_absolute_file_path() + ' is not available.')

    return redirect('web.views.edit_study', study_id=study.id)


@PermissionDecorator('change_study', 'configuration')
def export_visit_edit(request, study_id, export_id):
    study = get_object_or_404(Study, id=study_id)
    export_data = get_object_or_404(VisitExportData, id=export_id)
    if request.method == 'POST':
        visit_export_data_form = VisitExportDataEditForm(request.POST, instance=export_data)
        if visit_export_data_form.is_valid():
            visit_export_data_form.save()
            return redirect('web.views.edit_study', study_id=study.id)
    else:
        visit_export_data_form = VisitExportDataEditForm(instance=export_data)

    return wrap_response(request, 'visit_export_data/edit.html', {
        'form': visit_export_data_form,
        'study_id': study.id
    })


@PermissionDecorator('change_study', 'configuration')
def export_visit_execute(request, study_id, export_id):
    study = get_object_or_404(Study, id=study_id)
    export_data = get_object_or_404(VisitExportData, id=export_id)
    if export_data.filename != '' and export_data.filename is not None:
        exporter = CsvVisitExporter(export_data)
        exporter.execute()
    else:
        messages.add_message(request, messages.ERROR, export_data.get_absolute_file_path() + ' is not available.')

    return redirect('web.views.edit_study', study_id=study.id)
