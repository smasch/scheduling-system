# coding=utf-8
from .view_utils import wrap_response
from web.decorators import PermissionDecorator


@PermissionDecorator('change_configurationitem', 'configuration')
def configuration_items(request):
    return wrap_response(request, "configuration/index.html", {})
