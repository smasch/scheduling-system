# coding=utf-8
import logging
import ntpath
from wsgiref.util import FileWrapper

from django.http import HttpResponse

from web.models.constants import FILE_STORAGE

logger = logging.getLogger(__name__)


def path_to_filename(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def download(request):
    if request.GET and request.GET.get('file'):
        path = FILE_STORAGE.location + "/" + request.GET.get('file')
        response = HttpResponse(FileWrapper(open(path, 'rb')), content_type='application/force-download')
        response['Content-Disposition'] = f'attachment; filename={path_to_filename(path)}'
        return response
