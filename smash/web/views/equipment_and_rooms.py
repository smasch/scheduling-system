from .view_utils import wrap_response


def equipment_and_rooms(request):
    return wrap_response(request, "equipment_and_rooms/index.html", {})
