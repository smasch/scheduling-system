import logging

import nexmo

from web.models import ConfigurationItem
from web.models.constants import NEXMO_API_KEY, NEXMO_API_SECRET, NEXMO_DEFAULT_FROM

logger = logging.getLogger(__name__)


class Nexmo:
    """
    Gateway for sending text messages and making phone calls using Nexmo_.

    All you need is your Nexmo Account API and Secret, as shown in your Nexmo
    account dashboard.

    ``NEXMO_API_KEY``
      Should be set to your account's API Key.

    ``NEXMO_API_SECRET``
      Should be set to your account's secret.

    ``NEXMO_DEFAULT_FROM``
      Should be set to a phone number or name.

    .. _Nexmo: http://www.nexmo.com/
    """

    def __init__(self):
        api_key = ConfigurationItem.objects.get(type=NEXMO_API_KEY).value
        api_secret = ConfigurationItem.objects.get(type=NEXMO_API_SECRET).value
        self.client = nexmo.Client(key=api_key, secret=api_secret)
        self.default_from = ConfigurationItem.objects.get(type=NEXMO_DEFAULT_FROM).value
        if self.default_from is None or self.default_from == "":
            self.default_from = "SMASCH"

    def send_sms(self, device, token):
        body = f'Your authentication token is {token}'
        phone_number = device.number.as_e164
        logger.info("Sending authentication token to %s", phone_number)
        self.client.send_message({'to': phone_number, 'from': self.default_from, 'text': body})
