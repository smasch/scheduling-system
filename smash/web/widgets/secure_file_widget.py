import logging

from django import forms
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class SecuredFileWidget(forms.FileInput):
    """A FileField Widget that shows secure file link"""

    allow_multiple_selected = False

    def __init__(self, attrs=None):
        if attrs is None:
            attrs = {}
        super().__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None):
        output = []
        if value and hasattr(value, "url"):
            url = reverse("web.views.uploaded_files") + "?file=" + str(value)
            out = '<a href="{}">{}</a><br />{} '
            output.append(out.format(url, _("Download"), _("Change:")))
        output.append(super().render(name, value, attrs, renderer=renderer))
        return mark_safe("".join(output))
