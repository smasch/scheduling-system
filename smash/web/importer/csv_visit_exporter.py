import csv
import datetime
import logging

from web.models import Appointment, Visit
from ..models.etl.etl_export_data import field_can_be_exported
from ..models.etl.visit_export import VisitExportData

logger = logging.getLogger(__name__)


class CsvVisitExporter:
    def __init__(self, export_data: VisitExportData):
        self.export_data = export_data

    def execute(self):
        with open(self.export_data.get_absolute_file_path(), "w", encoding="utf-8") as csvfile:
            writer = csv.writer(csvfile, delimiter=self.export_data.csv_delimiter, quoting=csv.QUOTE_ALL)
            writer.writerow(self.create_header())
            appointments = Appointment.objects.order_by("-id")
            for appointment in appointments:
                row = self.appointment_to_row(appointment)
                writer.writerow(row)

    def appointment_to_row(self, appointment: Appointment):
        result = []
        if appointment.visit is not None and appointment.visit.subject is not None:
            result.append(appointment.visit.subject.nd_number)
        else:
            result.append("N/A")
        for mapping in self.export_data.column_mappings.order_by("id"):
            cell = ""
            for field in Appointment._meta.get_fields():
                if field_can_be_exported(field):
                    if mapping.table_name == Appointment._meta.db_table and field.name == mapping.column_name:
                        cell = self.get_cell_value_from_object(field, appointment)
            if appointment.visit is not None:
                for field in Visit._meta.get_fields():
                    if field_can_be_exported(field):
                        if mapping.table_name == Visit._meta.db_table and field.name == mapping.column_name:
                            cell = self.get_cell_value_from_object(field, appointment.visit)

            result.append(cell)
        return result

    def get_cell_value_from_object(self, field, data):
        if field.get_internal_type() == "ForeignKey":
            if self.export_data.export_object_as_id:
                return getattr(data, field.name + "_id")
            else:
                return getattr(data, field.name)
        elif field.get_internal_type() == "ManyToManyField":
            collection_value = ""
            for element in getattr(data, field.name).all():
                if self.export_data.export_object_as_id:
                    collection_value += str(element.id) + self.export_data.collection_delimiter
                else:
                    collection_value += str(element) + self.export_data.collection_delimiter
            return collection_value
        else:
            value = getattr(data, field.name)
            if isinstance(value, datetime.date):
                return value.strftime(self.export_data.date_format)
            else:
                return value

    def create_header(self):
        result = [self.export_data.subject_id_column_name]
        for mapping in self.export_data.column_mappings.order_by("id"):
            result.append(mapping.csv_column_name)
        return result
