# coding=utf-8
import logging

import timeout_decorator
from django.db import OperationalError, ProgrammingError
from django_cron import CronJobBase, Schedule

from web.importer.csv_subject_exporter import CsvSubjectExporter
from web.importer.csv_visit_exporter import CsvVisitExporter
from web.models import Study
from web.models.constants import CRON_JOB_TIMEOUT, GLOBAL_STUDY_ID
from web.models.etl.subject_export import SubjectExportData
from web.models.etl.visit_export import VisitExportData

logger = logging.getLogger(__name__)


class SubjectExporterCronJob(CronJobBase):
    RUN_AT_TIMES = []
    try:
        item = SubjectExportData.objects.filter(study_id=GLOBAL_STUDY_ID).first()
        if item is not None:
            RUN_AT_TIMES = item.run_at_times.split(';')
    except (OperationalError, ProgrammingError):  # sqlite and postgres throw different errors here
        logger.debug('Looks like db is not initialized')
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'web.export_subjects_daily_job'  # a unique code

    def __init__(self, study_id=GLOBAL_STUDY_ID):
        super().__init__()
        self.study = Study.objects.get(pk=study_id)

    @timeout_decorator.timeout(CRON_JOB_TIMEOUT)
    def do(self):
        for export_data in SubjectExportData.objects.filter(study=self.study).all():
            if export_data.filename is None or export_data.filename == '':
                logger.info("Exporting subjects skipped. File not defined ")
                return "export file not defined"
            filename = export_data.get_absolute_file_path()
            logger.info("Exporting subjects to file: %s", filename)
            # noinspection PyBroadException
            try:
                exporter = CsvSubjectExporter(export_data)
                exporter.execute()
                return "export is successful"
            except BaseException:
                logger.exception('There was a problem with exporting data to file: %s', filename)
                return "export crashed"


class VisitExporterCronJob(CronJobBase):
    RUN_AT_TIMES = []
    try:
        item = VisitExportData.objects.filter(study_id=GLOBAL_STUDY_ID).first()
        if item is not None:
            RUN_AT_TIMES = item.run_at_times.split(';')
    except (OperationalError, ProgrammingError):  # sqlite and postgres throw different errors here
        logger.debug('Looks like db is not initialized')
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'web.export_visits_daily_job'  # a unique code

    def __init__(self, study_id=GLOBAL_STUDY_ID):
        super().__init__()
        self.study = Study.objects.get(pk=study_id)

    @timeout_decorator.timeout(CRON_JOB_TIMEOUT)
    def do(self):
        for export_data in VisitExportData.objects.filter(study=self.study).all():
            if export_data.filename is None or export_data.filename == '':
                logger.info("Exporting visits skipped. File not defined ")
                return "export file not defined"
            filename = export_data.get_absolute_file_path()
            logger.info("Exporting visits to file: %s", filename)
            # noinspection PyBroadException
            try:
                exporter = CsvVisitExporter(export_data)
                exporter.execute()
                return "export is successful"
            except BaseException:
                logger.exception('There was a problem with exporting data to file: %s', filename)
                return "export crashed"
