import logging
from typing import List

from web.importer.etl_common import EtlCommon
from web.models import SubjectImportData
from web.models.custom_data import CustomStudySubjectValue
from web.models.study_subject import StudySubject

logger = logging.getLogger(__name__)


class SubjectImportReader(EtlCommon):
    def __init__(self, import_data: SubjectImportData):
        super().__init__(import_data)
        self.import_data = import_data
        self.study_subject_custom_fields = {}

    def load_data(self) -> List[StudySubject]:
        raise NotImplementedError()

    def get_custom_fields(self, study_subject: StudySubject) -> List[CustomStudySubjectValue]:
        return self.study_subject_custom_fields.get(study_subject.nd_number, [])

    def add_custom_field(self, field_value: CustomStudySubjectValue):
        if self.study_subject_custom_fields.get(field_value.study_subject.nd_number, None) is None:
            self.study_subject_custom_fields[field_value.study_subject.nd_number] = [field_value]
        else:
            self.study_subject_custom_fields[field_value.study_subject.nd_number].append(field_value)
