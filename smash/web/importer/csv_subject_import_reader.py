import csv
import logging
from typing import List, Type, Tuple

from django.db import models
from django.db.models import Field

from web.models import StudySubject, Subject, SubjectImportData, Language, SubjectType, Country
from .etl_common import EtlCommon
from .subject_import_reader import SubjectImportReader
from ..models.custom_data import CustomStudySubjectField, CustomStudySubjectValue
from ..models.custom_data.custom_study_subject_field import get_study_subject_field_id
from ..models.etl.subject_import import field_can_be_imported

logger = logging.getLogger(__name__)


class CsvSubjectImportReader(SubjectImportReader):
    def __init__(self, import_data: SubjectImportData):
        super().__init__(import_data)
        self.mappings = {}
        self.add_mappings(Subject)
        self.add_mappings(StudySubject)
        self.add_custom_field_mappings(import_data.study)

    def load_data(self) -> List[StudySubject]:
        study_subjects = []
        with open(self.import_data.get_absolute_file_path(), encoding="utf-8") as csv_file:
            reader = csv.reader(
                (EtlCommon.remove_bom(line) for line in csv_file), delimiter=self.import_data.csv_delimiter
            )
            headers = next(reader, None)
            for header in headers:
                field = self.get_table_and_field(header)[1]
                if field is None:
                    logger.warning("Don't know how to handle column %s", header)
            for row in reader:
                subject = Subject()
                if self.import_data.country is not None:
                    subject.country = self.import_data.country
                study_subject = StudySubject()
                study_subject.subject = subject
                study_subject.default_location = self.import_data.location
                study_subject.study = self.import_data.study

                for header, value in zip(headers, row):
                    field = self.get_table_and_field(header)[1]
                    if field is not None and field.name in ("nd_number", "screening_number"):
                        self.add_data(study_subject, header, value)
                if study_subject.nd_number is None or study_subject.nd_number == "":
                    study_subject.nd_number = study_subject.screening_number
                if study_subject.screening_number is None or study_subject.screening_number == "":
                    study_subject.screening_number = study_subject.nd_number

                for header, value in zip(headers, row):
                    self.add_data(study_subject, header, value)
                study_subjects.append(study_subject)
        return study_subjects

    def add_data(self, study_subject: StudySubject, column_name: str, raw_data: str):
        table, field = self.get_table_and_field(column_name)
        value = raw_data
        if field is None:
            return
        if table == CustomStudySubjectField:
            custom_field_value = CustomStudySubjectValue()
            custom_field_value.value = value
            custom_field_value.study_subject_field = field
            custom_field_value.study_subject = study_subject
            self.add_custom_field(custom_field_value)
            return

        if field.get_internal_type() == "DateField":
            value = self.get_date(value)

        if field.get_internal_type() == "ForeignKey":
            value = self.get_value_for_foreign_field(field, value)

        if table == Subject:
            old_val = self.get_field_value(study_subject.subject, field)
            setattr(study_subject.subject, field.name, self.get_new_value(old_val, value))
        elif table == StudySubject:
            old_val = self.get_field_value(study_subject, field)
            setattr(study_subject, field.name, self.get_new_value(old_val, value))
        else:
            logger.warning("Don't know how to handle column " + column_name + " with data " + value)

    @staticmethod
    def get_value_for_foreign_field(field: Field, value: str):
        if field.related_model == Language:
            if value == "":
                return None
            else:
                language = Language.objects.filter(name=value).first()
                if language is None:
                    language = Language.objects.create(name=value)
                return language
        elif field.related_model == Country:
            if value == "":
                return None
            else:
                country = Country.objects.filter(name=value).first()
                if country is None:
                    country = Country.objects.create(name=value)
                return country
        elif field.related_model == SubjectType:
            subject_type = SubjectType.objects.filter(name=value).first()
            if subject_type is None:
                subject_type = SubjectType.objects.all().first()
                logger.warning("Subject type does not exist: '%s'. Changing to: '%s'", str(value), subject_type.name)
            return subject_type
        else:
            logger.warning("Don't know how to handle type %s", str(field.related_model))
            return None

    @staticmethod
    def get_field_value(model_object: models.Model, field: Field):
        # for foreign keys we need to check if the key id is not none, otherwise for not nullable fields exception
        # would be raised
        if field.get_internal_type() == "ForeignKey" and getattr(model_object, field.name + "_id") is None:
            return None
        return getattr(model_object, field.name)

    def get_table_and_field(self, column_name: str) -> Tuple[Type[models.Model], Field]:
        return self.mappings.get(column_name, (None, None))

    def add_mappings(self, object_type: Type[models.Model]):
        for field in object_type._meta.get_fields():
            if field_can_be_imported(field):
                found = False
                for mapping in self.import_data.column_mappings.all():
                    if mapping.table_name == object_type._meta.db_table and field.name == mapping.column_name:
                        self.mappings[mapping.csv_column_name] = object_type, field
                        found = True
                if not found:
                    self.mappings[field.name] = object_type, field

    def add_custom_field_mappings(self, study):
        for field_type in CustomStudySubjectField.objects.filter(study=study):
            field_id = get_study_subject_field_id(field_type)
            value = field_type.name.replace(" ", "_")
            found = False
            for mapping in self.import_data.column_mappings.all():
                if mapping.table_name == CustomStudySubjectField._meta.db_table and field_id == mapping.column_name:
                    self.mappings[mapping.csv_column_name] = CustomStudySubjectField, field_type
                    found = True
            if not found:
                self.mappings[value] = CustomStudySubjectField, field_type
