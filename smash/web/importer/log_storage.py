import logging


class LogStorageHandler(logging.Handler):
    level_messages = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.level_messages = {}

    def emit(self, record):
        level = record.levelname
        if level not in self.level_messages:
            self.level_messages[level] = []
        self.level_messages[level].append(self.format(record))
