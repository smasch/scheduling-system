import codecs
import datetime
import logging
from typing import Type, Optional

import pytz
from django.db import models

from web.models import Provenance
from web.models.etl.etl_data import EtlData

logger = logging.getLogger(__name__)


class EtlException(Exception):
    pass


class EtlCommon:
    def __init__(self, import_data: EtlData):
        self.etl_data = import_data

    def get_date(self, value):
        try:
            result = datetime.datetime.strptime(value, self.etl_data.date_format)
            result.replace(tzinfo=pytz.UTC)
            return result
        except ValueError:
            logger.warning("Invalid date: %s", value)
            return None

    @staticmethod
    def get_new_value(old_value: object, new_value: object) -> object:
        if old_value is None or old_value == "":
            return new_value
        if new_value is None or new_value == "":
            return old_value
        if old_value == new_value:
            return new_value
        if isinstance(new_value, str) and isinstance(old_value, str):
            if new_value in old_value:
                return old_value
            if old_value in new_value:
                return new_value
        return new_value

    def create_provenance_and_change_data(
        self,
        object_to_change: models.Model,
        field_name: str,
        new_value: object,
        object_type: Type[models.Model],
    ) -> Optional[Provenance]:
        old_value = getattr(object_to_change, field_name)
        if old_value != new_value:
            setattr(object_to_change, field_name, new_value)
            return self.create_provenance(
                field_name, new_value, object_to_change, object_type, old_value
            )
        return None

    def create_provenance(
        self,
        field_name: str,
        new_value: object,
        object_to_change: models.Model,
        object_type: Type[models.Model],
        old_value: object,
    ) -> Provenance:
        description = f'{field_name} changed from "{old_value}" to "{new_value}"'
        p = Provenance(
            modified_table=object_type._meta.db_table,
            modified_table_id=object_to_change.id,
            modification_author=self.etl_data.import_worker,
            previous_value=old_value,
            new_value=new_value,
            modification_description=description,
            modified_field=field_name,
        )
        p.save()
        return p

    def create_provenance_for_new_object(
        self, object_type: Type[models.Model], new_object: models.Model
    ) -> list:
        result = []
        for field in object_type._meta.get_fields():
            if (
                field.get_internal_type() == "CharField"
                or field.get_internal_type() == "DateField"
                or field.get_internal_type() == "IntegerField"
                or field.get_internal_type() == "DateTimeField"
                or field.get_internal_type() == "BooleanField"
            ):
                new_value = getattr(new_object, field.name)
                if new_value is not None and new_value != "":
                    p = self.create_provenance(
                        field.name, new_value, new_object, object_type, ""
                    )
                    result.append(p)
        return result

    @staticmethod
    def remove_bom(line) -> str:
        if isinstance(line, str):
            return line[3:] if line.encode("utf8").startswith(codecs.BOM_UTF8) else line
        else:
            return line[3:] if line.startswith(codecs.BOM_UTF8) else line
