# coding=utf-8
import csv
import datetime
import logging
import sys
import traceback

import pytz

from web.models import StudySubject, Visit, Appointment, Location, AppointmentTypeLink, Subject, SubjectType
from web.models.etl.visit_import import VisitImportData
from .etl_common import EtlCommon, EtlException
from .warning_counter import MsgCounterHandler

logger = logging.getLogger(__name__)


class CsvVisitImportReader(EtlCommon):
    def __init__(self, data: VisitImportData):
        super().__init__(data)
        self.import_data = data

        if data.appointment_type is None:
            logger.warning("Appointment is not assigned")

        self.problematic_count = 0
        self.processed_count = 0
        self.warning_count = 0

        if data.import_worker is None:
            logger.warning("Import user is not assigned")

    def load_data(self):
        filename = self.import_data.get_absolute_file_path()
        warning_counter = MsgCounterHandler()
        logging.getLogger("").addHandler(warning_counter)

        result = []
        with open(filename, encoding="utf-8") as csv_file:
            reader = csv.reader(
                (EtlCommon.remove_bom(line) for line in csv_file), delimiter=self.import_data.csv_delimiter
            )
            headers = next(reader, None)
            for row in reader:
                # noinspection PyBroadException
                try:
                    data = {}
                    for h, v in zip(headers, row):
                        data[h] = v

                    nd_number = self.get_study_subject_id(data)

                    study_subject = self.get_study_subject_by_id(nd_number)

                    date = self.get_visit_date(data)

                    location = self.extract_location(data)

                    visit_number = self.get_visit_number(data)

                    for i in range(1, visit_number, 1):
                        if Visit.objects.filter(subject=study_subject, visit_number=i).count() == 0:
                            logger.warning(
                                "Previous visit (visit %s) for subject %s does not exist. Creating empty",
                                str(i),
                                nd_number,
                            )
                            Visit.objects.create(
                                subject=study_subject,
                                visit_number=i,
                                datetime_begin=date + datetime.timedelta(days=-1, minutes=i),
                                datetime_end=date + datetime.timedelta(days=-1, minutes=i),
                            )
                        else:
                            break

                    visits = Visit.objects.filter(subject=study_subject, visit_number=visit_number)
                    if len(visits) > 0:
                        logger.debug("Visit for subject %s already exists. Updating", nd_number)
                        visit = visits[0]

                        self.create_provenance_and_change_data(visit, "datetime_begin", date, Visit)
                        self.create_provenance_and_change_data(
                            visit, "datetime_end", date + datetime.timedelta(days=14), Visit
                        )
                        visit.save()
                    else:
                        visit = Visit.objects.create(
                            subject=study_subject,
                            visit_number=visit_number,
                            datetime_begin=date,
                            datetime_end=date + datetime.timedelta(days=14),
                        )
                        visit.save()
                        # visit does not have id until .save() is done
                        self.create_provenance_for_new_object(Visit, visit)

                    result.append(visit)

                    appointments = Appointment.objects.filter(
                        visit=visit, appointment_types=self.import_data.appointment_type
                    )
                    if len(appointments) > 0:
                        logger.debug("Appointment for subject %s already set. Updating", nd_number)
                        appointment = appointments[0]

                        self.create_provenance_and_change_data(appointment, "length", 60, Appointment)
                        self.create_provenance_and_change_data(appointment, "datetime_when", date, Appointment)
                        self.create_provenance_and_change_data(appointment, "location", location, Appointment)

                        appointment.save()
                    else:
                        appointment = Appointment.objects.create(
                            visit=visit, length=60, datetime_when=date, location=location
                        )
                        if self.import_data.appointment_type is not None:
                            AppointmentTypeLink.objects.create(
                                appointment_id=appointment.id, appointment_type=self.import_data.appointment_type
                            )

                        appointment.save()
                        # appointment does not have id until .save() is done
                        self.create_provenance_for_new_object(Appointment, appointment)
                    self.processed_count += 1
                except BaseException:
                    self.problematic_count += 1
                    traceback.print_exc(file=sys.stdout)
                    logger.exception("Problematic data: %s", ";".join(row))

        if "WARNING" in warning_counter.level2count:
            self.warning_count = warning_counter.level2count["WARNING"]
        logging.getLogger("").removeHandler(warning_counter)

        return result

    def get_visit_date(self, data: dict) -> datetime:
        try:
            return self.extract_date(data[self.import_data.visit_date_column_name])
        except KeyError as e:
            raise EtlException("Visit date is not defined") from e

    def get_study_subject_by_id(self, nd_number: str) -> StudySubject:
        study_subjects = StudySubject.objects.filter(nd_number=nd_number, study=self.import_data.study)
        if len(study_subjects) == 0:
            logger.debug("Subject %s does not exist. Creating", nd_number)
            subject = Subject.objects.create()
            study_subject = StudySubject.objects.create(
                subject=subject,
                study=self.import_data.study,
                nd_number=nd_number,
                screening_number=nd_number,
                type=SubjectType.objects.all().first(),
            )
        else:
            study_subject = study_subjects[0]
        return study_subject

    def get_study_subject_id(self, data: dict) -> str:
        try:
            nd_number = data[self.import_data.subject_id_column_name]
            return nd_number
        except KeyError as e:
            raise EtlException("Subject id is not defined") from e

    def extract_date(self, text: str) -> datetime:
        try:
            result = datetime.datetime.strptime(text, self.import_data.date_format)
        except ValueError:
            # by default use day after tomorrow
            result = datetime.datetime.now() + datetime.timedelta(days=2)
            logger.warning("Invalid date: %s", text)
        result = result.replace(hour=9, minute=0, tzinfo=pytz.UTC)
        return result

    def extract_location(self, data: dict) -> Location:
        try:
            text = data[self.import_data.location_column_name]
            locations = Location.objects.filter(name=text)
            if len(locations) > 0:
                return locations[0]
            else:
                logger.warning("Location with name does not exist: %s", text)
                return Location.objects.create(name=text)
        except KeyError as e:
            raise EtlException("Location is not defined") from e

    def get_summary(self):
        result = "<p>Number of successfully added appointments: <b>" + str(self.processed_count) + "</b></p>"
        style = ""
        if self.problematic_count > 0:
            style = ' color="red" '
        result += (
            "<p><font "
            + style
            + ">Number of problematic appointments: <b>"
            + str(self.problematic_count)
            + "</b></font></p>"
        )
        style = ""
        if self.warning_count > 0:
            style = ' color="brown" '
        result += "<p><font " + style + ">Number of raised warnings: <b>" + str(self.warning_count) + "</b></font></p>"
        return result

    def get_visit_number(self, data: dict) -> int:
        try:
            visit_number = data[self.import_data.visit_number_column_name]
            visit_number = int(visit_number) + (1 - self.import_data.study.redcap_first_visit_number)
            if visit_number < 1:
                logger.warning(
                    "Visit number is invalid. Visit number should start from: %d.",
                    self.import_data.study.redcap_first_visit_number,
                )
                visit_number = 1
            return visit_number
        except KeyError as e:
            raise EtlException("Visit number is not defined") from e
