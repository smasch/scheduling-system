# coding=utf-8
import datetime
import logging
import os
import os.path
import traceback

import timeout_decorator
from django.db import OperationalError, ProgrammingError
from django_cron import CronJobBase, Schedule

from web.models import ConfigurationItem, Study, VisitImportData, SubjectImportData
from web.models.constants import CRON_JOB_TIMEOUT, DEFAULT_FROM_EMAIL, GLOBAL_STUDY_ID
from web.smash_email import EmailSender
from web.importer import CsvSubjectImportReader
from web.importer.csv_visit_import_reader import CsvVisitImportReader
from web.importer.importer import Importer

logger = logging.getLogger(__name__)


class SubjectImporterCronJob(CronJobBase):
    RUN_AT_TIMES = []
    try:
        item = SubjectImportData.objects.filter(study_id=GLOBAL_STUDY_ID).first()
        if item is not None:
            RUN_AT_TIMES = item.run_at_times.split(";")
    except (OperationalError, ProgrammingError):  # sqlite and postgres throw different errors here
        logger.debug("Looks like db is not initialized")
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = "web.import_subjects_daily_job"  # a unique code

    def __init__(self, study_id=GLOBAL_STUDY_ID):
        super().__init__()
        self.study = Study.objects.get(pk=study_id)

    @timeout_decorator.timeout(CRON_JOB_TIMEOUT)
    def do(self):
        email_title = "Subjects daily import"
        email_recipients = ConfigurationItem.objects.get(type=DEFAULT_FROM_EMAIL).value

        for import_data in SubjectImportData.objects.filter(study=self.study).all():
            if import_data.filename is None or import_data.filename == "":
                logger.info("Importing subjects skipped. File not defined ")
                return "import file not defined"
            filename = import_data.get_absolute_file_path()
            logger.info("Importing subjects from file: %s", filename)
            if not os.path.isfile(filename):
                content = (
                    "<h3><font color='red'>File with imported data is not available in the system: "
                    + filename
                    + "</font></h3>"
                )
                EmailSender.send_email(email_title, content, email_recipients)
                return "import file not found"
            # noinspection PyBroadException
            try:
                importer = Importer(CsvSubjectImportReader(import_data))
                importer.execute()
                email_body = importer.get_summary()
                EmailSender.send_email(
                    email_title,
                    f"<h3>Data was successfully imported from file: {filename}</h3>{email_body}",
                    email_recipients,
                )
                backup_file(filename)
                return "import is successful"

            except BaseException:
                tb = traceback.format_exc()
                EmailSender.send_email(
                    email_title,
                    f"<h3><font color='red'>There was a problem with importing data from file: "
                    f"{filename}</font></h3><pre>{tb}</pre>",
                    email_recipients,
                )
                logger.exception("There was a problem with importing data from file: %s", filename)
                return "import crashed"


class VisitImporterCronJob(CronJobBase):
    RUN_AT_TIMES = []
    try:
        item = VisitImportData.objects.filter(study_id=GLOBAL_STUDY_ID).first()
        if item is not None:
            RUN_AT_TIMES = item.run_at_times.split(";")
    except (OperationalError, ProgrammingError):  # sqlite and postgres throw different errors here
        logger.debug("Looks like db is not initialized")
    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = "web.import_visits_daily_job"  # a unique code

    def __init__(self, study_id=GLOBAL_STUDY_ID):
        super().__init__()
        self.study = Study.objects.get(pk=study_id)

    @timeout_decorator.timeout(CRON_JOB_TIMEOUT)
    def do(self):
        email_title = "Visits daily import"
        email_recipients = ConfigurationItem.objects.get(type=DEFAULT_FROM_EMAIL).value

        for import_data in VisitImportData.objects.filter(study=self.study).all():
            if import_data.filename is None or import_data.filename == "":
                logger.info("Importing visits skipped. File not defined ")
                return "import file not defined"
            filename = import_data.get_absolute_file_path()
            logger.info("Importing visits from file: %s", filename)
            if not import_data.file_available():
                content = (
                    "<h3><font color='red'>File with imported data is not available in the system: "
                    + import_data.filename
                    + "</font></h3>"
                )
                EmailSender.send_email(email_title, content, email_recipients)
                return "import file not found"

            # noinspection PyBroadException
            try:
                importer = CsvVisitImportReader(import_data)
                importer.load_data()
                email_body = importer.get_summary()
                EmailSender.send_email(
                    email_title,
                    f"<h3>Data was successfully imported from file: {filename}</h3>{email_body}",
                    email_recipients,
                )
                backup_file(filename)
                return "import is successful"

            except BaseException:
                tb = traceback.format_exc()
                EmailSender.send_email(
                    email_title,
                    "<h3><font color='red'>There was a problem with importing data from file: "
                    f"{filename}</font></h3><pre>{tb}</pre>",
                    email_recipients,
                )
                logger.exception("There was a problem with importing data from file: %s", filename)
                return "import crashed"


def backup_file(filename):
    new_file = filename + "-" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M") + ".bac"
    os.rename(filename, new_file)
