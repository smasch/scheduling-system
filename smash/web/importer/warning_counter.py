import logging


class MsgCounterHandler(logging.Handler):
    level2count = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.level2count = {}

    def emit(self, record):
        level = record.levelname
        if level not in self.level2count:
            self.level2count[level] = 0
        self.level2count[level] += 1
