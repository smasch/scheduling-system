from django.test import TestCase

from web.forms import AvailabilityAddForm
from web.models import Worker
from web.models.constants import MONDAY_AS_DAY_OF_WEEK
from web.tests.functions import create_user, create_visit
from web.tests.functions import get_test_location


class AvailabilityAddFormTests(TestCase):
    def setUp(self):
        self.user = create_user()

        worker = Worker.get_by_user(self.user)
        worker.locations.set([get_test_location()])
        worker.save()

        self.visit = create_visit()

        self.sample_data = {'person': worker.id,
                            'available_from': '8:00',
                            'available_till': "9:00",
                            'day_number': MONDAY_AS_DAY_OF_WEEK,
                            }

    def test_validation(self):
        form = AvailabilityAddForm(data=self.sample_data)
        self.assertTrue(form.is_valid())

    def test_invalid(self):
        form = AvailabilityAddForm(data=self.sample_data)
        form.save()
        form = AvailabilityAddForm(data=self.sample_data)
        self.assertFalse(form.is_valid())

    def test_complex_invalid(self):
        form = AvailabilityAddForm(data=self.sample_data)
        form.save()
        self.sample_data["available_from"] = '10:00'
        self.sample_data["available_till"] = '11:00'
        form = AvailabilityAddForm(data=self.sample_data)
        form.save()

        self.sample_data["available_from"] = '7:00'
        self.sample_data["available_till"] = '20:00'
        form = AvailabilityAddForm(data=self.sample_data)
        self.assertFalse(form.is_valid())
