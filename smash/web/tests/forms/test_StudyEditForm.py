from django.test import TestCase

from web.forms.study_forms import StudyEditForm
from web.models.study import Study
from web.models.study_subject import StudySubject
from web.tests.functions import get_test_study, create_study_subject


class StudyEditFormTests(TestCase):

    def test_study_default_regex(self):
        # this will add a StudySubject with a ND number
        StudySubject.objects.all().delete()
        create_study_subject(nd_number='ND0001')
        form = StudyEditForm()
        form.instance = get_test_study()
        # set default regex
        nd_number_study_subject_regex_default = Study._meta.get_field(
            'nd_number_study_subject_regex').get_default()
        form.cleaned_data = {
            'nd_number_study_subject_regex': nd_number_study_subject_regex_default}

        self.assertTrue(form.clean()['nd_number_study_subject_regex'] == nd_number_study_subject_regex_default)
        # test wrong regex
        form = StudyEditForm()
        form.instance = get_test_study()
        nd_number_study_subject_regex_default = r'^nd\d{5}$'
        form.cleaned_data = {
            'nd_number_study_subject_regex': nd_number_study_subject_regex_default}
        self.assertFalse(form.is_valid())

    def test_study_other_regex(self):
        StudySubject.objects.all().delete()
        # this will add a StudySubject with a ND number
        create_study_subject(nd_number='nd00001')
        form = StudyEditForm()
        form.instance = get_test_study()
        # test new regex
        nd_number_study_subject_regex_default = r'^nd\d{5}$'
        form.cleaned_data = {'nd_number_study_subject_regex': nd_number_study_subject_regex_default}
        self.assertEqual(form.clean()['nd_number_study_subject_regex'], nd_number_study_subject_regex_default)
