import logging

from django.test import TestCase

from web.forms import WorkerForm
from web.models.constants import COUNTRY_OTHER_ID
from web.models.worker_study_role import ROLE_CHOICES_TECHNICIAN

logger = logging.getLogger(__name__)


class VoucherFormTests(TestCase):

    def test_password_miss_match_on_create(self):
        worker_data = VoucherFormTests.create_valid_worker_data()
        worker_data["password"] = "test123456x"
        worker_data["password2"] = "test123456y"

        worker_form = WorkerForm(data=worker_data)

        self.assertFalse(worker_form.is_valid())
        self.assertTrue("password2" in worker_form.errors)

    def test_password_to_short_on_create(self):
        worker_data = VoucherFormTests.create_valid_worker_data()
        worker_data["password"] = "x"
        worker_data["password2"] = "x"

        worker_form = WorkerForm(data=worker_data)

        self.assertFalse(worker_form.is_valid())
        self.assertTrue("password" in worker_form.errors)

    def test_password_to_easy_on_create(self):
        worker_data = VoucherFormTests.create_valid_worker_data()
        worker_data["password"] = "xxxxxxxxxx"
        worker_data["password2"] = "xxxxxxxxxx"

        worker_form = WorkerForm(data=worker_data)

        self.assertFalse(worker_form.is_valid())
        self.assertTrue("password" in worker_form.errors)

    def test_password_to_easy_on_create_2(self):
        worker_data = VoucherFormTests.create_valid_worker_data()
        worker_data["password"] = "2222222222"
        worker_data["password2"] = "2222222222"

        worker_form = WorkerForm(data=worker_data)

        self.assertFalse(worker_form.is_valid())
        self.assertTrue("password" in worker_form.errors)

    def test_invalid_login_on_create(self):
        worker_data = VoucherFormTests.create_valid_worker_data()
        worker_data["login"] = "some login - bla"

        worker_form = WorkerForm(data=worker_data)

        self.assertFalse(worker_form.is_valid())
        self.assertTrue("login" in worker_form.errors)

    def test_valid_form(self):
        worker_data = VoucherFormTests.create_valid_worker_data()
        worker_form = WorkerForm(data=worker_data)

        worker_form.is_valid()
        self.assertTrue(worker_form.is_valid())

    @staticmethod
    def create_valid_worker_data():
        form_data = {
            "login": "test.login",
            "password": "test123456",
            "password2": "test123456",
            "last_name": "Surname",
            "first_name": "Given name",
            "country": COUNTRY_OTHER_ID,
            "role": ROLE_CHOICES_TECHNICIAN,
        }
        return form_data
