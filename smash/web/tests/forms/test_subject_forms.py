import logging

from django.test import TestCase

from web.forms.subject_forms import is_valid_social_security_number
from web.tests.functions import create_subject

logger = logging.getLogger(__name__)


class SubjectFormsTests(TestCase):
    def setUp(self):
        super().setUp()
        self.subject = create_subject()

    def test_is_valid_social_security_number_too_short(self):
        self.assertFalse(is_valid_social_security_number("123"))

    def test_is_valid_social_security_number_not_a_number(self):
        # noinspection SpellCheckingInspection
        self.assertFalse(is_valid_social_security_number("ABCDEFGHIJKLM"))

    def test_is_valid_social_security_number_invalid(self):
        self.assertFalse(is_valid_social_security_number("1234567890123"))

    def test_is_valid_social_security_number(self):
        self.assertTrue(is_valid_social_security_number("1893120105732"))

    def test_is_valid_social_security_number_empty(self):
        self.assertTrue(is_valid_social_security_number(""))
