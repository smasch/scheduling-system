import logging

from django.test import TestCase

from web.forms.visit_import_data_form import VisitImportDataEditForm
from web.models import VisitImportData
from web.tests.functions import format_form_field, create_worker, get_test_study

logger = logging.getLogger(__name__)


class VisitImportDataEditFormTests(TestCase):
    def setUp(self):
        super().setUp()
        self.visit_import_data = VisitImportData.objects.create(study=get_test_study(),
                                                                import_worker=create_worker())

    def test_invalid_run_at_times(self):
        form_data = self.get_form_data(self.visit_import_data)
        form_data["run_at_times"] = "bla"

        voucher_form = VisitImportDataEditForm(instance=self.visit_import_data, data=form_data)

        self.assertFalse(voucher_form.is_valid())
        self.assertTrue("run_at_times" in voucher_form.errors)

    def test_valid_form(self):
        form_data = self.get_form_data(self.visit_import_data)

        voucher_form = VisitImportDataEditForm(instance=self.visit_import_data, data=form_data)

        self.assertTrue(voucher_form.is_valid())

    @staticmethod
    def get_form_data(visit_import_data: VisitImportData) -> dict:
        voucher_form = VisitImportDataEditForm(instance=visit_import_data)
        form_data = {}
        for key, value in list(voucher_form.initial.items()):
            form_data[key] = format_form_field(value)
        return form_data
