import logging

from parameterized import parameterized

from web.forms import StudySubjectAddForm
from web.forms.study_subject_forms import get_new_screening_number, get_study_subject_field_id
from web.models.constants import CUSTOM_FIELD_TYPE_TEXT, CUSTOM_FIELD_TYPE_BOOLEAN, \
    CUSTOM_FIELD_TYPE_INTEGER, CUSTOM_FIELD_TYPE_DOUBLE, CUSTOM_FIELD_TYPE_DATE, CUSTOM_FIELD_TYPE_SELECT_LIST
from web.models.custom_data import CustomStudySubjectField, CustomStudySubjectValue
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_subject, get_test_study, create_empty_study, \
    get_control_subject_type

logger = logging.getLogger(__name__)


class StudySubjectAddFormTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()

        location = self.worker.locations.all().first()
        self.subject = create_subject()
        self.study = get_test_study()
        self.sample_data = {
            'type': get_control_subject_type().id,
            'default_location': location.id,
            'screening_number': "123",
            'subject': self.subject.id,
            'postponed': False
        }

    def test_validation(self):
        form = StudySubjectAddForm(data=self.sample_data, user=self.user, study=self.study)
        form.is_valid()
        self.assertTrue(form.is_valid())

    @parameterized.expand([
        ('text', CUSTOM_FIELD_TYPE_TEXT, 'bla', 'bla'),
        ('bool', CUSTOM_FIELD_TYPE_BOOLEAN, True, 'True'),
        ('int', CUSTOM_FIELD_TYPE_INTEGER, 104, '104'),
        ('double', CUSTOM_FIELD_TYPE_DOUBLE, 201.25, '201.25'),
        ('date', CUSTOM_FIELD_TYPE_DATE, '2020-11-03', '2020-11-03'),
        ('select list', CUSTOM_FIELD_TYPE_SELECT_LIST, '1', 'BLA', 'BLA;BLA-BLA'),
    ])
    def test_add_with_custom_field(self, _, field_type, value, expected_serialized_value, possible_values=''):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="",
                                                       type=field_type, possible_values=possible_values)
        count = CustomStudySubjectValue.objects.filter(study_subject_field=field).count()

        self.sample_data[get_study_subject_field_id(field)] = value

        form = StudySubjectAddForm(data=self.sample_data, user=self.user, study=self.study)
        form.instance.subject_id = create_subject().id
        self.assertTrue(form.is_valid())
        subject = form.save()

        self.assertEqual(count + 1, CustomStudySubjectValue.objects.filter(study_subject_field=field).count())
        self.assertEqual(expected_serialized_value, subject.get_custom_data_value(field).value)

    def test_validation_for_study_without_columns(self):
        form = StudySubjectAddForm(data=self.sample_data, user=self.user, study=create_empty_study())
        self.assertTrue(form.is_valid())

    def test_validate_nd_number(self):
        self.sample_data['nd_number'] = 'invalid nd number'
        form = StudySubjectAddForm(data=self.sample_data, user=self.user, study=self.study)
        self.assertFalse(form.is_valid())
        self.assertTrue("nd_number" in form.errors)

    def test_invalid(self):
        form_data = self.sample_data
        form_data['screening_number'] = "123"

        form = StudySubjectAddForm(data=form_data, user=self.user, study=self.study)
        self.assertTrue(form.is_valid())
        form.instance.subject_id = self.subject.id
        form.save()

        form2 = StudySubjectAddForm(data=form_data, user=self.user, study=self.study)
        validation_status = form2.is_valid()
        self.assertFalse(validation_status)
        self.assertTrue("screening_number" in form2.errors)

    def test_invalid_2(self):
        form_data = self.sample_data
        form_data['nd_number'] = "ND0123"

        form = StudySubjectAddForm(data=form_data, user=self.user, study=self.study)
        form.is_valid()
        self.assertTrue(form.is_valid())
        form.instance.subject_id = self.subject.id
        form.save()

        form_data['screening_number'] = "2"
        form2 = StudySubjectAddForm(data=form_data, user=self.user, study=self.study)
        validation_status = form2.is_valid()
        self.assertFalse(validation_status)
        self.assertTrue("nd_number" in form2.errors)

    def test_get_new_screening_number(self):
        prefix = "X-"
        subject = create_study_subject()
        subject.screening_number = prefix + "300"
        subject.save()

        new_screening_number = get_new_screening_number(prefix)
        self.assertEqual(prefix + "301", new_screening_number)

    def test_get_new_screening_number_2(self):
        prefix = "X-"
        subject = create_study_subject()
        subject.screening_number = "L-1111; " + prefix + "300"
        subject.save()

        new_screening_number = get_new_screening_number(prefix)
        self.assertEqual(prefix + "301", new_screening_number)

    def test_get_new_screening_number_3(self):
        prefix = "X-"
        subject = create_study_subject()
        subject.screening_number = "P-1112; " + prefix + "300" + "; L-1111"
        subject.save()

        new_screening_number = get_new_screening_number(prefix)
        self.assertEqual(prefix + "301", new_screening_number)

    def test_get_new_screening_number_4(self):
        prefix = "X-"
        subject = create_study_subject()
        subject.screening_number = "P-1112; "
        subject.save()

        new_screening_number = get_new_screening_number(prefix)
        self.assertEqual(prefix + "001", new_screening_number)

    def test_get_new_screening_number_5(self):
        prefix = "X-"
        subject = create_study_subject()
        subject.screening_number = prefix + "100"
        subject.save()

        subject = create_study_subject()
        subject.screening_number = prefix + "200"
        subject.save()

        subject = create_study_subject()
        subject.screening_number = "Y-300"
        subject.save()

        subject = create_study_subject()
        subject.screening_number = prefix + "20"
        subject.save()

        new_screening_number = get_new_screening_number(prefix)
        self.assertEqual(prefix + "201", new_screening_number)

    def test_get_new_screening_number_6(self):
        prefix = "X-"
        subject = create_study_subject()
        subject.screening_number = "X-"
        subject.save()

        new_screening_number = get_new_screening_number(prefix)
        self.assertEqual(prefix + "001", new_screening_number)

    def test_form_with_readonly_custom_field(self):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz", readonly=True,
                                                       type=CUSTOM_FIELD_TYPE_TEXT)

        form = StudySubjectAddForm(user=self.user, study=self.study)

        self.assertTrue(form.fields[get_study_subject_field_id(field)].disabled)

    def test_form_with_unique_custom_field(self):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz", unique=True,
                                                       type=CUSTOM_FIELD_TYPE_TEXT)

        study_subject = create_study_subject()
        study_subject.set_custom_data_value(field, "bla")

        self.sample_data[get_study_subject_field_id(field)] = "bla2"
        form = StudySubjectAddForm(data=self.sample_data, user=self.user, study=self.study)
        self.assertTrue(form.is_valid())

        self.sample_data[get_study_subject_field_id(field)] = "bla"
        form = StudySubjectAddForm(data=self.sample_data, user=self.user, study=self.study)
        self.assertFalse(form.is_valid())
