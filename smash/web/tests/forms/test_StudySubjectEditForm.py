import logging

from parameterized import parameterized

from web.forms import StudySubjectEditForm
from web.models import StudySubject
from web.models.constants import CUSTOM_FIELD_TYPE_TEXT, CUSTOM_FIELD_TYPE_BOOLEAN, CUSTOM_FIELD_TYPE_INTEGER, \
    CUSTOM_FIELD_TYPE_DOUBLE, CUSTOM_FIELD_TYPE_DATE, CUSTOM_FIELD_TYPE_SELECT_LIST, CUSTOM_FIELD_TYPE_FILE
from web.models.custom_data import CustomStudySubjectField
from web.models.custom_data.custom_study_subject_field import get_study_subject_field_id
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_empty_study, get_test_study

logger = logging.getLogger(__name__)


class StudySubjectEditFormTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study_subject = create_study_subject()
        self.study = get_test_study()

        location = self.worker.locations.all()[0]
        self.sample_data = {
            'type': self.study_subject.type,
            'default_location': location.id,
            'screening_number': self.study_subject.screening_number,
            'nd_number': self.study_subject.nd_number,
            'subject': self.study_subject.subject.id,
            'id': self.study_subject.id,
            'postponed': False
        }

    def tearDown(self):
        StudySubject.objects.all().delete()

    def test_validation(self):
        edit_form = StudySubjectEditForm(self.sample_data, instance=self.study_subject)
        save_status = edit_form.is_valid()
        self.assertTrue(save_status)

    def test_validation_with_empty_study(self):
        self.study_subject.study = create_empty_study()
        self.study_subject.save()

        edit_form = StudySubjectEditForm(self.sample_data, instance=self.study_subject)
        save_status = edit_form.is_valid()
        self.assertTrue(save_status)

    def test_invalid_nd_number_edit(self):
        study_subject2 = create_study_subject(124)
        study_subject2.nd_number = "ND0124"
        study_subject2.screening_number = "124"
        study_subject2.save()

        self.sample_data['nd_number'] = "ND0124"
        edit_form = StudySubjectEditForm(self.sample_data, instance=self.study_subject)

        save_status = edit_form.is_valid()
        self.assertTrue("nd_number" in edit_form.errors)
        self.assertFalse(save_status)

    @parameterized.expand([
        ('text', CUSTOM_FIELD_TYPE_TEXT, 'bla', 'bla'),
        ('bool', CUSTOM_FIELD_TYPE_BOOLEAN, True, 'True'),
        ('int', CUSTOM_FIELD_TYPE_INTEGER, 10, '10'),
        ('double', CUSTOM_FIELD_TYPE_DOUBLE, 30.5, '30.5'),
        ('date', CUSTOM_FIELD_TYPE_DATE, '2020-11-05', '2020-11-05'),
        ('select list', CUSTOM_FIELD_TYPE_SELECT_LIST, '1', 'BLA', 'BLA;BLA-BLA'),
        ('select list empty value', CUSTOM_FIELD_TYPE_SELECT_LIST, '0', '', 'BLA;BLA-BLA'),
    ])
    def test_edit_with_custom_field(self, _, field_type, value, expected_serialized_value, possible_values=''):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="",
                                                       type=field_type, possible_values=possible_values)

        self.assertEqual(field.default_value, self.study_subject.get_custom_data_value(field).value)

        self.sample_data[get_study_subject_field_id(field)] = value

        edit_form = StudySubjectEditForm(self.sample_data, instance=self.study_subject)
        self.assertTrue(edit_form.is_valid())
        subject = edit_form.save()

        self.assertEqual(expected_serialized_value, subject.get_custom_data_value(field).value)

    @parameterized.expand([
        ('text', CUSTOM_FIELD_TYPE_TEXT, 'bla', 'bla'),
        ('bool', CUSTOM_FIELD_TYPE_BOOLEAN, True, 'True'),
        ('int', CUSTOM_FIELD_TYPE_INTEGER, 10, '10'),
        ('double', CUSTOM_FIELD_TYPE_DOUBLE, 30.5, '30.5'),
        ('date', CUSTOM_FIELD_TYPE_DATE, '2020-11-05', '2020-11-05'),
        ('select list', CUSTOM_FIELD_TYPE_SELECT_LIST, '1', 'BLA', 'BLA;BLA-BLA'),
        ('select list empty value', CUSTOM_FIELD_TYPE_SELECT_LIST, '0', '', 'BLA;BLA-BLA'),
        ('file', CUSTOM_FIELD_TYPE_FILE, 'bla.txt', 'bla.txt'),
    ])
    def test_initial_edit_with_custom_field(self, _, field_type, expected_initial_value, value, possible_values=''):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value=value,
                                                       type=field_type, possible_values=possible_values)

        form = StudySubjectEditForm(instance=self.study_subject)

        self.assertTrue(form[get_study_subject_field_id(field)].initial, expected_initial_value)

    def test_form_with_readonly_custom_field(self):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz", readonly=True,
                                                       type=CUSTOM_FIELD_TYPE_TEXT)

        form = StudySubjectEditForm(instance=self.study_subject)

        self.assertTrue(form.fields[get_study_subject_field_id(field)].disabled)

    def test_form_with_unique_custom_field(self):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz", unique=True,
                                                       type=CUSTOM_FIELD_TYPE_TEXT)

        study_subject2 = create_study_subject()
        study_subject2.set_custom_data_value(field, "bla")

        self.sample_data[get_study_subject_field_id(field)] = "bla2"
        form = StudySubjectEditForm(self.sample_data, instance=self.study_subject)
        self.assertTrue(form.is_valid())

        self.sample_data[get_study_subject_field_id(field)] = "bla"
        form = StudySubjectEditForm(self.sample_data, instance=self.study_subject)
        self.assertFalse(form.is_valid())
