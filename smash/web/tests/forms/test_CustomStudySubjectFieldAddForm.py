from django.test import TestCase
from parameterized import parameterized

from web.forms.custom_study_subject_field_forms import CustomStudySubjectFieldAddForm
from web.models.constants import (
    CUSTOM_FIELD_TYPE_TEXT,
    CUSTOM_FIELD_TYPE_BOOLEAN,
    CUSTOM_FIELD_TYPE_INTEGER,
    CUSTOM_FIELD_TYPE_DOUBLE,
    CUSTOM_FIELD_TYPE_DATE,
    CUSTOM_FIELD_TYPE_SELECT_LIST,
    CUSTOM_FIELD_TYPE_FILE,
)
from web.models.custom_data import CustomStudySubjectField
from web.tests.functions import get_test_study


class CustomStudySubjectFieldAddFormTest(TestCase):

    @parameterized.expand(
        [
            ("text", CUSTOM_FIELD_TYPE_TEXT, "bla", True),
            ("bool valid", CUSTOM_FIELD_TYPE_BOOLEAN, "True", True),
            ("bool invalid", CUSTOM_FIELD_TYPE_BOOLEAN, "bla", False),
            ("int valid", CUSTOM_FIELD_TYPE_INTEGER, "102", True),
            ("int invalid", CUSTOM_FIELD_TYPE_INTEGER, "bla", False),
            ("double valid", CUSTOM_FIELD_TYPE_DOUBLE, "202.25", True),
            ("double invalid", CUSTOM_FIELD_TYPE_DOUBLE, "bla", False),
            ("date valid", CUSTOM_FIELD_TYPE_DATE, "2021-01-20", True),
            ("date invalid", CUSTOM_FIELD_TYPE_DATE, "bla", False),
            (
                "select list valid",
                CUSTOM_FIELD_TYPE_SELECT_LIST,
                "abc",
                True,
                "abc;def;xyz",
            ),
            (
                "select list invalid",
                CUSTOM_FIELD_TYPE_SELECT_LIST,
                "bla",
                False,
                "abc;def",
            ),
            ("file", CUSTOM_FIELD_TYPE_FILE, None, True),
            ("file invalid", CUSTOM_FIELD_TYPE_FILE, "tmp", False),
            ("text", CUSTOM_FIELD_TYPE_TEXT, "bla", True, True),
        ]
    )
    def test_add_field(
        self, _, field_type, default_value, valid, possible_values="", tracked=False
    ):
        sample_data = {
            "default_value": default_value,
            "name": "1. name",
            "type": field_type,
            "possible_values": possible_values,
            "tracked": tracked,
        }

        form = CustomStudySubjectFieldAddForm(sample_data, study=get_test_study())
        if valid:
            self.assertTrue(form.is_valid())
            field = form.save()

            self.assertEqual(
                1, CustomStudySubjectField.objects.filter(id=field.id).count()
            )
            self.assertEqual(default_value, field.default_value)
            self.assertEqual(tracked, field.tracked)
        else:
            self.assertFalse(form.is_valid())
