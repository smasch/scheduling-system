# coding=utf-8
from django.test import TestCase

from web.forms import AppointmentAddForm
from web.models import Worker
from web.tests.functions import create_user, create_visit, create_location
from web.tests.functions import get_test_location


class AppointmentAddFormTests(TestCase):
    def setUp(self):
        location = get_test_location()
        self.user = create_user()

        worker = Worker.get_by_user(self.user)
        worker.locations.set([get_test_location()])
        worker.save()

        self.visit = create_visit()

        self.sample_data = {'first_name': 'name',
                            'length': '50',
                            'visit': self.visit.id,
                            'location': location.id,
                            'comment': 'A unicode comment with weird letters such as á è ü ñ ô',
                            'datetime_when': "2020-01-01",
                            }

    def test_validation(self):
        form = AppointmentAddForm(user=self.user, data=self.sample_data)
        self.assertTrue(form.is_valid())

    def test_validation_invalid_location(self):
        self.sample_data['location'] = create_location(name="xxx").id
        form = AppointmentAddForm(user=self.user, data=self.sample_data)

        self.assertFalse(form.is_valid())
        self.assertTrue("location" in form.errors)

    def test_invalid_length(self):
        self.sample_data['length'] = '-10'
        form = AppointmentAddForm(user=self.user, data=self.sample_data)

        self.assertFalse(form.is_valid())
