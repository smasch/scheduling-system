

from django.test import TestCase

from web.forms import VisitAddForm
from web.tests.functions import create_study_subject
from web.tests.functions import get_test_location


class VisitAddFormTests(TestCase):
    def setUp(self):
        get_test_location()
        self.subject = create_study_subject()

        self.sample_data = {'datetime_begin': "2017-01-01",
                            'datetime_end': "2017-02-02",
                            'subject': self.subject.id,
                            'appointment_types': '',
                            'post_mail_sent': False
                            }

    def test_validation(self):
        form = VisitAddForm(data=self.sample_data)
        is_valid = form.is_valid()
        self.assertTrue(is_valid)

    def test_invalid_validation(self):
        self.sample_data['datetime_begin'] = "2017-02-02"
        self.sample_data['datetime_end'] = "2017-01-01"
        form = VisitAddForm(data=self.sample_data)
        validation_status = form.is_valid()
        self.assertFalse(validation_status)
        self.assertTrue("datetime_begin" in form.errors)
        self.assertTrue("datetime_end" in form.errors)

    def test_invalid_no_dates(self):
        self.sample_data.pop('datetime_begin')
        form = VisitAddForm(data=self.sample_data)
        validation_status = form.is_valid()
        self.assertFalse(validation_status)
