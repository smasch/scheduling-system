import logging

from django.urls import reverse

from web.forms import VoucherForm
from web.models import Voucher
from web.models.constants import VOUCHER_STATUS_USED
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_voucher_type, format_form_field, create_voucher, \
    create_worker, create_voucher_partner

logger = logging.getLogger(__name__)


class VoucherFormTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.worker = create_worker()
        self.voucher_partner = create_voucher_partner()

    def test_create_voucher(self):
        self.login_as_admin()
        voucher_type = create_voucher_type()
        study_subject = create_study_subject()
        study_subject.voucher_types.add(voucher_type)
        create_voucher(study_subject)

        voucher_form = VoucherForm()
        form_data = {
            "status": VOUCHER_STATUS_USED,
            "usage_partner": self.voucher_partner.id,
            "issue_worker": self.worker.id,
            "hours": 10,
            "voucher_type": voucher_type.id
        }
        for key, value in list(voucher_form.initial.items()):
            form_data[key] = format_form_field(value)

        url = reverse('web.views.voucher_add') + '?study_subject_id=' + str(study_subject.id)
        response = self.client.post(url, data=form_data)
        self.assertEqual(response.status_code, 302)

        self.assertEqual(2, Voucher.objects.all().count())

    def test_invalid_usage_partner(self):
        self.login_as_admin()
        study_subject = create_study_subject()
        voucher = create_voucher(study_subject)
        voucher.status = VOUCHER_STATUS_USED
        voucher.save()

        form_data = self.get_voucher_form_data(voucher)
        form_data["usage_partner"] = ""

        voucher_form = VoucherForm(instance=voucher, data=form_data)

        self.assertFalse(voucher_form.is_valid())
        self.assertTrue("usage_partner" in voucher_form.errors)

    @staticmethod
    def get_voucher_form_data(voucher):
        voucher_form = VoucherForm(instance=voucher)
        form_data = {}
        for key, value in list(voucher_form.initial.items()):
            form_data[key] = format_form_field(value)
        return form_data
