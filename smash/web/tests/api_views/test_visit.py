# coding=utf-8
import datetime
import json
import logging
from typing import List

from django.urls import reverse

from web.api_views.visit import get_visits_filtered, get_visits_order
from web.models import Visit, AppointmentTypeLink, Appointment
from web.models.study_visit_list import VISIT_LIST_GENERIC, VISIT_LIST_EXCEEDED_TIME, VISIT_LIST_UNFINISHED, \
    VISIT_LIST_MISSING_APPOINTMENTS, VISIT_LIST_APPROACHING_WITHOUT_APPOINTMENTS, \
    VISIT_LIST_APPROACHING_FOR_MAIL_CONTACT
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_get_suffix, create_visit, create_appointment_type, \
    create_flying_team, create_appointment
from web.views.notifications import get_today_midnight_date

logger = logging.getLogger(__name__)


class TestVisitApi(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study_subject = create_study_subject()
        self.visit = create_visit(self.study_subject)

    def test_get_columns(self):
        response = self.client.get(reverse('web.api.visits.columns', kwargs={'visit_list_type': VISIT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        self.assertTrue(len(columns) > 0)

    def test_get_columns_for_time_exceeded(self):
        response = self.client.get(
            reverse('web.api.visits.columns', kwargs={'visit_list_type': VISIT_LIST_EXCEEDED_TIME}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        self.assertTrue(len(columns) > 0)

    def test_visits_exceeded(self):
        response = self.client.get(reverse('web.api.visits', kwargs={'visit_list_type': VISIT_LIST_EXCEEDED_TIME}))
        self.assertEqual(response.status_code, 200)

    def test_visits_generic(self):
        self.visit.appointment_types.add(create_appointment_type())
        self.visit.save()
        response = self.client.get(reverse('web.api.visits', kwargs={'visit_list_type': VISIT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

    def test_visits_unfinished(self):
        response = self.client.get(reverse('web.api.visits', kwargs={'visit_list_type': VISIT_LIST_UNFINISHED}))
        self.assertEqual(response.status_code, 200)

    def test_visits_missing_appointments(self):
        response = self.client.get(
            reverse('web.api.visits', kwargs={'visit_list_type': VISIT_LIST_MISSING_APPOINTMENTS}))
        self.assertEqual(response.status_code, 200)

    def test_visits_approaching_without_appointments(self):
        response = self.client.get(
            reverse('web.api.visits', kwargs={'visit_list_type': VISIT_LIST_APPROACHING_WITHOUT_APPOINTMENTS}))
        self.assertEqual(response.status_code, 200)

    def test_visits_approaching_for_mail_contact(self):
        response = self.client.get(
            reverse('web.api.visits', kwargs={'visit_list_type': VISIT_LIST_APPROACHING_FOR_MAIL_CONTACT}))
        self.assertEqual(response.status_code, 200)

    def test_visits_invalid(self):
        with self.assertRaises(TypeError):
            self.client.get(reverse('web.api.visits', kwargs={'visit_list_type': "invalid_type"}))

    def test_visits_general_search(self):
        name = "Piotrek"
        self.study_subject.subject.first_name = name
        self.study_subject.subject.save()
        create_visit(self.study_subject)

        params = {
            "columns[0][search][value]": "another_name",
            "columns[0][data]": "first_name"
        }
        url = ("%s" + create_get_suffix(params)) % reverse('web.api.visits',
                                                           kwargs={'visit_list_type': VISIT_LIST_GENERIC})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(name.encode('utf8') in response.content)

        params["columns[0][search][value]"] = name
        url = ("%s" + create_get_suffix(params)) % reverse('web.api.visits',
                                                           kwargs={'visit_list_type': VISIT_LIST_GENERIC})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(name.encode('utf8') in response.content)

    def check_visit_filtered(self, filters, result: List[Visit]):
        subjects = get_visits_filtered(Visit.objects.all(), filters)
        self.assertEqual(len(result), subjects.count())
        for index, result_visit in enumerate(result):
            self.assertEqual(result_visit, subjects[index])

    def check_visit_ordered(self, order, result: List[Visit]):
        visits = get_visits_order(Visit.objects.all(), order, "asc")
        self.assertEqual(len(result), visits.count())
        for index, result_visit in enumerate(result):
            self.assertEqual(result_visit, visits[index])

        visits = get_visits_order(Visit.objects.all(), order, "desc")
        length = len(result)
        self.assertEqual(length, visits.count())
        for index in range(length):
            self.assertEqual(result[length - index - 1], visits[index])

    def test_visits_sort_first_name(self):
        subject = self.study_subject
        subject.subject.first_name = "PPP"
        subject.subject.save()

        subject2 = create_study_subject(2)
        subject2.subject.first_name = "QQQ"
        subject2.subject.save()

        visit2 = create_visit(subject2)

        self.check_visit_ordered("first_name", [self.visit, visit2])

    def test_visits_sort_last_name(self):
        subject = self.study_subject
        subject.subject.last_name = "PPP"
        subject.subject.save()

        subject2 = create_study_subject(2)
        subject2.subject.last_name = "QQQ"
        subject2.subject.save()

        visit2 = create_visit(subject2)

        self.check_visit_ordered("last_name", [self.visit, visit2])

    def test_visits_sort_default_location(self):
        self.check_visit_ordered("default_location", [self.visit])

    def test_visits_sort_flying_team(self):
        self.check_visit_ordered("flying_team", [self.visit])

    def test_visits_sort_is_finished(self):
        self.check_visit_ordered("is_finished", [self.visit])

    def test_visits_sort_post_mail_sent(self):
        self.check_visit_ordered("post_mail_sent", [self.visit])

    def test_visits_sort_datetime_begin(self):
        visit1 = self.visit
        visit2 = create_visit(self.study_subject)
        visit1.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=1)
        visit2.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=2)
        visit1.save()
        visit2.save()

        self.check_visit_ordered("datetime_begin", [visit1, visit2])

    def test_visits_sort_datetime_end(self):
        visit1 = self.visit
        visit2 = create_visit(self.study_subject)
        visit1.datetime_end = get_today_midnight_date() + datetime.timedelta(days=1)
        visit2.datetime_end = get_today_midnight_date() + datetime.timedelta(days=2)
        visit1.save()
        visit2.save()

        self.check_visit_ordered("datetime_end", [visit1, visit2])

    def test_visits_sort_visit_number(self):
        visit1 = self.visit
        visit2 = create_visit(self.study_subject)

        visit2.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=2)
        visit2.save()
        visit1.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=1)
        visit1.save()

        # visit_number is adjusted automatically according to start date
        self.check_visit_ordered("visit_number", [visit1, visit2])

    def test_visits_sort_unknown(self):
        self.check_visit_ordered("some_unknown", [self.visit])
        self.check_visit_ordered("", [self.visit])
        self.check_visit_ordered(None, [self.visit])

    def test_visits_filter_last_name(self):
        subject = self.study_subject
        subject.subject.last_name = "XXX"
        subject.subject.save()

        self.check_visit_filtered([["last_name", "Q"]], [])
        self.check_visit_filtered([["last_name", "xx"]], [self.visit])

    def test_visits_filter_flying_team(self):
        subject = self.study_subject
        subject.flying_team = create_flying_team()
        subject.save()

        self.check_visit_filtered([["flying_team", "-1"]], [])
        self.check_visit_filtered([["flying_team", str(subject.flying_team.id)]], [self.visit])

    def test_visits_filter_default_location(self):
        self.check_visit_filtered([["default_location", "-1"]], [])
        self.check_visit_filtered([["default_location", str(self.study_subject.default_location.id)]], [self.visit])

    def test_visits_filter_is_finished(self):
        self.check_visit_filtered([["is_finished", str(not self.visit.is_finished).lower()]], [])
        self.check_visit_filtered([["is_finished", str(self.visit.is_finished).lower()]], [self.visit])

    def test_visits_filter_post_mail_sent(self):
        self.check_visit_filtered([["post_mail_sent", str(not self.visit.post_mail_sent).lower()]], [])
        self.check_visit_filtered([["post_mail_sent", str(self.visit.post_mail_sent).lower()]], [self.visit])

    def test_visits_filter_visit_number(self):
        self.check_visit_filtered([["visit_number", "-1"]], [])
        self.check_visit_filtered([["visit_number", str(self.visit.visit_number)]], [self.visit])

    def test_visits_filter_visible_appointment_types(self):
        appointment_type_1 = create_appointment_type()
        appointment_type_2 = create_appointment_type()
        self.visit.appointment_types.add(appointment_type_1)
        self.visit.save()

        self.check_visit_filtered([["visible_appointment_types", str(appointment_type_2.id)]], [])
        self.check_visit_filtered([["visible_appointment_types", str(appointment_type_1.id)]], [self.visit])

    def test_visits_filter_visible_appointment_types_missing(self):
        appointment_type_1 = create_appointment_type()
        appointment_type_2 = create_appointment_type()
        self.visit.appointment_types.add(appointment_type_1)
        self.visit.save()
        appointment = create_appointment(self.visit)

        AppointmentTypeLink.objects.create(appointment=appointment, appointment_type=appointment_type_2)

        self.check_visit_filtered([["visible_appointment_types_missing", str(appointment_type_2.id)]], [])
        self.check_visit_filtered([["visible_appointment_types_missing", str(appointment_type_1.id)]], [self.visit])

    def test_visits_filter_visible_appointment_types_done(self):
        appointment_type_1 = create_appointment_type()
        appointment_type_2 = create_appointment_type()
        self.visit.appointment_types.add(appointment_type_1)
        self.visit.save()
        appointment = create_appointment(self.visit)
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()

        AppointmentTypeLink.objects.create(appointment=appointment, appointment_type=appointment_type_1)

        self.check_visit_filtered([["visible_appointment_types_done", str(appointment_type_2.id)]], [])
        self.check_visit_filtered([["visible_appointment_types_done", str(appointment_type_1.id)]], [self.visit])

    def test_visits_filter_visible_appointment_types_in_progress(self):
        appointment_type_1 = create_appointment_type()
        appointment_type_2 = create_appointment_type()
        self.visit.appointment_types.add(appointment_type_1)
        self.visit.save()
        appointment = create_appointment(self.visit)

        AppointmentTypeLink.objects.create(appointment=appointment, appointment_type=appointment_type_1)

        self.check_visit_filtered([["visible_appointment_types_in_progress", str(appointment_type_2.id)]], [])
        self.check_visit_filtered([["visible_appointment_types_in_progress", str(appointment_type_1.id)]], [self.visit])

    def test_visits_filter_unknown(self):
        self.check_visit_filtered([["some_unknown", "unknown data"]], [self.visit])
        self.check_visit_filtered([["", ""]], [self.visit])
        self.check_visit_filtered([[None, None]], [self.visit])
