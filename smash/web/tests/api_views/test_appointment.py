# coding=utf-8

import datetime
import json
from typing import List

from django.urls import reverse

from web.api_views.appointment import get_appointments_filtered, get_appointments_order
from web.models import AppointmentTypeLink, AppointmentList, Appointment
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_visit, create_appointment, \
    create_appointment_type, create_get_suffix, create_flying_team, create_location
from web.views.appointment import APPOINTMENT_LIST_GENERIC, APPOINTMENT_LIST_APPROACHING, APPOINTMENT_LIST_UNFINISHED
from web.views.notifications import get_today_midnight_date


class TestAppointmentApi(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study_subject = create_study_subject()

    def test_appointments_valid(self):
        place = "Some new flying team location"
        name = "Piotrek"
        self.study_subject.subject.first_name = name
        self.study_subject.subject.save()
        visit = create_visit(self.study_subject)
        appointment = create_appointment(visit)
        appointment.flying_team = create_flying_team(place=place)
        appointment.save()
        appointment2 = create_appointment(visit, get_today_midnight_date())
        appointment2.visit = None

        appointment_type = create_appointment_type()
        appointment2.save()
        AppointmentTypeLink.objects.create(appointment=appointment2, appointment_type=appointment_type)

        url = reverse('web.api.appointments', kwargs={'appointment_type': APPOINTMENT_LIST_GENERIC})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(name.encode('utf8') in response.content)
        self.assertTrue(place.encode('utf8') in response.content)

    def test_flying_team_location_in_appointments(self):
        place = "Some new flying team location"
        visit = create_visit(self.study_subject)
        appointment = create_appointment(visit)
        appointment.flying_team = create_flying_team(place=place)
        appointment.save()

        url = reverse('web.api.appointments', kwargs={'appointment_type': APPOINTMENT_LIST_GENERIC})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(place.encode('utf8') in response.content)

    def test_appointments_approaching(self):
        name = "Piotrek"
        self.study_subject.subject.first_name = name
        self.study_subject.subject.save()
        visit = create_visit(self.study_subject)
        create_appointment(visit, get_today_midnight_date() + datetime.timedelta(days=2))

        url = reverse('web.api.appointments', kwargs={'appointment_type': APPOINTMENT_LIST_APPROACHING})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(name.encode('utf8') in response.content)

    def test_appointments_unfinished(self):
        name = "Piotrek"
        self.study_subject.subject.first_name = name
        self.study_subject.subject.save()
        visit = create_visit(self.study_subject)
        create_appointment(visit, get_today_midnight_date() + datetime.timedelta(days=-12))

        url = reverse('web.api.appointments', kwargs={'appointment_type': APPOINTMENT_LIST_UNFINISHED})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(name.encode('utf8') in response.content)

    def test_get_calendar_appointments(self):
        name = "Peter"
        self.study_subject.subject.first_name = name
        self.study_subject.subject.save()
        visit = create_visit(self.study_subject)
        appointment = create_appointment(visit, get_today_midnight_date())
        appointment.save()

        AppointmentTypeLink.objects.create(appointment=appointment, appointment_type=create_appointment_type())

        params = {
            "start_date": (get_today_midnight_date() + datetime.timedelta(days=2)).strftime("%Y-%m-%d"),
            "end_date": (get_today_midnight_date() + datetime.timedelta(days=3)).strftime("%Y-%m-%d"),
        }
        url = ("%s" + create_get_suffix(params)) % reverse('web.api.appointments',
                                                           kwargs={'appointment_type': APPOINTMENT_LIST_GENERIC})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(name.encode('utf8') in response.content)

        params["start_date"] = (get_today_midnight_date() + datetime.timedelta(days=-2)).strftime("%Y-%m-%d")
        url = ("%s" + create_get_suffix(params)) % reverse('web.api.appointments',
                                                           kwargs={'appointment_type': APPOINTMENT_LIST_GENERIC})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(name.encode('utf8') in response.content)

    def test_get_columns(self):
        response = self.client.get(
            reverse('web.api.appointments.columns', kwargs={'appointment_list_type': APPOINTMENT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        visible_columns = 0
        for column in columns:
            if column["visible"]:
                visible_columns += 1
        self.assertTrue(visible_columns > 0)

    def test_get_columns_for_require_contact(self):
        response = self.client.get(
            reverse('web.api.appointments.columns', kwargs={'appointment_list_type': APPOINTMENT_LIST_UNFINISHED}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        visible_columns = 0
        for column in columns:
            if column["visible"]:
                visible_columns += 1
        self.assertTrue(visible_columns > 0)

    def test_get_columns_when_no_list_is_available(self):
        AppointmentList.objects.all().delete()
        response = self.client.get(
            reverse('web.api.appointments.columns', kwargs={'appointment_list_type': APPOINTMENT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        self.assertTrue(len(columns) > 0)

    def check_appointment_filtered(self, filters, result: List[Appointment]):
        appointments = get_appointments_filtered(Appointment.objects.all(), filters)
        self.assertEqual(len(result), appointments.count())
        for index, result_appointment in enumerate(result):
            self.assertEqual(result_appointment, appointments[index])

    def check_appointment_ordered(self, order, result: List[Appointment]):
        appointments = get_appointments_order(Appointment.objects.all(), order, "asc")
        self.assertEqual(len(result), appointments.count())
        for index, result_appointment in enumerate(result):
            self.assertEqual(result_appointment, appointments[index])

        appointments = get_appointments_order(Appointment.objects.all(), order, "desc")
        length = len(result)
        self.assertEqual(length, appointments.count())
        for index in range(length):
            self.assertEqual(result[length - index - 1], appointments[index])

    def test_appointment_sort_first_name(self):
        subject = self.study_subject
        subject.subject.first_name = "PPP"
        subject.subject.save()

        subject2 = create_study_subject(2)
        subject2.subject.first_name = "QQQ"
        subject2.subject.save()

        appointment = create_appointment(create_visit(subject))
        appointment2 = create_appointment(create_visit(subject2))

        self.check_appointment_ordered("first_name", [appointment, appointment2])

    def test_appointment_sort_last_name(self):
        subject = self.study_subject
        subject.subject.last_name = "PPP"
        subject.subject.save()

        subject2 = create_study_subject(2)
        subject2.subject.last_name = "QQQ"
        subject2.subject.save()

        appointment = create_appointment(create_visit(subject))
        appointment2 = create_appointment(create_visit(subject2))

        self.check_appointment_ordered("last_name", [appointment, appointment2])

    def test_appointment_sort_default_location(self):
        subject = self.study_subject
        subject2 = create_study_subject(2)

        appointment = create_appointment(create_visit(subject))
        appointment.location = create_location(name="x")
        appointment.save()
        appointment2 = create_appointment(create_visit(subject2))
        appointment2.location = create_location(name="y")
        appointment2.save()

        self.check_appointment_ordered("location", [appointment, appointment2])

    def test_appointment_sort_flying_team(self):
        subject = self.study_subject
        subject2 = create_study_subject(2)

        appointment = create_appointment(create_visit(subject))
        appointment.flying_team = create_flying_team(place="x")
        appointment.save()
        appointment2 = create_appointment(create_visit(subject2))
        appointment2.flying_team = create_flying_team(place="y")
        appointment2.save()

        self.check_appointment_ordered("flying_team", [appointment, appointment2])

    def test_appointment_sort_post_mail_sent(self):
        appointment = create_appointment(create_visit(self.study_subject))

        self.check_appointment_ordered("post_mail_sent", [appointment])

    def test_appointment_sort_datetime_when(self):
        subject = self.study_subject
        subject2 = create_study_subject(2)

        appointment = create_appointment(create_visit(subject))
        appointment.datetime_when = datetime.datetime(2017, 10, 10, tzinfo=datetime.timezone.utc)
        appointment.save()
        appointment2 = create_appointment(create_visit(subject2))
        appointment2.datetime_when = datetime.datetime(2017, 10, 12, tzinfo=datetime.timezone.utc)
        appointment2.save()

        self.check_appointment_ordered("datetime_when", [appointment, appointment2])

    def test_appointment_sort_unknown(self):
        appointment = create_appointment(create_visit(self.study_subject))

        self.check_appointment_ordered("some_unknown", [appointment])
        self.check_appointment_ordered("", [appointment])
        self.check_appointment_ordered(None, [appointment])

    def test_subjects_filter_first_name(self):
        subject = self.study_subject.subject
        subject.first_name = "QQ"
        subject.save()

        appointment = create_appointment(create_visit(self.study_subject))

        self.check_appointment_filtered([["first_name", "QQ"]], [appointment])
        self.check_appointment_filtered([["first_name", "PP"]], [])

    def test_subjects_filter_last_name(self):
        subject = self.study_subject.subject
        subject.last_name = "QQ"
        subject.save()

        appointment = create_appointment(create_visit(self.study_subject))

        self.check_appointment_filtered([["last_name", "QQ"]], [appointment])
        self.check_appointment_filtered([["last_name", "PP"]], [])

    def test_subjects_filter_location(self):
        appointment = create_appointment(create_visit(self.study_subject))

        self.check_appointment_filtered([["location", str(appointment.location.id)]], [appointment])
        self.check_appointment_filtered([["location", "-1"]], [])

    def test_subjects_filter_flying_team(self):
        appointment = create_appointment(create_visit(self.study_subject))
        appointment.flying_team = create_flying_team()
        appointment.save()

        self.check_appointment_filtered([["flying_team", str(appointment.flying_team.id)]], [appointment])
        self.check_appointment_filtered([["flying_team", "-1"]], [])

    def test_subjects_filter_appointment_types(self):
        appointment = create_appointment(create_visit(self.study_subject))
        appointment_type = create_appointment_type()
        AppointmentTypeLink.objects.create(appointment_type=appointment_type, appointment=appointment)

        self.check_appointment_filtered([["appointment_types", str(appointment_type.id)]], [appointment])
        self.check_appointment_filtered([["appointment_types", "-1"]], [])

    def test_appointment_filter_unknown(self):
        appointment = create_appointment(create_visit(self.study_subject))

        self.check_appointment_filtered([["some_unknown", "unk"]], [appointment])
        self.check_appointment_filtered([["", ""]], [appointment])
        self.check_appointment_filtered([[None, None]], [appointment])
