# coding=utf-8

from django.urls import reverse

from web.models import MissingSubject
from web.tests import LoggedInTestCase


class TestRedcapApi(LoggedInTestCase):

    def test_ignore_missing_subject(self):
        missing_subject = MissingSubject.objects.create(ignore=False)

        response = self.client.get(
            reverse('web.api.redcap.ignore_missing_subject', kwargs={'missing_subject_id': missing_subject.id}))
        self.assertEqual(response.status_code, 200)

        missing_subject = MissingSubject.objects.filter(id=missing_subject.id)[0]

        self.assertTrue(missing_subject.ignore)

    def test_unignore_missing_subject(self):
        missing_subject = MissingSubject.objects.create(ignore=True)

        response = self.client.get(
            reverse('web.api.redcap.unignore_missing_subject', kwargs={'missing_subject_id': missing_subject.id}))
        self.assertEqual(response.status_code, 200)

        missing_subject = MissingSubject.objects.filter(id=missing_subject.id)[0]

        self.assertFalse(missing_subject.ignore)
