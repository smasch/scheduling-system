# coding=utf-8
import json

from django.urls import reverse

from web.tests import LoggedInTestCase
from web.tests.functions import create_flying_team


class TestFlyingTeamApi(LoggedInTestCase):

    def test_flying_teams(self):
        flying_team_name = "some flying_team"

        response = self.client.get(reverse("web.api.flying_teams"))
        self.assertEqual(response.status_code, 200)

        create_flying_team(flying_team_name)

        response = self.client.get(reverse("web.api.flying_teams"))
        flying_teams = json.loads(response.content)["flying_teams"]

        found = False
        for flying_team in flying_teams:
            if flying_team["name"] == flying_team_name:
                found = True

        self.assertTrue(found)
