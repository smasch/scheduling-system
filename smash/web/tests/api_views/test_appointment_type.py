# coding=utf-8
import json

from django.urls import reverse

from web.tests import LoggedInTestCase
from web.tests.functions import create_appointment_type


class TestApi(LoggedInTestCase):

    def test_appointment_types(self):
        type_name = "some type name"

        response = self.client.get(reverse('web.api.appointment_types'))
        self.assertEqual(response.status_code, 200)

        create_appointment_type(code=type_name)

        response = self.client.get(reverse('web.api.appointment_types'))
        appointment_types = json.loads(response.content)['appointment_types']

        found = False
        for appointment_type in appointment_types:
            if appointment_type['type'] == type_name:
                found = True

        self.assertTrue(found)
