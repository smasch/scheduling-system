# coding=utf-8


from django.urls import reverse

from web.models import ConfigurationItem
from web.models.constants import CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE, VALUE_TYPE_PASSWORD
from web.tests import LoggedInTestCase
from web.tests.functions import create_configuration_item


class TestConfigurationItemApi(LoggedInTestCase):
    def test_configuration_items(self):
        create_configuration_item()
        response = self.client.get(reverse('web.api.configuration_items'))
        self.assertEqual(response.status_code, 200)

    def test_password_item(self):
        item = ConfigurationItem.objects.create()
        item.type = "TEST"
        item.value = "secret_password"
        item.name = "yyy"
        item.value_type = VALUE_TYPE_PASSWORD
        item.save()
        response = self.client.get(reverse('web.api.configuration_items'), {'length': "100"})
        self.assertFalse(item.value in response.content.decode("utf-8"))
        self.assertTrue(item.value_type in response.content.decode("utf-8"))
        self.assertEqual(response.status_code, 200)

    def test_configuration_modify_invalid(self):
        response = self.client.get(reverse('web.api.update_configuration_item'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue("error".encode('utf8') in response.content)

    def test_configuration_modify_invalid_2(self):
        response = self.client.get(reverse('web.api.update_configuration_item'), {'id': "-15", 'value': "XXX"})
        self.assertEqual(response.status_code, 200)
        self.assertTrue("error".encode('utf8') in response.content)

    def test_configuration_modify(self):
        item = create_configuration_item()
        new_val = 'new val'
        response = self.client.get(reverse('web.api.update_configuration_item'), {'id': item.id, 'value': new_val})
        self.assertEqual(response.status_code, 200)
        updated_item = ConfigurationItem.objects.get(id=item.id)
        self.assertEqual(new_val, updated_item.value)

    def test_configuration_modify_CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE_invalid_value(self):
        item = ConfigurationItem.objects.get(type=CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE)
        invalid_val = 'invalid color'

        response = self.client.get(reverse('web.api.update_configuration_item'), {'id': item.id, 'value': invalid_val})
        self.assertEqual(response.status_code, 200)
        updated_item = ConfigurationItem.objects.get(type=CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE)
        self.assertNotEqual(invalid_val, updated_item.value)

    def test_configuration_modify_CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE_valid_value(self):
        item = ConfigurationItem.objects.get(type=CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE)
        invalid_val = '#FFFFFF'

        response = self.client.get(reverse('web.api.update_configuration_item'), {'id': item.id, 'value': invalid_val})
        self.assertEqual(response.status_code, 200)
        updated_item = ConfigurationItem.objects.get(type=CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE)
        self.assertEqual(invalid_val, updated_item.value)
