# coding=utf-8
import datetime
import json
import logging
from typing import List

from django.urls import reverse

from web.api_views.voucher import get_vouchers_filtered, get_vouchers_order
from web.models import Voucher
from web.models.constants import VOUCHER_STATUS_USED, VOUCHER_STATUS_NEW
from web.tests import LoggedInTestCase
from web.tests.functions import create_get_suffix, create_voucher
from web.views.notifications import get_today_midnight_date

logger = logging.getLogger(__name__)


class TestVoucherApi(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.voucher = create_voucher()

    def test_vouchers_general(self):
        response = self.client.get(reverse('web.api.vouchers'))
        self.assertEqual(response.status_code, 200)

    def test_vouchers_general_search(self):
        name = "Piotrek"
        self.voucher.study_subject.subject.first_name = name
        self.voucher.study_subject.subject.save()

        params = {
            "columns[0][search][value]": "another_name",
            "columns[0][data]": "first_name"
        }
        url = ("%s" + create_get_suffix(params)) % reverse('web.api.vouchers')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(name.encode('utf8') in response.content)

        params["columns[0][search][value]"] = name
        url = ("%s" + create_get_suffix(params)) % reverse('web.api.vouchers')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(name.encode('utf8') in response.content)

    def check_voucher_filtered(self, filters, result: List[Voucher]):
        vouchers = get_vouchers_filtered(Voucher.objects.all(), filters)
        self.assertEqual(len(result), vouchers.count())
        for index, result_voucher in enumerate(result):
            self.assertEqual(result_voucher, vouchers[index])

    def check_voucher_ordered(self, order, result: List[Voucher]):
        vouchers = get_vouchers_order(Voucher.objects.all(), order, "asc")
        self.assertEqual(len(result), vouchers.count())
        for index, result_voucher in enumerate(result):
            self.assertEqual(result_voucher, vouchers[index])

        vouchers = get_vouchers_order(Voucher.objects.all(), order, "desc")
        length = len(result)
        self.assertEqual(length, vouchers.count())
        for index in range(length):
            self.assertEqual(result[length - index - 1], vouchers[index])

    def test_vouchers_sort_number(self):
        voucher = self.voucher
        voucher.number = "PPP"
        voucher.save()

        voucher2 = create_voucher()
        voucher2.number = "QQQ"
        voucher2.save()

        self.check_voucher_ordered("number", [voucher, voucher2])

    def test_subjects_sort_id(self):
        voucher = self.voucher

        voucher2 = create_voucher()

        self.check_voucher_ordered("id", [voucher, voucher2])

    def test_vouchers_sort_issue_date(self):
        voucher = self.voucher
        voucher.issue_date = get_today_midnight_date()
        voucher.save()

        voucher2 = create_voucher()
        voucher2.issue_date = get_today_midnight_date() + datetime.timedelta(days=1)
        voucher2.save()

        self.check_voucher_ordered("issue_date", [voucher, voucher2])

    def test_vouchers_sort_expiry_date(self):
        voucher = self.voucher
        voucher.expiry_date = get_today_midnight_date()
        voucher.save()

        voucher2 = create_voucher()
        voucher2.expiry_date = get_today_midnight_date() + datetime.timedelta(days=1)
        voucher2.save()

        self.check_voucher_ordered("expiry_date", [voucher, voucher2])

    def test_vouchers_sort_type(self):
        voucher = self.voucher
        voucher.voucher_type.code = "ZZ"
        voucher.voucher_type.save()

        voucher2 = create_voucher()
        voucher2.voucher_type.code = "AA"
        voucher2.save()

        self.check_voucher_ordered("type", [voucher2, voucher])

    def test_vouchers_sort_status(self):
        voucher = self.voucher
        voucher.status = VOUCHER_STATUS_USED
        voucher.save()

        voucher2 = create_voucher()
        voucher2.status = VOUCHER_STATUS_NEW
        voucher2.save()

        self.check_voucher_ordered("status", [voucher2, voucher])

    def test_vouchers_sort_unknown(self):
        self.check_voucher_ordered("unk", [self.voucher])

    def test_vouchers_filter_nd_number(self):
        voucher = self.voucher
        voucher.number = "PPP"
        voucher.save()

        voucher2 = create_voucher()
        voucher2.number = "QQQ"
        voucher2.save()

        self.check_voucher_filtered([["number", "P"]], [voucher])

    def test_vouchers_filter_last_name(self):
        voucher = self.voucher
        voucher.study_subject.subject.last_name = "PPP"
        voucher.study_subject.subject.save()

        create_voucher()

        self.check_voucher_filtered([["last_name", "P"]], [voucher])

    def test_vouchers_filter_type(self):
        voucher = self.voucher

        create_voucher()

        self.check_voucher_filtered([["type", str(voucher.voucher_type.id)]], [voucher])

    def test_vouchers_filter_status(self):
        voucher = self.voucher
        voucher.status = VOUCHER_STATUS_NEW
        voucher.save()

        voucher2 = create_voucher()
        voucher2.status = VOUCHER_STATUS_USED
        voucher2.save()

        self.check_voucher_filtered([["status", VOUCHER_STATUS_NEW]], [voucher])

    def test_vouchers_filter_voucher_partner(self):
        voucher = self.voucher

        create_voucher()

        self.check_voucher_filtered([["voucher_partner", str(voucher.usage_partner_id)]], [voucher])

    def test_vouchers_filter_feedback(self):
        voucher = self.voucher
        voucher.feedback = "XAS"
        voucher.save()

        create_voucher()

        self.check_voucher_filtered([["feedback", "X"]], [voucher])

    def test_vouchers_filter_unknown(self):
        voucher = self.voucher

        self.check_voucher_filtered([["unk", "X"]], [voucher])

    def test_vouchers_filter_unknown_2(self):
        voucher = self.voucher

        self.check_voucher_filtered([[None, "X"]], [voucher])

    def test_vouchers_filter_empty(self):
        voucher = self.voucher

        self.check_voucher_filtered([["", "X"]], [voucher])

    def test_get_columns(self):
        response = self.client.get(reverse('web.api.vouchers.columns'))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        self.assertTrue(len(columns) > 0)
