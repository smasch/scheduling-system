# coding=utf-8
import json

from django.urls import reverse

from web.tests import LoggedInTestCase
from web.models import StudySubject, Language
from web.tests.functions import create_study_subject, create_language
from web.api_views.subject import get_subjects_filtered, get_subjects_order


class TestLanguagesApi(LoggedInTestCase):

    def test_languages(self):
        lang_name = "French"

        response = self.client.get(reverse('web.api.locations'))
        self.assertEqual(response.status_code, 200)

        create_language(lang_name)

        response = self.client.get(reverse('web.api.languages'))
        languages = json.loads(response.content)['languages']

        found = False
        for lang in languages:
            if lang['name'] == lang_name:
                found = True

        self.assertTrue(found)

    def test_default_written_communication_language_filter(self):
        lang_name = "French"
        create_language(lang_name)
        language_fr = Language.objects.filter(name=lang_name)[0]

        study_subject_a = create_study_subject()
        study_subject_a.subject.default_written_communication_language = language_fr
        study_subject_a.subject.save()
        create_study_subject()
        create_study_subject()

        subjects = StudySubject.objects.all()
        self.assertEqual(len(subjects), 3)

        subjects_fr = StudySubject.objects.filter(
            subject__default_written_communication_language__id=language_fr.id)
        self.assertEqual(len(subjects_fr), 1)

        filtered_subjects = get_subjects_filtered(
            subjects, [('default_written_communication_language', language_fr.id)])
        self.assertEqual(len(filtered_subjects), 1)
        self.assertEqual(filtered_subjects[0].subject.default_written_communication_language.name, lang_name)

    def test_default_written_communication_language_null_order(self):
        lang_name = "French"
        create_language(lang_name)
        language_fr = Language.objects.filter(name=lang_name)[0]

        study_subject_a = create_study_subject()
        study_subject_a.subject.default_written_communication_language = language_fr
        study_subject_a.subject.save()
        create_study_subject()
        create_study_subject()

        subjects = StudySubject.objects.all()
        self.assertEqual(len(subjects), 3)

        ordered_subjects = get_subjects_order(
            subjects, 'default_written_communication_language', "asc")

        self.assertEqual(len(ordered_subjects), 3)
        self.assertEqual(ordered_subjects[0].subject.default_written_communication_language, None)
        self.assertEqual(ordered_subjects[1].subject.default_written_communication_language, None)
        self.assertEqual(ordered_subjects[2].subject.default_written_communication_language.name, lang_name)

        ordered_subjects = get_subjects_order(
            subjects, 'default_written_communication_language', "-")
        self.assertEqual(len(ordered_subjects), 3)
        self.assertEqual(ordered_subjects[0].subject.default_written_communication_language.name, lang_name)
        self.assertEqual(ordered_subjects[1].subject.default_written_communication_language, None)
        self.assertEqual(ordered_subjects[2].subject.default_written_communication_language, None)
