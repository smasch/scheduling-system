import logging

from django.test import TestCase

from web.api_views.serialization_utils import str_to_yes_no, str_to_yes_no_null, bool_to_yes_no_null, bool_to_yes_no

logger = logging.getLogger(__name__)


class SerializationUtilsTests(TestCase):
    def test_str_to_yes_no(self):
        self.assertEqual(str_to_yes_no('true'), 'YES')
        with self.assertRaises(AttributeError):
            str_to_yes_no(None)
        self.assertEqual(str_to_yes_no('potato'), 'NO')
        self.assertEqual(str_to_yes_no('false'), 'NO')

    def test_str_to_yes_no_null(self):
        self.assertEqual(str_to_yes_no_null('true'), 'YES')
        self.assertEqual(str_to_yes_no_null(None), None)
        self.assertEqual(str_to_yes_no_null('potato'), 'NO')
        self.assertEqual(str_to_yes_no_null('false'), 'NO')

    def test_bool_to_yes_no_null(self):
        self.assertEqual(bool_to_yes_no_null(True), 'YES')
        self.assertEqual(bool_to_yes_no_null(None), 'N/A')
        self.assertEqual(bool_to_yes_no_null(False), 'NO')

    def test_bool_to_yes_no(self):
        self.assertEqual(bool_to_yes_no(True), 'YES')
        self.assertEqual(bool_to_yes_no(False), 'NO')
