# coding=utf-8
import json

from django.urls import reverse

from web.tests import LoggedInTestCase
from web.tests.functions import create_location


class TestApi(LoggedInTestCase):

    def test_locations(self):
        location_name = "some location"

        response = self.client.get(reverse('web.api.locations'))
        self.assertEqual(response.status_code, 200)

        create_location(location_name)

        response = self.client.get(reverse('web.api.locations'))
        locations = json.loads(response.content)['locations']

        found = False
        for location in locations:
            if location['name'] == location_name:
                found = True

        self.assertTrue(found)
