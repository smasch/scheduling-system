# coding=utf-8
import logging

from django.urls import reverse

from web.tests import LoggedInTestCase
from web.tests.functions import create_voucher_type

logger = logging.getLogger(__name__)


class TestVoucherTypeApi(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.voucher_type = create_voucher_type()

    def test_voucher_types_render(self):
        response = self.client.get(reverse('web.api.voucher_types'))
        self.assertEqual(response.status_code, 200)
