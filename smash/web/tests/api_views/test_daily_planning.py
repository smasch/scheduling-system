# coding=utf-8
import json
import logging

from django.test import RequestFactory
from django.urls import reverse
from django.utils import timezone

from web.api_views.daily_planning import get_workers_for_daily_planning, get_generic_appointment_events
from web.models import Availability, Holiday, AppointmentTypeLink, Worker
from web.models.constants import TUESDAY_AS_DAY_OF_WEEK
from web.tests import LoggedInTestCase
from web.tests.functions import create_worker, create_study_subject, create_appointment, create_flying_team, \
    create_visit, create_appointment_type, get_test_location, create_user

logger = logging.getLogger(__name__)


class TestDailyPlanningApi(LoggedInTestCase):
    def test_empty_availabilities(self):
        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)

        availabilities = json.loads(response.content)['availabilities']

        self.assertEqual(0, len(availabilities))

    def test_events_with_appointments(self):
        flying_team = create_flying_team()
        flying_team.place = "UTF name: ät"
        flying_team.save()
        subject = create_study_subject()
        visit = create_visit(subject)

        appointment = create_appointment(visit)
        appointment.datetime_when = timezone.now().replace(year=2017, month=9, day=5,
                                                           hour=12)
        appointment.length = 30
        appointment.location = self.worker.locations.all()[0]
        appointment.flying_team = flying_team
        appointment.save()

        AppointmentTypeLink.objects.create(appointment=appointment, appointment_type=create_appointment_type())

        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.content)

        self.assertEqual(0, len(content['availabilities']))
        self.assertEqual(1, len(content['data'][0]['events']))

    def test_nonempty_availabilities(self):
        availability = Availability.objects.create(person=self.worker, day_number=TUESDAY_AS_DAY_OF_WEEK,
                                                   available_from="8:00", available_till="16:00")
        availability.save()
        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)

        availabilities = json.loads(response.content)['availabilities']

        self.assertEqual(1, len(availabilities))

    def test_nonempty_availabilities_with_non_overlapping_holidays(self):
        availability = Availability.objects.create(person=self.worker, day_number=TUESDAY_AS_DAY_OF_WEEK,
                                                   available_from="8:00", available_till="16:00")
        availability.save()

        holiday = Holiday.objects.create(person=self.worker,
                                         datetime_start=timezone.now().replace(day=1, hour=5),
                                         datetime_end=timezone.now().replace(day=2, hour=20)
                                         )
        holiday.save()

        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)

        availabilities = json.loads(response.content)['availabilities']

        self.assertEqual(1, len(availabilities))

    def test_empty_availabilities_due_to_holidays(self):
        availability = Availability.objects.create(person=self.worker, day_number=TUESDAY_AS_DAY_OF_WEEK,
                                                   available_from="8:00", available_till="16:00")
        availability.save()

        holiday = Holiday.objects.create(person=self.worker,
                                         datetime_start=timezone.now().replace(year=2017, month=9, day=5,
                                                                               hour=5),
                                         datetime_end=timezone.now().replace(year=2017, month=9, day=5,
                                                                             hour=20)
                                         )
        holiday.save()

        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)

        availabilities = json.loads(response.content)['availabilities']

        self.assertEqual(0, len(availabilities))

    def test_empty_availabilities_due_to_holidays_2(self):
        availability = Availability.objects.create(person=self.worker, day_number=TUESDAY_AS_DAY_OF_WEEK,
                                                   available_from="8:00", available_till="16:00")
        availability.save()

        holiday = Holiday.objects.create(person=self.worker,
                                         datetime_start=timezone.now().replace(year=2017, month=9, day=1,
                                                                               hour=12),
                                         datetime_end=timezone.now().replace(year=2017, month=9, day=17,
                                                                             hour=12)
                                         )
        holiday.save()

        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)

        availabilities = json.loads(response.content)['availabilities']

        self.assertEqual(0, len(availabilities))

    def test_nonempty_availabilities_with_overlapping_holidays(self):
        availability = Availability.objects.create(person=self.worker, day_number=TUESDAY_AS_DAY_OF_WEEK,
                                                   available_from="8:00", available_till="16:00")
        availability.save()

        holiday = Holiday.objects.create(person=self.worker,
                                         datetime_start=timezone.now().replace(year=2017, month=9, day=5,
                                                                               hour=12),
                                         datetime_end=timezone.now().replace(year=2017, month=9, day=7,
                                                                             hour=20)
                                         )
        holiday.save()

        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)

        availabilities = json.loads(response.content)['availabilities']
        self.assertEqual(1, len(availabilities))

        holidays = json.loads(response.content)['holidays']
        self.assertEqual(1, len(holidays))

    def test_multi_day_holiday(self):
        holiday = Holiday.objects.create(person=self.worker,
                                         datetime_start=timezone.now().replace(year=2017, month=9, day=4,
                                                                               hour=10, minute=0, second=0,
                                                                               microsecond=0),
                                         datetime_end=timezone.now().replace(year=2017, month=9, day=6,
                                                                             hour=15, minute=0, second=0,
                                                                             microsecond=0)
                                         )  # monday to wed
        holiday.save()

        # monday
        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-04"}))
        self.assertEqual(response.status_code, 200)
        holidays = json.loads(response.content)['holidays']
        self.assertEqual(1, len(holidays))

        holiday = holidays[0]
        self.assertTrue('2017-09-04T10:00:00' in holiday['link_when'])
        self.assertTrue('2017-09-04T23:59:00' in holiday['link_end'])

        # tuesday
        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)
        holidays = json.loads(response.content)['holidays']
        self.assertEqual(1, len(holidays))

        holiday = holidays[0]
        self.assertTrue('2017-09-05T00:00:00' in holiday['link_when'])
        self.assertTrue('2017-09-05T23:59:00' in holiday['link_end'])

        # wed
        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-06"}))
        self.assertEqual(response.status_code, 200)
        holidays = json.loads(response.content)['holidays']
        self.assertEqual(1, len(holidays))

        holiday = holidays[0]
        self.assertTrue('2017-09-06T00:00:00' in holiday['link_when'])
        self.assertTrue('2017-09-06T15:00:00' in holiday['link_end'])

    def test_nonempty_availabilities_with_included_holidays(self):
        availability = Availability.objects.create(person=self.worker, day_number=TUESDAY_AS_DAY_OF_WEEK,
                                                   available_from="8:00", available_till="16:00")
        availability.save()

        holiday = Holiday.objects.create(person=self.worker,
                                         datetime_start=timezone.now().replace(year=2017, month=9, day=5,
                                                                               hour=12),
                                         datetime_end=timezone.now().replace(year=2017, month=9, day=5,
                                                                             hour=13)
                                         )
        holiday.save()

        response = self.client.get(reverse('web.api.events', kwargs={'date': "2017-09-05"}))
        self.assertEqual(response.status_code, 200)

        availabilities = json.loads(response.content)['availabilities']
        holidays = json.loads(response.content)['holidays']
        self.assertEqual(1, len(holidays))

        self.assertEqual(2, len(availabilities))

    def test_get_workers_for_daily_planning(self):
        factory = RequestFactory()
        request = factory.get('/api/workers')
        request.user = self.user

        user = create_user(username="a", email='jacob@bla', password="b")

        invalid_worker = Worker.get_by_user(user)
        invalid_worker.locations.set([get_test_location()])
        invalid_worker.save()
        invalid_worker.disable()

        workers = get_workers_for_daily_planning(request)
        self.assertFalse(invalid_worker in workers)

    def test_get_workers_for_daily_planning_2(self):
        factory = RequestFactory()
        request = factory.get('/api/workers')
        request.user = self.user

        invalid_worker = create_worker()
        invalid_worker.locations.set([get_test_location()])
        invalid_worker.save()
        invalid_worker.disable()

        workers = get_workers_for_daily_planning(request)
        self.assertFalse(invalid_worker in workers)

    def test_get_generic_appointment_events(self):
        factory = RequestFactory()
        request = factory.get('/api/workers')
        request.user = self.user

        appointment = create_appointment()
        appointment.datetime_when = timezone.now().replace(year=2017, month=9, day=5,
                                                           hour=12)
        appointment.length = 30
        appointment.location = self.worker.locations.all()[0]
        appointment.visit = None
        appointment.save()

        events = get_generic_appointment_events(request, "2017-09-05")
        self.assertEqual(1, len(events))
