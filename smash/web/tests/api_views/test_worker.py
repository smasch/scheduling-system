# coding=utf-8
import datetime
import json

from django.test import RequestFactory
from django.urls import reverse

from web.api_views.worker import availabilities
from web.models import Availability
from web.models.constants import TUESDAY_AS_DAY_OF_WEEK
from web.models.worker_study_role import WORKER_STAFF
from web.tests import LoggedInTestCase
from web.tests.functions import create_voucher_partner


class TestWorkerApi(LoggedInTestCase):
    def test_specializations(self):
        specialization_name = "some spec"

        response = self.client.get(reverse('web.api.specializations'))
        self.assertEqual(response.status_code, 200)

        specializations = json.loads(response.content)['specializations']

        self.assertFalse(specialization_name in specializations)

        self.worker.specialization = specialization_name
        self.worker.save()

        response = self.client.get(reverse('web.api.specializations'))
        specializations = json.loads(response.content)['specializations']

        self.assertTrue(specialization_name in specializations)

    def test_units(self):
        unit_name = "some unit"

        response = self.client.get(reverse('web.api.units'))
        self.assertEqual(response.status_code, 200)

        units = json.loads(response.content)['units']

        self.assertFalse(unit_name in units)

        self.worker.unit = unit_name
        self.worker.save()

        response = self.client.get(reverse('web.api.units'))
        units = json.loads(response.content)['units']

        self.assertTrue(unit_name in units)

    def test_workers_for_daily_planning(self):
        response = self.client.get(reverse('web.api.workers.daily_planning'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.worker.first_name.encode('utf8') in response.content)

    def test_workers_for_daily_planning_with_start_date(self):
        today = datetime.datetime.today()
        start_date = today.strftime("%Y-%m-%d")
        params = {'start_date': start_date}
        response = self.client.get(reverse('web.api.workers.daily_planning'), data=params)
        self.assertEqual(response.status_code, 200)

    def test_voucher_partners(self):
        voucher_partner = create_voucher_partner()
        response = self.client.get(reverse('web.api.workers', kwargs={'worker_role': WORKER_STAFF}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(voucher_partner.first_name.encode('utf8') in response.content)

    def test_empty_availabilities(self):
        factory = RequestFactory()
        request = factory.get('/api/workers?start_date=2017-10-20&end_date=2017-11-20')
        request.user = self.user

        result = availabilities(request)
        entries = json.loads(result.content)['availabilities']
        for entry in entries:
            self.assertEqual(0, len(entry["workers"]))

    def test_non_empty_availabilities(self):
        factory = RequestFactory()
        request = factory.get('/api/workers?start_date=2017-10-20&end_date=2017-11-20')
        request.user = self.user

        availability = Availability.objects.create(person=self.worker, day_number=TUESDAY_AS_DAY_OF_WEEK,
                                                   available_from="8:00", available_till="16:00")
        availability.save()

        result = availabilities(request)
        entries = json.loads(result.content)['availabilities']
        count = 0
        for entry in entries:
            count += len(entry["workers"])
        self.assertTrue(count > 0)

    def test_get_worker_availability(self):
        today = datetime.datetime.today().replace(hour=8, minute=00, second=0, microsecond=0)
        availability = Availability.objects.create(person=self.worker, day_number=today.isoweekday(),
                                                   available_from="8:00", available_till="16:00")
        availability.save()
        params = {}
        params['start_date'] = today.strftime("%Y-%m-%d-%H-%M")
        params['end_date'] = (today + datetime.timedelta(hours=4)).strftime("%Y-%m-%d-%H-%M")
        params['worker_id'] = self.worker.id
        response = self.client.get(reverse('web.api.get_worker_availability'), data=params)
        availability = json.loads(response.content)['availability']
        self.assertEqual(availability, 100.0)
        today = today.replace(hour=16, minute=1)
        params = {}
        params['start_date'] = today.strftime("%Y-%m-%d-%H-%M")
        params['end_date'] = (today + datetime.timedelta(hours=4)).strftime("%Y-%m-%d-%H-%M")
        params['worker_id'] = self.worker.id
        response = self.client.get(reverse('web.api.get_worker_availability'), data=params)
        availability = json.loads(response.content)['availability']
        self.assertEqual(availability, 0.0)
        today = today.replace(hour=14, minute=0)
        params = {}
        params['start_date'] = today.strftime("%Y-%m-%d-%H-%M")
        params['end_date'] = (today + datetime.timedelta(hours=4)).strftime("%Y-%m-%d-%H-%M")
        params['worker_id'] = self.worker.id
        response = self.client.get(reverse('web.api.get_worker_availability'), data=params)
        availability = json.loads(response.content)['availability']
        self.assertEqual(availability, 50.0)
