# coding=utf-8
import datetime
import json
import logging
from typing import List

from django.urls import reverse
from django.utils import timezone
from parameterized import parameterized
from six import ensure_str

from web.api_views.subject import get_subjects_order, get_subjects_filtered, serialize_subject, get_subject_columns
from web.importer.warning_counter import MsgCounterHandler
from web.models import StudySubject, Appointment, Study, Worker, SubjectColumns, StudyColumns, SubjectType
from web.models.constants import GLOBAL_STUDY_ID, CUSTOM_FIELD_TYPE_TEXT, CUSTOM_FIELD_TYPE_BOOLEAN, \
    CUSTOM_FIELD_TYPE_INTEGER, CUSTOM_FIELD_TYPE_DOUBLE, \
    CUSTOM_FIELD_TYPE_DATE, CUSTOM_FIELD_TYPE_SELECT_LIST, CUSTOM_FIELD_TYPE_FILE
from web.models.custom_data import CustomStudySubjectField
from web.models.custom_data.custom_study_subject_field import get_study_subject_field_id
from web.models.study_subject_list import SUBJECT_LIST_GENERIC, SUBJECT_LIST_NO_VISIT, SUBJECT_LIST_REQUIRE_CONTACT, \
    StudySubjectList, SUBJECT_LIST_VOUCHER_EXPIRY
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_get_suffix, create_visit, \
    create_appointment, create_empty_study_columns, create_contact_attempt, create_flying_team, create_worker, \
    get_test_study, get_patient_subject_type, get_control_subject_type
from web.views.notifications import get_today_midnight_date

logger = logging.getLogger(__name__)


class TestSubjectApi(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study_subject = create_study_subject()
        self.study_subject.subject.city = "Belvaux"
        self.study_subject.subject.address = "123, rue Waassertrap"
        self.study_subject.subject.save()
        self.warning_counter = MsgCounterHandler()
        logging.getLogger('').addHandler(self.warning_counter)

    def tearDown(self):
        logging.getLogger('').removeHandler(self.warning_counter)

    def get_warning_count(self):
        if "WARNING" in self.warning_counter.level2count:
            return self.warning_counter.level2count["WARNING"]
        return 0

    def test_cities(self):
        city_name = "some city"

        response = self.client.get(reverse('web.api.cities'))
        self.assertEqual(response.status_code, 200)

        cities = json.loads(response.content)['cities']

        self.assertFalse(city_name in cities)

        self.study_subject.subject.city = city_name
        self.study_subject.subject.save()

        response = self.client.get(reverse('web.api.cities'))
        cities = json.loads(response.content)['cities']

        self.assertTrue(city_name in cities)

    def test_subject_types(self):
        response = self.client.get(reverse('web.api.subject_types'))
        self.assertEqual(response.status_code, 200)

        types = json.loads(response.content)['types']

        for subject_type in types:
            self.assertIsNotNone(SubjectType.objects.get(pk=subject_type['id']))

    def test_get_columns(self):
        response = self.client.get(
            reverse('web.api.subjects.columns', kwargs={'subject_list_type': SUBJECT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        self.assertTrue(len(columns) >= 20)

    def test_get_columns_for_require_contact(self):
        response = self.client.get(
            reverse('web.api.subjects.columns', kwargs={'subject_list_type': SUBJECT_LIST_REQUIRE_CONTACT}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        visible_columns = 0
        for column in columns:
            if column["visible"]:
                visible_columns += 1
        self.assertTrue(visible_columns < 20)

    def test_get_columns_when_no_list_is_available(self):
        StudySubjectList.objects.all().delete()
        response = self.client.get(
            reverse('web.api.subjects.columns', kwargs={'subject_list_type': SUBJECT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        self.assertTrue(len(columns) > 0)

    def test_get_columns_when_study_has_no_data_columns(self):
        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        study.columns = create_empty_study_columns()
        study.save()

        response = self.client.get(
            reverse('web.api.subjects.columns', kwargs={'subject_list_type': SUBJECT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

        columns = json.loads(response.content)['columns']
        self.assertTrue(len(columns) >= 20)

    def test_referrals(self):
        referral_name = "some referral"

        response = self.client.get(reverse('web.api.referrals'))
        self.assertEqual(response.status_code, 200)

        referrals = json.loads(response.content)['referrals']

        self.assertFalse(referral_name in referrals)

        self.study_subject.referral = referral_name
        self.study_subject.save()

        response = self.client.get(reverse('web.api.referrals'))
        referrals = json.loads(response.content)['referrals']

        self.assertTrue(referral_name in referrals)

    def test_subjects_general(self):
        response = self.client.get(reverse('web.api.subjects', kwargs={'subject_list_type': SUBJECT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

    def test_subjects_general_via_post(self):
        response = self.client.post(reverse('web.api.subjects', kwargs={'subject_list_type': SUBJECT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

    def test_subjects_general_via_invalid_method(self):
        response = self.client.delete(reverse('web.api.subjects', kwargs={'subject_list_type': SUBJECT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 405)

    def test_subjects_general_with_special_characters(self):
        contact_attempt = create_contact_attempt(subject=self.study_subject)
        contact_attempt.worker.first_name = "à special character"
        contact_attempt.worker.save()
        response = self.client.get(reverse('web.api.subjects', kwargs={'subject_list_type': SUBJECT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

    def test_subjects_voucher_almost_expired(self):
        response = self.client.get(
            reverse('web.api.subjects', kwargs={'subject_list_type': SUBJECT_LIST_VOUCHER_EXPIRY}))
        self.assertEqual(response.status_code, 200)

    def test_subjects_no_visit(self):
        response = self.client.get(reverse('web.api.subjects', kwargs={'subject_list_type': SUBJECT_LIST_NO_VISIT}))
        self.assertEqual(response.status_code, 200)

    def test_subjects_require_contact(self):
        response = self.client.get(
            reverse('web.api.subjects', kwargs={'subject_list_type': SUBJECT_LIST_REQUIRE_CONTACT}))
        self.assertEqual(response.status_code, 200)

    def test_subjects_invalid(self):
        response = self.client.get(reverse('web.api.subjects', kwargs={'subject_list_type': "bla"}))
        self.assertEqual(response.status_code, 500)

    def test_subjects_general_search(self):
        params = {
            "columns[0][search][value]": "another_name",
            "columns[0][data]": "first_name"
        }
        self.check_search_generic_not_exist(params, "Searching by name found something that does not work")

        params["columns[0][search][value]"] = self.study_subject.subject.first_name
        self.check_search_generic_exists(params, "Searching by name does not work")

    def check_subject_filtered(self, filters, result: List[StudySubject]):
        subjects = get_subjects_filtered(StudySubject.objects.all(), filters)
        self.assertEqual(len(result), subjects.count())
        for index, result_subject in enumerate(result):
            self.assertEqual(result_subject, subjects[index])

    def check_subject_ordered(self, order, result: List[StudySubject]):
        subjects = get_subjects_order(StudySubject.objects.all(), order, "asc")
        if isinstance(subjects, list):  # sort by screening_number returns a list instead of a queryset
            self.assertEqual(len(result), len(subjects))
        else:
            self.assertEqual(len(result), subjects.count())
        for index, result_subject in enumerate(result):
            self.assertEqual(result_subject, subjects[index])

        subjects = get_subjects_order(StudySubject.objects.all(), order, "desc")
        length = len(result)
        if isinstance(subjects, list):
            self.assertEqual(len(result), len(subjects))
        else:
            self.assertEqual(len(result), subjects.count())
        for index in range(length):
            self.assertEqual(result[length - index - 1], subjects[index])

    def test_subjects_sort_nd_number(self):
        subject = self.study_subject
        subject.nd_number = "PPP"
        subject.save()

        subject2 = create_study_subject(2)
        subject2.nd_number = "QQQ"
        subject2.save()

        self.check_subject_ordered("nd_number", [subject, subject2])

    def test_subjects_sort_id(self):
        subject = self.study_subject

        subject2 = create_study_subject(2)

        self.check_subject_ordered("id", [subject, subject2])

    def test_subjects_sort_date_born(self):
        subject = self.study_subject
        subject.subject.date_born = get_today_midnight_date()
        subject.subject.save()

        subject2 = create_study_subject(2)
        subject2.subject.date_born = get_today_midnight_date() + datetime.timedelta(days=1)
        subject2.subject.save()

        self.check_subject_ordered("date_born", [subject, subject2])

    def test_subjects_sort_contact_on(self):
        subject = self.study_subject
        subject.datetime_contact_reminder = get_today_midnight_date()
        subject.save()

        subject2 = create_study_subject(2)
        subject2.datetime_contact_reminder = get_today_midnight_date() + datetime.timedelta(days=1)
        subject2.save()

        self.check_subject_ordered("datetime_contact_reminder", [subject, subject2])

    def test_subjects_sort_default_location(self):
        subject = self.study_subject

        self.check_subject_ordered("default_location", [subject])

    def test_subjects_sort_flying_team(self):
        subject = self.study_subject
        subject.flying_team = create_flying_team()
        subject.save()

        self.check_subject_ordered("flying_team", [subject])

    def test_subjects_sort_screening_number(self):
        subject = self.study_subject
        subject.screening_number = "P-001"
        subject.save()

        subject2 = create_study_subject(2)
        subject2.screening_number = "Q-001"
        subject2.save()

        self.check_subject_ordered("screening_number", [subject, subject2])

    def test_subjects_sort_last_name(self):
        subject = self.study_subject
        subject.subject.last_name = "XXX"
        subject.subject.save()

        subject2 = create_study_subject(2)
        subject2.subject.last_name = "YYY"
        subject2.subject.save()

        self.check_subject_ordered("last_name", [subject, subject2])

    def test_subjects_sort_referral(self):
        subject = self.study_subject
        subject.referral = "XXX"
        subject.save()

        subject2 = create_study_subject(2)
        subject2.referral = "YYY"
        subject2.save()

        self.check_subject_ordered("referral", [subject, subject2])

    def test_subjects_sort_dead(self):
        study_subject = self.study_subject
        study_subject.subject.dead = True
        study_subject.subject.save()

        study_subject2 = create_study_subject(2)
        study_subject2.subject.dead = False
        study_subject2.subject.save()

        self.check_subject_ordered("dead", [study_subject2, study_subject])

    def test_subjects_sort_resigned(self):
        subject = self.study_subject
        subject.resigned = True
        subject.save()

        subject2 = create_study_subject(2)
        subject2.resigned = False
        subject2.save()

        self.check_subject_ordered("resigned", [subject2, subject])

    def test_subjects_sort_endpoint_reached(self):
        subject = self.study_subject
        subject.endpoint_reached = True
        subject.save()

        subject2 = create_study_subject(2)
        subject2.endpoint_reached = False
        subject2.save()

        self.check_subject_ordered("endpoint_reached", [subject2, subject])

    def test_subjects_sort_postponed(self):
        subject = self.study_subject
        subject.postponed = True
        subject.save()

        subject2 = create_study_subject(2)
        subject2.postponed = False
        subject2.save()

        self.check_subject_ordered("postponed", [subject2, subject])

    def test_subjects_filter_dead(self):
        subject = self.study_subject.subject
        subject.dead = True
        subject.save()

        study_subject2 = create_study_subject(2)
        study_subject2.subject.dead = False
        study_subject2.subject.save()

        self.check_subject_filtered([["dead", "true"]], [self.study_subject])
        self.check_subject_filtered([["dead", "false"]], [study_subject2])

    def test_subjects_filter_nd_number(self):
        subject = self.study_subject
        subject.nd_number = "PPP"
        subject.save()

        subject2 = create_study_subject(2)
        subject2.nd_number = "QQQ"
        subject2.save()

        self.check_subject_filtered([["nd_number", "P"]], [subject])

    def test_subjects_filter_screening_number(self):
        subject = self.study_subject
        subject.screening_number = "PPP"
        subject.save()

        subject2 = create_study_subject(2)
        subject2.screening_number = "QQQ"
        subject2.save()

        self.check_subject_filtered([["screening_number", "Q"]], [subject2])

    def test_subjects_filter_last_name(self):
        subject = self.study_subject
        subject.subject.last_name = "XXX"
        subject.subject.save()

        subject2 = create_study_subject(2)
        subject2.subject.last_name = "YYY"
        subject2.subject.save()

        self.check_subject_filtered([["last_name", "Q"]], [])

    def test_subjects_filter_resigned(self):
        subject = self.study_subject
        subject.resigned = True
        subject.save()

        subject2 = create_study_subject(2)
        subject2.resigned = False
        subject2.save()

        self.check_subject_filtered([["resigned", "true"]], [subject])
        self.check_subject_filtered([["resigned", "false"]], [subject2])

    def test_subjects_filter_endpoint_reached(self):
        subject = self.study_subject
        subject.endpoint_reached = True
        subject.save()

        subject2 = create_study_subject(2)
        subject2.endpoint_reached = False
        subject2.save()

        self.check_subject_filtered([["endpoint_reached", "true"]], [subject])
        self.check_subject_filtered([["endpoint_reached", "false"]], [subject2])

    def test_subjects_filter_postponed(self):
        subject = self.study_subject
        subject.postponed = True
        subject.save()

        subject2 = create_study_subject(2)
        subject2.postponed = False
        subject2.save()

        self.check_subject_filtered([["postponed", "true"]], [subject])
        self.check_subject_filtered([["postponed", "false"]], [subject2])

    def test_subjects_filter_default_location(self):
        subject = self.study_subject

        self.check_subject_filtered([["default_location", str(subject.default_location.id)]], [subject])
        self.check_subject_filtered([["default_location", "-1"]], [])

    def test_subjects_filter_flying_team(self):
        subject = self.study_subject
        subject.flying_team = create_flying_team()
        subject.save()

        self.check_subject_filtered([["flying_team", str(subject.flying_team.id)]], [subject])
        self.check_subject_filtered([["flying_team", "-1"]], [])

    def test_subjects_filter_information_sent(self):
        subject = self.study_subject
        subject.information_sent = True
        subject.save()

        self.check_subject_filtered([["information_sent", "true"]], [subject])
        self.check_subject_filtered([["information_sent", "false"]], [])

    def test_subjects_filter_referral(self):
        subject = self.study_subject
        subject.referral = "xyz"
        subject.save()

        self.check_subject_filtered([["referral", "xyz"]], [subject])
        self.check_subject_filtered([["referral", "false"]], [])

    def test_subjects_filter_type(self):
        subject = self.study_subject
        subject.type = get_patient_subject_type()
        subject.save()

        self.check_subject_filtered([["type", get_patient_subject_type().id]], [subject])
        self.check_subject_filtered([["type", get_control_subject_type().id]], [])

    def test_subjects_filter_unknown(self):
        subject = self.study_subject

        self.check_subject_filtered([["some_unknown", "unknown data"]], [subject])
        self.check_subject_filtered([["", ""]], [subject])
        self.check_subject_filtered([[None, None]], [subject])

    def test_serialize_subject(self):
        study_subject = self.study_subject
        study_subject.subject.dead = True
        study_subject.flying_team = create_flying_team()
        study_subject.datetime_contact_reminder = get_today_midnight_date()
        study_subject.health_partner = create_worker()
        create_contact_attempt(subject=study_subject)
        create_visit(subject=study_subject)

        subject_json = serialize_subject(study_subject)
        self.assertEqual("YES", subject_json["dead"])

    def test_subjects_filter_visit_1_DONE(self):
        subject = self.study_subject

        visit = create_visit(subject)
        appointment = create_appointment(visit)
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()
        visit.mark_as_finished()

        self.check_subject_filtered([["visit_1", "DONE"]], [subject])
        self.check_subject_filtered([["visit_2", "DONE"]], [])

        self.check_subject_filtered([["visit_1", "MISSED"]], [])
        self.check_subject_filtered([["visit_1", "EXCEED"]], [])
        self.check_subject_filtered([["visit_1", "IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "SHOULD_BE_IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "UPCOMING"]], [])

    def test_subjects_filter_visit_1_MISSED(self):
        subject = self.study_subject

        visit = create_visit(subject)
        appointment = create_appointment(visit)
        appointment.status = Appointment.APPOINTMENT_STATUS_CANCELLED
        appointment.save()
        visit.mark_as_finished()

        self.check_subject_filtered([["visit_1", "MISSED"]], [subject])
        self.check_subject_filtered([["visit_2", "MISSED"]], [])

        self.check_subject_filtered([["visit_1", "DONE"]], [])
        self.check_subject_filtered([["visit_1", "EXCEED"]], [])
        self.check_subject_filtered([["visit_1", "IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "SHOULD_BE_IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "UPCOMING"]], [])

    def test_subjects_filter_visit_1_EXCEED(self):
        subject = self.study_subject

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-365 * 2)
        visit.datetime_end = get_today_midnight_date() + datetime.timedelta(days=-365 * 1)
        visit.save()

        self.check_subject_filtered([["visit_1", "EXCEED"]], [subject])
        self.check_subject_filtered([["visit_2", "EXCEED"]], [])

        self.check_subject_filtered([["visit_1", "DONE"]], [])
        self.check_subject_filtered([["visit_1", "MISSED"]], [])
        self.check_subject_filtered([["visit_1", "IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "SHOULD_BE_IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "UPCOMING"]], [])

    def test_subjects_filter_visit_1_IN_PROGRESS(self):
        subject = self.study_subject

        visit = create_visit(subject)
        appointment = create_appointment(visit)
        appointment.status = Appointment.APPOINTMENT_STATUS_SCHEDULED
        appointment.save()

        self.check_subject_filtered([["visit_1", "IN_PROGRESS"]], [subject])
        self.check_subject_filtered([["visit_2", "IN_PROGRESS"]], [])

        self.check_subject_filtered([["visit_1", "DONE"]], [])
        self.check_subject_filtered([["visit_1", "MISSED"]], [])
        self.check_subject_filtered([["visit_1", "EXCEED"]], [])
        self.check_subject_filtered([["visit_1", "SHOULD_BE_IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "UPCOMING"]], [])

    def test_subjects_filter_visit_1_SHOULD_BE_IN_PROGRESS(self):
        subject = self.study_subject

        create_visit(subject)

        self.check_subject_filtered([["visit_1", "SHOULD_BE_IN_PROGRESS"]], [subject])
        self.check_subject_filtered([["visit_2", "SHOULD_BE_IN_PROGRESS"]], [])

        self.check_subject_filtered([["visit_1", "DONE"]], [])
        self.check_subject_filtered([["visit_1", "MISSED"]], [])
        self.check_subject_filtered([["visit_1", "EXCEED"]], [])
        self.check_subject_filtered([["visit_1", "IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "UPCOMING"]], [])

    def test_subjects_filter_visit_1_UPCOMING(self):
        subject = self.study_subject

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=2)
        visit.datetime_end = get_today_midnight_date() + datetime.timedelta(days=20)
        visit.save()

        self.check_subject_filtered([["visit_1", "UPCOMING"]], [subject])
        self.check_subject_filtered([["visit_2", "UPCOMING"]], [])

        self.check_subject_filtered([["visit_1", "DONE"]], [])
        self.check_subject_filtered([["visit_1", "MISSED"]], [])
        self.check_subject_filtered([["visit_1", "EXCEED"]], [])
        self.check_subject_filtered([["visit_1", "IN_PROGRESS"]], [])
        self.check_subject_filtered([["visit_1", "SHOULD_BE_IN_PROGRESS"]], [])

    def test_subjects_filter_visit_1_visit_2_combined(self):
        subject = self.study_subject

        visit = create_visit(subject)
        appointment = create_appointment(visit)
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()
        visit.mark_as_finished()

        self.check_subject_filtered([["visit_1", "DONE"]], [subject])
        self.check_subject_filtered([["visit_2", "UPCOMING"]], [subject])

        self.check_subject_filtered([["visit_1", "DONE"], ["visit_2", "UPCOMING"]], [subject])
        self.check_subject_filtered([["visit_1", "UPCOMING"], ["visit_2", "DONE"]], [])

    def test_subjects_ordered_by_visit_1(self):
        subject = self.study_subject
        subject2 = create_study_subject(2)

        visit = create_visit(subject)
        appointment = create_appointment(visit)
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()

        create_visit(subject2, datetime_begin=get_today_midnight_date() + datetime.timedelta(days=31),
                     datetime_end=get_today_midnight_date() + datetime.timedelta(days=61))

        self.check_subject_ordered("visit_1", [subject, subject2])

    def test_invalid_order(self):
        self.check_subject_ordered(None, [self.study_subject])

    def test_subjects_ordered_by_contact_attempt(self):
        subject = self.study_subject
        subject2 = create_study_subject(2)

        contact_attempt = create_contact_attempt(subject=subject)
        contact_attempt.datetime_when = timezone.now().replace(1900, 1, 1)
        contact_attempt.save()

        create_contact_attempt(subject=subject2)

        self.check_subject_ordered("last_contact_attempt", [subject, subject2])

    def test_subjects_ordered_by_information_sent(self):
        subject = self.study_subject
        subject.information_sent = False
        subject.save()
        subject2 = create_study_subject(2)
        subject2.information_sent = True
        subject2.save()

        self.check_subject_ordered("information_sent", [subject, subject2])

    def test_subjects_ordered_by_type(self):
        subject = self.study_subject
        subject.type = get_control_subject_type()
        subject.save()
        subject2 = create_study_subject(2)
        subject2.type = get_patient_subject_type()
        subject2.save()

        self.check_subject_ordered("type", [subject, subject2])

    def test_subjects_ordered_by_social_security_number(self):
        subject = self.study_subject
        subject.subject.social_security_number = "01"
        subject.subject.save()
        subject2 = create_study_subject(2)
        subject2.subject.social_security_number = "02"
        subject2.subject.save()

        self.check_subject_ordered("social_security_number", [subject, subject2])

    def test_subjects_ordered_by_health_partner(self):
        subject = self.study_subject
        subject.health_partner = Worker.objects.create(first_name='first1', last_name="name2222", email='jacob@bla', )
        subject.save()
        subject2 = create_study_subject(2)
        subject2.health_partner = Worker.objects.create(first_name='first2', last_name="name1111", email='jacob@bla', )
        subject2.save()

        self.check_subject_ordered("health_partner_first_name", [subject, subject2])
        self.check_subject_ordered("health_partner_last_name", [subject2, subject])

    def test_subjects_filter_health_partner_first_name(self):
        subject = self.study_subject
        subject.health_partner = Worker.objects.create(first_name='first1', last_name="name2222", email='jacob@bla', )
        subject.save()

        self.check_subject_filtered([["health_partner_first_name", "first1"]], [subject])
        self.check_subject_filtered([["health_partner_first_name", "unknown"]], [])

    def test_subjects_filter_health_partner_last_name(self):
        subject = self.study_subject
        subject.health_partner = Worker.objects.create(first_name='first1', last_name="name2222", email='jacob@bla', )
        subject.save()

        self.check_subject_filtered([["health_partner_last_name", "name2222"]], [subject])
        self.check_subject_filtered([["health_partner_last_name", "unknown"]], [])

    def test_subjects_filter_social_security_number(self):
        subject = self.study_subject.subject
        subject.social_security_number = "123"
        subject.save()

        self.check_subject_filtered([["social_security_number", "12"]], [self.study_subject])
        self.check_subject_filtered([["social_security_number", "unknown"]], [])

    def test_get_subject_order_for_columns(self):
        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        subject_list = StudySubjectList.objects.create(study=study, type="custom",
                                                       visible_subject_columns=SubjectColumns.objects.create(),
                                                       visible_subject_study_columns=StudyColumns.objects.create())
        for key in vars(subject_list.visible_subject_columns):
            if not getattr(subject_list.visible_subject_columns, key):
                setattr(subject_list.visible_subject_columns, key, True)
        subject_list.visible_subject_columns.save()

        for key in vars(subject_list.visible_subject_study_columns):
            if not getattr(subject_list.visible_subject_study_columns, key):
                setattr(subject_list.visible_subject_study_columns, key, True)
        subject_list.visible_subject_study_columns.save()

        for key in vars(study.columns):
            if not getattr(study.columns, key):
                setattr(study.columns, key, True)
        study.columns.save()

        available_columns = get_subject_columns(None, "custom")
        temp = ensure_str(available_columns.content)
        columns = json.loads(temp)['columns']
        for row in columns:
            if row['sortable']:
                order = row['type']
                get_subjects_order(StudySubject.objects.none(), order, "asc")
                self.assertEqual(self.get_warning_count(), 0, msg="Sorting by \"" + order + "\" does not work")

    @parameterized.expand([
        ('text', CUSTOM_FIELD_TYPE_TEXT, 'bla'),
        ('bool', CUSTOM_FIELD_TYPE_BOOLEAN, 'True'),
        ('int', CUSTOM_FIELD_TYPE_INTEGER, '103'),
        ('double', CUSTOM_FIELD_TYPE_DOUBLE, '205.5'),
        ('date', CUSTOM_FIELD_TYPE_DATE, '2020-11-05'),
        ('select list', CUSTOM_FIELD_TYPE_SELECT_LIST, 'BLA'),
    ])
    def test_subjects_filter_by_custom_field(self, _, field_type, value):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="", type=field_type)
        study_subject = create_study_subject(2)
        study_subject.set_custom_data_value(field, value)

        self.check_subject_filtered([[get_study_subject_field_id(field), value]], [study_subject])
        self.check_subject_filtered([[get_study_subject_field_id(field), value + "-bla"]], [])

    def test_subjects_filter_by_custom_file_field(self):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="",
                                                       type=CUSTOM_FIELD_TYPE_FILE)
        study_subject = create_study_subject(2)
        study_subject.set_custom_data_value(field, 'test_file.txt')

        self.check_subject_filtered([[get_study_subject_field_id(field), 'Available']], [study_subject])
        self.check_subject_filtered([[get_study_subject_field_id(field), "N/A"]], [])

    def test_subjects_filter_by_two_custom_fields(self):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz",
                                                       type=CUSTOM_FIELD_TYPE_TEXT)
        field2 = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz",
                                                        type=CUSTOM_FIELD_TYPE_TEXT)
        study_subject = create_study_subject(2)
        study_subject.set_custom_data_value(field, "bla")
        study_subject.set_custom_data_value(field2, "ab")

        self.check_subject_filtered([[get_study_subject_field_id(field), "bla"],
                                     [get_study_subject_field_id(field2), "ab"]], [study_subject])
        self.check_subject_filtered([[get_study_subject_field_id(field), "bla"],
                                     [get_study_subject_field_id(field2), "abcs"]], [])

    @parameterized.expand([
        ('text', CUSTOM_FIELD_TYPE_TEXT, 'bla', 'cla', 'dla'),
        ('bool', CUSTOM_FIELD_TYPE_BOOLEAN, '', 'False', 'True'),
        ('int', CUSTOM_FIELD_TYPE_INTEGER, '106', '107', '201'),
        ('double', CUSTOM_FIELD_TYPE_DOUBLE, '206.12', '300.3', '400'),
        ('date', CUSTOM_FIELD_TYPE_DATE, '2020-11-05', '2021-01-05', '2021-01-06',),
        ('select list', CUSTOM_FIELD_TYPE_SELECT_LIST, 'BLA', 'BLA2', 'BLA3'),
        ('file list', CUSTOM_FIELD_TYPE_FILE, 'file1.txt', 'file2.txt', 'file3.txt'),
    ])
    def test_subjects_sort_by_custom_fields(self, _, field_type, value1, value2, value3):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="", type=field_type)
        study_subject = self.study_subject
        study_subject2 = create_study_subject(3)
        study_subject3 = create_study_subject(4)
        study_subject.set_custom_data_value(field, value1)
        study_subject2.set_custom_data_value(field, value3)
        study_subject3.set_custom_data_value(field, value2)

        self.check_subject_ordered(get_study_subject_field_id(field), [study_subject, study_subject3, study_subject2])

    @parameterized.expand([
        ('text', CUSTOM_FIELD_TYPE_TEXT, 'bla'),
        ('bool', CUSTOM_FIELD_TYPE_BOOLEAN, 'True'),
        ('int', CUSTOM_FIELD_TYPE_INTEGER, '340'),
        ('double', CUSTOM_FIELD_TYPE_DOUBLE, '540.8'),
        ('date', CUSTOM_FIELD_TYPE_DATE, '2020-11-07'),
        ('select list', CUSTOM_FIELD_TYPE_SELECT_LIST, 'BLA'),
        ('file', CUSTOM_FIELD_TYPE_FILE, 'test_file.txt'),
    ])
    def test_subjects_columns_for_custom(self, _, field_type, value):
        CustomStudySubjectField.objects.create(study=Study.objects.filter(id=GLOBAL_STUDY_ID)[0], default_value=value,
                                               type=field_type)
        response = self.client.get(
            reverse('web.api.subjects.columns', kwargs={'subject_list_type': SUBJECT_LIST_GENERIC}))
        self.assertEqual(response.status_code, 200)

    @parameterized.expand([
        ('text', CUSTOM_FIELD_TYPE_TEXT, 'bla'),
        ('bool', CUSTOM_FIELD_TYPE_BOOLEAN, 'True'),
        ('int', CUSTOM_FIELD_TYPE_INTEGER, '341'),
        ('double', CUSTOM_FIELD_TYPE_DOUBLE, '541.54'),
        ('date', CUSTOM_FIELD_TYPE_DATE, '2020-11-07'),
        ('select list', CUSTOM_FIELD_TYPE_SELECT_LIST, 'BLA'),
        ('file', CUSTOM_FIELD_TYPE_FILE, 'test_file.txt'),
    ])
    def test_serialize_subject_with_custom_field(self, _, field_type, value):
        field = CustomStudySubjectField.objects.create(study=get_test_study(),
                                                       default_value='',
                                                       type=field_type)

        study_subject = self.study_subject
        study_subject.set_custom_data_value(field, value)

        subject_json = serialize_subject(study_subject)
        self.assertIsNotNone(subject_json[get_study_subject_field_id(field)])
        self.assertTrue(subject_json[get_study_subject_field_id(field)] != '')

    def test_search_by_street(self):
        params = {
            "columns[0][search][value]": self.study_subject.subject.address,
            "columns[0][data]": "address"
        }
        self.check_search_generic_exists(params, "Searching by street does not work")

    def test_search_by_unknown_address(self):
        params = {
            "columns[0][search][value]": "bla",
            "columns[0][data]": "address"
        }
        self.check_search_generic_not_exist(params, "Searching by street found something that does not exist")

    def test_search_by_city(self):
        params = {
            "columns[0][search][value]": self.study_subject.subject.city,
            "columns[0][data]": "address"
        }
        self.check_search_generic_exists(params, "Searching by city does not work")

    def test_search_by_country(self):
        params = {
            "columns[0][search][value]": self.study_subject.subject.country.name,
            "columns[0][data]": "address"
        }
        self.check_search_generic_exists(params, "Searching by country does not work")

    def check_search_generic_exists(self, params, message: str):
        url = ("%s" + create_get_suffix(params)) % reverse('web.api.subjects',
                                                           kwargs={'subject_list_type': SUBJECT_LIST_GENERIC})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.study_subject.subject.first_name.encode('utf8') in response.content, message)

    def check_search_generic_not_exist(self, params, message: str):
        url = ("%s" + create_get_suffix(params)) % reverse('web.api.subjects',
                                                           kwargs={'subject_list_type': SUBJECT_LIST_GENERIC})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.study_subject.subject.first_name.encode('utf8') in response.content, message)
