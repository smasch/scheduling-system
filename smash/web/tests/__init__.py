import logging
import os
import sys

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import Client
from django.test import TestCase

from web.decorators import PermissionDecorator
from web.models import Worker
from .functions import create_worker, create_user, add_permissions_to_worker

# if manage.py test was called, use test settings
if 'test' in sys.argv:
    try:
        settings.MEDIA_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
    except ImportError:
        pass

logger = logging.getLogger(__name__)


class LoggedInTestCase(TestCase):
    _super_worker: Worker = None
    _admin_worker: Worker = None
    _staff_worker: Worker = None

    def setUp(self):
        self.password = 'passwd1234'

        username = 'piotr'
        password = 'top_secret'
        # noinspection SpellCheckingInspection
        password_hash = 'pbkdf2_sha256$216000$EBmA2dPji3lf$O17j8LDPyH5bNYdv6LSmRlSSKnKwFRBQqgozunlZK2E='
        self.user = get_user_model().objects.create(username=username, password=password_hash, email='jacob@bla')
        self.user.save()
        # self.user = get_user_model().objects.create_user(username=username, email='jacob@bla', password=password)

        self.worker = create_worker(self.user, True)

        self.client = Client()
        self.client.login(username=username, password=password)

    @property
    def super_worker(self) -> Worker:
        if self._super_worker is None:
            self._super_worker = create_worker(
                user=get_user_model().objects.create_superuser(username='super', password=self.password,
                                                               email='a@mail.com'))
        return self._super_worker

    @property
    def admin_worker(self) -> Worker:
        if self._admin_worker is None:
            self._admin_worker = Worker.get_by_user(create_user(username='admin', password=self.password))
            add_permissions_to_worker(self._admin_worker, PermissionDecorator.codenames)
        return self._admin_worker

    @property
    def staff_worker(self) -> Worker:
        if self._staff_worker is None:
            self._staff_worker = Worker.get_by_user(create_user(username='staff', password=self.password))
        return self._staff_worker

    def login_as_staff(self):
        self.client.logout()
        self.client.login(username=self.staff_worker.user.username, password=self.password)

    def login_as_admin(self):
        self.client.logout()
        self.client.login(username=self.admin_worker.user.username, password=self.password)

    def login_as_super(self):
        self.client.logout()
        self.client.login(username=self.super_worker.user.username, password=self.password)
