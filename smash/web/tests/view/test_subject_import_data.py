import logging

from django.urls import reverse

from web.forms.subject_import_data_form import SubjectImportDataEditForm
from web.models import SubjectImportData
from web.models.constants import CUSTOM_FIELD_TYPE_TEXT
from web.models.custom_data import CustomStudySubjectField
from web.tests import LoggedInTestCase
from web.tests.functions import get_test_study, format_form_field

logger = logging.getLogger(__name__)


class SubjectImportDataViewViewTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study = get_test_study()
        self.subject_import_data = SubjectImportData.objects.create(study=get_test_study())

    def test_render_edit(self):
        self.login_as_admin()
        response = self.client.get(reverse('web.views.import_subject_edit',
                                           kwargs={'study_id': self.study.id,
                                                   'import_id': self.subject_import_data.id}))
        self.assertEqual(response.status_code, 200)

    def test_save_edit(self):
        self.login_as_admin()
        form_data = self.get_form_data(self.subject_import_data)

        response = self.client.post(
            reverse('web.views.import_subject_edit',
                    kwargs={'study_id': self.study.id, 'import_id': self.subject_import_data.id}), data=form_data)

        self.assertEqual(response.status_code, 302)
        self.assertTrue("study" in response['Location'])

    def test_form_data_for_custom_field(self):
        form_data = self.get_form_data(self.subject_import_data)
        field_count = len(form_data)
        CustomStudySubjectField.objects.create(study=self.study, name='HW', type=CUSTOM_FIELD_TYPE_TEXT)

        form_data_with_custom_field = self.get_form_data(self.subject_import_data)
        field_count_with_custom_field = len(form_data_with_custom_field)
        self.assertTrue(field_count < field_count_with_custom_field)

    @staticmethod
    def get_form_data(subject_import_data: SubjectImportData) -> dict:
        voucher_form = SubjectImportDataEditForm(instance=subject_import_data)
        form_data = {}
        for key, value in list(voucher_form.initial.items()):
            form_data[key] = format_form_field(value)
        for key, field in voucher_form.fields.items():
            if form_data.get(key) is None:
                form_data[key] = field.initial

        return form_data
