import logging

from django.contrib.auth import get_user_model
from django.urls import reverse

from web.forms import WorkerForm
from web.models import Worker
from web.models.worker_study_role import WORKER_STAFF, ROLE_CHOICES_DOCTOR, WORKER_HEALTH_PARTNER, \
    WORKER_VOUCHER_PARTNER
from web.tests import create_worker
from web.tests.functions import create_language, create_location, create_availability, format_form_field
from .. import LoggedInTestCase

logger = logging.getLogger(__name__)


class WorkerViewTests(LoggedInTestCase):
    def test_render_workers_list_request(self):
        self.login_as_admin()

        create_worker()

        response = self.client.get(reverse('web.views.workers'))
        self.assertEqual(response.status_code, 200)

    def test_render_workers_list_request_staff(self):
        self.login_as_staff()

        create_worker()

        response = self.client.get(reverse('web.views.workers'))
        self.assertEqual(response.status_code, 302)

    def test_render_worker_type_request(self):
        self.login_as_admin()

        create_worker()

        response = self.client.get(reverse('web.views.workers', kwargs={'worker_type': WORKER_STAFF}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('web.views.workers', kwargs={'worker_type': WORKER_HEALTH_PARTNER}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('web.views.workers', kwargs={'worker_type': WORKER_VOUCHER_PARTNER}))
        self.assertEqual(response.status_code, 200)

    def test_render_worker_type_request_staff(self):
        self.login_as_staff()

        create_worker()

        response = self.client.get(reverse('web.views.workers', kwargs={'worker_type': WORKER_STAFF}))
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('web.views.workers', kwargs={'worker_type': WORKER_HEALTH_PARTNER}))
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('web.views.workers', kwargs={'worker_type': WORKER_VOUCHER_PARTNER}))
        self.assertEqual(response.status_code, 302)

    def test_render_add_worker_request(self):
        self.login_as_admin()

        response = self.client.get(reverse('web.views.worker_add', kwargs={'worker_type': WORKER_STAFF}))
        self.assertEqual(response.status_code, 200)

    def test_render_add_worker_request_without_permissions(self):
        response = self.client.get(reverse('web.views.worker_add', kwargs={'worker_type': WORKER_STAFF}))
        self.assertEqual(response.status_code, 302)

    def test_render_add_worker_request_for_voucher_partner(self):
        self.login_as_admin()

        response = self.client.get(reverse('web.views.worker_add', kwargs={'worker_type': WORKER_VOUCHER_PARTNER}))
        self.assertEqual(response.status_code, 200)

    def test_render_worker_added_request(self):
        self.login_as_admin()

        worker_count = Worker.objects.all().count()
        user_count = get_user_model().objects.all().count()

        language = create_language()
        location = create_location()

        form_data = self.create_add_worker_form_data(language, location)

        response = self.client.post(reverse('web.views.worker_add', kwargs={'worker_type': WORKER_STAFF}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)

        self.assertEqual(worker_count + 1, Worker.objects.all().count())
        self.assertEqual(user_count + 1, get_user_model().objects.all().count())

    def create_add_worker_form_data(self, language, location):
        form_data = self.get_form_data(Worker())
        form_data["first_name"] = "John"
        form_data["last_name"] = "Doe"
        form_data["phone_number"] = "0123456789"
        form_data["unit"] = "TEST DEPARTMENT"
        form_data["email"] = "john.doe@unknown.domain.com"
        form_data["specialization"] = "tester"
        form_data["login"] = "tester.login"
        form_data["password"] = "123qweasdzxc"
        form_data["password2"] = "123qweasdzxc"
        form_data["languages"] = [language.id]
        form_data["locations"] = [location.id]
        form_data["role"] = ROLE_CHOICES_DOCTOR
        return form_data

    def test_security_in_worker_added_request(self):
        self.client.logout()

        language = create_language()
        location = create_location()
        count = Worker.objects.all().count()

        form_data = self.create_add_worker_form_data(language, location)

        self.client.post(reverse('web.views.worker_add', kwargs={'worker_type': WORKER_STAFF}), data=form_data)

        new_count = Worker.objects.all().count()
        # new user shouldn't be added
        self.assertEqual(count, new_count)

    @staticmethod
    def get_form_data(worker=None):
        form = WorkerForm(instance=worker)
        form_data = {}
        for key, value in list(form.initial.items()):
            form_data[key] = format_form_field(value)
        return form_data

    def test_render_edit_worker_request(self):
        self.login_as_admin()
        worker = create_worker()

        form_data = self.get_form_data(worker)
        form_data["last_name"] = "XYZ"
        form_data["role"] = ROLE_CHOICES_DOCTOR

        response = self.client.post(reverse('web.views.worker_edit', args=[worker.id]), data=form_data)
        self.assertEqual(response.status_code, 302)

        updated_worker = Worker.objects.get(id=worker.id)
        self.assertEqual(updated_worker.last_name, form_data["last_name"])

    def test_edit_worker_superuser(self):
        self.login_as_super()

        response = self.client.get(reverse('web.views.worker_edit', args=[self.super_worker.id]))
        self.assertEqual(response.status_code, 200)

    def test_edit_worker_without_permissions(self):
        self.login_as_staff()

        response = self.client.get(reverse('web.views.worker_edit', args=[self.staff_worker.id]))
        self.assertEqual(response.status_code, 302)

    def test_edit_worker_with_permissions(self):
        self.login_as_admin()

        response = self.client.get(reverse('web.views.worker_edit', args=[self.admin_worker.id]))
        self.assertEqual(response.status_code, 200)

    def test_render_add_availability_request(self):
        self.login_as_admin()
        response = self.client.get(reverse('web.views.worker_availability_add', args=[self.admin_worker.id]))
        self.assertEqual(response.status_code, 200)

    def test_render_edit_availability_request(self):
        self.login_as_admin()

        availability = create_availability(self.admin_worker)

        response = self.client.get(reverse('web.views.worker_availability_edit', args=[availability.id]))
        self.assertEqual(response.status_code, 200)

    def test_render_add_holiday_request(self):
        self.login_as_admin()

        response = self.client.get(reverse('web.views.worker_holiday_add', args=[self.admin_worker.id]))
        self.assertEqual(response.status_code, 200)
