import datetime
import logging
from datetime import date

from django.test import TestCase

from web.utils import get_weekdays_in_period

logger = logging.getLogger(__name__)


class Utils(TestCase):
    def test_get_weekdays_in_period(self):
        from_date = date(2018, 10, 9)
        to_date = date(2018, 10, 12)
        weekdays = get_weekdays_in_period(from_date, to_date)
        self.assertEqual(weekdays, {2, 3, 4})

        to_date = datetime.datetime(2018, 10, 12, 00, 00, 00)
        from_date = datetime.datetime(2018, 10, 9, 00, 00, 00)
        weekdays = get_weekdays_in_period(from_date, to_date)
        self.assertEqual(weekdays, {2, 3, 4})
