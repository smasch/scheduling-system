import logging

from django.urls import reverse

from web.tests import LoggedInTestCase
from web.tests.functions import create_location

logger = logging.getLogger(__name__)


class LocationsTests(LoggedInTestCase):

    def test_rooms_delete_request(self):
        self.login_as_admin()
        location = create_location()
        page = reverse('web.views.location_delete',
                       kwargs={'pk': str(location.id)})
        self.client.post(page)
        location.refresh_from_db()
        self.assertTrue(location.removed)
