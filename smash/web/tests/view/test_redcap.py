import logging

from django.urls import reverse

from web.tests.functions import prepare_test_redcap_connection
from .. import LoggedInTestCase

logger = logging.getLogger(__name__)


class RedcapViewTests(LoggedInTestCase):
    def test_render_workers_list_request(self):
        response = self.client.get(reverse('web.views.missing_redcap_subject'))
        self.assertEqual(response.status_code, 200)

    # @unittest.skip("test redcap server is down")
    def test_render_workers_list_request_with_valid_connection(self):
        prepare_test_redcap_connection()

        response = self.client.get(reverse('web.views.missing_redcap_subject'))
        self.assertEqual(response.status_code, 200)

    def test_render_add_worker_request(self):
        response = self.client.get(reverse('web.views.inconsistent_redcap_subject'))
        self.assertEqual(response.status_code, 200)

    # @unittest.skip("test redcap server is down")
    def test_render_add_worker_request_with_valid_connection(self):
        prepare_test_redcap_connection()

        response = self.client.get(reverse('web.views.inconsistent_redcap_subject'))
        self.assertEqual(response.status_code, 200)
