import datetime
import logging

from django.contrib.auth.models import AnonymousUser
from django.utils import timezone

from web.models import Appointment, Location, AppointmentTypeLink, Study, Visit
from web.models.constants import GLOBAL_STUDY_ID, VOUCHER_STATUS_USED
from web.tests import LoggedInTestCase
from web.tests.functions import create_appointment, create_location, create_worker, create_appointment_type, \
    create_empty_notification_parameters, create_study_subject, create_visit, create_voucher, create_contact_attempt, \
    create_full_notification_parameters
from web.views.notifications import \
    get_approaching_visits_for_mail_contact, \
    get_approaching_visits_for_mail_contact_count, \
    get_approaching_visits_without_appointments, \
    get_approaching_visits_without_appointments_count, \
    get_exceeded_visit_notifications_count, \
    get_filter_locations, \
    get_notifications, \
    get_subject_with_no_visit_notifications_count, \
    get_subjects_with_reminder_count, \
    get_visits_without_appointments_count, \
    get_visits_with_missing_appointments_count, \
    get_today_midnight_date, \
    get_unfinished_appointments, \
    get_unfinished_appointments_count, \
    get_unfinished_visits, get_exceeded_visits, get_subject_voucher_expiry_notifications_count

logger = logging.getLogger(__name__)


class NotificationViewTests(LoggedInTestCase):
    def test_get_exceeded_visit_notifications_count(self):
        original_notification = get_exceeded_visit_notifications_count(self.user)

        subject = create_study_subject()
        Visit.objects.create(datetime_begin=timezone.now().replace(year=2011, month=1, day=1),
                             datetime_end=timezone.now().replace(year=2011, month=1, day=2),
                             subject=subject)

        # first visit doesn't go to notifications
        notification = get_exceeded_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

        Visit.objects.create(datetime_begin=timezone.now().replace(year=2012, month=1, day=1),
                             datetime_end=timezone.now().replace(year=2012, month=1, day=2),
                             subject=subject)

        notification = get_exceeded_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_exceeded_visit_notifications_count_for_first_visit(self):
        subject = create_study_subject()
        visit = Visit.objects.create(datetime_begin=timezone.now().replace(year=2011, month=1, day=1),
                                     datetime_end=timezone.now().replace(year=2011, month=1, day=2),
                                     subject=subject)
        appointment = create_appointment(visit)
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()
        notification = get_exceeded_visit_notifications_count(self.user)
        self.assertEqual(1, notification.count)

    def test_get_exceeded_visit_notifications_count_2(self):
        original_notification = get_visits_without_appointments_count(self.user)

        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_end = timezone.now().replace(year=2011)
        visit.is_finished = True
        visit.save()
        create_appointment(visit)

        notification = get_exceeded_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_visits_without_appointments_count(self):
        original_notification = get_visits_without_appointments_count(self.user)
        subject = create_study_subject()
        create_visit(subject)

        notification = get_visits_without_appointments_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_visits_with_missing_appointments_count(self):
        original_notification = get_visits_with_missing_appointments_count(self.user)

        appointment_type = create_appointment_type()
        visit = create_visit()
        visit.appointment_types.add(appointment_type)
        visit.save()

        notification = get_visits_with_missing_appointments_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_visits_with_missing_appointments_count_2(self):
        original_notification = get_visits_with_missing_appointments_count(self.user)

        appointment_type = create_appointment_type()
        visit = create_visit()
        visit.appointment_types.add(appointment_type)
        visit.save()

        appointment = create_appointment(visit)
        AppointmentTypeLink.objects.create(appointment=appointment, appointment_type=appointment_type)
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()

        notification = get_visits_with_missing_appointments_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_notifications(self):
        result = get_notifications(self.user)

        self.assertEqual(0, result[0])
        self.assertTrue(isinstance(result[1], list))
        self.assertTrue(len(result[1]) > 0)

    def test_get_notifications_with_empty_study_notification(self):
        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        study.notification_parameters = create_empty_notification_parameters()
        study.save()

        result = get_notifications(self.user)

        self.assertEqual(0, result[0])
        self.assertEqual(0, len(result[1]))

    def test_get_notifications_with_full_study_notification(self):
        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        study.notification_parameters = create_full_notification_parameters()
        study.save()

        result = get_notifications(self.user)

        self.assertEqual(0, result[0])
        self.assertTrue(len(result[1]) > 0)

    def test_get_visits_without_appointments_count_2(self):
        appointment_type = create_appointment_type()
        original_notification = get_visits_without_appointments_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.appointment_types.add(appointment_type)
        visit.save()

        notification = get_visits_without_appointments_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_visits_without_appointments_count_3(self):
        original_notification = get_visits_without_appointments_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)

        appointment.status = Appointment.APPOINTMENT_STATUS_CANCELLED
        appointment.save()

        notification = get_visits_without_appointments_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_unfinished_visits_order(self):
        subject = create_study_subject()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-10)
        visit.save()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-8)
        visit.save()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-12)
        visit.save()

        visits = get_unfinished_visits(self.user)
        self.assertEqual(3, visits.count())

        # check sort order
        self.assertTrue(visits[0].datetime_begin < visits[1].datetime_begin)
        self.assertTrue(visits[1].datetime_begin < visits[2].datetime_begin)

    def test_get_exceeded_visits(self):
        subject = create_study_subject()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-12)
        visit.datetime_end = get_today_midnight_date() + datetime.timedelta(days=-10)
        visit.visit_number = 1
        visit.is_finished = True
        visit.save()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-10)
        visit.datetime_end = get_today_midnight_date() + datetime.timedelta(days=-8)
        visit.visit_number = 2
        visit.save()

        visits = get_exceeded_visits(self.user)
        self.assertEqual(1, visits.count())

    def test_get_exceeded_visits_with_scheduled_appointment(self):
        subject = create_study_subject()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-12)
        visit.datetime_end = get_today_midnight_date() + datetime.timedelta(days=-10)
        visit.visit_number = 1
        visit.is_finished = True
        visit.save()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-10)
        visit.datetime_end = get_today_midnight_date() + datetime.timedelta(days=-8)
        visit.visit_number = 2
        visit.save()
        create_appointment(visit)

        visits = get_exceeded_visits(self.user)
        self.assertEqual(0, visits.count())

    def test_get_approaching_visits_without_appointments_count(self):
        original_notification = get_approaching_visits_without_appointments_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=2)
        visit.save()

        notification = get_approaching_visits_without_appointments_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_approaching_visits_without_appointments_count_2(self):
        original_notification = get_approaching_visits_without_appointments_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=2)
        visit.save()
        create_appointment(visit)

        notification = get_approaching_visits_without_appointments_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_approaching_visits_without_appointments_count_3(self):
        original_notification = get_approaching_visits_without_appointments_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=2)
        visit.save()
        appointment = create_appointment(visit)

        appointment.status = Appointment.APPOINTMENT_STATUS_CANCELLED
        appointment.save()

        notification = get_approaching_visits_without_appointments_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_approaching_visits_without_appointments_order(self):
        subject = create_study_subject()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=10)
        visit.save()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=8)
        visit.save()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=12)
        visit.save()

        visits = get_approaching_visits_without_appointments(self.user)
        self.assertEqual(3, visits.count())

        # check sort order
        self.assertTrue(visits[0].datetime_begin < visits[1].datetime_begin)
        self.assertTrue(visits[1].datetime_begin < visits[2].datetime_begin)

    def test_get_subject_with_no_visit_notifications_count(self):
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        create_study_subject()

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_subject_with_no_visit_notifications_count_with_new_voucher(self):
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        study_subject = create_study_subject()
        create_voucher(study_subject)

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_subject_with_no_visit_notifications_count_with_used_voucher(self):
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        study_subject = create_study_subject()
        voucher = create_voucher(study_subject)
        voucher.status = VOUCHER_STATUS_USED
        voucher.save()

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_subject_with_no_visit_notifications_count_2(self):
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        subject = create_study_subject()

        visit = create_visit(subject)
        visit.is_finished = True
        visit.save()

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_subject_with_no_visit_notifications_count_3(self):
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        subject = create_study_subject()

        create_visit(subject)

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_subject_with_no_visit_notifications_count_4(self):
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        study_subject = create_study_subject()
        study_subject.subject.dead = True
        study_subject.subject.save()

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_subject_with_no_visit_notifications_count_5(self):
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        subject = create_study_subject()
        subject.resigned = True
        subject.save()

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_subject_with_no_visit_notifications_count_6(self):
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        subject = create_study_subject()
        subject.endpoint_reached = True
        subject.save()

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_unfinished_appointments_count(self):
        original_notification = get_unfinished_appointments_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)
        appointment.datetime_when = timezone.now().replace(year=2011)
        appointment.status = Appointment.APPOINTMENT_STATUS_SCHEDULED
        appointment.save()

        notification = get_unfinished_appointments_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_unfinished_appointments_count_for_general_appointments(self):
        appointment = create_appointment()
        appointment.visit = None
        appointment.datetime_when = timezone.now().replace(year=2011, month=1, day=1)
        appointment.status = Appointment.APPOINTMENT_STATUS_SCHEDULED
        appointment.save()

        notification = get_unfinished_appointments_count(self.user)
        self.assertEqual(0, notification.count)

    def test_get_unfinished_appointments_count_2(self):
        original_notification = get_unfinished_appointments_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)
        appointment.datetime_when = timezone.now().replace(year=2011, month=1, day=1)
        appointment.status = Appointment.APPOINTMENT_STATUS_CANCELLED
        appointment.save()

        notification = get_unfinished_appointments_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_unfinished_appointments_order(self):
        subject = create_study_subject()
        visit = create_visit(subject)

        appointment = create_appointment(visit, get_today_midnight_date() + datetime.timedelta(days=-10))
        appointment.status = Appointment.APPOINTMENT_STATUS_SCHEDULED
        appointment.save()
        appointment = create_appointment(visit, get_today_midnight_date() + datetime.timedelta(days=-8))
        appointment.status = Appointment.APPOINTMENT_STATUS_SCHEDULED
        appointment.save()
        appointment = create_appointment(visit, get_today_midnight_date() + datetime.timedelta(days=-12))
        appointment.status = Appointment.APPOINTMENT_STATUS_SCHEDULED
        appointment.save()

        appointments = get_unfinished_appointments(self.user)
        self.assertEqual(3, appointments.count())

        # check sort order
        self.assertTrue(appointments[0].datetime_when < appointments[1].datetime_when)
        self.assertTrue(appointments[1].datetime_when < appointments[2].datetime_when)

    def test_get_subject_with_no_visit_notifications_count_for_many_locations(self):
        create_location("l1")
        create_location("l2")
        original_notification = get_subject_with_no_visit_notifications_count(self.user)
        create_study_subject()

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_subject_with_no_visit_notifications_count_for_invalid_location(self):
        worker = create_worker()
        worker.locations.set([create_location("l2")])
        worker.save()

        original_notification = get_subject_with_no_visit_notifications_count(worker)

        subject = create_study_subject()
        subject.default_location = create_location("l1")
        subject.save()

        notification = get_subject_with_no_visit_notifications_count(worker)
        self.assertEqual(original_notification.count, notification.count)

        worker.locations.set(Location.objects.filter(name="l1"))
        worker.save()

        notification = get_subject_with_no_visit_notifications_count(worker)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_approaching_visits_for_mail_contact_count(self):
        original_notification = get_approaching_visits_for_mail_contact_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=100)
        visit.save()

        notification = get_approaching_visits_for_mail_contact_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_approaching_visits_for_mail_contact_count_2(self):
        original_notification = get_approaching_visits_for_mail_contact_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=100)
        visit.save()
        create_appointment(visit)

        notification = get_approaching_visits_for_mail_contact_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_approaching_visits_for_mail_contact_count_3(self):
        original_notification = get_approaching_visits_for_mail_contact_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=100)
        visit.save()
        appointment = create_appointment(visit)

        appointment.status = Appointment.APPOINTMENT_STATUS_CANCELLED
        appointment.save()

        notification = get_approaching_visits_for_mail_contact_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

    def test_get_approaching_visits_for_mail_contact_count_4(self):
        original_notification = get_approaching_visits_for_mail_contact_count(self.user)
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=100)
        visit.post_mail_sent = True
        visit.save()

        notification = get_approaching_visits_for_mail_contact_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_approaching_visits_for_mail_contact_order(self):
        subject = create_study_subject()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=110)
        visit.save()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=108)
        visit.save()

        visit = create_visit(subject)
        visit.datetime_begin = get_today_midnight_date() + datetime.timedelta(days=112)
        visit.save()

        visits = get_approaching_visits_for_mail_contact(self.user)
        self.assertEqual(3, visits.count())

        # check sort order
        self.assertTrue(visits[0].datetime_begin < visits[1].datetime_begin)
        self.assertTrue(visits[1].datetime_begin < visits[2].datetime_begin)

    def test_get_subjects_with_reminder_count(self):
        original_without_visit_notification = get_subject_with_no_visit_notifications_count(self.user)
        original_notification = get_subjects_with_reminder_count(self.user)
        subject = create_study_subject()
        subject.datetime_contact_reminder = get_today_midnight_date() + datetime.timedelta(days=-1)
        subject.save()

        notification = get_subjects_with_reminder_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_without_visit_notification.count, notification.count)

    def test_get_subjects_with_reminder_count_2(self):
        original_without_visit_notification = get_subject_with_no_visit_notifications_count(self.user)
        original_notification = get_subjects_with_reminder_count(self.user)
        subject = create_study_subject()
        subject.datetime_contact_reminder = get_today_midnight_date() + datetime.timedelta(hours=1)
        subject.save()

        notification = get_subjects_with_reminder_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_without_visit_notification.count, notification.count)

    def test_get_subjects_with_reminder_count_3(self):
        original_without_visit_notification = get_subject_with_no_visit_notifications_count(self.user)
        original_notification = get_subjects_with_reminder_count(self.user)
        subject = create_study_subject()
        subject.datetime_contact_reminder = get_today_midnight_date() + datetime.timedelta(days=2)
        subject.save()

        notification = get_subjects_with_reminder_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

        notification = get_subject_with_no_visit_notifications_count(self.user)
        self.assertEqual(original_without_visit_notification.count, notification.count)

    def test_get_filter_locations_for_invalid_user(self):
        locations = get_filter_locations(AnonymousUser())
        self.assertEqual(0, locations.count())

    def test_get_filter_locations_for_invalid_user_2(self):
        try:
            get_filter_locations("invalid class")
            self.fail("Exception expected")
        except TypeError:
            pass

    def test_get_subjects_with_expiry_vouchers(self):
        original_notification = get_subject_voucher_expiry_notifications_count(self.user)
        voucher = create_voucher()
        voucher.expiry_date = get_today_midnight_date()
        voucher.save()

        notification = get_subject_voucher_expiry_notifications_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)

        voucher.expiry_date = get_today_midnight_date() + datetime.timedelta(days=365)
        voucher.save()

        notification = get_subject_voucher_expiry_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

    def test_get_subjects_with_expiry_vouchers_and_contact_attempt(self):
        original_notification = get_subject_voucher_expiry_notifications_count(self.user)
        voucher = create_voucher()
        voucher.expiry_date = get_today_midnight_date()
        voucher.save()
        contact_attempt = create_contact_attempt(voucher.study_subject)

        notification = get_subject_voucher_expiry_notifications_count(self.user)
        self.assertEqual(original_notification.count, notification.count)

        contact_attempt.datetime_when = timezone.now().replace(year=2011, month=11, day=11)
        contact_attempt.save()

        notification = get_subject_voucher_expiry_notifications_count(self.user)
        self.assertEqual(original_notification.count + 1, notification.count)
