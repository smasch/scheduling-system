# coding=utf-8
from django.urls import reverse

from web.models import Appointment, AppointmentTypeLink
from web.models.constants import CUSTOM_FIELD_TYPE_TEXT
from web.models.custom_data import CustomStudySubjectField
from web.models.custom_data.custom_study_subject_field import get_study_subject_field_id
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_appointment, create_visit, create_appointment_type, \
    get_test_study, create_language
from web.views.export import subject_to_row_for_fields_processor, DROP_OUT_FIELD, get_subjects_as_array


class TestExportView(LoggedInTestCase):
    def test_export_subjects_to_csv(self):
        self.login_as_admin()
        create_study_subject()
        response = self.client.get(
            reverse('web.views.export_to_csv', kwargs={'study_id': get_test_study().id, 'data_type': "subjects"}))
        self.assertEqual(response.status_code, 200)

    def test_export_subjects_to_csv_without_permission(self):
        self.client.get(reverse("web.views.mail_templates"))
        create_study_subject()
        response = self.client.get(
            reverse('web.views.export_to_csv', kwargs={'study_id': get_test_study().id, 'data_type': "subjects"}))
        self.assertEqual(response.status_code, 302)

    def test_render_export(self):
        self.login_as_admin()
        create_study_subject()
        response = self.client.get(reverse('web.views.export', kwargs={'study_id': get_test_study().id}))
        self.assertEqual(response.status_code, 200)

    def test_render_export_without_permission(self):
        create_study_subject()
        response = self.client.get(reverse('web.views.export', kwargs={'study_id': get_test_study().id}))
        self.assertEqual(response.status_code, 302)

    def test_export_appointments_to_csv(self):
        self.login_as_admin()
        create_appointment()
        response = self.client.get(
            reverse('web.views.export_to_csv', kwargs={'study_id': get_test_study().id, 'data_type': "appointments"}))
        self.assertEqual(response.status_code, 200)

    def test_export_subjects_to_excel(self):
        self.login_as_admin()
        create_study_subject()
        response = self.client.get(
            reverse('web.views.export_to_excel', kwargs={'study_id': get_test_study().id, 'data_type': "subjects"}))
        self.assertEqual(response.status_code, 200)

    def test_export_subjects_to_excel_without_permission(self):
        create_study_subject()
        response = self.client.get(
            reverse('web.views.export_to_excel', kwargs={'study_id': get_test_study().id, 'data_type': "subjects"}))
        self.assertEqual(response.status_code, 302)

    def test_export_appointments_to_excel(self):
        self.login_as_admin()
        appointment = create_appointment()
        appointment.visit = None
        appointment.save()
        AppointmentTypeLink.objects.create(appointment=appointment, appointment_type=create_appointment_type())

        response = self.client.get(
            reverse('web.views.export_to_excel', kwargs={'study_id': get_test_study().id, 'data_type': "appointments"}))
        self.assertEqual(response.status_code, 200)

    def test_subject_to_row_for_fields_when_not_resigned(self):
        subject = create_study_subject()
        subject.resigned = False
        subject.save()

        subject2row = subject_to_row_for_fields_processor(subject.study, [DROP_OUT_FIELD])
        result = subject2row.subject_to_row_for_fields(subject)

        self.assertFalse(result[0])

    def test_subject_to_row_for_fields_when_resigned(self):
        subject = create_study_subject()
        subject.resigned = True
        subject.save()

        subject2row = subject_to_row_for_fields_processor(subject.study, [DROP_OUT_FIELD])
        result = subject2row.subject_to_row_for_fields(subject)

        self.assertFalse(result[0])

    def test_subject_to_row_for_fields_when_dropped_out(self):
        subject = create_study_subject()
        subject.resigned = True
        subject.save()

        visit = create_visit(subject)
        appointment = create_appointment(visit)
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()

        subject2row = subject_to_row_for_fields_processor(subject.study, [DROP_OUT_FIELD])
        result = subject2row.subject_to_row_for_fields(subject)
        self.assertTrue(result[0])

    def test_subject_with_custom_field(self):
        subject = create_study_subject()
        field = CustomStudySubjectField.objects.create(study=subject.study, name='HW', type=CUSTOM_FIELD_TYPE_TEXT)
        subject.set_custom_data_value(field, 'bbb')

        subjects = get_subjects_as_array(get_test_study(), get_study_subject_field_id(field))
        self.assertEqual('bbb', subjects[1][0])

    def test_export_appointments_with_columns(self):
        self.login_as_admin()
        create_appointment()
        response = self.client.get(
            reverse('web.views.export_to_csv', kwargs={'study_id': get_test_study().id, 'data_type': "appointments"}),
            {'fields': "nd_number,first_name,location"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode("UTF-8").count("\n"), 2)

    def test_export_subjects_to_csv_with_languages(self):
        self.login_as_admin()
        study_subject = create_study_subject()
        language = create_language()
        study_subject.subject.languages.set([language])
        study_subject.subject.save()
        response = self.client.get(
            reverse('web.views.export_to_csv', kwargs={'study_id': get_test_study().id, 'data_type': "subjects"}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(language.name in response.content.decode("UTF-8"))
