import logging

from django.urls import reverse

from web.forms.custom_study_subject_field_forms import CustomStudySubjectFieldEditForm
from web.models.constants import CUSTOM_FIELD_TYPE_TEXT
from web.models.custom_data import CustomStudySubjectField
from web.tests import LoggedInTestCase
from web.tests.functions import get_test_study, format_form_field

logger = logging.getLogger(__name__)


class CustomStudySubjectFieldViewTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study = get_test_study()

    def test_render_add(self):
        self.login_as_admin()
        response = self.client.get(
            reverse('web.views.custom_study_subject_field_add', kwargs={'study_id': self.study.id}))
        self.assertEqual(response.status_code, 200)

    def test_render_edit(self):
        self.login_as_admin()
        field = CustomStudySubjectField.objects.create(study=self.study, type=CUSTOM_FIELD_TYPE_TEXT)
        response = self.client.get(
            reverse('web.views.custom_study_subject_field_edit',
                    kwargs={'study_id': self.study.id, 'field_id': field.id}))
        self.assertEqual(response.status_code, 200)

    def test_save_edit_field(self):
        self.login_as_admin()
        field = CustomStudySubjectField.objects.create(study=self.study, name='HW', type=CUSTOM_FIELD_TYPE_TEXT)
        form_data = CustomStudySubjectFieldViewTests.create_edit_form_data_for_study(field)
        form_data['name'] = 'test'
        response = self.client.post(
            reverse('web.views.custom_study_subject_field_edit',
                    kwargs={'study_id': self.study.id, 'field_id': field.id}), data=form_data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue("study" in response['Location'])
        field = CustomStudySubjectField.objects.get(id=field.id)
        self.assertEqual('test', field.name)

    def test_save_add_field(self):
        self.login_as_admin()
        field = CustomStudySubjectField.objects.create(study=self.study, name='HW', type=CUSTOM_FIELD_TYPE_TEXT)
        count = CustomStudySubjectField.objects.all().count()
        form_data = CustomStudySubjectFieldViewTests.create_edit_form_data_for_study(field)
        response = self.client.post(
            reverse('web.views.custom_study_subject_field_add', kwargs={'study_id': self.study.id}), data=form_data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(count + 1, CustomStudySubjectField.objects.all().count())

    def test_delete_field(self):
        self.login_as_admin()
        field = CustomStudySubjectField.objects.create(study=self.study, name='HW', type=CUSTOM_FIELD_TYPE_TEXT)
        response = self.client.post(
            reverse('web.views.custom_study_subject_field_delete',
                    kwargs={'study_id': self.study.id, 'field_id': field.id}))
        self.assertEqual(response.status_code, 302)
        count = CustomStudySubjectField.objects.filter(id=field.id).count()
        self.assertEqual(0, count)

    @staticmethod
    def create_edit_form_data_for_study(field: CustomStudySubjectField):
        study_form = CustomStudySubjectFieldEditForm(instance=field)

        form_data = {}
        for key, value in list(study_form.initial.items()):
            form_data[key] = format_form_field(value)
        return form_data
