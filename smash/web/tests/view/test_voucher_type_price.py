import logging

from django.urls import reverse

from web.forms import VoucherTypePriceForm
from web.models import VoucherType, VoucherTypePrice
from web.tests.functions import create_voucher_type, create_voucher_type_price, format_form_field
from .. import LoggedInTestCase

logger = logging.getLogger(__name__)


class VoucherTypePriceViewTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.voucher_type = create_voucher_type()

    def test_render_add_voucher_type_price_request(self):
        url = reverse('web.views.voucher_type_price_add', kwargs={'voucher_type_id': self.voucher_type.id})
        response = self.client.get(url
                                   )
        self.assertEqual(response.status_code, 200)

    def test_render_edit_voucher_type_price_request(self):
        voucher_type_price = create_voucher_type_price()
        url = reverse('web.views.voucher_type_price_edit',
                      kwargs={'pk': voucher_type_price.id, 'voucher_type_id': self.voucher_type.id})

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_add_voucher_type_price(self):
        form_data = {
            'price': "11.0",
            'start_date': "2017-01-01",
            'end_date': "2018-01-01"
        }
        url = reverse('web.views.voucher_type_price_add', kwargs={'voucher_type_id': self.voucher_type.id})
        response = self.client.post(url, data=form_data)
        self.assertEqual(response.status_code, 302)

        self.assertEqual(1, VoucherType.objects.all().count())

    def test_edit_voucher_type_price(self):
        voucher_type_price = create_voucher_type_price()
        form = VoucherTypePriceForm(instance=voucher_type_price)
        form_data = {}
        for key, value in list(form.initial.items()):
            if value is not None:
                form_data[f'{key}'] = format_form_field(value)
        form_data['price'] = 90.50

        url = reverse('web.views.voucher_type_price_edit',
                      kwargs={'pk': voucher_type_price.id, 'voucher_type_id': voucher_type_price.voucher_type.id})
        self.client.post(url, data=form_data)

        self.assertEqual(90.50, VoucherTypePrice.objects.get(id=voucher_type_price.id).price)
