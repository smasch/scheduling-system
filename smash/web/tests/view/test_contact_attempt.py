import datetime

from django.urls import reverse
from django.utils import timezone

from web.forms import ContactAttemptEditForm
from web.models import ContactAttempt
from web.models.constants import CONTACT_TYPES_EMAIL
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_contact_attempt, format_form_field, create_visit, \
    create_appointment


class ContactAttemptViewTests(LoggedInTestCase):
    def test_contact_attempt_add_get(self):
        subject = create_study_subject()
        response = self.client.get(reverse('web.views.contact_add', kwargs={'subject_id': subject.id}))
        self.assertContains(response, f'selected>{self.worker}', 1)
        self.assertContains(response, f'selected>{subject}', 1)

    def test_contact_attempt_add_post_valid(self):
        subject = create_study_subject()
        self.assertEqual(0, ContactAttempt.objects.filter(subject=subject).count())
        now = datetime.datetime.now()
        now_aware = timezone.make_aware(now, timezone.get_default_timezone())
        contact_type = CONTACT_TYPES_EMAIL
        comment = "this is a comment"
        form_data = {'datetime_when': now, 'worker': self.worker.id, 'type': contact_type, 'comment': comment}
        response = self.client.post(
            reverse('web.views.contact_add', kwargs={'subject_id': subject.id}), data=form_data)
        # check correct redirection to subject edit page
        self.assertRedirects(response, reverse('web.views.subject_edit', kwargs={'subject_id': subject.id}))
        contact_attempts = ContactAttempt.objects.filter(subject=subject).all()
        self.assertEqual(1, len(contact_attempts))
        contact_attempt = contact_attempts[0]
        self.assertEqual(now_aware, contact_attempt.datetime_when)
        self.assertEqual(contact_type, contact_attempt.type)
        self.assertEqual(subject, contact_attempt.subject)
        self.assertEqual(self.worker, contact_attempt.worker)
        self.assertEqual(comment, contact_attempt.comment)
        self.assertFalse(contact_attempt.success)
        # follow redirect to check if the new contact attempt is correctly listed
        response = self.client.get(response.url)
        self.assertContains(response, comment, 1)

    def test_contact_attempt_add_from_appointment_valid(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)
        form_data = {
            'datetime_when': datetime.datetime.now(),
            'worker': self.worker.id,
            'type': CONTACT_TYPES_EMAIL,
            'comment': "this is a comment"}
        response = self.client.post(
            reverse('web.views.contact_add', kwargs={'subject_id': subject.id}) + "?from_appointment=" + str(
                appointment.id),
            data=form_data, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, ContactAttempt.objects.filter(subject=subject).count())

    def test_contact_attempt_add_post_invalid(self):
        subject = create_study_subject()
        self.assertEqual(0, ContactAttempt.objects.filter(subject=subject).count())
        contact_type = CONTACT_TYPES_EMAIL
        comment = "this is a comment"
        form_data = {'type': contact_type, 'comment': comment}
        response = self.client.post(
            reverse('web.views.contact_add', kwargs={'subject_id': subject.id}), data=form_data)
        self.assertContains(response, "This field is required", 2)
        self.assertEqual(0, ContactAttempt.objects.filter(subject=subject).count())

    def test_contact_attempt_edit_post(self):
        contact_attempt = create_contact_attempt()

        form_subject = ContactAttemptEditForm(instance=contact_attempt, user=self.user)
        form_data = {}
        for key, value in list(form_subject.initial.items()):
            if value is not None:
                form_data[key] = format_form_field(value)

        response = self.client.post(
            reverse('web.views.contact_edit',
                    kwargs={'subject_id': contact_attempt.subject.id, 'contact_attempt_id': contact_attempt.id}),
            data=form_data,
            follow=True)

        self.assertEqual(response.status_code, 200)

    def test_contact_attempt_edit_from_appointment(self):
        contact_attempt = create_contact_attempt()
        visit = create_visit(contact_attempt.subject)
        appointment = create_appointment(visit)

        form_subject = ContactAttemptEditForm(instance=contact_attempt, user=self.user)
        form_data = {}
        for key, value in list(form_subject.initial.items()):
            if value is not None:
                form_data[key] = format_form_field(value)

        response = self.client.post(
            reverse('web.views.contact_edit',
                    kwargs={'subject_id': contact_attempt.subject.id,
                            'contact_attempt_id': contact_attempt.id}) + "?from_appointment=" + str(appointment.id),
            data=form_data,
            follow=True)

        self.assertEqual(response.status_code, 200)
