import logging

from django.urls import reverse

from web.models import VoucherType
from web.models.constants import GLOBAL_STUDY_ID
from web.tests.functions import create_voucher_type
from .. import LoggedInTestCase

logger = logging.getLogger(__name__)


class VoucherTypeViewTests(LoggedInTestCase):
    def test_render_add_voucher_type_request(self):
        self.login_as_admin()
        response = self.client.get(reverse('web.views.voucher_type_add'))
        self.assertEqual(response.status_code, 200)

    def test_render_edit_voucher_type_request(self):
        self.login_as_admin()
        voucher_type = create_voucher_type()
        response = self.client.get(reverse('web.views.voucher_type_edit', kwargs={'pk': voucher_type.id}))
        self.assertEqual(response.status_code, 200)

    def test_add_voucher_type(self):
        self.login_as_admin()
        form_data = {
            'code': "X",
            'description': "y",
            'study': str(GLOBAL_STUDY_ID),
        }
        response = self.client.post(reverse('web.views.voucher_type_add'), data=form_data)
        self.assertEqual(response.status_code, 302)

        self.assertEqual(1, VoucherType.objects.all().count())

    def test_render_add_voucher_type_request_staff(self):
        self.login_as_staff()
        response = self.client.get(reverse('web.views.voucher_type_add'))
        self.assertEqual(response.status_code, 302)

    def test_render_edit_voucher_type_request_staff(self):
        self.login_as_staff()
        voucher_type = create_voucher_type()
        response = self.client.get(reverse('web.views.voucher_type_edit', kwargs={'pk': voucher_type.id}))
        self.assertEqual(response.status_code, 302)
