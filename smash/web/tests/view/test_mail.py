import datetime
import logging

from web.models.constants import CUSTOM_FIELD_TYPE_DATE, GLOBAL_STUDY_ID, CUSTOM_FIELD_TYPE_TEXT
from web.models.custom_data import CustomStudySubjectField
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject
from web.views.virus_mail import KitRequestEmailSendJob, count_subjects

logger = logging.getLogger(__name__)


class VirusMailTests(LoggedInTestCase):
    def test_send_email(self):
        job = KitRequestEmailSendJob()
        result = job.do()
        self.assertEqual("mail sent", result)

    def test_empty_count_subjects(self):
        result = count_subjects(datetime.datetime.now() - datetime.timedelta(days=1), "Positive")
        self.assertEqual(0, result)

    def test_count_subjects(self):
        create_study_subject()
        study_subject = create_study_subject()
        collect_field = CustomStudySubjectField.objects.create(study_id=GLOBAL_STUDY_ID,
                                                               default_value='',
                                                               name='Visit 0 RT-PCR collection date',
                                                               type=CUSTOM_FIELD_TYPE_DATE)
        status_field = CustomStudySubjectField.objects.create(study_id=GLOBAL_STUDY_ID,
                                                              default_value='',
                                                              name='Virus 0 RT-PCR',
                                                              type=CUSTOM_FIELD_TYPE_TEXT)
        study_subject.set_custom_data_value(collect_field, datetime.datetime.now().strftime("%Y-%m-%d"))
        study_subject.set_custom_data_value(status_field, "Positive")

        result = count_subjects(datetime.datetime.now(), "Positive")
        self.assertEqual(1, result)
