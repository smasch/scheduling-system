import logging

from django.urls import reverse

from web.models import MailTemplate
from web.models.constants import MAIL_TEMPLATE_CONTEXT_VOUCHER
from web.tests import LoggedInTestCase
from web.tests.functions import create_voucher, get_resource_path

logger = logging.getLogger(__name__)


class MailTests(LoggedInTestCase):
    def test_generate_vouchers(self):
        voucher = create_voucher()
        MailTemplate(name="name", language=None,
                     context=MAIL_TEMPLATE_CONTEXT_VOUCHER,
                     template_file=get_resource_path('upcoming_appointment_FR.docx')).save()

        page = reverse('web.views.mail_template_generate_for_vouchers') + "?voucher_id=" + str(voucher.id)
        response = self.client.get(page)
        self.assertEqual(response.status_code, 200)

    def test_list_mail_templates(self):
        self.login_as_admin()
        response = self.client.get(reverse("web.views.mail_templates"))
        self.assertEqual(response.status_code, 200)

    def test_list_mail_templates_without_permission(self):
        response = self.client.get(reverse("web.views.mail_templates"))
        self.assertEqual(response.status_code, 302)
