import datetime
import logging

from django.urls import reverse

from web.forms import VoucherForm
from web.models import Voucher
from web.models.constants import VOUCHER_STATUS_NEW, VOUCHER_STATUS_USED, VOUCHER_STATUS_EXPIRED
from web.tests.functions import create_voucher, create_study_subject, format_form_field, create_voucher_type, \
    create_voucher_partner, create_worker
from web.views.notifications import get_today_midnight_date
from web.views.voucher import ExpireVouchersJob
from .. import LoggedInTestCase

logger = logging.getLogger(__name__)


class VoucherTypeViewTests(LoggedInTestCase):
    def test_render_add_voucher_request(self):
        self.login_as_admin()
        study_subject = create_study_subject()
        url = reverse('web.views.voucher_add') + "?study_subject_id=" + str(study_subject.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_render_edit_voucher_request(self):
        self.login_as_admin()
        voucher = create_voucher()
        response = self.client.get(reverse('web.views.voucher_edit', kwargs={'pk': voucher.id}))
        self.assertEqual(response.status_code, 200)

    def test_render_add_voucher_request_staff(self):
        self.login_as_staff()
        study_subject = create_study_subject()
        url = reverse('web.views.voucher_add') + "?study_subject_id=" + str(study_subject.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

    def test_render_edit_voucher_request_staff(self):
        self.login_as_staff()
        voucher = create_voucher()
        response = self.client.get(reverse('web.views.voucher_edit', kwargs={'pk': voucher.id}))
        self.assertEqual(response.status_code, 302)

    def test_add_voucher(self):
        self.login_as_admin()
        voucher_type = create_voucher_type()
        study_subject = create_study_subject()
        study_subject.voucher_types.add(voucher_type)

        usage_partner = create_voucher_partner()
        usage_partner.voucher_types.add(voucher_type)
        usage_partner.save()

        worker = create_worker()

        visit_detail_form = VoucherForm()
        form_data = {
            "usage_partner": usage_partner.id,
            "issue_worker": worker.id,
            "status": VOUCHER_STATUS_NEW,
            "hours": 10,
            "voucher_type": voucher_type.id
        }
        for key, value in list(visit_detail_form.initial.items()):
            form_data[key] = format_form_field(value)

        url = reverse('web.views.voucher_add') + '?study_subject_id=' + str(study_subject.id)
        response = self.client.post(url, data=form_data)
        self.assertEqual(response.status_code, 302)

        self.assertEqual(1, Voucher.objects.all().count())

    def test_edit_voucher(self):
        self.login_as_admin()
        voucher = create_voucher()
        usage_partner = create_voucher_partner()
        usage_partner.voucher_types.add(voucher.voucher_type)
        usage_partner.save()
        voucher_form = VoucherForm(instance=voucher)
        form_data = {}
        for key, value in list(voucher_form.initial.items()):
            form_data[key] = format_form_field(value)

        form_data["usage_partner"] = usage_partner.id

        url = reverse('web.views.voucher_edit', kwargs={'pk': voucher.id})
        response = self.client.post(url, data=form_data)
        self.assertEqual(response.status_code, 302)

        self.assertEqual(1, Voucher.objects.all().count())

    def test_expire_voucher_cron_job(self):
        self.login_as_admin()
        voucher = create_voucher()
        voucher.status = VOUCHER_STATUS_NEW
        voucher.expiry_date = "2011-01-01"
        voucher.save()
        status = ExpireVouchersJob().do()
        self.assertTrue("1" in status)
        updated_voucher = Voucher.objects.get(id=voucher.id)
        self.assertEqual(VOUCHER_STATUS_EXPIRED, updated_voucher.status)

    def test_expire_voucher_cron_job_with_no_vouchers_to_update(self):
        self.login_as_admin()
        voucher = create_voucher()
        voucher.status = VOUCHER_STATUS_USED
        voucher.expiry_date = "2011-01-01"
        voucher.save()
        voucher = create_voucher()
        voucher.status = VOUCHER_STATUS_NEW
        voucher.expiry_date = get_today_midnight_date() + datetime.timedelta(days=2)
        voucher.save()
        status = ExpireVouchersJob().do()
        self.assertTrue("0" in status)
