import logging

from django.urls import reverse

from web.forms import StudyEditForm, StudyNotificationParametersEditForm, StudyColumnsEditForm
from web.models.constants import GLOBAL_STUDY_ID
from web.tests import LoggedInTestCase
from web.tests.functions import get_test_study, format_form_field

logger = logging.getLogger(__name__)


class StudyViewTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study = get_test_study()

    def test_render_study_edit(self):
        self.login_as_admin()
        response = self.client.get(reverse('web.views.edit_study', kwargs={'study_id': GLOBAL_STUDY_ID}))
        self.assertEqual(response.status_code, 200)

    def test_render_study_edit_staff(self):
        self.login_as_staff()
        response = self.client.get(reverse('web.views.edit_study', kwargs={'study_id': GLOBAL_STUDY_ID}))
        self.assertEqual(response.status_code, 302)

    def test_save_study(self):
        form_data = self.create_edit_form_data_for_study()

        response = self.client.post(
            reverse('web.views.edit_study', kwargs={'study_id': GLOBAL_STUDY_ID}), data=form_data)

        self.assertEqual(response.status_code, 302)
        self.assertFalse("edit" in response['Location'])

    def test_save_invalid_data(self):
        self.login_as_admin()
        form_data = {}

        response = self.client.post(
            reverse('web.views.edit_study', kwargs={'study_id': GLOBAL_STUDY_ID}), data=form_data)

        self.assertEqual(response.status_code, 200)

    def test_save_invalid_data_staff(self):
        self.login_as_staff()
        form_data = {}

        response = self.client.post(
            reverse('web.views.edit_study', kwargs={'study_id': GLOBAL_STUDY_ID}), data=form_data)

        self.assertEqual(response.status_code, 302)

    def test_save_study_and_continue_without_changing_page(self):
        self.login_as_admin()
        form_data = self.create_edit_form_data_for_study()
        form_data['_continue'] = True

        response = self.client.post(
            reverse('web.views.edit_study', kwargs={'study_id': GLOBAL_STUDY_ID}), data=form_data)

        self.assertEqual(response.status_code, 302)
        self.assertTrue("edit" in response['Location'])

    def test_save_study_without_changing_page(self):
        self.login_as_admin()
        form_data = self.create_edit_form_data_for_study()

        response = self.client.post(
            reverse('web.views.edit_study', kwargs={'study_id': GLOBAL_STUDY_ID}), data=form_data)

        self.assertEqual(response.status_code, 302)
        self.assertTrue("appointments" in response['Location'])

    def create_edit_form_data_for_study(self):
        study_form = StudyEditForm(instance=self.study, prefix="study")
        notifications_form = StudyNotificationParametersEditForm(instance=self.study.notification_parameters,
                                                                 prefix="notifications")
        study_columns_form = StudyColumnsEditForm(instance=self.study.columns,
                                                  prefix="columns")

        form_data = {}
        for key, value in list(study_form.initial.items()):
            form_data[f'study-{key}'] = format_form_field(value)
        for key, value in list(notifications_form.initial.items()):
            form_data[f'notifications-{key}'] = format_form_field(value)
        for key, value in list(study_columns_form.initial.items()):
            form_data[f'columns-{key}'] = format_form_field(value)
        return form_data
