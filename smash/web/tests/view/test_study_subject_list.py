import logging

from django.urls import reverse

from web.forms.study_subject_list_form import StudySubjectListEditForm, StudySubjectColumnsEditForm, \
    SubjectColumnsEditForm
from web.models import StudySubjectList, StudyColumns, SubjectColumns
from web.models.study_subject_list import SUBJECT_LIST_GENERIC
from web.tests import LoggedInTestCase
from web.tests.functions import get_test_study, format_form_field

logger = logging.getLogger(__name__)


class StudySubjectListViewTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study = get_test_study()
        visible_subject_study_columns = StudyColumns.objects.create()
        visible_subject_columns = SubjectColumns.objects.create()
        self.list = StudySubjectList.objects.create(study=self.study, type=SUBJECT_LIST_GENERIC,
                                                    visible_subject_columns=visible_subject_columns,
                                                    visible_subject_study_columns=visible_subject_study_columns
                                                    )

    def test_render_edit(self):
        self.login_as_admin()
        response = self.client.get(reverse('web.views.study_subject_list_edit',
                                           kwargs={'study_id': self.study.id, 'study_subject_list_id': self.list.id}))
        self.assertEqual(response.status_code, 200)

    def test_save_study(self):
        self.login_as_admin()
        form_data = self.create_edit_form_data_for_study()

        response = self.client.post(reverse('web.views.study_subject_list_edit',
                                            kwargs={'study_id': self.study.id, 'study_subject_list_id': self.list.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)
        print(response['Location'])
        self.assertFalse("subject_list" in response['Location'])

    def create_edit_form_data_for_study(self):
        list_form = StudySubjectListEditForm(instance=self.list, prefix="list")
        study_subject_columns_form = StudySubjectColumnsEditForm(instance=self.list.visible_subject_study_columns,
                                                                 prefix="study_subject")
        subject_columns_form = SubjectColumnsEditForm(instance=self.list.visible_subject_columns,
                                                      prefix="subject")

        form_data = {}
        for key, value in list(list_form.initial.items()):
            form_data[f'list-{key}'] = format_form_field(value)
        for key, value in list(study_subject_columns_form.initial.items()):
            form_data[f'study_subject-{key}'] = format_form_field(value)
        for key, value in list(subject_columns_form.initial.items()):
            form_data[f'subject_columns_form-{key}'] = format_form_field(value)
        return form_data
