import datetime
import logging

from django.urls import reverse

from web.forms.voucher_partner_session_forms import VoucherPartnerSessionForm
from web.models import Voucher
from web.models.constants import VOUCHER_STATUS_USED, VOUCHER_STATUS_IN_USE
from web.tests.functions import create_voucher, format_form_field
from .. import LoggedInTestCase

logger = logging.getLogger(__name__)


class VoucherTypeViewTests(LoggedInTestCase):
    def test_render_add_voucher_partner_session_request(self):
        voucher = create_voucher()
        url = reverse('web.views.voucher_partner_sessions_add', kwargs={'pk': voucher.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_add_voucher_partner_session_without_using_time(self):
        voucher = create_voucher()
        voucher.hours = 10
        voucher.save()

        form = VoucherPartnerSessionForm()
        form_data = {
            "length": 50,
            "date": datetime.datetime.now(),
        }
        for key, value in list(form.initial.items()):
            form_data[key] = format_form_field(value)

        url = reverse('web.views.voucher_partner_sessions_add', kwargs={'pk': voucher.id})
        response = self.client.post(url, data=form_data)
        self.assertEqual(response.status_code, 302)

        voucher = Voucher.objects.get(id=voucher.id)
        self.assertEqual(VOUCHER_STATUS_IN_USE, voucher.status)

    def test_add_voucher_partner_session_with_using_time(self):
        voucher = create_voucher()
        voucher.hours = 1
        voucher.save()

        form = VoucherPartnerSessionForm()
        form_data = {
            "length": 60,
            "date": datetime.datetime.now(),
        }
        for key, value in list(form.initial.items()):
            form_data[key] = format_form_field(value)

        url = reverse('web.views.voucher_partner_sessions_add', kwargs={'pk': voucher.id})
        response = self.client.post(url, data=form_data)
        self.assertEqual(response.status_code, 302)

        voucher = Voucher.objects.get(id=voucher.id)
        self.assertEqual(VOUCHER_STATUS_USED, voucher.status)
