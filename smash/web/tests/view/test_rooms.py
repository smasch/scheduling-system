import logging

from django.urls import reverse

from web.models import Room
from web.tests import LoggedInTestCase
from web.tests.functions import create_room, create_item

logger = logging.getLogger(__name__)


class RoomsTests(LoggedInTestCase):
    def test_rooms_requests(self):
        self.login_as_admin()
        pages = [
            'web.views.equipment_and_rooms.rooms',
            'web.views.equipment_and_rooms.rooms_add',
        ]

        for page in pages:
            response = self.client.get(reverse(page))
            self.assertEqual(response.status_code, 200)

    def test_rooms_requests_without_permission(self):
        pages = [
            'web.views.equipment_and_rooms.rooms',
            'web.views.equipment_and_rooms.rooms_add',
        ]

        for page in pages:
            response = self.client.get(reverse(page))
            self.assertEqual(response.status_code, 302)

    def test_rooms_edit_request(self):
        self.login_as_admin()
        room = create_room()
        page = reverse('web.views.equipment_and_rooms.rooms_edit',
                       kwargs={'room_id': str(room.id)})
        response = self.client.get(page)
        self.assertEqual(response.status_code, 200)

    def test_rooms_delete_request(self):
        self.login_as_admin()
        room = create_room()
        page = reverse('web.views.equipment_and_rooms.rooms_delete',
                       kwargs={'room_id': str(room.id)})
        response = self.client.get(page)
        self.assertEqual(response.status_code, 302)
        room.refresh_from_db()
        self.assertTrue(room.removed)

    def test_rooms_add(self):
        self.login_as_admin()
        page = reverse('web.views.equipment_and_rooms.rooms_add')
        item = create_item()
        data = {
            'city': 'Belval',
            'address': 'Av. du Swing 123456',
            'room_number': 123,
            'floor': 456,
            'is_vehicle': False,
            'owner': 'No one',
            'equipment': [item.id]
        }
        response = self.client.post(page, data)
        self.assertEqual(response.status_code, 302)

        freshly_created = Room.objects.filter(address=data['address'])
        self.assertEqual(len(freshly_created), 1)

    def test_rooms_edit(self):
        self.login_as_admin()
        room = create_room()
        page = reverse('web.views.equipment_and_rooms.rooms_edit',
                       kwargs={'room_id': str(room.id)})
        data = {
            'city': 'Belval2',
            'address': 'Av. du Swing ABCDE',
            'room_number': 5,
            'floor': 8,
            'is_vehicle': True,
            'owner': 'Everyone',
        }
        response = self.client.post(page, data)
        self.assertEqual(response.status_code, 302)

        freshly_edited = Room.objects.get(id=room.id)
        for key in data:
            self.assertEqual(getattr(freshly_edited, key, ''), data[key])

    def test_rooms_delete(self):
        self.login_as_admin()
        room = create_room()
        page = reverse('web.views.equipment_and_rooms.rooms_delete',
                       kwargs={'room_id': str(room.id)})

        response = self.client.get(page)
        self.assertEqual(response.status_code, 302)

        freshly_deleted = Room.objects.filter(id=room.id, removed=False)
        self.assertEqual(len(freshly_deleted), 0)
