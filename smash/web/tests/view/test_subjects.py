import datetime
import logging

from django.contrib.auth.models import Permission
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse

from web.forms import SubjectAddForm, SubjectEditForm, StudySubjectAddForm, StudySubjectEditForm
from web.models import MailTemplate, StudySubject, StudyColumns, Visit, Provenance, Subject
from web.models.constants import SEX_CHOICES_MALE, COUNTRY_AFGHANISTAN_ID, COUNTRY_OTHER_ID, \
    MAIL_TEMPLATE_CONTEXT_SUBJECT, CUSTOM_FIELD_TYPE_FILE, CUSTOM_FIELD_TYPE_TEXT
from web.models.custom_data import CustomStudySubjectField
from web.models.custom_data.custom_study_subject_field import get_study_subject_field_id
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_visit, create_appointment, get_test_location, \
    create_language, get_resource_path, get_test_study, format_form_field, get_patient_subject_type, \
    get_control_subject_type
from web.views.notifications import get_today_midnight_date

logger = logging.getLogger(__name__)


class SubjectsViewTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study_subject = create_study_subject()
        self.study = get_test_study()

    def test_render_subjects_add(self):
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()

        response = self.client.get(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}))
        self.assertEqual(response.status_code, 200)

    def test_render_subject_edit(self):
        response = self.client.get(reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id}))
        self.assertEqual(response.status_code, 200)

    def test_render_subject_edit_with_mail_templates(self):
        language = create_language(name="German")
        template_name = "german_template"
        template_file = get_resource_path('upcoming_appointment_FR.docx')
        self.study_subject.subject.default_written_communication_language = language
        self.study_subject.subject.save()

        MailTemplate(name=template_name, language=language, context=MAIL_TEMPLATE_CONTEXT_SUBJECT,
                     template_file=template_file).save()

        response = self.client.get(reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(template_name.encode('utf8') in response.content)

    def test_render_subject_visit_details(self):
        visit = create_visit(self.study_subject)
        create_appointment(visit)

        response = self.client.get(
            reverse('web.views.subject_visit_details', kwargs={'subject_id': self.study_subject.id}))
        self.assertEqual(response.status_code, 200)
        self.assertFalse("Add visit".encode('utf8') in response.content)

    def test_render_subject_visit_details_without_visit(self):
        response = self.client.get(
            reverse('web.views.subject_visit_details', kwargs={'subject_id': self.study_subject.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue("Add visit".encode('utf8') in response.content)

    def test_save_subject_edit_when_resigned_without_reason(self):
        form_data = self.create_edit_form_data_for_study_subject()

        form_data['subject-dead'] = "True"
        form_data['study_subject-resigned'] = "True"
        response = self.client.post(
            reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id}), data=form_data)

        self.assertEqual(response.status_code, 200)
        self.assertTrue("Resign reason cannot be empty".encode('utf8') in response.content)

    def test_save_subject_edit_when_endpoint_reached(self):
        study_subject = create_study_subject()
        visit = create_visit(subject=study_subject)
        appointment = create_appointment(visit=visit)
        visit.save()
        appointment.save()
        study_subject.save()

        form_data = self.create_edit_form_data_for_study_subject(instance=study_subject)

        form_data['study_subject-endpoint_reached'] = "True"
        response = self.client.post(
            reverse('web.views.subject_edit', kwargs={'subject_id': study_subject.id}), data=form_data)

        self.assertEqual(response.status_code, 302)
        updated_study_subject = StudySubject.objects.filter(id=study_subject.id)[0]
        self.assertTrue(updated_study_subject.endpoint_reached)
        visit_count = Visit.objects.filter(subject=study_subject).count()
        self.assertEqual(1, visit_count)

    def test_save_subject_edit_when_resigned(self):
        study_subject = create_study_subject()
        visit = create_visit(subject=study_subject)
        appointment = create_appointment(visit=visit)
        visit.save()
        appointment.save()
        study_subject.save()

        form_data = self.create_edit_form_data_for_study_subject(instance=study_subject)

        form_data['study_subject-resigned'] = "True"
        form_data['study_subject-resign_reason'] = "Doesn't want to participate"
        response = self.client.post(
            reverse('web.views.subject_edit', kwargs={'subject_id': study_subject.id}), data=form_data)

        self.assertEqual(response.status_code, 302)
        updated_study_subject = StudySubject.objects.filter(id=study_subject.id)[0]
        self.assertTrue(updated_study_subject.resigned)
        visit_count = Visit.objects.filter(subject=study_subject).count()
        self.assertEqual(1, visit_count)

    def test_save_subject_edit(self):
        form_data = self.create_edit_form_data_for_study_subject()

        form_data['subject-dead'] = "True"
        form_data['study_subject-resigned'] = "True"
        form_data['study_subject-resign_reason'] = "doesn't want to participate"

        url = reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id})
        response = self.client.post(url, data=form_data)

        self.assertEqual(response.status_code, 302)
        updated_study_subject = StudySubject.objects.filter(id=self.study_subject.id)[0]
        self.assertTrue(updated_study_subject.subject.dead)
        self.assertTrue(updated_study_subject.resigned)

    def test_save_subject_edit_with_continue(self):
        form_data = self.create_edit_form_data_for_study_subject()
        form_data['_continue'] = True

        response = self.client.post(reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id}),
                                    data=form_data,
                                    follow=True)

        self.assertEqual(response.status_code, 200)

    def test_edit_with_type(self):
        create_visit(subject=self.study_subject)

        form_data = self.create_edit_form_data_for_study_subject()
        form_data['study_subject-type'] = get_patient_subject_type().id

        self.client.post(reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id}),
                         data=form_data)

        result = StudySubject.objects.get(id=self.study_subject.id)
        self.assertIsNotNone(result.visit_used_to_compute_followup_date)

    def test_edit_with_type_with_no_visit(self):
        form_data = self.create_edit_form_data_for_study_subject()
        form_data['study_subject-type'] = get_patient_subject_type().id

        self.client.post(reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id}),
                         data=form_data)

        result = StudySubject.objects.get(id=self.study_subject.id)
        self.assertIsNone(result.visit_used_to_compute_followup_date)

    def test_edit_with_the_same_type(self):
        create_visit(subject=self.study_subject)

        form_data = self.create_edit_form_data_for_study_subject()
        form_data['study_subject-type'] = get_control_subject_type().id

        self.client.post(reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id}),
                         data=form_data)
        self.assertIsNone(self.study_subject.visit_used_to_compute_followup_date)

    def test_delete_subject(self):
        self.login_as_super()
        study_subject = create_study_subject()
        subject = study_subject.subject
        self.assertEqual(1, Subject.objects.filter(id=subject.id).count())
        self.assertEqual(1, StudySubject.objects.filter(id=study_subject.id).count())
        url = reverse('web.views.subject_delete', kwargs={'pk': subject.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(0, Subject.objects.filter(id=subject.id).count())
        self.assertEqual(0, StudySubject.objects.filter(id=study_subject.id).count())

    def test_delete_study_subject(self):
        self.login_as_super()
        study_subject = create_study_subject()
        subject = study_subject.subject
        self.assertEqual(1, Subject.objects.filter(id=subject.id).count())
        self.assertEqual(1, StudySubject.objects.filter(id=study_subject.id).count())
        url = reverse('web.views.study_subject_delete', kwargs={'pk': study_subject.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(1, Subject.objects.filter(id=subject.id).count())
        self.assertEqual(0, StudySubject.objects.filter(id=study_subject.id).count())

    def create_edit_form_data_for_study_subject(self, instance: StudySubject = None):
        if instance is None:
            instance = self.study_subject
        form_study_subject = StudySubjectEditForm(instance=instance, prefix="study_subject")
        form_subject = SubjectEditForm(instance=instance.subject, prefix="subject")
        form_data = {}
        for key, value in list(form_study_subject.initial.items()):
            form_data[f'study_subject-{key}'] = format_form_field(value)
        for key, value in list(form_subject.initial.items()):
            form_data[f'subject-{key}'] = format_form_field(value)
        form_data["study_subject-referral_letter"] = SimpleUploadedFile("file.txt", b"file_content")
        return form_data

    def create_add_form_data_for_study_subject(self):
        form_study_subject = StudySubjectAddForm(prefix="study_subject", user=self.user, study=self.study)
        form_subject = SubjectAddForm(prefix="subject")
        form_data = {}
        for key, value in list(form_study_subject.initial.items()):
            form_data[f'study_subject-{key}'] = format_form_field(value)
        for key, value in list(form_subject.initial.items()):
            form_data[f'subject-{key}'] = format_form_field(value)
        self.add_valid_form_data_for_subject_add(form_data)
        form_data["study_subject-default_location"] = get_test_location().id
        return form_data

    def test_subjects_add_2(self):
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()

        form = SubjectAddForm(data=form_data, prefix="subject")
        print(form.errors)
        self.assertTrue(form.is_valid())

        form = StudySubjectAddForm(data=form_data, prefix="study_subject", user=self.user, study=self.study)
        print(form.errors)
        self.assertTrue(form.is_valid())

        form_data["study_subject-type"] = get_control_subject_type().id
        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(response.url)
        self.assertContains(response, "Subject created")

        subject = StudySubject.objects.all().order_by("-id")[0]
        self.assertEqual("L-001", subject.screening_number,
                         "prefix should start by L "
                         "as default location prefix is not defined and subject type is control")

    def test_subjects_add_with_referral_letter_file(self):
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        StudyColumns.objects.all().update(referral_letter=True)

        form_data = self.create_add_form_data_for_study_subject()

        form_data["study_subject-type"] = get_control_subject_type().id
        form_data["study_subject-referral_letter"] = SimpleUploadedFile("file.txt", b"file_content")

        form = SubjectAddForm(data=form_data, prefix="subject")
        self.assertTrue(form.is_valid())

        form = StudySubjectAddForm(data=form_data, prefix="study_subject", user=self.user, study=self.study)
        self.assertTrue(form.is_valid())

        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(response.url)
        self.assertContains(response, "Subject created")

        subject = StudySubject.objects.all().order_by("-id")[0]

        # check if edit page renders properly
        response = self.client.get(reverse('web.views.subject_edit', kwargs={'subject_id': subject.id}))
        self.assertEqual(response.status_code, 200)

        # check if file can be downloaded
        url = reverse('web.views.uploaded_files') + "?file=" + str(subject.referral_letter)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def add_valid_form_data_for_subject_add(self, form_data):
        form_data["subject-country"] = COUNTRY_AFGHANISTAN_ID
        form_data["subject-first_name"] = "John"
        form_data["subject-last_name"] = "Doe"
        form_data["subject-sex"] = SEX_CHOICES_MALE
        form_data["study_subject-type"] = get_patient_subject_type().id
        form_data["study_subject-subject"] = self.study_subject.id
        form_data["study_subject-postponed"] = False

    def test_subjects_add_patient(self):
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()

        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(response.url)
        self.assertContains(response, "Subject created")

        subject = StudySubject.objects.all().order_by("-id")[0]
        self.assertEqual("P-001", subject.screening_number,
                         "prefix should start by P "
                         "as default location prefix is not defined and subject type is patient")

    def test_subjects_add_subject_with_custom_file_field(self):
        field = CustomStudySubjectField.objects.create(study=self.study, type=CUSTOM_FIELD_TYPE_FILE)
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()

        form_data["study_subject-" + get_study_subject_field_id(field)] = SimpleUploadedFile("my-custom-file-name.txt",
                                                                                             b"file_content")
        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)

        subject = StudySubject.objects.all().order_by("-id")[0]
        self.assertTrue('my-custom-file-name' in subject.get_custom_data_value(field).value)

    def test_subjects_add_invalid(self):
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()
        form_data["study_subject-type"] = get_control_subject_type().id

        form_data["subject-country"] = COUNTRY_OTHER_ID
        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)

        self.assertTrue("Invalid data".encode('utf8') in response.content)

    def test_subjects_add_with_prefixed_location(self):
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()

        self.add_valid_form_data_for_subject_add(form_data)
        location = get_test_location()
        location.prefix = 'X'
        location.save()
        form_data["study_subject-default_location"] = get_test_location().id
        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(response.url)
        self.assertContains(response, "Subject created")

        subject = StudySubject.objects.all().order_by("-id")[0]
        self.assertEqual("X-001", subject.screening_number,
                         "prefix should start by X as default location prefix is equal to 'X'")

    def test_render_subjects(self):
        response = self.client.get(reverse('web.views.subjects'))
        self.assertEqual(response.status_code, 200)

    def test_render_subjects_with_no_visit(self):
        response = self.client.get(reverse('web.views.subject_no_visits'))
        self.assertEqual(response.status_code, 200)

    def test_render_subjects_voucher_expiry(self):
        response = self.client.get(reverse('web.views.subject_voucher_expiry'))
        self.assertEqual(response.status_code, 200)

    def test_render_subjects_require_contact(self):
        self.study_subject.datetime_contact_reminder = get_today_midnight_date() + datetime.timedelta(days=-1)

        response = self.client.get(reverse('web.views.subject_require_contact'))
        self.assertEqual(response.status_code, 200)

    def test_save_subject_edit_when_type_changed(self):
        form_data = self.create_edit_form_data_for_study_subject(self.study_subject)

        count = Provenance.objects.all().count()
        form_data["study_subject-type"] = get_patient_subject_type().id
        response = self.client.post(
            reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id}), data=form_data)

        self.assertEqual(response.status_code, 302)

        self.assertEqual(count + 1, Provenance.objects.all().count())

    def test_save_subject_when_resigned_is_not_in_study(self):
        self.study.columns.resigned = False
        self.study.columns.save()

        form_data = self.create_edit_form_data_for_study_subject()

        form_data['subject-dead'] = "True"

        url = reverse('web.views.subject_edit', kwargs={'subject_id': self.study_subject.id})
        response = self.client.post(url, data=form_data)

        self.assertEqual(response.status_code, 302)
        updated_study_subject = StudySubject.objects.get(id=self.study_subject.id)
        self.assertTrue(updated_study_subject.subject.dead)

    def test_subjects_add_when_screening_number_disabled(self):
        self.study.columns.screening_number = False
        self.study.columns.save()
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()

        count = StudySubject.objects.all().count()
        self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}), data=form_data)

        self.assertEqual(count + 1, StudySubject.objects.all().count())

    def test_subjects_add_subject_with_custom_file_field_tracked(self):
        field = CustomStudySubjectField.objects.create(name="test-tracked", study=self.study,
                                                       type=CUSTOM_FIELD_TYPE_TEXT, tracked=True)
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()

        form_data["study_subject-" + get_study_subject_field_id(field)] = "BLA"
        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)

        subject = StudySubject.objects.all().order_by("-id")[0]

        self.assertTrue('BLA' in subject.get_custom_data_value(field).value)
        self.assertTrue(len(subject.provenances) == 0)

        form_data["study_subject-" + get_study_subject_field_id(field)] = "BLABLA"
        response = self.client.post(reverse('web.views.subject_edit', kwargs={'subject_id': subject.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)
        self.assertTrue('BLABLA' in subject.get_custom_data_value(field).value)
        self.assertEqual("test-tracked", subject.provenances[0].modified_field)
        self.assertEqual('BLA', subject.provenances[0].previous_value)
        self.assertEqual('BLABLA', subject.provenances[0].new_value)

    def test_subjects_add_subject_with_multiple_custom_file_field_tracked(self):
        field = CustomStudySubjectField.objects.create(name="test-tracked", study=self.study,
                                                       type=CUSTOM_FIELD_TYPE_TEXT, tracked=True)
        field2 = CustomStudySubjectField.objects.create(name="test-tracked2", study=self.study,
                                                        type=CUSTOM_FIELD_TYPE_TEXT, tracked=True)
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()

        form_data["study_subject-" + get_study_subject_field_id(field)] = "BLA"
        form_data["study_subject-" + get_study_subject_field_id(field2)] = "lorem"
        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)

        subject = StudySubject.objects.all().order_by("-id")[0]

        self.assertTrue('BLA' in subject.get_custom_data_value(field).value)
        self.assertTrue('lorem' in subject.get_custom_data_value(field2).value)
        self.assertTrue(len(subject.provenances) == 0)

        form_data["study_subject-" + get_study_subject_field_id(field)] = "BLABLA"
        form_data["study_subject-" + get_study_subject_field_id(field2)] = "loremlorem"
        response = self.client.post(reverse('web.views.subject_edit', kwargs={'subject_id': subject.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)
        self.assertTrue('BLABLA' in subject.get_custom_data_value(field).value)
        self.assertEqual("test-tracked", subject.provenances[1].modified_field)
        self.assertEqual('BLA', subject.provenances[1].previous_value)
        self.assertEqual('BLABLA', subject.provenances[1].new_value)

        self.assertTrue('loremlorem' in subject.get_custom_data_value(field2).value)
        self.assertEqual("test-tracked2", subject.provenances[0].modified_field)
        self.assertEqual('lorem', subject.provenances[0].previous_value)
        self.assertEqual('loremlorem', subject.provenances[0].new_value)

    def test_subjects_add_subject_with_custom_file_field_not_tracked(self):
        field = CustomStudySubjectField.objects.create(name="test-tracked", study=self.study,
                                                       type=CUSTOM_FIELD_TYPE_TEXT, tracked=False)
        self.worker.roles.all()[0].permissions.add(Permission.objects.get(codename="add_subject"))
        self.worker.save()
        form_data = self.create_add_form_data_for_study_subject()

        form_data["study_subject-" + get_study_subject_field_id(field)] = "BLA"
        response = self.client.post(reverse('web.views.subject_add', kwargs={'study_id': self.study.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)

        subject = StudySubject.objects.all().order_by("-id")[0]

        self.assertTrue('BLA' in subject.get_custom_data_value(field).value)
        self.assertTrue(len(subject.provenances) == 0)

        form_data["study_subject-" + get_study_subject_field_id(field)] = "BLABLA"
        response = self.client.post(reverse('web.views.subject_edit', kwargs={'subject_id': subject.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 302)
        self.assertTrue(len(subject.provenances) == 0)
