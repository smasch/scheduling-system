import logging

from django.db import models
from django.forms import ModelForm
from django.urls import reverse

from web.forms.subject_type_forms import SubjectTypeAddForm, SubjectTypeEditForm
from web.models import SubjectType, Study
from web.models.constants import GLOBAL_STUDY_ID
from web.models.study import FOLLOW_UP_INCREMENT_IN_YEARS
from web.tests import LoggedInTestCase
from web.tests.functions import format_form_field, get_patient_subject_type

logger = logging.getLogger(__name__)


class SubjectTypeTests(LoggedInTestCase):
    def test_subject_type_requests(self):
        self.login_as_admin()
        pages = [
            'web.views.subject_types',
            'web.views.subject_type_add',
        ]

        for page in pages:
            response = self.client.get(get_url(page))
            self.assertEqual(response.status_code, 200)

    def test_subject_type_requests_without_permission(self):
        pages = [
            'web.views.subject_types',
            'web.views.subject_type_add',
        ]

        for page in pages:
            response = self.client.get(get_url(page))
            self.assertEqual(response.status_code, 302)

    def test_subject_type_add(self):
        self.login_as_admin()
        page = get_url('web.views.subject_type_add')

        form_data = create_form_with_init_data(SubjectTypeAddForm)
        form_data['name'] = 'bla'

        response = self.client.post(page, form_data)
        self.assertEqual(response.status_code, 302)

        freshly_created = SubjectType.objects.filter(name=form_data['name'])
        self.assertEqual(len(freshly_created), 1)

    def test_subject_type_edit(self):
        self.login_as_admin()
        patient_type = get_patient_subject_type()
        page = get_url('web.views.subject_type_edit', patient_type)
        form_data = create_form_with_init_data(SubjectTypeEditForm, patient_type)
        form_data['name'] = 'bla'

        response = self.client.post(page, form_data)
        self.assertEqual(response.status_code, 302)

        freshly_edited = SubjectType.objects.get(id=patient_type.id)
        self.assertEqual(freshly_edited.name, form_data["name"])

    def test_subject_type_edit_request(self):
        self.login_as_admin()
        page = get_url('web.views.subject_type_edit', get_patient_subject_type())
        response = self.client.get(page)
        self.assertEqual(response.status_code, 200)


def get_url(view_name: str, subject_type: SubjectType = None):
    if subject_type is None:
        return reverse(view_name, kwargs={
            'study_id': str(GLOBAL_STUDY_ID),
        })
    else:
        return reverse(view_name, kwargs={
            'subject_type_id': str(subject_type.id),
            'study_id': str(subject_type.study.id),
        })


def create_form_with_init_data(form_type: [ModelForm], instance: models.Model = None):
    if instance is None:
        form = form_type(instance=instance, study=Study.objects.get(pk=GLOBAL_STUDY_ID))
    else:
        form = form_type(instance=instance)
    form_data = {'name': 'abc',
                 'screening_number_prefix': 'a',
                 'follow_up_delta_time': '1',
                 'follow_up_delta_units': FOLLOW_UP_INCREMENT_IN_YEARS
                 }
    for key, value in list(form.initial.items()):
        form_data[key] = format_form_field(value)
    return form_data
