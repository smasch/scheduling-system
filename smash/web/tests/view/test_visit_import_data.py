import logging

from django.urls import reverse

from web.forms.visit_import_data_form import VisitImportDataEditForm
from web.models import VisitImportData
from web.tests import LoggedInTestCase
from web.tests.functions import get_test_study, format_form_field

logger = logging.getLogger(__name__)


class VisitImportDataViewViewTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.study = get_test_study()
        self.visit_import_data = VisitImportData.objects.create(study=get_test_study())

    def test_render_edit(self):
        self.login_as_admin()
        response = self.client.get(reverse('web.views.import_visit_edit',
                                           kwargs={'study_id': self.study.id, 'import_id': self.visit_import_data.id}))
        self.assertEqual(response.status_code, 200)

    def test_save_edit(self):
        self.login_as_admin()
        form_data = self.get_form_data(self.visit_import_data)

        response = self.client.post(
            reverse('web.views.import_visit_edit',
                    kwargs={'study_id': self.study.id, 'import_id': self.visit_import_data.id}), data=form_data)

        self.assertEqual(response.status_code, 302)
        self.assertTrue("study" in response['Location'])

    @staticmethod
    def get_form_data(visit_import_data: VisitImportData) -> dict:
        voucher_form = VisitImportDataEditForm(instance=visit_import_data)
        form_data = {}
        for key, value in list(voucher_form.initial.items()):
            form_data[key] = format_form_field(value)
        return form_data
