from django.urls import reverse

from web.tests import LoggedInTestCase

import logging
logger = logging.getLogger(__name__)


class ConfigurationViewTests(LoggedInTestCase):
    def test_visit_details_request(self):
        self.login_as_admin()
        response = self.client.get(reverse('web.views.configuration'))
        self.assertEqual(response.status_code, 200)

    def test_visit_details_request_without_permissions(self):
        self.login_as_staff()
        response = self.client.get(reverse('web.views.configuration'))
        self.assertEqual(response.status_code, 302)
