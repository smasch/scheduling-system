import logging

from django.urls import reverse

from web.tests.functions import create_item
from web.models import Item
from web.tests import LoggedInTestCase

logger = logging.getLogger(__name__)


class EquipmentTests(LoggedInTestCase):

    def test_list_without_permissions(self):
        self.login_as_staff()
        response = self.client.get(reverse('web.views.equipment'))
        self.assertEqual(response.status_code, 302)

    def test_equipment_requests(self):
        self.login_as_admin()
        pages = [
            'web.views.equipment',
            'web.views.equipment_add',
        ]

        for page in pages:
            response = self.client.get(reverse(page))
            self.assertEqual(response.status_code, 200)

    def test_equipment_edit_request(self):
        self.login_as_admin()
        item = create_item()
        page = reverse('web.views.equipment_edit',
                       kwargs={'equipment_id': str(item.id)})
        response = self.client.get(page)
        self.assertEqual(response.status_code, 200)

    def test_equipment_delete_request(self):
        self.login_as_admin()
        item = create_item()
        page = reverse('web.views.equipment_delete',
                       kwargs={'equipment_id': str(item.id)})
        response = self.client.get(page)
        self.assertEqual(response.status_code, 302)

    def test_equipment_add(self):
        self.login_as_admin()
        page = reverse('web.views.equipment_add')
        data = {
            'name': 'The mysterious potion',
            'disposable': True,
            'is_fixed': False,
        }
        response = self.client.post(page, data)
        self.assertEqual(response.status_code, 302)

        freshly_created = Item.objects.filter(name=data['name'])
        self.assertEqual(len(freshly_created), 1)

    def test_equipment_edit(self):
        self.login_as_admin()
        item = create_item()
        page = reverse('web.views.equipment_edit',
                       kwargs={'equipment_id': str(item.id)})
        data = {
            'name': 'Even more mysterious potion',
            'disposable': True,
            'is_fixed': True,
        }
        response = self.client.post(page, data)
        self.assertEqual(response.status_code, 302)

        freshly_edited = Item.objects.get(id=item.id)
        for key in data:
            self.assertEqual(getattr(freshly_edited, key, ''), data[key])

    def test_equipment_delete(self):
        self.login_as_admin()
        item = create_item()
        page = reverse('web.views.equipment_delete',
                       kwargs={'equipment_id': str(item.id)})

        response = self.client.get(page)
        self.assertEqual(response.status_code, 302)

        freshly_deleted = Item.objects.filter(id=item.id)
        self.assertEqual(len(freshly_deleted), 0)
