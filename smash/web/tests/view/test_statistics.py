# coding=utf-8

from django.urls import reverse
from django.utils import timezone

from web.tests import LoggedInTestCase

__author__ = 'Valentin Grouès'


class TestStatisticsView(LoggedInTestCase):
    def test_statistics_request(self):
        self.login_as_admin()
        url = reverse('web.views.statistics')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        current_month = timezone.now().month - 1 or 12
        content = response.content.decode('utf8')
        self.assertIn(f'<option value="{current_month}" selected>', content)
        response = self.client.get(url, {"month": 10, "year": 2017, "subject_type": -1, "visit": -1})
        content = response.content.decode('utf8')
        self.assertIn('<option value="10" selected>October', content)

    def test_statistics_request_extended(self):
        self.login_as_admin()
        url = reverse('web.views.statistics', )
        response = self.client.get(url, {"month": 10, "year": 2017, "subject_type": 1, "visit": 1})
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url, {"month": 10, "year": 2017, "subject_type": 2, "visit": 1})
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url, {"month": 10, "year": 2017, "subject_type": 2, "visit": -1})
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url, {"month": 10, "year": 2017, "subject_type": 1, "visit": -1})
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url, {"month": 10, "year": 2017, "subject_type": 3, "visit": -1})
        self.assertEqual(response.status_code, 200)

    def test_statistics_request_without_permission(self):
        url = reverse('web.views.statistics')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
