import logging
import random

from django.urls import reverse

from web.models import FlyingTeam
from web.tests import LoggedInTestCase
from web.tests.functions import create_flying_team

logger = logging.getLogger(__name__)


class FlyingTeamTests(LoggedInTestCase):
    @staticmethod
    def generate_more_or_less_random_name():
        letters = [chr(x) for x in range(97, 122)]
        return 'Random' + ''.join(random.choice(letters) for x in range(15))

    def test_flying_team_requests(self):
        self.login_as_admin()
        pages = [
            'web.views.equipment_and_rooms.flying_teams',
            'web.views.equipment_and_rooms.flying_teams_add',
        ]

        for page in pages:
            response = self.client.get(reverse(page))
            self.assertEqual(response.status_code, 200)

    def test_flying_team_requests_without_permission(self):
        pages = [
            'web.views.equipment_and_rooms.flying_teams',
            'web.views.equipment_and_rooms.flying_teams_add',
        ]

        for page in pages:
            response = self.client.get(reverse(page))
            self.assertEqual(response.status_code, 302)

    def test_flying_team_add(self):
        self.login_as_admin()
        page = reverse('web.views.equipment_and_rooms.flying_teams_add')
        data = {
            'place': self.generate_more_or_less_random_name()
        }
        response = self.client.post(page, data)
        self.assertEqual(response.status_code, 302)

        freshly_created = FlyingTeam.objects.filter(place=data['place'])
        self.assertEqual(len(freshly_created), 1)

    def test_flying_team_edit(self):
        self.login_as_admin()
        flying_team = create_flying_team()
        page = reverse('web.views.equipment_and_rooms.flying_teams_edit',
                       kwargs={'flying_team_id': str(flying_team.id)})
        data = {
            'place': self.generate_more_or_less_random_name()
        }
        response = self.client.post(page, data)
        self.assertEqual(response.status_code, 302)

        freshly_edited = FlyingTeam.objects.get(id=flying_team.id)
        self.assertEqual(freshly_edited.place, data["place"])

    def test_flying_team_edit_request(self):
        self.login_as_admin()
        flying_team = create_flying_team()
        page = reverse('web.views.equipment_and_rooms.flying_teams_edit',
                       kwargs={'flying_team_id': str(flying_team.id)})
        response = self.client.get(page)
        self.assertEqual(response.status_code, 200)
