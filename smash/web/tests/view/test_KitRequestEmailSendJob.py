import datetime

from django.core import mail
from django_cron.models import CronJobLog
from mockito import when

from web.models import Item, AppointmentTypeLink, Worker, ConfigurationItem
from web.models.constants import KIT_EMAIL_HOUR_CONFIGURATION_TYPE, KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE
from web.tests import LoggedInTestCase
from web.tests.functions import create_appointment_type, create_appointment, create_language
from web.views.kit import KitRequestEmailSendJob
from web.views.notifications import get_today_midnight_date


class KitRequestEmailSendJobTests(LoggedInTestCase):
    def setUp(self):
        create_language(name="English")

    def test_kit_requests_send_email(self):
        CronJobLog.objects.all().delete()
        configuration = ConfigurationItem.objects.get(type=KIT_EMAIL_HOUR_CONFIGURATION_TYPE)
        configuration.value = "00:00"
        configuration.save()

        job = KitRequestEmailSendJob()
        when(job).match_day_of_week().thenReturn(True)

        item_name = "Test item to be ordered"
        item = Item.objects.create(disposable=True, name=item_name)
        appointment_type = create_appointment_type()
        appointment_type.required_equipment.add(item)
        appointment_type.save()

        appointment = create_appointment()
        appointment.datetime_when = get_today_midnight_date() + datetime.timedelta(days=2)
        appointment.save()
        AppointmentTypeLink.objects.create(appointment=appointment, appointment_type=appointment_type)

        workers_count = Worker.objects.all().count()
        status = job.do()

        self.assertEqual("mail sent", status)
        self.assertEqual(1, len(mail.outbox))
        self.assertEqual(workers_count, Worker.objects.all().count())

    def test_match_day_of_week_with_asterisk(self):
        item = ConfigurationItem.objects.get(type=KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE)
        item.value = "*"
        item.save()

        self.assertTrue(KitRequestEmailSendJob().match_day_of_week())

    def test_match_day_of_week_with_unknown(self):
        item = ConfigurationItem.objects.get(type=KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE)
        item.value = "unknown_day_of_week"
        item.save()

        self.assertFalse(KitRequestEmailSendJob().match_day_of_week())
