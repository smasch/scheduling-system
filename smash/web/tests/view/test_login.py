# coding=utf-8
from django.conf import settings
from django.contrib import auth as django_auth
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from web.models import Worker
from web.tests.functions import create_user


class TestLoginView(TestCase):
    def test_login(self):
        client = Client()

        user = create_user()
        password = 'top_secret'
        username = user.username
        login_url = reverse(settings.LOGIN_URL)
        self.assertFalse(django_auth.get_user(client).is_authenticated)
        form_data = {'auth-username': username, 'auth-password': password, 'login_view-current_step': 'auth'}
        response = client.post(login_url, data=form_data, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertTrue(django_auth.get_user(client).is_authenticated)
        worker = Worker.get_by_user(user)
        self.assertIsNotNone(worker)
        worker.last_name = 'Grouès'
        worker.save()
        response = client.post(login_url, data=form_data, follow=True)
        self.assertEqual(200, response.status_code)

    def test_login_failed(self):
        client = Client()
        user = create_user()
        username = user.username
        login_url = reverse(settings.LOGIN_URL)
        response = client.post(login_url, data={'auth-username': username, 'auth-password': 'wrong-password',
                                                'login_view-current_step': 'auth'}, follow=False)
        self.assertContains(response, 'Please enter a correct')
        self.assertContains(response, 'and password.')
        self.assertFalse(django_auth.get_user(client).is_authenticated)

    def test_logout(self):
        self.test_login()
        logout_url = reverse('logout')
        self.client.get(logout_url)
        self.assertFalse(django_auth.get_user(self.client).is_authenticated)
