# coding=utf-8
import datetime
import logging

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from django.utils import timezone

from web.forms import SubjectEditForm, StudySubjectEditForm, AppointmentEditForm
from web.models import Appointment, StudySubject, Visit
from web.models.study import FOLLOW_UP_INCREMENT_IN_WEEKS
from web.tests import LoggedInTestCase
from web.tests.functions import create_study_subject, create_visit, create_appointment, create_flying_team, \
    format_form_field, get_test_location
from web.views.notifications import get_today_midnight_date

logger = logging.getLogger(__name__)


class AppointmentsViewTests(LoggedInTestCase):

    def test_get_add_general_appointment(self):
        # test get without visit_id
        response = self.client.get(reverse('web.views.appointment_add_general'))
        self.assertEqual(response.status_code, 200)

    def test_get_add_appointment(self):
        # test get with visit_id
        subject = create_study_subject()
        visit = create_visit(subject)
        response = self.client.get(reverse('web.views.appointment_add',
                                           kwargs={'visit_id': visit.id}))
        self.assertEqual(response.status_code, 200)

    def test_post_add_general_appointment(self):
        form_data = self.create_add_appointment_form_data()
        response = self.client.post(reverse('web.views.appointment_add_general'), data=form_data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(1, Appointment.objects.count())

    @staticmethod
    def create_add_appointment_form_data():
        location = get_test_location()
        form_data = {}
        form_data['datetime_when'] = datetime.datetime.today()
        form_data['location'] = location.id
        form_data['length'] = 10
        form_data['comment'] = 'A comment with weird letters such as á è ü ñ ô'
        return form_data

    def test_add_appointment(self):
        count = Appointment.objects.count()
        subject = create_study_subject()
        visit = create_visit(subject)
        form_data = self.create_add_appointment_form_data()
        response = self.client.post(reverse('web.views.appointment_add', kwargs={'visit_id': visit.id}),
                                    data=form_data,
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(count + 1, Appointment.objects.count())

    def test_add_appointment_to_finished_visit(self):
        count = Appointment.objects.count()
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.is_finished = True
        visit.save()
        form_data = self.create_add_appointment_form_data()
        response = self.client.post(reverse('web.views.appointment_add', kwargs={'visit_id': visit.id}),
                                    data=form_data,
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(count, Appointment.objects.count())

    def test_add_appointment_to_deceased_subject(self):
        count = Appointment.objects.count()
        subject = create_study_subject()
        subject.subject.dead = True
        subject.subject.save()
        visit = create_visit(subject)
        form_data = self.create_add_appointment_form_data()
        response = self.client.post(reverse('web.views.appointment_add', kwargs={'visit_id': visit.id}),
                                    data=form_data,
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(count, Appointment.objects.count())

    def test_appointments_list_request(self):
        response = self.client.get(reverse('web.views.appointments'))
        self.assertEqual(response.status_code, 200)

    def test_appointments_edit(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit, when=timezone.now())
        new_comment = 'new unicode comment with accents à è ì ò ù'
        new_status = appointment.APPOINTMENT_STATUS_NO_SHOW
        new_last_name = "new last name"
        form_data = self.prepare_form(appointment, subject)
        form_data["appointment-comment"] = new_comment
        form_data["appointment-status"] = new_status
        form_data["subject-last_name"] = new_last_name
        response = self.client.post(
            reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}), data=form_data)
        self.assertEqual(response.status_code, 302)
        updated_appointment = Appointment.objects.filter(id=appointment.id)[0]
        updated_subject = StudySubject.objects.filter(id=subject.id)[0]
        self.assertEqual(new_comment, updated_appointment.comment)
        self.assertEqual(new_status, updated_appointment.status)
        self.assertEqual(new_last_name, updated_subject.subject.last_name)

    def test_edit_appointments_for_deceased_subject(self):
        subject = create_study_subject()
        subject.subject.dead = True
        subject.subject.save()
        visit = create_visit(subject)
        appointment = create_appointment(visit, when=timezone.now())
        form_data = self.prepare_form(appointment, subject)
        response = self.client.get(
            reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}), data=form_data,
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Appointment cannot be edited")

    def test_appointments_edit_from_visit(self):
        visit = create_visit()
        appointment = create_appointment(visit, when=timezone.now())
        form_data = self.prepare_form(appointment, visit.subject)
        response = self.client.post(reverse('web.views.appointment_edit',
                                            kwargs={'appointment_id': appointment.id}) + "?from_visit",
                                    data=form_data,
                                    follow=True)
        self.assertEqual(response.status_code, 200)

    def test_appointments_edit_without_visit(self):
        appointment = create_appointment()
        appointment.visit = None
        appointment.save()

        response = self.client.get(
            reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}))
        self.assertEqual(response.status_code, 200)

    def test_render_appointments_edit(self):
        appointment = create_appointment()
        appointment.comment = "APPOINTMENT_COMMENT_DATA"
        appointment.save()
        appointment.visit.subject.comments = "STUDY_SUBJECT_COMMENT_DATA"
        appointment.visit.subject.save()
        appointment.visit.subject.subject.first_name = "SUBJECT_FIRST_NAME_DATA"
        appointment.visit.subject.subject.save()

        response = self.client.get(
            reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(appointment.comment.encode('utf8') in response.content,
                        msg="Appointment data not visible in rendered page")
        self.assertTrue(appointment.visit.subject.comments.encode('utf8') in response.content,
                        msg="Subject study data not visible in rendered page")
        self.assertTrue(appointment.visit.subject.subject.first_name.encode('utf8') in response.content,
                        msg="Subject data not visible in rendered page")

    def test_save_appointments_edit_without_visit(self):
        appointment = create_appointment()
        appointment.visit = None
        appointment.save()

        form_appointment = AppointmentEditForm(user=self.user, instance=appointment, prefix="appointment")
        form_data = {}
        for key, value in list(form_appointment.initial.items()):
            if value is not None:
                form_data[f'appointment-{key}'] = value
        response = self.client.post(
            reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}), data=form_data)

        self.assertEqual(response.status_code, 200)

    def test_save_appointments_edit_with_continue(self):
        appointment = create_appointment(when=datetime.datetime.now())
        appointment.visit = None
        appointment.save()

        form_appointment = AppointmentEditForm(user=self.user, instance=appointment, prefix="appointment")
        form_data = {'_continue': True}
        for key, value in list(form_appointment.initial.items()):
            if value is not None:
                form_data[f'appointment-{key}'] = value
        response = self.client.post(reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}),
                                    data=form_data, follow=True)

        self.assertEqual(response.status_code, 200)

    def test_save_as_finished_appointments_edit_without_visit(self):
        appointment = create_appointment(None, get_today_midnight_date())
        appointment.visit = None
        appointment.save()

        form_appointment = AppointmentEditForm(user=self.user, instance=appointment, prefix="appointment")
        form_data = {}
        for key, value in list(form_appointment.initial.items()):
            if value is not None:
                form_data[f'appointment-{key}'] = format_form_field(value)
        form_data['appointment-status'] = Appointment.APPOINTMENT_STATUS_FINISHED
        self.client.post(reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}),
                         data=form_data)

        appointment_result = Appointment.objects.filter(id=appointment.id)[0]
        self.assertEqual(Appointment.APPOINTMENT_STATUS_FINISHED, appointment_result.status)

    def test_save_appointments_edit_with_invalid_nd_number(self):
        subject = create_study_subject(nd_number='DUMB_ND_NUMBER')
        visit = create_visit(subject)
        appointment = create_appointment(visit, get_today_midnight_date())

        form_data = self.prepare_form(appointment, subject)
        form_data["appointment-status"] = Appointment.APPOINTMENT_STATUS_FINISHED

        response = self.client.post(reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}),
                                    data=form_data)

        self.assertEqual(response.status_code, 200)

        updated_subject = StudySubject.objects.get(id=subject.id)
        self.assertFalse(updated_subject.information_sent)

    def test_save_appointments_edit_with_valid_nd_number(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = timezone.now().replace(year=2011)
        visit.save()
        appointment = create_appointment(visit, get_today_midnight_date())

        form_data = self.prepare_form(appointment, subject)
        form_data["appointment-status"] = Appointment.APPOINTMENT_STATUS_FINISHED
        form_data["study-subject-nd_number"] = "ND9999"

        response = self.client.post(
            reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}), data=form_data)

        self.assertEqual(response.status_code, 302)

        updated_subject = StudySubject.objects.get(id=subject.id)
        self.assertTrue(updated_subject.information_sent)

        updated_visit = Visit.objects.get(id=visit.id)

        self.assertEqual(appointment.datetime_when, updated_visit.datetime_begin)
        self.assertNotEqual(visit.datetime_end, updated_visit.datetime_end)

    def test_save_appointments_edit(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        visit.datetime_begin = timezone.now().replace(year=2011)
        visit.save()
        visit = Visit.objects.get(id=visit.id)

        appointment = create_appointment(visit, get_today_midnight_date())
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()

        appointment2 = create_appointment(visit, get_today_midnight_date())

        form_data = self.prepare_form(appointment2, subject)
        form_data["appointment-status"] = Appointment.APPOINTMENT_STATUS_FINISHED
        form_data["study-subject-nd_number"] = "ND9999"

        response = self.client.post(
            reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment2.id}), data=form_data)

        self.assertEqual(response.status_code, 302)

        updated_visit = Visit.objects.get(id=visit.id)

        self.assertNotEqual(appointment.datetime_when, updated_visit.datetime_begin)
        self.assertEqual(visit.datetime_begin, updated_visit.datetime_begin)
        self.assertEqual(visit.datetime_end, updated_visit.datetime_end)

    def prepare_form(self, appointment, subject):
        form_appointment = AppointmentEditForm(user=self.user, instance=appointment, prefix="appointment")
        form_study_subject = StudySubjectEditForm(instance=subject, prefix="study-subject")
        form_subject = SubjectEditForm(instance=subject.subject, prefix="subject")
        form_data = {}
        for key, value in list(form_appointment.initial.items()):
            form_data[f'appointment-{key}'] = format_form_field(value)
        for key, value in list(form_study_subject.initial.items()):
            form_data[f'study-subject-{key}'] = format_form_field(value)
        for key, value in list(form_subject.initial.items()):
            form_data[f'subject-{key}'] = format_form_field(value)
        form_data["study-subject-referral_letter"] = SimpleUploadedFile("file.txt", b"file_content")
        return form_data

    def test_subject_flying_team_location(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit, get_today_midnight_date())

        form_data = self.prepare_form(appointment, subject)
        form_data["appointment-status"] = Appointment.APPOINTMENT_STATUS_FINISHED
        form_data["appointment-flying_team"] = create_flying_team().id
        form_data['appointment-status'] = Appointment.APPOINTMENT_STATUS_FINISHED
        form_data["study-subject-nd_number"] = "ND9999"

        self.client.post(reverse('web.views.appointment_edit', kwargs={'appointment_id': appointment.id}),
                         data=form_data)

        updated_subject = StudySubject.objects.get(id=subject.id)
        self.assertIsNotNone(updated_subject.flying_team)

    def test_delete_appointment(self):
        appointment = create_appointment()
        appointment.visit = None
        appointment.save()

        # without permission
        self.login_as_staff()
        self.client.post(reverse('web.views.appointment_delete', kwargs={'pk': appointment.id}))
        self.assertEqual(1, Appointment.objects.all().count())

        # with permission
        self.login_as_super()
        self.client.post(reverse('web.views.appointment_delete', kwargs={'pk': appointment.id}))
        self.assertEqual(0, Appointment.objects.all().count())

    def test_delete_appointment_with_visit(self):
        self.login_as_super()
        appointment = create_appointment()

        self.client.post(reverse('web.views.appointment_delete', kwargs={'pk': appointment.id}))

        self.assertEqual(0, Appointment.objects.all().count())

    def test_delete_invalid_finished_appointment(self):
        self.login_as_super()
        appointment = create_appointment()
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.visit = None
        appointment.save()

        response = self.client.post(reverse('web.views.appointment_delete', kwargs={'pk': appointment.id}), follow=True)
        self.assertEqual(200, response.status_code)

        self.assertEqual(1, Appointment.objects.all().count())

    def test_delete_invalid_finished_appointment_with_visit(self):
        self.login_as_super()
        appointment = create_appointment()
        appointment.status = Appointment.APPOINTMENT_STATUS_FINISHED
        appointment.save()

        self.client.post(reverse('web.views.appointment_delete', kwargs={'pk': appointment.id}))

        self.assertEqual(1, Appointment.objects.all().count())

    def test_post_add_general_recurring_appointment(self):
        form_data = self.create_add_appointment_form_data()
        form_data['is_recurring'] = True
        form_data['until_recurring'] = datetime.datetime.today() + datetime.timedelta(days=18)
        form_data['time_unit_recurring'] = FOLLOW_UP_INCREMENT_IN_WEEKS
        self.client.post(reverse('web.views.appointment_add_general'), data=form_data)
        self.assertEqual(3, Appointment.objects.count())

    def test_post_add_general_recurring_appointment_without_end_date(self):
        form_data = self.create_add_appointment_form_data()
        form_data['is_recurring'] = True
        form_data['time_unit_recurring'] = FOLLOW_UP_INCREMENT_IN_WEEKS
        response = self.client.post(reverse('web.views.appointment_add_general'), data=form_data)
        self.assertTrue("Please introduce a valid date".encode('utf8') in response.content)

    def test_post_add_general_recurring_too_many_appointment(self):
        form_data = self.create_add_appointment_form_data()
        form_data['is_recurring'] = True
        form_data['until_recurring'] = datetime.datetime.today() + datetime.timedelta(days=1000)
        form_data['time_unit_recurring'] = FOLLOW_UP_INCREMENT_IN_WEEKS
        self.client.post(reverse('web.views.appointment_add_general'), data=form_data)
        self.assertEqual(0, Appointment.objects.count())
