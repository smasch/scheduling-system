from django.test import TestCase

from web.models import Worker
from web.tests.functions import create_location, create_user, create_worker, get_test_location
from web.views.notifications import get_filter_locations


class ViewFunctionsTests(TestCase):
    def setUp(self):
        create_location("testLocation")

    def test_locations_for_user_no_locations(self):
        user = create_user()

        self.assertEqual(0, len(get_filter_locations(user)))

    def test_locations_for_user(self):
        user = create_user()
        worker = Worker.get_by_user(user)
        worker.locations.set([get_test_location()])
        worker.save()
        create_location()

        self.assertEqual(1, len(get_filter_locations(user)))

    def test_locations_for_worker_no_location(self):
        worker = create_worker()

        self.assertEqual(0, len(get_filter_locations(worker)))

    def test_locations_for_worker_with_location(self):
        worker = create_worker()
        worker.locations.set([get_test_location()])
        worker.save()
        create_location()

        self.assertEqual(1, len(get_filter_locations(worker)))

    def test_removed_locations(self):
        location = get_test_location()
        worker = create_worker()
        worker.locations.set([location])
        worker.save()
        create_location()

        location.removed = True
        location.save()

        self.assertEqual(0, len(get_filter_locations(worker)))
