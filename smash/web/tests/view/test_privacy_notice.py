import os

from django.contrib.messages import get_messages
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse

from web.forms import PrivacyNoticeForm, WorkerAcceptPrivacyNoticeForm
from web.models import PrivacyNotice, Study, Worker
from web.models.constants import GLOBAL_STUDY_ID
from web.tests import LoggedInTestCase


class PrivacyNoticeTests(LoggedInTestCase):
    def setUp(self):
        super().setUp()
        self.assertEqual(0, PrivacyNotice.objects.count())
        self.login_as_admin()

        form_data = {"name": "example", "summary": "example summary"}

        file_data = {"document": SimpleUploadedFile("file.txt", b"file_content")}

        form = PrivacyNoticeForm(form_data, file_data)
        self.assertTrue(form.is_valid())

        page = reverse("web.views.privacy_notice_add")
        response = self.client.post(page, data={**form_data, **file_data})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(1, PrivacyNotice.objects.count())

    def tearDown(self):
        for privacy_notice in PrivacyNotice.objects.all():
            path = privacy_notice.document.path
            privacy_notice.delete()
            self.assertFalse(os.path.isfile(path))

    def test_edit_privacy_notice(self):
        self.assertEqual(1, PrivacyNotice.objects.count())
        pn = PrivacyNotice.objects.all()[0]
        form_data = {"name": "example2", "summary": pn.summary}

        file_data = {"document": SimpleUploadedFile("file.txt", b"file_content")}

        form = PrivacyNoticeForm(form_data, file_data, instance=pn)
        self.assertTrue(form.is_valid())

        page = reverse("web.views.privacy_notice_edit", kwargs={"pk": pn.id})
        response = self.client.post(page, data={**form_data, **file_data})
        self.assertEqual(response.status_code, 302)
        pn = PrivacyNotice.objects.all()[0]
        self.assertEqual(pn.name, "example2")

    def test_delete_privacy_notice(self):
        self.assertEqual(1, PrivacyNotice.objects.count())
        pn = PrivacyNotice.objects.all()[0]
        page = reverse("web.views.privacy_notice_delete", kwargs={"pk": pn.id})
        response = self.client.post(page)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(0, PrivacyNotice.objects.count())

    def test_privacy_notice_middleware_superuser(self):
        self.login_as_admin()
        # assign privacy notice
        pn = PrivacyNotice.objects.all()[0]
        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        study.acceptance_of_study_privacy_notice_required = True
        study.study_privacy_notice = pn
        study.save()

        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        self.assertEqual(study.acceptance_of_study_privacy_notice_required, True)
        self.assertEqual(study.study_privacy_notice.id, pn.id)

        self.login_as_super()
        self.assertEqual(self.staff_worker.privacy_notice_accepted, False)
        page = reverse("web.views.appointments")
        response = self.client.get(page)
        self.assertEqual(response.status_code, 200)
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 0)

    def test_privacy_notice_middleware(self):
        self.login_as_admin()
        # assign privacy notice
        pn = PrivacyNotice.objects.all()[0]
        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        study.acceptance_of_study_privacy_notice_required = True
        study.study_privacy_notice = pn
        study.save()

        study = Study.objects.filter(id=GLOBAL_STUDY_ID)[0]
        self.assertEqual(study.acceptance_of_study_privacy_notice_required, True)
        self.assertEqual(study.study_privacy_notice.id, pn.id)

        self.login_as_staff()
        self.assertEqual(self.staff_worker.privacy_notice_accepted, False)
        page = reverse("web.views.appointments")
        response = self.client.get(page)
        self.assertEqual(response.status_code, 302)
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "You can't use the system until you accept the privacy notice.")
        # accept privacy notice
        form_data = {"privacy_notice_accepted": True}
        form = WorkerAcceptPrivacyNoticeForm(form_data)
        self.assertTrue(form.is_valid())
        page = reverse("web.views.accept_privacy_notice", kwargs={"pk": pn.id})
        response = self.client.post(page, data={**form_data})
        self.assertEqual(response.status_code, 302)
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertIn("Privacy notice accepted", messages)
        # check acceptance
        worker = Worker.objects.filter(id=self.staff_worker.id).first()
        self.assertEqual(worker.privacy_notice_accepted, True)
        page = reverse("web.views.appointments")
        response = self.client.get(page)
        self.assertEqual(response.status_code, 200)
        messages = list(get_messages(response.wsgi_request))
        worker = Worker.get_by_user(response.wsgi_request.user)
        self.assertEqual(worker.privacy_notice_accepted, True)
