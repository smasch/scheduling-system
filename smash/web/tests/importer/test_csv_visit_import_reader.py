# coding=utf-8

import logging
import os

from django.conf import settings
from django.test import TestCase
from django.utils import timezone

from web.importer import CsvVisitImportReader, MsgCounterHandler
from web.importer.csv_visit_import_reader import EtlException
from web.models import Appointment, Visit, StudySubject, AppointmentTypeLink, AppointmentType, VisitImportData, \
    Provenance
from web.tests.functions import get_resource_path, create_study_subject, create_appointment_type, create_location, \
    get_test_study, create_worker, create_study

logger = logging.getLogger(__name__)


class TestCsvVisitReader(TestCase):
    def setUp(self):
        appointment_type = create_appointment_type(code="SAMPLE_2")
        study = get_test_study()
        study.redcap_first_visit_number = 0
        study.save()
        settings.ETL_ROOT = os.path.dirname(get_resource_path('tns_vouchers_import.csv'))
        self.visit_import_data = VisitImportData.objects.create(study=get_test_study(),
                                                                appointment_type=appointment_type,
                                                                import_worker=create_worker(),
                                                                date_format="%Y%m%d",
                                                                csv_delimiter=";")
        self.warning_counter = MsgCounterHandler()
        logging.getLogger('').addHandler(self.warning_counter)

        create_study_subject(nd_number='cov-000111')
        create_study_subject(nd_number='cov-222333')
        create_study_subject(nd_number='cov-444444')

        create_location(name="Laboratoires réunis	23 Route de Diekirch	6555	Bollendorf-Pont")
        create_location(name="PickenDoheem")
        create_location(name="Ketterthill	1-3, rue de la Continentale 	4917	Bascharage")

    def tearDown(self):
        logging.getLogger('').removeHandler(self.warning_counter)

    def test_load_data(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'
        visits = CsvVisitImportReader(self.visit_import_data).load_data()
        self.assertEqual(3, len(visits))
        visit = Visit.objects.filter(id=visits[0].id)[0]
        self.assertEqual("cov-000111", visit.subject.nd_number)
        self.assertEqual(1, visit.visit_number)

        appointment = Appointment.objects.filter(visit=visit)[0]
        self.assertEqual("Laboratoires réunis	23 Route de Diekirch	6555	Bollendorf-Pont",
                         appointment.location.name)

        self.assertEqual(10, appointment.datetime_when.day)
        self.assertEqual(4, appointment.datetime_when.month)
        self.assertEqual(2020, appointment.datetime_when.year)

        self.assertEqual(0, self.get_warnings_count())

    def test_data_provenance_for_update_visit_load_data(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'

        Visit.objects.create(subject=StudySubject.objects.get(nd_number='cov-000111', study=get_test_study()),
                             visit_number=1,
                             datetime_begin=timezone.now(),
                             datetime_end=timezone.now())

        visit = CsvVisitImportReader(self.visit_import_data).load_data()[0]

        self.assertEqual(1, Provenance.objects.filter(modified_table=Visit._meta.db_table,
                                                      modified_table_id=visit.id,
                                                      modified_field='datetime_begin',
                                                      previous_value__contains=str(timezone.now().year)
                                                      ).count())

        self.assertEqual(0, Provenance.objects.filter(modified_table=Visit._meta.db_table,
                                                      modified_table_id=visit.id,
                                                      modified_field='datetime_begin',
                                                      previous_value__exact='').count())

    def test_data_provenance_for_update_appointment_load_data(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'

        old_visit = Visit.objects.create(
            subject=StudySubject.objects.get(nd_number='cov-000111', study=get_test_study()),
            visit_number=1,
            datetime_begin=timezone.now(),
            datetime_end=timezone.now())
        old_appointment = Appointment.objects.create(visit=old_visit, length=15, location=create_location())
        AppointmentTypeLink.objects.create(appointment=old_appointment,
                                           appointment_type=self.visit_import_data.appointment_type)

        CsvVisitImportReader(self.visit_import_data).load_data()

        self.assertEqual(1, Provenance.objects.filter(modified_table=Appointment._meta.db_table,
                                                      modified_table_id=old_appointment.id,
                                                      modified_field='length',
                                                      previous_value__exact='15'
                                                      ).count())

        self.assertEqual(0, Provenance.objects.filter(modified_table=Appointment._meta.db_table,
                                                      modified_table_id=old_appointment.id,
                                                      modified_field='length',
                                                      previous_value__exact='').count())

    def test_data_provenance_for_create_appointment_load_data(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'

        visit = CsvVisitImportReader(self.visit_import_data).load_data()[0]
        appointment = visit.appointment_set.all()[0]

        self.assertEqual(0, Provenance.objects.filter(modified_table=Appointment._meta.db_table,
                                                      modified_table_id=appointment.id,
                                                      modified_field='length',
                                                      previous_value__exact='15'
                                                      ).count())

        self.assertEqual(1, Provenance.objects.filter(modified_table=Appointment._meta.db_table,
                                                      modified_table_id=appointment.id,
                                                      modified_field='length',
                                                      previous_value__exact='').count())

    def test_data_provenance_for_create_visit_load_data(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'

        visit = CsvVisitImportReader(self.visit_import_data).load_data()[0]

        self.assertEqual(0, Provenance.objects.filter(modified_table=Visit._meta.db_table,
                                                      modified_table_id=visit.id,
                                                      modified_field='datetime_begin',
                                                      previous_value__contains=str(timezone.now().year)
                                                      ).count())

        self.assertEqual(1, Provenance.objects.filter(modified_table=Visit._meta.db_table,
                                                      modified_table_id=visit.id,
                                                      modified_field='datetime_begin',
                                                      previous_value__exact='').count())

    def test_load_data_with_existing_visit(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'
        visit = Visit.objects.create(subject=StudySubject.objects.filter(nd_number='cov-000111')[0],
                                     datetime_end=timezone.now(),
                                     datetime_begin=timezone.now(),
                                     visit_number=1)
        visits = CsvVisitImportReader(self.visit_import_data).load_data()
        visit = Visit.objects.filter(id=visits[0].id)[0]
        self.assertEqual("cov-000111", visit.subject.nd_number)

        appointment = Appointment.objects.filter(visit=visit)[0]
        self.assertEqual("Laboratoires réunis	23 Route de Diekirch	6555	Bollendorf-Pont",
                         appointment.location.name)

        self.assertEqual(10, appointment.datetime_when.day)
        self.assertEqual(4, appointment.datetime_when.month)
        self.assertEqual(2020, appointment.datetime_when.year)

        self.assertEqual(0, self.get_warnings_count())

    def test_load_data_with_existing_visit_and_appointment(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'
        visit = Visit.objects.create(subject=StudySubject.objects.filter(nd_number='cov-000111')[0],
                                     datetime_end=timezone.now(),
                                     datetime_begin=timezone.now(),
                                     visit_number=1)
        appointment = Appointment.objects.create(visit=visit, length=1, datetime_when=timezone.now(),
                                                 location=create_location())

        AppointmentTypeLink.objects.create(appointment_id=appointment.id,
                                           appointment_type=AppointmentType.objects.filter(code="SAMPLE_2")[0])

        visits = CsvVisitImportReader(self.visit_import_data).load_data()
        visit = Visit.objects.filter(id=visits[0].id)[0]
        self.assertEqual("cov-000111", visit.subject.nd_number)

        self.assertEqual(1, Appointment.objects.filter(visit=visit).count())

        appointment = Appointment.objects.filter(visit=visit)[0]
        self.assertEqual("Laboratoires réunis	23 Route de Diekirch	6555	Bollendorf-Pont",
                         appointment.location.name)

        self.assertEqual(10, appointment.datetime_when.day)
        self.assertEqual(4, appointment.datetime_when.month)
        self.assertEqual(2020, appointment.datetime_when.year)

        self.assertEqual(0, self.get_warnings_count())

    def test_load_data_with_visit_and_no_previous_visits(self):
        self.visit_import_data.filename = 'tns_vouchers_3_import.csv'

        visits = CsvVisitImportReader(self.visit_import_data).load_data()

        subject_visits = Visit.objects.filter(subject=StudySubject.objects.filter(nd_number='cov-000111')[0])

        self.assertEqual(3, len(subject_visits))

        visit = Visit.objects.filter(id=visits[0].id)[0]
        self.assertEqual("cov-000111", visit.subject.nd_number)

        appointment = Appointment.objects.filter(visit=visit)[0]
        self.assertEqual("Laboratoires réunis	23 Route de Diekirch	6555	Bollendorf-Pont",
                         appointment.location.name)

        self.assertEqual(10, appointment.datetime_when.day)
        self.assertEqual(4, appointment.datetime_when.month)
        self.assertEqual(2020, appointment.datetime_when.year)

        self.assertEqual(2, self.get_warnings_count())

    def test_load_data_with_no_subject(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'
        StudySubject.objects.filter(nd_number="cov-000111").delete()
        visits = CsvVisitImportReader(self.visit_import_data).load_data()
        self.assertEqual(3, len(visits))
        visit = Visit.objects.filter(id=visits[0].id)[0]
        self.assertEqual("cov-000111", visit.subject.nd_number)
        self.assertEqual(1, visit.visit_number)

        self.assertEqual(0, self.get_warnings_count())

    def test_dont_add_links_for_existing_appointments(self):
        self.visit_import_data.filename = 'tns_vouchers_import.csv'
        CsvVisitImportReader(self.visit_import_data).load_data()
        links = AppointmentTypeLink.objects.all().count()

        CsvVisitImportReader(self.visit_import_data).load_data()
        self.assertEqual(links, AppointmentTypeLink.objects.all().count())

        self.assertEqual(0, self.get_warnings_count())

    def test_get_study_subject_by_invalid_id_column(self):
        self.assertRaises(EtlException,
                          CsvVisitImportReader(self.visit_import_data).get_study_subject_id, {'invalid_id': 'x001'})

    def test_get_study_subject_by_valid_id_column(self):
        subject_id = CsvVisitImportReader(self.visit_import_data).get_study_subject_id({'donor_id': 'x001'})
        self.assertEqual('x001', subject_id)

    def test_get_visit_date_by_invalid_id_column(self):
        self.assertRaises(EtlException,
                          CsvVisitImportReader(self.visit_import_data).get_visit_date,
                          {'invalid_date': '19901010'})

    def test_get_visit_date_by_valid_id_column(self):
        visit_date = CsvVisitImportReader(self.visit_import_data).get_visit_date({'dateofvisit': '19901010'})
        self.assertEqual(1990, visit_date.year)

    def test_get_location_by_invalid_id_column(self):
        self.assertRaises(EtlException,
                          CsvVisitImportReader(self.visit_import_data).extract_location, {'invalid_id': 'x001'})

    def test_get_location_by_valid_id_column(self):
        location = CsvVisitImportReader(self.visit_import_data).extract_location({'adressofvisit': 'x001'})
        self.assertEqual('x001', location.name)
        self.assertEqual(1, self.get_warnings_count())

    def test_get_visit_number_by_invalid_id_column(self):
        self.assertRaises(EtlException,
                          CsvVisitImportReader(self.visit_import_data).get_visit_number, {'invalid_id': '1'})

    def test_get_visit_number_by_valid_id_column(self):
        self.visit_import_data.study.redcap_first_visit_number = 0
        self.visit_import_data.study.save()
        visit_number = CsvVisitImportReader(self.visit_import_data).get_visit_number({'visit_id': '1'})
        # normalized visit number is from 1
        self.assertEqual(2, visit_number)

    def test_get_visit_number_by_valid_id_column_started_from_one(self):
        self.visit_import_data.study.redcap_first_visit_number = 1
        self.visit_import_data.study.save()
        visit_number = CsvVisitImportReader(self.visit_import_data).get_visit_number({'visit_id': '1'})
        self.assertEqual(1, visit_number)

    def test_get_study_subject_by_id_for_existing_subject(self):
        reader = CsvVisitImportReader(self.visit_import_data)
        study_subject = StudySubject.objects.get(nd_number='cov-000111')
        study_subject_from_data = reader.get_study_subject_by_id('cov-000111')
        self.assertEqual(study_subject, study_subject_from_data)

    def test_get_study_subject_by_id_for_existing_subject_in_different_study(self):
        reader = CsvVisitImportReader(self.visit_import_data)

        study_subject = create_study_subject(nd_number='cov-09458', study=create_study())
        study_subject_from_data = reader.get_study_subject_by_id('cov-09458')
        self.assertNotEqual(study_subject, study_subject_from_data)

    def get_warnings_count(self):
        if "WARNING" in self.warning_counter.level2count:
            return self.warning_counter.level2count["WARNING"]
        else:
            return 0
