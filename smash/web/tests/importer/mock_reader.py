import logging
from typing import List

from web.importer.subject_import_reader import SubjectImportReader
from web.models import SubjectImportData, StudySubject

logger = logging.getLogger(__name__)


class MockReader(SubjectImportReader):
    def __init__(self, import_data: SubjectImportData, study_subjects: List[StudySubject]):
        super().__init__(import_data)
        self.study_subjects = study_subjects

    def load_data(self):
        return self.study_subjects
