# coding=utf-8

import logging
import os
import tempfile
from shutil import copyfile

from django.conf import settings
from django.core import mail
from django.test import TestCase
from django_cron.models import CronJobLog

from web.importer import SubjectImporterCronJob, VisitImporterCronJob
from web.models import Visit, VisitImportData, SubjectImportData, ConfigurationItem
from web.models.constants import DEFAULT_FROM_EMAIL
from web.tests.functions import get_resource_path, get_test_study, create_appointment_type, create_worker, \
    create_tns_column_mapping

logger = logging.getLogger(__name__)


class TestCronJobImporter(TestCase):

    def setUp(self):
        super().setUp()
        self.study = get_test_study()
        self.study.redcap_first_visit_number = 0
        self.study.save()
        self.visit_import_data = VisitImportData.objects.create(study=self.study,
                                                                appointment_type=create_appointment_type(),
                                                                import_worker=create_worker(),
                                                                csv_delimiter=';')
        self.subject_import_data = SubjectImportData.objects.create(study=self.study,
                                                                    import_worker=create_worker(),
                                                                    csv_delimiter=';')
        conf = ConfigurationItem.objects.get(type=DEFAULT_FROM_EMAIL)
        conf.value = "john.doe@uni.lu"
        conf.save()
        create_tns_column_mapping(self.subject_import_data)
        CronJobLog.objects.all().delete()

    def test_import_without_configuration(self):
        job = SubjectImporterCronJob()

        status = job.do()

        self.assertEqual("import file not defined", status)
        self.assertEqual(0, len(mail.outbox))

    def test_import(self):
        filename = get_resource_path('tns_subjects_import.csv')
        _, tmp = tempfile.mkstemp()
        copyfile(filename, tmp)

        settings.ETL_ROOT = os.path.dirname(tmp)

        self.subject_import_data.filename = os.path.basename(tmp)
        self.subject_import_data.save()

        status = SubjectImporterCronJob(self.study.id).do()

        self.assertEqual("import is successful", status)
        self.assertEqual(1, len(mail.outbox))

    def test_import_visit(self):
        filename = get_resource_path('tns_vouchers_3_import.csv')
        _, tmp = tempfile.mkstemp()
        copyfile(filename, tmp)

        settings.ETL_ROOT = os.path.dirname(tmp)

        self.visit_import_data.filename = os.path.basename(tmp)
        self.visit_import_data.save()

        job = VisitImporterCronJob(study_id=self.study.id)

        status = job.do()

        self.assertEqual("import is successful", status)
        self.assertEqual(1, len(mail.outbox))
        self.assertEqual(3, Visit.objects.filter(subject__study=self.study).count())
