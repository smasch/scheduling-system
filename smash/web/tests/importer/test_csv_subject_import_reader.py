# coding=utf-8

import logging

from django.test import TestCase

from web.importer import CsvSubjectImportReader, MsgCounterHandler
from web.models import SubjectImportData, EtlColumnMapping, StudySubject, Country
from web.models.constants import COUNTRY_AFGHANISTAN_ID, CUSTOM_FIELD_TYPE_TEXT
from web.models.custom_data import CustomStudySubjectField
from web.tests.functions import get_resource_path, get_test_study, create_tns_column_mapping, create_location, \
    get_control_subject_type, get_patient_subject_type

logger = logging.getLogger(__name__)


class TestCsvReader(TestCase):
    def setUp(self):
        self.subject_import_data = SubjectImportData.objects.create(study=get_test_study(), date_format="%d-%m-%Y")
        EtlColumnMapping.objects.create(etl_data=self.subject_import_data,
                                        column_name="screening_number",
                                        csv_column_name="participant_id",
                                        table_name=StudySubject._meta.db_table)
        self.warning_counter = MsgCounterHandler()
        logging.getLogger('').addHandler(self.warning_counter)

    def tearDown(self):
        logging.getLogger('').removeHandler(self.warning_counter)

    def test_load_data(self):
        self.subject_import_data.filename = get_resource_path('import.csv')
        study_subjects = CsvSubjectImportReader(self.subject_import_data).load_data()

        self.assertEqual(1, len(study_subjects))
        study_subject = study_subjects[0]
        self.assertEqual("Piotr", study_subject.subject.first_name)
        self.assertEqual("Gawron", study_subject.subject.last_name)
        self.assertEqual("Cov-000001", study_subject.screening_number)
        self.assertEqual("Cov-000001", study_subject.nd_number)

        self.assertEqual(1, study_subject.subject.date_born.day)
        self.assertEqual(2, study_subject.subject.date_born.month)
        self.assertEqual(2020, study_subject.subject.date_born.year)

        self.assertIsNotNone(study_subject.study)

    def test_load_custom_field(self):
        CustomStudySubjectField.objects.create(study=get_test_study(), name='my custom field',
                                               type=CUSTOM_FIELD_TYPE_TEXT)

        self.subject_import_data.filename = get_resource_path('import_custom_field.csv')
        reader = CsvSubjectImportReader(self.subject_import_data)
        study_subjects = reader.load_data()

        self.assertEqual(3, len(study_subjects))
        self.assertEqual(len(reader.get_custom_fields(study_subjects[0])), 1)
        self.assertEqual(reader.get_custom_fields(study_subjects[0])[0].value, "Bla")
        self.assertEqual(reader.get_custom_fields(study_subjects[1])[0].value, "BUI")
        self.assertEqual(reader.get_custom_fields(study_subjects[2])[0].value, "")

    def test_load_problematic_dates(self):
        self.subject_import_data.filename = get_resource_path('import_date_of_birth.csv')
        study_subjects = CsvSubjectImportReader(self.subject_import_data).load_data()

        self.assertEqual(3, len(study_subjects))
        self.assertIsNone(study_subjects[0].subject.date_born)
        self.assertIsNone(study_subjects[1].subject.date_born)
        self.assertIsNone(study_subjects[2].subject.date_born)

    def test_load_language(self):
        self.subject_import_data.filename = get_resource_path('import_language.csv')
        study_subjects = CsvSubjectImportReader(self.subject_import_data).load_data()

        self.assertEqual(3, len(study_subjects))
        self.assertIsNotNone(study_subjects[0].subject.default_written_communication_language)
        self.assertIsNotNone(study_subjects[1].subject.default_written_communication_language)
        self.assertIsNone(study_subjects[2].subject.default_written_communication_language)

    def test_load_type(self):
        self.subject_import_data.filename = get_resource_path('import_type.csv')
        study_subjects = CsvSubjectImportReader(self.subject_import_data).load_data()

        self.assertEqual(4, len(study_subjects))
        self.assertEqual(get_patient_subject_type(), study_subjects[0].type)
        self.assertEqual(get_control_subject_type(), study_subjects[1].type)
        self.assertIsNotNone(study_subjects[2].type)
        self.assertIsNotNone(study_subjects[3].type)

    def test_load_data_for_tns(self):
        self.subject_import_data = SubjectImportData.objects.create(study=get_test_study(),
                                                                    date_format="%d/%m/%Y",
                                                                    csv_delimiter=";",
                                                                    filename=get_resource_path(
                                                                        'tns_subjects_import.csv'))
        create_tns_column_mapping(self.subject_import_data)
        study_subjects = CsvSubjectImportReader(self.subject_import_data).load_data()
        self.assertEqual(3, len(study_subjects))
        study_subject = study_subjects[1]
        self.assertEqual("John2", study_subject.subject.first_name)
        self.assertEqual("Doe2", study_subject.subject.last_name)
        self.assertEqual("cov-222333", study_subject.screening_number)
        self.assertEqual("cov-222333", study_subject.nd_number)
        self.assertEqual("621000000", study_subject.subject.phone_number)
        self.assertEqual("john.doe@neverland.lu", study_subject.subject.email)
        self.assertEqual("5, avenue blabla", study_subject.subject.address)
        self.assertEqual("9940", study_subject.subject.postal_code)
        self.assertEqual("Belval", study_subject.subject.city)

        self.assertTrue("Gregory House2" in study_subject.comments)

        self.assertEqual(1, study_subject.subject.date_born.day)
        self.assertEqual(2, study_subject.subject.date_born.month)
        self.assertEqual(1977, study_subject.subject.date_born.year)

        self.assertIsNotNone(study_subject.study)
        self.assertEqual(0, self.get_warnings_count())

    def get_warnings_count(self):
        if "WARNING" in self.warning_counter.level2count:
            return self.warning_counter.level2count["WARNING"]
        else:
            return 0

    def test_load_default_country(self):
        self.subject_import_data.filename = get_resource_path('import.csv')
        self.subject_import_data.country = Country.objects.get(pk=COUNTRY_AFGHANISTAN_ID)
        study_subjects = CsvSubjectImportReader(self.subject_import_data).load_data()

        self.assertEqual(study_subjects[0].subject.country, Country.objects.get(pk=COUNTRY_AFGHANISTAN_ID))

    def test_load_default_location(self):
        self.subject_import_data.filename = get_resource_path('import.csv')
        self.subject_import_data.location = create_location()
        study_subjects = CsvSubjectImportReader(self.subject_import_data).load_data()

        self.assertEqual(study_subjects[0].default_location, self.subject_import_data.location)

    def test_load_country(self):
        self.subject_import_data.filename = get_resource_path('import_with_country.csv')
        study_subjects = CsvSubjectImportReader(self.subject_import_data).load_data()

        self.assertEqual(study_subjects[0].subject.country.name, "New country")

    def test_load_unknown_column(self):
        self.subject_import_data.filename = get_resource_path('import_with_unknown.csv')
        CsvSubjectImportReader(self.subject_import_data).load_data()

        self.assertEqual(1, self.get_warnings_count())
