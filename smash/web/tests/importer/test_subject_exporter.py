# coding=utf-8

import logging

from django.test import TestCase
from django.utils.timezone import now

from web.tests.functions import create_study_subject, get_test_study, create_language
from ...importer.csv_subject_exporter import CsvSubjectExporter
from ...models import Subject, StudySubject
from ...models.constants import CUSTOM_FIELD_TYPE_TEXT
from ...models.custom_data import CustomStudySubjectField
from ...models.custom_data.custom_study_subject_field import get_study_subject_field_id
from ...models.etl.etl_export_data import field_can_be_exported
from ...models.etl.subject_export import SubjectExportData

logger = logging.getLogger(__name__)


class TestExporter(TestCase):
    def setUp(self):
        self.study = get_test_study()
        self.visit_export_data: SubjectExportData = SubjectExportData.objects.create(
            study=self.study, filename="test.csv"
        )
        self.visit_export_data.csv_delimiter = ","

        for field in Subject._meta.get_fields():
            if field_can_be_exported(field):
                field_id = Subject._meta.db_table + "-" + field.name
                value = field_id
                self.visit_export_data.set_column_mapping(Subject, field.name, value)

        for field in StudySubject._meta.get_fields():
            if field_can_be_exported(field):
                field_id = StudySubject._meta.db_table + " - " + field.name
                value = field_id
                self.visit_export_data.set_column_mapping(StudySubject, field.name, value)

        for field_type in CustomStudySubjectField.objects.filter(study=self.study):
            field_id = get_study_subject_field_id(field_type.id)
            self.visit_export_data.set_column_mapping(CustomStudySubjectField, field_id, field_id)

    def test_export(self):
        study_subject = create_study_subject()

        exporter = CsvSubjectExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue(study_subject.nd_number in data)

    def test_export_type_as_id(self):
        study_subject = create_study_subject()

        self.visit_export_data.export_object_as_id = True
        exporter = CsvSubjectExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertFalse(study_subject.type.name in data)
            self.assertTrue(str(study_subject.type_id) in data)

    def test_export_type_as_string(self):
        study_subject = create_study_subject()

        self.visit_export_data.export_object_as_id = False
        exporter = CsvSubjectExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue(study_subject.type.name in data)

    def test_export_custom_field(self):
        field = CustomStudySubjectField.objects.create(
            study=get_test_study(), name="my custom field", type=CUSTOM_FIELD_TYPE_TEXT
        )

        self.visit_export_data.set_column_mapping(
            CustomStudySubjectField, get_study_subject_field_id(field), "my_custom_field"
        )

        study_subject = create_study_subject()
        study_subject.set_custom_data_value(field, "blah-blah")

        self.visit_export_data.export_object_as_id = True
        exporter = CsvSubjectExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue("my_custom_field" in data)
            self.assertTrue("blah-blah" in data)

    def test_export_languages(self):
        study_subject = create_study_subject()
        language = create_language(name="Polish")
        study_subject.subject.languages.set([language])
        study_subject.subject.save()

        self.visit_export_data.set_column_mapping(Subject, "languages", "languages")
        self.visit_export_data.export_object_as_id = False

        exporter = CsvSubjectExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue("Polish" in data)

    def test_export_date_format(self):
        study_subject = create_study_subject()
        study_subject.subject.date_born = now()
        study_subject.subject.save()

        self.visit_export_data.date_format = "%Y/%m/%d"
        self.visit_export_data.export_object_as_id = False

        exporter = CsvSubjectExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue(study_subject.subject.date_born.strftime(self.visit_export_data.date_format) in data)
