# coding=utf-8

import logging
from typing import Optional

from django.test import TestCase
from django.utils import timezone

from web.importer import Importer, CsvSubjectImportReader
from web.models import Subject, StudySubject, Provenance, SubjectImportData, EtlColumnMapping
from web.tests.functions import create_study_subject, get_test_study, get_control_subject_type, get_resource_path
from .mock_reader import MockReader
from ...models.constants import CUSTOM_FIELD_TYPE_TEXT
from ...models.custom_data import CustomStudySubjectField

logger = logging.getLogger(__name__)


class TestImporter(TestCase):

    def setUp(self):
        self.study = get_test_study()
        self.visit_import_data = SubjectImportData.objects.create(study=self.study)

    def test_import_new_subject(self):
        study_subjects = []
        subject = Subject()
        subject.first_name = "Piotr"
        subject.last_name = "Gawron"
        study_subject = StudySubject()
        study_subject.screening_number = "Cov-123456"
        study_subject.subject = subject
        study_subject.study = self.study
        study_subjects.append(study_subject)

        importer = Importer(reader=MockReader(self.visit_import_data, study_subjects))
        subject_counter = Subject.objects.count()
        study_subject_counter = StudySubject.objects.count()
        provenance_counter = Provenance.objects.count()
        importer.execute()

        self.assertEqual(subject_counter + 1, Subject.objects.count())
        self.assertEqual(study_subject_counter + 1, StudySubject.objects.count())
        self.assertNotEqual(provenance_counter, Provenance.objects.count())

        self.assertEqual(1, importer.added_count)
        self.assertEqual(0, importer.problematic_count)
        self.assertEqual(0, importer.warning_count)

    def test_import_invalid(self):
        study_subjects = []
        study_subject = StudySubject()
        study_subject.screening_number = "Cov-123456"
        study_subject.study = self.study
        study_subjects.append(study_subject)

        importer = Importer(reader=MockReader(self.visit_import_data, study_subjects))
        study_subject_counter = StudySubject.objects.count()
        importer.execute()

        self.assertEqual(study_subject_counter, StudySubject.objects.count())

        self.assertEqual(0, importer.added_count)
        self.assertEqual(1, importer.problematic_count)

    def test_import_no_study(self):
        study_subjects = []
        study_subject = StudySubject()
        study_subject.screening_number = "Cov-123456"
        study_subjects.append(study_subject)

        importer = Importer(reader=MockReader(self.visit_import_data, study_subjects))
        study_subject_counter = StudySubject.objects.count()
        importer.execute()

        self.assertEqual(study_subject_counter, StudySubject.objects.count())

        self.assertEqual(0, importer.added_count)
        self.assertEqual(1, importer.problematic_count)

    def test_merge_without_type(self):
        existing_study_subject = create_study_subject()
        study_subjects = []
        study_subject = self.create_valid_study_subject(existing_study_subject)
        study_subject.type = None
        study_subjects.append(study_subject)

        importer = Importer(reader=MockReader(self.visit_import_data, study_subjects))
        study_subject_counter = StudySubject.objects.count()
        importer.execute()

        self.assertEqual(study_subject_counter, StudySubject.objects.count())

        self.assertEqual(1, importer.merged_count)
        self.assertEqual(0, importer.problematic_count)

    def test_import_without_type(self):
        study_subjects = []
        study_subject = self.create_valid_study_subject(None)
        study_subject.type = None
        study_subjects.append(study_subject)

        importer = Importer(reader=MockReader(self.visit_import_data, study_subjects))
        study_subject_counter = StudySubject.objects.count()
        importer.execute()

        self.assertEqual(study_subject_counter + 1, StudySubject.objects.count())

        self.assertEqual(1, importer.added_count)
        self.assertEqual(0, importer.problematic_count)
        self.assertIsNotNone(StudySubject.objects.filter(nd_number=study_subject.nd_number).first().type)

    def test_import_merge_subject(self):
        existing_study_subject = create_study_subject()
        study_subjects = []
        study_subject = self.create_valid_study_subject(existing_study_subject)
        study_subjects.append(study_subject)

        importer = Importer(reader=MockReader(self.visit_import_data, study_subjects))
        provenance_counter = Provenance.objects.count()
        subject_counter = Subject.objects.count()
        study_subject_counter = StudySubject.objects.count()
        importer.execute()

        self.assertEqual(subject_counter, Subject.objects.count())
        self.assertEqual(study_subject_counter, StudySubject.objects.count())
        self.assertNotEqual(provenance_counter, Provenance.objects.count())

        self.assertEqual(0, importer.added_count)
        self.assertEqual(1, importer.merged_count)
        self.assertEqual(0, importer.problematic_count)
        self.assertEqual(0, importer.warning_count)

        existing_study_subject = StudySubject.objects.filter(id=existing_study_subject.id)[0]
        self.assertEqual(existing_study_subject.subject.first_name, study_subject.subject.first_name)
        self.assertEqual(existing_study_subject.subject.last_name, study_subject.subject.last_name)
        self.assertEqual(existing_study_subject.subject.date_born.strftime("%Y-%m-%d"),
                         study_subject.subject.date_born.strftime("%Y-%m-%d"))

    def create_valid_study_subject(self, existing_study_subject: Optional[StudySubject]):
        subject = Subject()
        subject.first_name = "XYZ"
        subject.last_name = "AAA"
        subject.date_born = timezone.now()
        study_subject = StudySubject()
        study_subject.type = get_control_subject_type()
        study_subject.study = self.study
        study_subject.subject = subject
        if existing_study_subject is None:
            study_subject.screening_number = "Cov-12345"
            study_subject.nd_number = "Cov-12345"
        else:
            study_subject.screening_number = existing_study_subject.screening_number
            study_subject.nd_number = existing_study_subject.nd_number
        return study_subject

    def test_load_with_merge_custom_field(self):
        study_subject = create_study_subject(nd_number='Cov-000001')
        study_subject.screening_number = study_subject.nd_number
        study_subject.save()

        self.test_load_custom_field()

    def test_load_custom_field(self):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), name='my custom field',
                                                       type=CUSTOM_FIELD_TYPE_TEXT)

        subject_import_data = SubjectImportData.objects.create(study=get_test_study(), date_format="%d-%m-%Y")
        EtlColumnMapping.objects.create(etl_data=subject_import_data,
                                        column_name="screening_number",
                                        csv_column_name="participant_id",
                                        table_name=StudySubject._meta.db_table)
        EtlColumnMapping.objects.create(etl_data=subject_import_data,
                                        column_name="nd_number",
                                        csv_column_name="participant_id",
                                        table_name=StudySubject._meta.db_table)

        subject_import_data.filename = get_resource_path('import_custom_field.csv')
        reader = CsvSubjectImportReader(subject_import_data)

        importer = Importer(reader=reader)
        importer.execute()

        self.assertEqual(3, StudySubject.objects.count())
        study_subject = StudySubject.objects.filter(nd_number='Cov-000001').first()
        self.assertEqual("Bla", study_subject.get_custom_data_value(field).value)
