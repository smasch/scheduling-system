# coding=utf-8

import logging

from django.test import TestCase
from django.utils.timezone import now

from web.tests.functions import get_test_study, create_appointment, create_visit, create_study_subject
from ...importer.csv_visit_exporter import CsvVisitExporter
from ...models import Appointment, Visit
from ...models.etl.etl_export_data import field_can_be_exported
from ...models.etl.visit_export import VisitExportData

logger = logging.getLogger(__name__)


class TestVisitExporter(TestCase):
    def setUp(self):
        self.study = get_test_study()
        self.visit_export_data: VisitExportData = VisitExportData.objects.create(
            study=self.study, filename="visit_test.csv"
        )
        self.visit_export_data.csv_delimiter = ","

        for field in Appointment._meta.get_fields():
            if field_can_be_exported(field):
                field_id = Appointment._meta.db_table + "-" + field.name
                value = field_id
                self.visit_export_data.set_column_mapping(Appointment, field.name, value)

        for field in Visit._meta.get_fields():
            if field_can_be_exported(field):
                field_id = Visit._meta.db_table + " - " + field.name
                value = field_id
                self.visit_export_data.set_column_mapping(Visit, field.name, value)

    def test_export(self):
        appointment = create_appointment(visit=create_visit())
        appointment.comment = "comm_ent"
        appointment.save()

        exporter = CsvVisitExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue(appointment.comment in data)

    def test_export_subject_number(self):
        create_appointment(visit=create_visit(subject=create_study_subject(nd_number="ND0123")))

        exporter = CsvVisitExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue("ND0123" in data)

    def test_export_general_appointment(self):
        appointment = create_appointment()
        appointment.visit = None
        appointment.comment = "comm_ent"
        appointment.save()

        exporter = CsvVisitExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue(appointment.comment in data)

    def test_export_visit_number(self):
        appointment = create_appointment()
        appointment.visit.visit_number = 1
        appointment.visit.save()

        exporter = CsvVisitExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue("visit_number" in data)
            self.assertTrue('"' + str(appointment.visit.visit_number) + '"' in data)

    def test_export_when(self):
        appointment = create_appointment()
        appointment.datetime_when = now()
        appointment.save()

        exporter = CsvVisitExporter(self.visit_export_data)
        exporter.execute()

        with open(self.visit_export_data.get_absolute_file_path(), "r", encoding="utf-8") as file:
            data = file.read()
            self.assertTrue(appointment.datetime_when.strftime(self.visit_export_data.date_format) in data)
