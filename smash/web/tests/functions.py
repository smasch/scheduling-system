import datetime
import logging
import os
from typing import Union

from django.contrib.auth.models import Permission
from django.contrib.auth import get_user_model
from django.utils.timezone import make_aware, is_aware
from django.conf import settings

from web.models import (
    Location,
    AppointmentType,
    StudySubject,
    Worker,
    Visit,
    Appointment,
    ConfigurationItem,
    Language,
    ContactAttempt,
    FlyingTeam,
    Availability,
    Subject,
    Study,
    StudyColumns,
    StudyNotificationParameters,
    VoucherType,
    VoucherTypePrice,
    Voucher,
    Room,
    Item,
    WorkerStudyRole,
    StudyRedCapColumns,
    EtlColumnMapping,
    SubjectImportData,
    SubjectType,
)
from web.models.constants import (
    REDCAP_TOKEN_CONFIGURATION_TYPE,
    REDCAP_BASE_URL_CONFIGURATION_TYPE,
    SEX_CHOICES_MALE,
    CONTACT_TYPES_PHONE,
    MONDAY_AS_DAY_OF_WEEK,
    COUNTRY_AFGHANISTAN_ID,
    VOUCHER_STATUS_NEW,
    GLOBAL_STUDY_ID,
    DEFAULT_LOCALE_NAME,
)
from web.models.worker_study_role import ROLE_CHOICES_DOCTOR, WORKER_VOUCHER_PARTNER
from web.redcap_connector import RedcapSubject
from web.views.notifications import get_today_midnight_date

logger = logging.getLogger(__name__)


def create_get_suffix(params):
    result = "?"
    for key in params:
        result += key + "=" + str(params[key]) + "&"
    return result


def create_location(name="test"):
    return Location.objects.create(name=name)


def create_voucher_type():
    return VoucherType.objects.create(code="X", study=get_test_study())


def create_voucher_type_price():
    return VoucherTypePrice.objects.create(
        voucher_type=create_voucher_type(),
        price=12.34,
        start_date=get_today_midnight_date(),
        end_date=get_today_midnight_date(),
    )


def create_empty_study_columns():
    study_columns = StudyColumns.objects.create(
        postponed=False,
        datetime_contact_reminder=False,
        type=False,
        default_location=False,
        flying_team=False,
        screening_number=False,
        nd_number=False,
        comments=False,
        referral=False,
        information_sent=False,
        resigned=False,
        endpoint_reached=False,
        excluded=False,
        resign_reason=False,
    )

    return study_columns


def create_study(name="test"):
    study_columns = StudyColumns.objects.create()
    notification_parameters = StudyNotificationParameters.objects.create()
    redcap_columns = StudyRedCapColumns.objects.create()
    return Study.objects.create(
        name=name,
        columns=study_columns,
        notification_parameters=notification_parameters,
        redcap_columns=redcap_columns,
    )


TEST_ID_COUNTER = 0


def get_test_id():
    global TEST_ID_COUNTER  # pylint: disable=global-statement
    result = str(TEST_ID_COUNTER)
    TEST_ID_COUNTER += 1
    return result


def create_voucher(study_subject=None, partner=None, worker=None):
    if study_subject is None:
        study_subject = create_study_subject()
    if partner is None:
        partner = create_voucher_partner()
    if worker is None:
        worker = create_worker()
    number = str(get_test_id())
    return Voucher.objects.create(
        number=number,
        study_subject=study_subject,
        issue_date=get_today_midnight_date(),
        expiry_date=get_today_midnight_date(),
        voucher_type=create_voucher_type(),
        usage_partner=partner,
        issue_worker=worker,
        status=VOUCHER_STATUS_NEW,
    )


def create_empty_notification_parameters():
    return StudyNotificationParameters.objects.create(
        exceeded_visits_visible=False,
        unfinished_visits_visible=False,
        approaching_visits_without_appointments_visible=False,
        unfinished_appointments_visible=False,
        visits_with_missing_appointments_visible=False,
        subject_no_visits_visible=False,
        approaching_visits_for_mail_contact_visible=False,
        subject_require_contact_visible=False,
        missing_redcap_subject_visible=False,
        inconsistent_redcap_subject_visible=False,
        subject_voucher_expiry_visible=False,
    )


def create_full_notification_parameters():
    return StudyNotificationParameters.objects.create(
        exceeded_visits_visible=True,
        unfinished_visits_visible=True,
        approaching_visits_without_appointments_visible=True,
        unfinished_appointments_visible=True,
        visits_with_missing_appointments_visible=True,
        subject_no_visits_visible=True,
        approaching_visits_for_mail_contact_visible=True,
        subject_require_contact_visible=True,
        missing_redcap_subject_visible=True,
        inconsistent_redcap_subject_visible=True,
        subject_voucher_expiry_visible=True,
    )


def create_empty_study(name="test"):
    study_columns = create_empty_study_columns()
    notification_parameters = create_empty_notification_parameters()
    result = create_study(name)
    result.columns = study_columns
    result.notification_parameters = notification_parameters
    result.save()
    return result


def get_test_location():
    locations = Location.objects.filter(name="test")
    if len(locations) > 0:
        return locations[0]
    else:
        return create_location()


def get_test_study() -> Study:
    studies = Study.objects.filter(name="test-study")
    if len(studies) > 0:
        return studies[0]
    else:
        return create_study("test-study")


def create_appointment_type(code="C", default_duration=10, description="test"):
    return AppointmentType.objects.create(
        code=code,
        default_duration=default_duration,
        description=description,
    )


def create_contact_attempt(subject=None, worker=None):
    if subject is None:
        subject = create_study_subject()
    if worker is None:
        worker = create_worker()

    return ContactAttempt.objects.create(
        subject=subject,
        worker=worker,
        type=CONTACT_TYPES_PHONE,
        datetime_when=get_today_midnight_date(),
        success=True,
        comment="Successful contact attempt",
    )


def create_subject():
    return Subject.objects.create(
        first_name="Piotr",
        last_name="Gawron",
        sex=SEX_CHOICES_MALE,
        country_id=COUNTRY_AFGHANISTAN_ID,
    )


def create_study_subject(
    subject_id: int = 1,
    subject: Subject = None,
    nd_number: str = "ND0001",
    study: Study = None,
) -> StudySubject:
    if study is None:
        study = get_test_study()
    if subject is None:
        subject = create_subject()
    study_subject = StudySubject.objects.create(
        default_location=get_test_location(),
        type=get_control_subject_type(),
        screening_number="piotr's number" + str(subject_id),
        study=study,
        subject=subject,
    )
    if (
        nd_number is not None
    ):  # null value in column "nd_number" violates not-null constraint
        study_subject.nd_number = nd_number
        study_subject.save()

    return study_subject


def get_control_subject_type() -> SubjectType:
    return SubjectType.objects.filter(name="CONTROL").first()


def get_patient_subject_type() -> SubjectType:
    return SubjectType.objects.filter(name="PATIENT").first()


def create_study_subject_with_multiple_screening_numbers(
    subject_id=1, subject=None, screening_number=None
):
    if subject is None:
        subject = create_subject()

    if screening_number is None:
        screening_number = f"E-00{subject_id}; L-00{subject_id}"
    return StudySubject.objects.create(
        default_location=get_test_location(),
        type=get_control_subject_type(),
        screening_number=screening_number,
        study=get_test_study(),
        subject=subject,
    )


def create_red_cap_subject():
    result = RedcapSubject()
    result.nd_number = "123"
    return result


def create_user(
    username: str = None, password: str = None, email: str = "jacob@bla"
) -> get_user_model():
    if username is None:
        username = "piotr"
    if password is None:
        password = "top_secret"
    user = get_user_model().objects.create_user(
        username=username, email=email, password=password
    )

    create_worker(user)
    return user


def add_permissions_to_worker(worker, codenames):
    roles = WorkerStudyRole.objects.filter(worker=worker, study_id=GLOBAL_STUDY_ID)
    perms = Permission.objects.filter(codename__in=codenames).all()
    roles[0].permissions.set(perms)
    roles[0].save()


def create_worker(user=None, with_test_location=False):
    worker = Worker.objects.create(
        first_name="piotr",
        last_name="gawron",
        email="jacob@bla.com",
        user=user,
        specialization="spec",
        unit="LCSB",
        phone_number="0123456789",
    )
    if with_test_location:
        worker.locations.set([get_test_location()])
        worker.save()
    WorkerStudyRole.objects.create(
        worker=worker, study_id=GLOBAL_STUDY_ID, name=ROLE_CHOICES_DOCTOR
    )
    return worker


def create_voucher_partner():
    worker = Worker.objects.create(
        first_name="piotr",
        last_name="gawron",
        email="jacob@bla.com",
        specialization="spec",
        unit="LCSB",
        phone_number="0123456789",
    )
    WorkerStudyRole.objects.create(
        worker=worker, study_id=GLOBAL_STUDY_ID, name=WORKER_VOUCHER_PARTNER
    )
    return worker


def create_availability(
    worker=None,
    available_from=None,
    available_till=None,
    day_number=MONDAY_AS_DAY_OF_WEEK,
):
    if available_from is None:
        available_from = "8:00"
    if available_till is None:
        available_till = "18:00"

    if worker is None:
        worker = create_worker()
    availability = Availability.objects.create(
        person=worker,
        day_number=day_number,
        available_from=available_from,
        available_till=available_till,
    )
    return availability


def create_visit(
    subject: StudySubject = None, datetime_begin=None, datetime_end=None
) -> Visit:
    if subject is None:
        subject = create_study_subject()
    if datetime_begin is None:
        datetime_begin = get_today_midnight_date() + datetime.timedelta(days=-31)
    if datetime_end is None:
        datetime_end = get_today_midnight_date() + datetime.timedelta(days=31)
    return Visit.objects.create(
        datetime_begin=datetime_begin,
        datetime_end=datetime_end,
        subject=subject,
        is_finished=False,
    )


def create_appointment(visit=None, when=None, length=30) -> Appointment:
    if visit is None:
        visit = create_visit()
    # if when is None:
    #     when = get_today_midnight_date()

    if isinstance(when, str):
        when_datetime = datetime.datetime.strptime(when, "YYYY-MM-DD")
        if not is_aware(when_datetime):
            make_aware(when_datetime)
    else:
        when_datetime = when

    return Appointment.objects.create(
        visit=visit,
        length=length,
        location=get_test_location(),
        status=Appointment.APPOINTMENT_STATUS_SCHEDULED,
        datetime_when=when_datetime,
    )


def create_appointment_without_visit(when=None, length=30):
    return Appointment.objects.create(
        length=length,
        location=get_test_location(),
        status=Appointment.APPOINTMENT_STATUS_SCHEDULED,
        datetime_when=when,
    )


def create_configuration_item():
    item = ConfigurationItem.objects.create()
    item.type = "TEST"
    item.value = "xxx"
    item.name = "yyy"
    item.save()
    return item


def create_flying_team(place=None):
    if place is None:
        place = "CHEM "
    result = FlyingTeam.objects.create(place=place)
    return result


def create_item(name="Test item", is_fixed=False, disposable=False):
    item = Item(name=name, is_fixed=is_fixed, disposable=disposable)
    item.save()
    return item


def create_room(
    owner="Test owner",
    city="Test city",
    address="Test address",
    equipment=None,
    floor=1,
    is_vehicle=False,
    room_number=1,
):
    if equipment is None:
        equipment = []
    room = Room(
        owner=owner,
        city=city,
        address=address,
        floor=floor,
        is_vehicle=is_vehicle,
        room_number=room_number,
    )
    room.save()
    room.equipment.set(equipment)  # Cannot be made in constructor/with single save
    room.save()
    return room


def create_language(name="French", locale=DEFAULT_LOCALE_NAME) -> Language:
    language = Language(name=name, locale=locale)
    language.save()
    return language


def get_resource_path(filename):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", filename)


def format_form_field(value):
    if isinstance(value, datetime.date):
        return value.strftime("%Y-%m-%d")
    elif isinstance(value, datetime.datetime):
        return value.strftime("%Y-%m-%d %H:%M")
    elif value is None:
        return ""
    else:
        return value


def prepare_test_redcap_connection():
    Language.objects.create(name="Finnish").save()
    Language.objects.create(name="Italian").save()
    token_item = ConfigurationItem.objects.filter(type=REDCAP_TOKEN_CONFIGURATION_TYPE)[
        0
    ]
    # noinspection SpellCheckingInspection
    token_item.value = settings.REDCAP_TEST_API_TOKEN
    token_item.save()
    url_item = ConfigurationItem.objects.filter(
        type=REDCAP_BASE_URL_CONFIGURATION_TYPE
    )[0]
    url_item.value = settings.REDCAP_TEST_URL
    url_item.save()


def datetimeify_date(date: Union[datetime.date, str, bytes]) -> datetime.datetime:
    if isinstance(
        date, datetime.date
    ):  # If it's date, then just make sure that timezone support is there
        if is_aware(date):
            return date
        else:
            return make_aware(date)
    if isinstance(date, bytes):  # If it's bytes, then convert to string and carry on...
        date = date.decode("utf8")
    if isinstance(
        date, str
    ):  # If it's string, convert to datetime with timezone support
        return make_aware(datetime.datetime.strptime(date, "%Y-%m-%d"))

    actual_type = str(type(date))
    raise TypeError(
        f"Date should be either a subclass of 'datetime.date', string or bytes! But is: {actual_type} instead"
    )


def create_tns_column_mapping(subject_import_data: SubjectImportData):
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="nd_number",
        csv_column_name="donor_id",
        table_name=StudySubject._meta.db_table,
    )
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="comments",
        csv_column_name="treatingphysician",
        table_name=StudySubject._meta.db_table,
    )
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="first_name",
        csv_column_name="firstname",
        table_name=Subject._meta.db_table,
    )
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="first_name",
        csv_column_name="sig_firstname",
        table_name=Subject._meta.db_table,
    )
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="last_name",
        csv_column_name="lastname",
        table_name=Subject._meta.db_table,
    )
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="last_name",
        csv_column_name="sig_lastname",
        table_name=Subject._meta.db_table,
    )
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="phone_number",
        csv_column_name="phonenr",
        table_name=Subject._meta.db_table,
    )
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="date_born",
        csv_column_name="dateofbirth",
        table_name=Subject._meta.db_table,
    )
    EtlColumnMapping.objects.create(
        etl_data=subject_import_data,
        column_name="next_of_kin_name",
        csv_column_name="representative",
        table_name=Subject._meta.db_table,
    )
