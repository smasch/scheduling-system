# coding=utf-8

from django.test import TestCase
from mockito import when, verify, ARGS
from phonenumbers import PhoneNumber
from two_factor.plugins.phonenumber.models import PhoneDevice

from web.nexmo_gateway import Nexmo


class TestNexmo(TestCase):

    @staticmethod
    def test_send_sms():
        nexmo = Nexmo()
        when(nexmo.client).send_message(*ARGS).thenReturn(True)
        device = PhoneDevice()
        device.number = PhoneNumber()
        nexmo.send_sms(device=device, token="xxy")
        verify(nexmo.client).send_message(*ARGS)
