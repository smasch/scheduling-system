from unittest import TestCase

from web.management.commands.holidays import get_easter_sunday_date, get_ascension_day, get_pentecost_day


class TestHolidays(TestCase):
    def test_get_easter_sunday_date(self):
        easter_sunday = get_easter_sunday_date(2010)
        self.assertEqual(4, easter_sunday.month)
        self.assertEqual(4, easter_sunday.day)
        easter_sunday = get_easter_sunday_date(2017)
        self.assertEqual(4, easter_sunday.month)
        self.assertEqual(16, easter_sunday.day)

    def test_get_ascension_day(self):
        expected_values = [
            (2018, 5, 10),
            (2017, 5, 25),
            (2016, 5, 5),
            (2015, 5, 14),
            (2014, 5, 29)
        ]
        for year, month, day in expected_values:
            easter_sunday = get_easter_sunday_date(year)
            ascension_day = get_ascension_day(easter_sunday)
            self.assertEqual(month, ascension_day.month)
            self.assertEqual(day, ascension_day.day)

    def test_get_pentecost_day(self):
        expected_values = [
            (2018, 5, 21),
            (2017, 6, 5),
            (2016, 5, 16),
        ]
        for year, month, day in expected_values:
            easter_sunday = get_easter_sunday_date(year)
            pentecost_day = get_pentecost_day(easter_sunday)
            self.assertEqual(month, pentecost_day.month)
            self.assertEqual(day, pentecost_day.day)
