# coding=utf-8
import datetime

from django.test import TestCase

from web.models import Visit, AppointmentTypeLink
from web.statistics import get_previous_year_and_month_for_date, StatisticsManager
from web.tests.functions import (
    create_appointment,
    create_appointment_type,
    get_control_subject_type,
    get_patient_subject_type,
)
from web.views.notifications import get_today_midnight_date

__author__ = "Valentin Grouès"


class TestStatistics(TestCase):
    def setUp(self):
        self.now = get_today_midnight_date()
        self.appointment_type = create_appointment_type()
        appointment = create_appointment(when=self.now)
        AppointmentTypeLink.objects.create(
            appointment=appointment, appointment_type=self.appointment_type
        )
        self.visit_start = appointment.visit.datetime_begin
        self.visit_end = appointment.visit.datetime_end
        appointment.save()
        self.subject = appointment.visit.subject
        self.statistics_manager = StatisticsManager()

    def test_get_previous_year_and_month_for_date(self):
        test_date = datetime.datetime(year=2014, month=10, day=13)
        previous_year, previous_month = get_previous_year_and_month_for_date(test_date)
        self.assertEqual(2014, previous_year)
        self.assertEqual(9, previous_month)
        test_date = datetime.datetime(year=2014, month=1, day=13)
        previous_year, previous_month = get_previous_year_and_month_for_date(test_date)
        self.assertEqual(2013, previous_year)
        self.assertEqual(12, previous_month)

    def test_get_statistics_for_month_one_appointment(self):
        statistics = self.statistics_manager.get_statistics_for_month(
            self.visit_start.month, self.visit_start.year
        )
        self.check_statistics(statistics, 1, 0, 0, {"C": [0, 0]}, ["Scheduled"])

        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month, self.now.year
        )
        self.check_statistics(statistics, 0, 0, 1, {"C": [1, 1]}, ["Scheduled"])

        statistics = self.statistics_manager.get_statistics_for_month(
            self.visit_end.month, self.visit_end.year
        )
        self.check_statistics(statistics, 0, 1, 0, {"C": [0, 0]}, ["Scheduled"])

    def test_get_statistics_for_month_one_appointment_visit(self):
        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month, self.now.year, visit="1"
        )
        self.check_statistics(statistics, 0, 0, 1, {"C": [1, 1]}, ["Scheduled"])

        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month, self.now.year, visit="2"
        )
        self.check_statistics(statistics, 0, 0, 0, {"C": [0, 0]}, ["Scheduled"])

    def test_get_statistics_for_month_one_appointment_subject_type(self):
        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month, self.now.year, subject_type=get_control_subject_type()
        )
        self.check_statistics(statistics, 0, 0, 1, {"C": [1, 1]}, ["Scheduled"])

        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month, self.now.year, subject_type=get_patient_subject_type()
        )
        self.check_statistics(statistics, 0, 0, 0, {"C": [0, 0]}, ["Scheduled"])

    def test_get_statistics_for_month_one_appointment_subject_type_and_visit(self):
        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month,
            self.now.year,
            subject_type=get_control_subject_type(),
            visit="1",
        )
        self.check_statistics(statistics, 0, 0, 1, {"C": [1, 1]}, ["Scheduled"])

        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month,
            self.now.year,
            subject_type=get_patient_subject_type(),
            visit="1",
        )
        self.check_statistics(statistics, 0, 0, 0, {"C": [0, 0]}, ["Scheduled"])

    def test_get_statistics_for_month_multiple_visits(self):
        second_visit = Visit.objects.create(
            datetime_begin=self.now + datetime.timedelta(days=-32),
            datetime_end=self.now + datetime.timedelta(days=31),
            subject=self.subject,
            is_finished=False,
        )
        second_appointment = create_appointment(second_visit, when=self.now)
        AppointmentTypeLink.objects.create(
            appointment=second_appointment, appointment_type=self.appointment_type
        )

        second_appointment.status = "Cancelled"
        second_appointment.save()
        self.statistics_manager = StatisticsManager()

        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month, self.now.year
        )
        self.check_statistics(
            statistics, 0, 0, 2, {"C": [2, 1, 1]}, ["Cancelled", "Scheduled"]
        )

        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month, self.now.year, visit="1"
        )
        self.check_statistics(
            statistics, 0, 0, 1, {"C": [1, 1, 0]}, ["Cancelled", "Scheduled"]
        )

        statistics = self.statistics_manager.get_statistics_for_month(
            self.now.month, self.now.year, visit="2"
        )
        self.check_statistics(
            statistics, 0, 0, 1, {"C": [1, 0, 1]}, ["Cancelled", "Scheduled"]
        )

    def check_statistics(
        self,
        statistics,
        expected_visits_started,
        expected_visits_ended,
        expected_appointments_count,
        expected_appointments_details,
        expected_statuses,
    ):
        self.assertEqual(
            expected_visits_started, statistics["general"]["visits_started"]
        )
        self.assertEqual(expected_visits_ended, statistics["general"]["visits_ended"])
        self.assertEqual(expected_statuses, statistics["statuses_list"])
        self.assertEqual(
            expected_appointments_count, statistics["general"]["appointments"]
        )
        self.assertEqual(expected_appointments_details, statistics["appointments"])
