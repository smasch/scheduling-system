# coding=utf-8
from unittest.mock import Mock

from django.http import HttpResponseRedirect
from django.test import TestCase

from smash.middleware.force_2fa_middleware import Force2FAMiddleware

NOT_REDIRECTED = "Not redirected"


class TestForce2FAMiddleware(TestCase):

    def setUp(self):
        self.middleware = Force2FAMiddleware(lambda x: NOT_REDIRECTED)

    def test_verified(self):
        """
        if user verified and we don't redirect
        """
        request = Mock()
        request.session = {}
        request.user = Mock()
        request.path = 'subjects'
        request.user.is_authenticated = True
        request.user.is_verified = lambda: True
        result = self.middleware(request)
        self.assertEqual(result, NOT_REDIRECTED)

    def test_not_verified(self):
        """
        if user not verified and not in 2fa section, we redirect to 2fa section
        """
        request = Mock()
        request.session = {}
        request.user = Mock()
        request.path = 'subjects'
        request.user.is_authenticated = True
        request.user.is_verified = lambda: False
        result = self.middleware(request)
        self.assertTrue(isinstance(result, HttpResponseRedirect))
        self.assertEqual(result.url, '/account/two_factor/')

    def test_not_verified_2fa_section(self):
        """
        if user not verified but in 2fa section, we don't redirect
        """
        request = Mock()
        request.session = {}
        request.user = Mock()
        request.path = 'two_factor'
        request.user.is_authenticated = True
        request.user.is_verified = lambda: False
        result = self.middleware(request)
        self.assertEqual(result, NOT_REDIRECTED)
