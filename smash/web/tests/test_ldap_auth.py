# coding=utf-8
import logging
import sys

import ldap
from django.test import Client
from django.test import TestCase
from django.test.utils import override_settings
from django_auth_ldap.config import LDAPSearch
from fakeldap import MockLDAP
from mock import patch

from smash.settings import AUTH_LDAP_BASE_DN, AUTH_LDAP_FILTER
from web.models import Worker
from web.tests.functions import create_user

logger = logging.getLogger(__name__)

ldap_username = "ldap_login"
ldap_password = 'ldap_secret_pass'

ldap_disabled_username = "ldap_disabled_login"
ldap_disabled_password = 'ldap_disabled_secret_pass'

ldap_disabled_remotely_username = "ldap_disabled_remotely_login"
ldap_disabled_remotely_password = 'ldap_disabled_remotely_secret_pass'

local_username = "local_login"
local_password = 'local_secret_pass'
local_remote_password = 'local_remote_secret_pass'

tree = {
    "uid=" + ldap_username + ",cn=users,cn=accounts,dc=uni,dc=lu": {
        "uid": ldap_username,
    },
    "uid=" + ldap_disabled_username + ",cn=users,cn=accounts,dc=uni,dc=lu": {
        "uid": ldap_disabled_username,
    },
    "uid=" + ldap_disabled_remotely_username + ",cn=users,cn=accounts,dc=uni,dc=lu": {
        "uid": ldap_disabled_remotely_username,
    },
    "uid=" + local_username + ",cn=users,cn=accounts,dc=uni,dc=lu": {
        "uid": local_username,
    }
}

# ldap_patcher = patch('django_auth_ldap.backend.LDAPBackend.ldap')
ldap_patcher2 = patch('django_auth_ldap.config._LDAPConfig.get_ldap')


class CustomMockLDAP(MockLDAP):
    cidict = ldap.cidict

    def initialize(self, uri, trace_level=0, trace_file=sys.stdout, trace_stack_limit=None, **kwargs):
        return super().initialize(uri, trace_level, trace_file, trace_stack_limit)


_mock_ldap = CustomMockLDAP(tree)

# normal user that can login
_mock_ldap.set_return_value('simple_bind_s',
                            ("uid=" + ldap_username + ",cn=users,cn=accounts,dc=uni,dc=lu",
                             ldap_password),
                            True)

# user that was not disabled remotely
_mock_ldap.set_return_value('simple_bind_s',
                            ("uid=" + ldap_disabled_username + ",cn=users,cn=accounts,dc=uni,dc=lu",
                             ldap_disabled_password),
                            True)

# user that was disabled remotely
_mock_ldap.set_return_value('simple_bind_s',
                            ("uid=" + ldap_disabled_remotely_username + ",cn=users,cn=accounts,dc=uni,dc=lu",
                             ldap_disabled_remotely_password),
                            False)

# local user that has a remote account (but local should be used)
_mock_ldap.set_return_value('simple_bind_s',
                            ("uid=" + local_username + ",cn=users,cn=accounts,dc=uni,dc=lu",
                             local_remote_password),
                            True)


@override_settings(
    AUTH_LDAP_USER_SEARCH=LDAPSearch(
        # pylint: disable=no-member
        AUTH_LDAP_BASE_DN, ldap.SCOPE_ONELEVEL, AUTH_LDAP_FILTER
    ),
    AUTHENTICATION_BACKENDS=('web.auth.CustomLDAPBackend.CustomLDAPBackend',
                             'web.auth.CustomModelBackend.CustomModelBackend'))
class TestLdapLoginView(TestCase):
    def setUp(self):
        self.ldap_worker = Worker.get_by_user(create_user(username=ldap_username, password=None))
        self.ldap_worker.ldap_user = True
        self.ldap_worker.save()
        self.ldap_disabled_worker = Worker.get_by_user(create_user(username=ldap_disabled_username, password=None))
        self.ldap_disabled_worker.ldap_user = True
        self.ldap_disabled_worker.save()
        self.ldap_disabled_worker.disable()

        self.ldap_disabled_remotely_worker = Worker.get_by_user(
            create_user(username=ldap_disabled_remotely_username, password=None))
        self.ldap_disabled_remotely_worker.ldap_user = True
        self.ldap_disabled_remotely_worker.save()

        self.local_worker = Worker.get_by_user(create_user(username=local_username, password=local_password))

        # Patch where the ldap library is used:
        self.mock_ldap = ldap_patcher2.start()
        self.mock_ldap.return_value = _mock_ldap
        ldap_patcher2.start().return_value = _mock_ldap

    def tearDown(self):
        _mock_ldap.reset()
        self.mock_ldap.stop()

    def test_login_via_ldap(self):
        client = Client()
        self.assertTrue(client.login(username=ldap_username, password=ldap_password))

    def test_login_via_ldap_for_disabled(self):
        client = Client()
        self.assertFalse(client.login(username=ldap_disabled_username, password=ldap_disabled_password))

    def test_login_via_ldap_for_disabled_remotely(self):
        client = Client()
        self.assertFalse(
            client.login(username=ldap_disabled_remotely_username, password=ldap_disabled_remotely_password))

    def test_login_via_ldap_with_invalid_credentials(self):
        client = Client()
        self.assertFalse(client.login(username=ldap_username, password="bla"))

    def test_login_via_local(self):
        client = Client()
        self.assertTrue(client.login(username=local_username, password=local_password))

    def test_login_via_local_using_ldap_password(self):
        client = Client()
        self.assertFalse(client.login(username=local_username, password=local_remote_password))
