from django.test import TestCase

from web.models import Appointment, Visit, StudySubject, Study
from web.models.constants import CUSTOM_FIELD_TYPE_TEXT
from web.models.custom_data import CustomStudySubjectField
from web.tests.functions import create_visit, create_study
from web.tests.functions import (
    get_test_study,
    create_study_subject,
    create_appointment,
    create_study_subject_with_multiple_screening_numbers,
)


class SubjectModelTests(TestCase):
    def test_signal_mark_as_endpoint_reached(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)

        subject.endpoint_reached = True
        subject.save()
        appointment_status = Appointment.objects.filter(id=appointment.id)[0].status
        visit_finished = Visit.objects.filter(id=visit.id)[0].is_finished

        self.assertTrue(subject.endpoint_reached)
        self.assertTrue(visit_finished)
        self.assertEqual(Appointment.APPOINTMENT_STATUS_CANCELLED, appointment_status)

    def test_mark_as_endpoint_reached(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)

        subject.mark_as_endpoint_reached()
        appointment_status = Appointment.objects.filter(id=appointment.id)[0].status
        visit_finished = Visit.objects.filter(id=visit.id)[0].is_finished

        self.assertTrue(subject.endpoint_reached)
        self.assertTrue(visit_finished)
        self.assertEqual(Appointment.APPOINTMENT_STATUS_CANCELLED, appointment_status)

    def test_signal_mark_as_resigned(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)

        subject.resigned = True
        subject.save()
        appointment_status = Appointment.objects.filter(id=appointment.id)[0].status
        visit_finished = Visit.objects.filter(id=visit.id)[0].is_finished

        self.assertTrue(subject.resigned)
        self.assertTrue(visit_finished)
        self.assertEqual(Appointment.APPOINTMENT_STATUS_CANCELLED, appointment_status)

    def test_mark_as_resigned(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)

        subject.mark_as_resigned()
        appointment_status = Appointment.objects.filter(id=appointment.id)[0].status
        visit_finished = Visit.objects.filter(id=visit.id)[0].is_finished

        self.assertTrue(subject.resigned)
        self.assertTrue(visit_finished)
        self.assertEqual(Appointment.APPOINTMENT_STATUS_CANCELLED, appointment_status)

    def test_signal_mark_as_excluded(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)

        subject.excluded = True
        subject.save()
        appointment_status = Appointment.objects.filter(id=appointment.id)[0].status
        visit_finished = Visit.objects.filter(id=visit.id)[0].is_finished

        self.assertTrue(subject.excluded)
        self.assertTrue(visit_finished)
        self.assertEqual(Appointment.APPOINTMENT_STATUS_CANCELLED, appointment_status)

    def test_mark_as_excluded(self):
        subject = create_study_subject()
        visit = create_visit(subject)
        appointment = create_appointment(visit)

        subject.mark_as_excluded()
        appointment_status = Appointment.objects.filter(id=appointment.id)[0].status
        visit_finished = Visit.objects.filter(id=visit.id)[0].is_finished

        self.assertTrue(subject.excluded)
        self.assertTrue(visit_finished)
        self.assertEqual(Appointment.APPOINTMENT_STATUS_CANCELLED, appointment_status)

    def test_check_nd_number_regex(self):
        study = get_test_study()

        # delete everything
        StudySubject.objects.all().delete()
        # get default regex
        nd_number_study_subject_regex_default = Study._meta.get_field("nd_number_study_subject_regex").get_default()

        self.assertTrue(StudySubject.check_nd_number_regex(nd_number_study_subject_regex_default, study))
        # this will add a studysubject with a NDnumber
        create_study_subject(nd_number="ND0001")

        self.assertTrue(StudySubject.check_nd_number_regex(nd_number_study_subject_regex_default, study))
        # delete everything
        StudySubject.objects.all().delete()
        # this will add a studysubject with a NDnumber
        create_study_subject(nd_number="ND00001")
        self.assertFalse(StudySubject.check_nd_number_regex(nd_number_study_subject_regex_default, study))

    def test_sort_matched_screening_first(self):
        def create_result(phrase, subject_id=1):
            phrase = phrase.format(subject_id=subject_id)
            phrase = phrase.split(";")
            for i, _ in enumerate(phrase):
                letter, num = _.strip().split("-")
                phrase[i] = (letter, int(num))
            return phrase

        subject = create_study_subject_with_multiple_screening_numbers(subject_id=1)
        self.assertEqual(
            subject.sort_matched_screening_first("L"), create_result("L-00{subject_id}; E-00{subject_id}", subject_id=1)
        )
        self.assertEqual(
            subject.sort_matched_screening_first("L-00"),
            create_result("L-00{subject_id}; E-00{subject_id}", subject_id=1),
        )
        self.assertEqual(
            subject.sort_matched_screening_first("E"), create_result("E-00{subject_id}; L-00{subject_id}", subject_id=1)
        )
        self.assertEqual(
            subject.sort_matched_screening_first("-"), create_result("E-00{subject_id}; L-00{subject_id}", subject_id=1)
        )
        self.assertEqual(
            subject.sort_matched_screening_first(""), create_result("E-00{subject_id}; L-00{subject_id}", subject_id=1)
        )
        self.assertEqual(
            subject.sort_matched_screening_first("001"),
            create_result("E-00{subject_id}; L-00{subject_id}", subject_id=1),
        )
        self.assertEqual(
            subject.sort_matched_screening_first("00"),
            create_result("E-00{subject_id}; L-00{subject_id}", subject_id=1),
        )
        self.assertEqual(
            subject.sort_matched_screening_first("potato"),
            create_result("E-00{subject_id}; L-00{subject_id}", subject_id=1),
        )

    def test_sort_matched_screening_first_bad_number(self):
        subject_id = 2
        screening_number = f"L-0/0{subject_id}; E-00{subject_id}; F-0/1{subject_id}"

        subject = create_study_subject_with_multiple_screening_numbers(
            subject_id=subject_id, screening_number=screening_number
        )

        self.assertEqual(subject.sort_matched_screening_first("E-"), [("E", subject_id), ("F", "0/12"), ("L", "0/02")])

        self.assertEqual(subject.sort_matched_screening_first("L-"), [("L", "0/02"), ("E", subject_id), ("F", "0/12")])

    def test_sort_matched_screening_first_bad_format(self):
        screening_number = "potato; tomato"
        subject_id = 3
        subject = create_study_subject_with_multiple_screening_numbers(
            subject_id=subject_id, screening_number=screening_number
        )

        self.assertEqual(subject.sort_matched_screening_first("L-"), [("potato", None), ("tomato", None)])

    def test_subject_with_custom_field(self):
        field = CustomStudySubjectField.objects.create(
            study=get_test_study(), default_value="xyz", type=CUSTOM_FIELD_TYPE_TEXT
        )

        subject = create_study_subject()

        self.assertEqual(1, len(subject.custom_data_values))
        value = subject.custom_data_values[0]
        self.assertEqual("xyz", value.value)
        self.assertEqual(subject, value.study_subject)
        self.assertEqual(field, value.study_subject_field)

    def test_subject_with_removed_custom_field(self):
        field = CustomStudySubjectField.objects.create(
            study=get_test_study(), default_value="xyz", type=CUSTOM_FIELD_TYPE_TEXT
        )

        subject = create_study_subject()
        self.assertEqual(1, len(subject.custom_data_values))
        field.delete()

        self.assertEqual(0, len(subject.custom_data_values))

    def test_subject_with_custom_field_tracked(self):
        CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz", type=CUSTOM_FIELD_TYPE_TEXT,
                                               tracked=True)

        subject = create_study_subject()

        self.assertEqual(1, len(subject.custom_data_values))
        value = subject.custom_data_values[0]
        self.assertEqual("xyz", value.value)
        self.assertTrue(value.study_subject_field.tracked)

    def test_subject_with_removed_custom_field_not_tracked(self):
        field = CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz",
                                                       type=CUSTOM_FIELD_TYPE_TEXT)

        subject = create_study_subject()
        self.assertEqual(1, len(subject.custom_data_values))
        self.assertFalse(subject.custom_data_values[0].study_subject_field.tracked)
        field.delete()

        CustomStudySubjectField.objects.create(study=get_test_study(), default_value="xyz", type=CUSTOM_FIELD_TYPE_TEXT,
                                               tracked=False)

        subject = create_study_subject()
        self.assertEqual(1, len(subject.custom_data_values))
        self.assertFalse(subject.custom_data_values[0].study_subject_field.tracked)

    def test_subject_with_field_from_different_study(self):
        field = CustomStudySubjectField.objects.create(study=create_study("bla"), type=CUSTOM_FIELD_TYPE_TEXT)

        subject = create_study_subject()
        self.assertIsNone(subject.get_custom_data_value(field))

    def test_status(self):
        subject = create_study_subject()
        # noinspection PySetFunctionToLiteral
        statuses = set(
            [
                subject.status,
            ]
        )
        subject.endpoint_reached = True
        statuses.add(subject.status)
        subject.excluded = True
        statuses.add(subject.status)
        subject.resigned = True
        statuses.add(subject.status)
        subject.subject.dead = True
        statuses.add(subject.status)

        print(statuses)
        self.assertEqual(5, len(statuses))
