from django.test import TestCase

from web.models.missing_subject import MissingSubject
from web.tests.functions import create_study_subject, create_red_cap_subject


class MissingSubjectTests(TestCase):
    def test_create_with_empty_redcap_subject(self):
        subject = create_study_subject()
        missing_subject = MissingSubject.create(red_cap_subject=None, smash_subject=subject)
        self.assertIsNotNone(str(missing_subject))
        self.assertIsNotNone(str(missing_subject))

    def test_create_with_empty_smash_subject(self):
        red_cap_subject = create_red_cap_subject()
        missing_subject = MissingSubject.create(red_cap_subject=red_cap_subject, smash_subject=None)
        self.assertIsNotNone(str(missing_subject))
        self.assertIsNotNone(str(missing_subject))
