from django.test import TestCase

from web.models.inconsistent_subject import InconsistentField


class InconsistentFieldTests(TestCase):
    def test_create_with_empty_data(self):
        inconsistent_field = InconsistentField.create("field name", None, None)
        self.assertIsNotNone(str(inconsistent_field))
        self.assertIsNotNone(str(inconsistent_field))
