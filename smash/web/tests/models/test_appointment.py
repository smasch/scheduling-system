import logging

from django.db.models import ProtectedError
from django.test import TestCase

from web.tests.functions import create_appointment, create_room, create_flying_team

logger = logging.getLogger(__name__)


class AppointmentModelTests(TestCase):
    def test_title_for_appointment_without_visit(self):
        comment = "some comment title"
        appointment = create_appointment()
        appointment.visit = None
        appointment.comment = comment
        appointment.save()

        self.assertEqual(comment, appointment.title())

    def test_title_for_appointment_with_visit(self):
        appointment = create_appointment()
        self.assertIsNotNone(appointment.title())

    def test_remove_location_for_using_appointment(self):
        appointment = create_appointment()
        with self.assertRaises(ProtectedError):
            appointment.location.delete()

    def test_remove_room_for_using_appointment(self):
        appointment = create_appointment()
        appointment.room = create_room()
        appointment.save()
        with self.assertRaises(ProtectedError):
            appointment.room.delete()

    def test_remove_flying_team_for_using_appointment(self):
        appointment = create_appointment()
        appointment.flying_team = create_flying_team()
        appointment.save()
        with self.assertRaises(ProtectedError):
            appointment.flying_team.delete()
