import logging

from django.test import TestCase

from web.models import AppointmentTypeLink
from web.tests.functions import create_appointment, create_appointment_type
from web.views.notifications import get_today_midnight_date

logger = logging.getLogger(__name__)


class AppointmentTypeLinkTests(TestCase):
    def test_save(self):
        link = AppointmentTypeLink(appointment=create_appointment(), appointment_type=create_appointment_type())
        link.date_when = get_today_midnight_date()
        link.save()
        self.assertIsNone(link.date_when.tzname())
