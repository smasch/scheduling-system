from django.test import TestCase

from web.models import Appointment, Visit
from web.tests.functions import create_study_subject, create_appointment, create_subject, create_visit, create_language


class SubjectModelTests(TestCase):
    def test_signal_mark_as_dead(self):
        study_subject = create_study_subject()
        subject = study_subject.subject
        visit = create_visit(study_subject)
        appointment = create_appointment(visit)

        subject.dead = True
        subject.save()
        appointment_status = Appointment.objects.filter(id=appointment.id)[0].status
        visit_finished = Visit.objects.filter(id=visit.id)[0].is_finished

        self.assertTrue(subject.dead)
        self.assertTrue(visit_finished)
        self.assertEqual(Appointment.APPOINTMENT_STATUS_CANCELLED, appointment_status)

    def test_mark_as_dead(self):
        study_subject = create_study_subject()
        subject = study_subject.subject
        visit = create_visit(study_subject)
        appointment = create_appointment(visit)

        subject.mark_as_dead()
        appointment_status = Appointment.objects.filter(id=appointment.id)[0].status
        visit_finished = Visit.objects.filter(id=visit.id)[0].is_finished

        self.assertTrue(subject.dead)
        self.assertTrue(visit_finished)
        self.assertEqual(Appointment.APPOINTMENT_STATUS_CANCELLED, appointment_status)

    def test_str(self):
        subject = create_subject()

        self.assertIsNotNone(str(subject))

    def test_remove_subject_language(self):
        study_subject = create_study_subject()
        subject = study_subject.subject

        subject.default_written_communication_language = create_language()
        subject.save()

        subject.default_written_communication_language.delete()
        subject.refresh_from_db()

        self.assertIsNone(subject.default_written_communication_language)
