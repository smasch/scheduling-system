# coding=utf-8
import io
from typing import List

from django.test import TestCase
from docx import Document

from web.models import MailTemplate, StudySubject, Language, Appointment, Study
from web.models.constants import MAIL_TEMPLATE_CONTEXT_APPOINTMENT, MAIL_TEMPLATE_CONTEXT_VISIT, \
    MAIL_TEMPLATE_CONTEXT_SUBJECT, MAIL_TEMPLATE_CONTEXT_VOUCHER, CUSTOM_FIELD_TYPE_TEXT, GLOBAL_STUDY_ID
from web.models.custom_data import CustomStudySubjectField
from web.models.mail_template import DATE_FORMAT_SHORT, get_mails_template_subject_tags, get_field_id
from web.tests.functions import create_language, get_resource_path, create_appointment, create_user, \
    create_study_subject, \
    create_visit, create_voucher, create_worker, create_flying_team, get_test_study, get_test_location


class MailTemplateModelTests(TestCase):
    def setUp(self):
        self.french_language = create_language("French", "fr_FR")
        self.english_language = create_language("English", "en_GB")
        self.template_file = get_resource_path('upcoming_appointment_FR.docx')
        self.user = create_user()

    def test_get_appointment_mail_templates(self):
        context = MAIL_TEMPLATE_CONTEXT_APPOINTMENT
        function_to_test = MailTemplate.get_appointment_mail_templates
        self.check_get_mail_templates(context, function_to_test)

    def test_get_visit_mail_templates(self):
        context = MAIL_TEMPLATE_CONTEXT_VISIT
        function_to_test = MailTemplate.get_visit_mail_templates
        self.check_get_mail_templates(context, function_to_test)

    def test_get_subject_mail_templates(self):
        context = MAIL_TEMPLATE_CONTEXT_SUBJECT
        function_to_test = MailTemplate.get_subject_mail_templates
        self.check_get_mail_templates(context, function_to_test)

    def test_get_voucher_mail_templates(self):
        context = MAIL_TEMPLATE_CONTEXT_VOUCHER
        function_to_test = MailTemplate.get_voucher_mail_templates
        self.check_get_mail_templates(context, function_to_test)

    def check_get_mail_templates(self, context, function_to_test):
        # create french template
        template_name_french = "test_fr"
        appointment_template_french = MailTemplate(name=template_name_french, language=self.french_language,
                                                   context=context,
                                                   template_file=self.template_file)
        appointment_template_french.save()
        # create english template
        template_name_english = "test_en"
        appointment_template_english = MailTemplate(name=template_name_english, language=self.english_language,
                                                    context=context,
                                                    template_file=self.template_file)
        appointment_template_english.save()
        active_templates, inactive_templates = function_to_test([self.french_language])
        self.assertEqual(1, len(active_templates), "only on active template should be returned (for french)")
        self.assertEqual(1, len(inactive_templates), "only on inactive template should be returned (for english)")
        self.assertEqual(template_name_french, active_templates[0].name)
        self.assertEqual(template_name_english, inactive_templates[0].name)
        # now tries with multiple languages
        active_templates, inactive_templates = function_to_test(
            [self.english_language, self.french_language])
        self.assertEqual(2, len(active_templates), "two active templates should be returned (for french and english)")
        self.assertEqual(0, len(inactive_templates), "no inactive template should be returned")
        # also checks the correct sorting
        self.assertEqual(template_name_english, active_templates[0].name)
        self.assertEqual(template_name_french, active_templates[1].name)

        # tries with no languages
        active_templates, inactive_templates = function_to_test([])
        self.assertEqual(0, len(active_templates), "no active templates should be returned")
        self.assertEqual(2, len(inactive_templates), "two inactive templates should be returned (french and english)")

    def test_apply_appointment(self):
        template_name_french = "test_fr"
        appointment = create_appointment()
        appointment_template_french = MailTemplate(name=template_name_french, language=self.french_language,
                                                   context=MAIL_TEMPLATE_CONTEXT_APPOINTMENT,
                                                   template_file=self.template_file)
        stream = io.BytesIO()
        appointment_template_french.apply(appointment, self.user, stream)
        doc = Document(stream)
        worker_name = str(self.user.worker)
        location = appointment.location.name
        self.check_doc_contains(doc, [worker_name, location])

    def test_appointment_with_unicode_flying_team(self):
        template_name_french = "test_fr"
        appointment = create_appointment()
        # noinspection PyNonAsciiChar
        flying_team_name = "Liège"
        appointment.flying_team = create_flying_team(flying_team_name)
        appointment_template_french = MailTemplate(name=template_name_french, language=self.french_language,
                                                   context=MAIL_TEMPLATE_CONTEXT_APPOINTMENT,
                                                   template_file=self.template_file)
        stream = io.BytesIO()
        appointment_template_french.apply(appointment, self.user, stream)
        doc = Document(stream)
        self.check_doc_contains(doc, [flying_team_name])

    def test_apply_subject(self):
        subject = create_study_subject()
        doc = self.create_doc_for_subject(subject, self.french_language, MAIL_TEMPLATE_CONTEXT_SUBJECT)
        worker_name = str(self.user.worker)

        self.check_doc_contains(doc, [worker_name, str(subject), str(subject.subject.country), subject.nd_number,
                                      subject.type.name])

    def test_apply_subject_with_non_existing_custom_field(self):
        subject = create_study_subject()
        CustomStudySubjectField.objects.create(name="Diagnosis", type=CUSTOM_FIELD_TYPE_TEXT,
                                               study=get_test_study(), unique=True)
        doc = self.create_doc_for_subject(subject, self.french_language, MAIL_TEMPLATE_CONTEXT_SUBJECT)
        worker_name = str(self.user.worker)

        self.check_doc_contains(doc, [worker_name, str(subject), str(subject.subject.country), subject.nd_number,
                                      subject.type.name])

    def create_doc_for_subject(self, subject: StudySubject, language: Language, context: str):
        subject_template_french = MailTemplate(name="test_fr", language=language,
                                               context=context,
                                               template_file=self.template_file)
        stream = io.BytesIO()
        subject_template_french.apply(subject, self.user, stream)
        doc = Document(stream)
        return doc

    def test_apply_voucher(self):
        template_name_french = "test_without_language"
        subject = create_study_subject()
        subject_template_french = MailTemplate(name=template_name_french, language=None,
                                               context=MAIL_TEMPLATE_CONTEXT_VOUCHER,
                                               template_file="voucher_test.docx")
        voucher = create_voucher(study_subject=subject)
        stream = io.BytesIO()
        subject_template_french.apply(voucher, self.user, stream)
        doc = Document(stream)
        worker_name = str(self.user.worker)

        self.check_doc_contains(doc, [worker_name, str(subject), voucher.number, voucher.usage_partner.address,
                                      voucher.expiry_date.strftime(DATE_FORMAT_SHORT),
                                      voucher.issue_date.strftime(DATE_FORMAT_SHORT)
                                      ])

    def test_apply_voucher_for_template_with_table(self):
        template_name_french = "test_without_language"
        subject = create_study_subject()
        subject_template_french = MailTemplate(name=template_name_french, language=None,
                                               context=MAIL_TEMPLATE_CONTEXT_VOUCHER,
                                               template_file="template_with_tables.docx")
        voucher = create_voucher(study_subject=subject)
        stream = io.BytesIO()
        subject_template_french.apply(voucher, self.user, stream)
        doc = Document(stream)
        worker_name = str(self.user.worker)

        self.check_doc_contains(doc, [worker_name, str(subject), voucher.number, voucher.usage_partner.address,
                                      voucher.expiry_date.strftime(DATE_FORMAT_SHORT),
                                      voucher.issue_date.strftime(DATE_FORMAT_SHORT)
                                      ])

    def test_valid_voucher_replacement(self):
        subject = create_study_subject()
        voucher = create_voucher(study_subject=subject)
        voucher.activity_type = 'act type'

        replacements = MailTemplate.get_voucher_replacements(voucher)

        activity_type_in_replacements = False
        for key in replacements:
            self.assertIsNotNone(key)
            self.assertIsNotNone(replacements[key], msg="None value for key: " + key)
            if voucher.activity_type in replacements[key]:
                activity_type_in_replacements = True
        self.assertTrue(activity_type_in_replacements)

    def test_valid_subject_replacement(self):
        subject = create_study_subject()

        replacements = MailTemplate.get_subject_replacements(subject)
        for key in replacements:
            self.assertIsNotNone(key)
            self.assertIsNotNone(replacements[key], msg="None value for key: " + key)

        for entry in get_mails_template_subject_tags():
            key = entry[0]
            self.assertIsNotNone(replacements.get(key, None), f"Value for field {key} does not exist")

    def test_valid_appointment_replacement(self):
        appointment = create_appointment()
        replacements = MailTemplate.get_appointment_replacements(appointment)

        for key in replacements:
            self.assertIsNotNone(key)
            self.assertIsNotNone(replacements[key], msg="None value for key: " + key)

    def test_valid_generic_replacement(self):
        replacements = MailTemplate.get_generic_replacements(self.user.worker)

        for key in replacements:
            self.assertIsNotNone(key)
            self.assertIsNotNone(replacements[key], msg="None value for key: " + key)

    def test_valid_generic_replacement_for_empty_worker(self):
        replacements = MailTemplate.get_generic_replacements(None)

        for key in replacements:
            self.assertIsNotNone(key)
            self.assertIsNotNone(replacements[key], msg="None value for key: " + key)

    def test_get_mail_templates_for_context_without_language(self):
        template_name_french = "test_without_language"
        MailTemplate(name=template_name_french, language=None,
                     context=MAIL_TEMPLATE_CONTEXT_VOUCHER,
                     template_file="voucher_test.docx").save()

        templates = MailTemplate.get_mail_templates_for_context([], context=MAIL_TEMPLATE_CONTEXT_VOUCHER)
        self.assertEqual(1, len(templates[0]))

        templates = MailTemplate.get_mail_templates_for_context([self.english_language],
                                                                context=MAIL_TEMPLATE_CONTEXT_VOUCHER)
        self.assertEqual(0, len(templates[0]))

    def test_get_mail_templates_for_context_without_language_2(self):
        template_name_french = "test_without_language"
        MailTemplate(name=template_name_french, language=self.english_language,
                     context=MAIL_TEMPLATE_CONTEXT_VOUCHER,
                     template_file="voucher_test.docx").save()

        templates = MailTemplate.get_mail_templates_for_context([], context=MAIL_TEMPLATE_CONTEXT_VOUCHER)
        self.assertEqual(0, len(templates[0]))

    def test_apply_visit(self):
        template_name_french = "test_fr"
        visit = create_visit()
        visit_template_french = MailTemplate(name=template_name_french, language=self.french_language,
                                             context=MAIL_TEMPLATE_CONTEXT_VISIT,
                                             template_file=self.template_file)
        stream = io.BytesIO()
        visit_template_french.apply(visit, self.user, stream)
        doc = Document(stream)
        worker_name = str(self.user.worker)

        self.check_doc_contains(doc, [worker_name, str(visit.subject), str(visit.subject.subject.country),
                                      visit.subject.nd_number,
                                      visit.subject.type.name,
                                      visit.datetime_begin.strftime(DATE_FORMAT_SHORT),
                                      visit.datetime_end.strftime(DATE_FORMAT_SHORT)])

    def check_doc_contains(self, doc, needles: List[str]):
        founds = [False] * len(needles)
        count_found = 0
        for paragraph in doc.paragraphs:
            for i, text in enumerate(needles):
                if not founds[i] and text in paragraph.text:
                    founds[i] = True
                    count_found += 1
        for table in doc.tables:
            for row in table.rows:
                for cell in row.cells:
                    for paragraph in cell.paragraphs:
                        for i, text in enumerate(needles):
                            if not founds[i] and text in paragraph.text:
                                founds[i] = True
                                count_found += 1

        if count_found != len(needles):
            for i, text in enumerate(needles):
                self.assertTrue(founds[i], f"{text} was not found in the generated Word document")

    def test_get_generic_replacements(self):
        worker = create_worker()
        result = MailTemplate.get_generic_replacements(worker)
        self.assertEqual(result['##WORKER_EMAIL##'], worker.email)

    def test_apply_general_appointment(self):
        appointment = Appointment.objects.create(
            length=30,
            location=get_test_location(),
            status=Appointment.APPOINTMENT_STATUS_SCHEDULED)

        template_name_french = "test_fr"
        appointment_template_french = MailTemplate(name=template_name_french,
                                                   language=self.french_language,
                                                   context=MAIL_TEMPLATE_CONTEXT_APPOINTMENT,
                                                   template_file=self.template_file)
        stream = io.BytesIO()
        appointment_template_french.apply(appointment, self.user, stream)
        doc = Document(stream)
        worker_name = str(self.user.worker)
        location = appointment.location.name
        self.check_doc_contains(doc, [worker_name, location])

    def test_custom_fields_in_templates(self):
        length = len(get_mails_template_subject_tags())
        CustomStudySubjectField.objects.create(study=Study.objects.get(id=GLOBAL_STUDY_ID),
                                               default_value="val123",
                                               type=CUSTOM_FIELD_TYPE_TEXT, possible_values="")
        self.assertEqual(length + 1, len(get_mails_template_subject_tags()))

    def test_replacement_for_custom_field(self):
        study = Study.objects.get(id=GLOBAL_STUDY_ID)
        field = CustomStudySubjectField.objects.create(study=study,
                                                       name="custom-name",
                                                       default_value="val123",
                                                       type=CUSTOM_FIELD_TYPE_TEXT, possible_values="")
        subject = create_study_subject(study=study)
        subject.set_custom_field_value(field.name, "test val")

        replacements = MailTemplate.get_subject_replacements(subject)
        key = get_field_id(field)
        self.assertEqual(replacements.get(key, None), "test val", f"Wrong value for field {key}")

    def test_replacement_for_custom_field_with_no_subject(self):
        study = Study.objects.get(id=GLOBAL_STUDY_ID)
        field = CustomStudySubjectField.objects.create(study=study,
                                                       name="custom-name",
                                                       default_value="val123",
                                                       type=CUSTOM_FIELD_TYPE_TEXT, possible_values="")
        replacements = MailTemplate.get_subject_replacements()
        key = get_field_id(field)
        self.assertIsNone(replacements.get(key, None), f"Wrong value for field {key}")

    def test_get_field_id(self):
        field = CustomStudySubjectField.objects.create(study=Study.objects.get(id=GLOBAL_STUDY_ID),
                                                       name="bla bla-5",
                                                       default_value="val123",
                                                       type=CUSTOM_FIELD_TYPE_TEXT, possible_values="")
        self.assertEqual("##S_C_BLA_BLA_5##", get_field_id(field))
