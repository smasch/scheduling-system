import logging

from django.test import TestCase

from web.tests.functions import create_study_subject
from web.models import ContactAttempt
from web.tests import create_worker

logger = logging.getLogger(__name__)


class ContactAttemptTests(TestCase):
    def test_str(self):
        contact_attempt = ContactAttempt(worker=create_worker(), subject=create_study_subject())

        self.assertIsNotNone(str(contact_attempt))
        self.assertIsNotNone(str(contact_attempt))
