import logging

from django.test import TestCase

from web.tests.functions import create_voucher_type

logger = logging.getLogger(__name__)


class VoucherTypeTests(TestCase):
    def test_to_string(self):
        voucher_type = create_voucher_type()

        self.assertTrue(voucher_type.code in str(voucher_type))
        self.assertTrue(voucher_type.code in str(voucher_type))
