import logging

from django.test import TestCase

from web.models import Room

logger = logging.getLogger(__name__)


class RoomTests(TestCase):
    def test_str(self):
        room = Room(room_number=4, address="some street 2/3", city="Luxembourg")

        self.assertIsNotNone(str(room))
        self.assertIsNotNone(str(room))
