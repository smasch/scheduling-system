import logging

from django.test import TestCase

from web.models import Item

logger = logging.getLogger(__name__)


class ItemTests(TestCase):
    def test_str(self):
        item = Item(name="test item")

        self.assertIsNotNone(str(item))
        self.assertIsNotNone(str(item))
