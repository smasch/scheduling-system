import logging

from django.test import TestCase

from web.models import Availability
from web.tests import create_worker
from web.models.constants import MONDAY_AS_DAY_OF_WEEK

logger = logging.getLogger(__name__)


class AvailabilityTests(TestCase):
    def test_str(self):
        availability = Availability(person=create_worker(), day_number=MONDAY_AS_DAY_OF_WEEK)

        self.assertTrue("MONDAY" in str(availability))
        self.assertTrue("MONDAY" in str(availability))
