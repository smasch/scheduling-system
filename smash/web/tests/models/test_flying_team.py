import logging

from django.test import TestCase

from web.models import FlyingTeam

logger = logging.getLogger(__name__)


class FlyingTeamTests(TestCase):
    def test_image_img(self):
        flying_team = FlyingTeam(place="xxx")

        self.assertTrue("x" in str(flying_team))
