from django.test import TestCase
from parameterized import parameterized

from web.models import ConfigurationItem
from web.models.constants import CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE, \
    NO_SHOW_APPOINTMENT_COLOR_CONFIGURATION_TYPE, KIT_EMAIL_HOUR_CONFIGURATION_TYPE, \
    KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE, DEFAULT_FROM_EMAIL, VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE, \
    KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE, VISIT_SHOW_VISIT_NUMBER_FROM_ZERO
from web.tests.functions import create_configuration_item


class ConfigurationItemModelTests(TestCase):
    def test_init_data(self):
        items = ConfigurationItem.objects.filter(type=CANCELLED_APPOINTMENT_COLOR_CONFIGURATION_TYPE)
        self.assertTrue(len(items) > 0)

        items = ConfigurationItem.objects.filter(type=NO_SHOW_APPOINTMENT_COLOR_CONFIGURATION_TYPE)
        self.assertTrue(len(items) > 0)

    def test_str(self):
        configuration_item = create_configuration_item()
        self.assertIsNotNone(str(configuration_item))

    @parameterized.expand([
        ('valid KIT_EMAIL_HOUR_CONFIGURATION_TYPE', KIT_EMAIL_HOUR_CONFIGURATION_TYPE, '09:00', True),
        ('invalid KIT_EMAIL_HOUR_CONFIGURATION_TYPE', KIT_EMAIL_HOUR_CONFIGURATION_TYPE, 'text', False),

        ('valid VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE', VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE, '12:00', True),
        ('valid VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE', VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE, '', True),
        ('invalid VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE', VIRUS_EMAIL_HOUR_CONFIGURATION_TYPE, 'text', False),

        ('valid KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE', KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE, 'MONDAY', True),
        ('invalid KIT_EMAIL_DAY_OF_WEEK_CONFIGURATION_TYPE', KIT_EMAIL_HOUR_CONFIGURATION_TYPE, 'unknown day', False),

        ('valid DEFAULT_FROM_EMAIL', DEFAULT_FROM_EMAIL, 'smasch@uni.lu', True),
        ('invalid DEFAULT_FROM_EMAIL', DEFAULT_FROM_EMAIL, 'smasch uni lu', False),

        ('valid single KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE',
         KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE, 'smasch@uni.lu', True),
        ('valid empty KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE',
         KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE, '', True),
        ('valid multiple KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE',
         KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE, 'smasch@uni.lu;recipient@uni.lu', True),
        ('invalid KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE',
         KIT_RECIPIENT_EMAIL_CONFIGURATION_TYPE, 'smasch uni lu', False),

        ('valid positive VISIT_SHOW_VISIT_NUMBER_FROM_ZERO', VISIT_SHOW_VISIT_NUMBER_FROM_ZERO, 'true', True),
        ('valid negative VISIT_SHOW_VISIT_NUMBER_FROM_ZERO', VISIT_SHOW_VISIT_NUMBER_FROM_ZERO, 'false', True),
        ('invalid negative VISIT_SHOW_VISIT_NUMBER_FROM_ZERO', VISIT_SHOW_VISIT_NUMBER_FROM_ZERO, 'blah', False),

    ])
    def test_validate(self, _, item_type: str, value: str, validation_result: bool):
        item = ConfigurationItem.objects.get(type=item_type)
        item.value = value
        self.assertEqual(validation_result, ConfigurationItem.is_valid(item))
