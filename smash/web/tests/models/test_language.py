from django.core.files.images import ImageFile
from django.test import TestCase

from web.models import Language
from web.tests.functions import get_resource_path, create_language


class LanguageTests(TestCase):
    def test_image_img(self):
        language = Language(image=ImageFile(open(get_resource_path("PL.png"), "rb")))

        img_html_tag = language.image_img()
        self.assertTrue("flag-icon" in img_html_tag)

    def test_installed_on_valid_locale(self):
        language = create_language("English", "en_GB")

        self.assertTrue(language.is_locale_installed())

    def test_installed_on_invalid_locale(self):
        language = create_language("English", "blabla")
        self.assertFalse(language.is_locale_installed())
