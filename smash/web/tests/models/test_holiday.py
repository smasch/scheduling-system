import logging

from django.test import TestCase

from web.models import Holiday
from web.tests import create_worker

logger = logging.getLogger(__name__)


class HolidayTests(TestCase):
    def test_str(self):
        holiday = Holiday(person=create_worker())

        self.assertIsNotNone(str(holiday))
        self.assertIsNotNone(str(holiday))
