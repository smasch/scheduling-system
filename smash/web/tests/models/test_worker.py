import datetime

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.test import Client, TestCase

from web.models import Holiday, Worker
from web.models.worker import role_choices_by_worker_type, worker_type_by_worker
from web.models.worker_study_role import WORKER_STAFF, ROLE_CHOICES_SECRETARY, ROLE_CHOICES_HEALTH_PARTNER, \
    WORKER_HEALTH_PARTNER, ROLE_CHOICES_VOUCHER_PARTNER, WORKER_VOUCHER_PARTNER
from web.tests.functions import create_worker, create_user
from web.views.notifications import get_today_midnight_date


class WorkerModelTests(TestCase):
    def test_disable(self):
        client = Client()
        username = 'piotr'
        password = 'top_secret'
        user = create_user(username=username, email='jacob@bla', password=password)
        worker = Worker.get_by_user(user)

        disable_status = worker.disable()
        self.assertTrue(disable_status)

        success = client.login(username=username, password=password)
        self.assertFalse(success)
        self.assertFalse(get_user_model().objects.get(id=user.id).is_active)
        self.assertFalse(worker.is_active())

    def test_enable(self):
        client = Client()
        username = 'piotr'
        password = 'top_secret'
        user = create_user(username=username, email='jacob@bla', password=password)
        user.is_active = False
        user.save()
        worker = Worker.get_by_user(user)

        enable_status = worker.enable()
        self.assertTrue(enable_status)

        success = client.login(username=username, password=password)
        self.assertTrue(success)
        self.assertTrue(get_user_model().objects.get(id=user.id).is_active)
        self.assertTrue(worker.is_active())

    def test_disable_invalid(self):
        worker = create_worker()
        disable_status = worker.disable()
        self.assertFalse(disable_status)

    def test_is_active_for_invalid(self):
        worker = create_worker()
        self.assertFalse(worker.is_active())

    def test_get_by_user_for_None(self):
        worker = Worker.get_by_user(None)
        self.assertIsNone(worker)

    def test_get_by_user_for_Worker(self):
        worker = create_worker()
        worker_2 = Worker.get_by_user(worker)
        self.assertEqual(worker, worker_2)

    def test_get_by_user_for_invalid(self):
        try:
            Worker.get_by_user("invalid object")
            self.fail("Exception expected")
        except TypeError:
            pass

    def test_is_on_leave(self):
        worker = create_worker()
        self.assertFalse(worker.is_on_leave())
        Holiday(person=worker,
                datetime_start=get_today_midnight_date() + datetime.timedelta(days=-2),
                datetime_end=get_today_midnight_date() + datetime.timedelta(days=2)).save()

        self.assertTrue(worker.is_on_leave())

    def test_current_leave_details(self):
        worker = create_worker()
        self.assertEqual(worker.current_leave_details(), None)
        h = Holiday(person=worker,
                    datetime_start=get_today_midnight_date() + datetime.timedelta(days=-2),
                    datetime_end=get_today_midnight_date() + datetime.timedelta(days=2))
        h.save()

        self.assertEqual(worker.current_leave_details(), h)

    def test_get_by_user_for_anonymous(self):
        self.assertIsNone(Worker.get_by_user(AnonymousUser()))

    def test_role_choices_by_worker_type(self):
        with self.assertRaises(Exception):
            role_choices_by_worker_type("invalid arg")

    def test_worker_type_by_worker_without_role(self):
        self.assertEqual(WORKER_STAFF, worker_type_by_worker(Worker()))

    def test_worker_type_by_worker_with_secretary_role(self):
        worker = create_worker()
        worker.roles.update(name=ROLE_CHOICES_SECRETARY)
        self.assertEqual(WORKER_STAFF, worker_type_by_worker(worker))

    def test_worker_type_by_worker_with_health_partner_role(self):
        worker = create_worker()
        worker.roles.update(name=ROLE_CHOICES_HEALTH_PARTNER)
        self.assertEqual(WORKER_HEALTH_PARTNER, worker_type_by_worker(worker))

    def test_worker_type_by_worker_with_voucher_partner_role(self):
        worker = create_worker()
        worker.roles.update(name=ROLE_CHOICES_VOUCHER_PARTNER)
        self.assertEqual(WORKER_VOUCHER_PARTNER, worker_type_by_worker(worker))

    def test_worker_type_by_worker_with_invalid_role(self):
        worker = create_worker()
        worker.roles.update(name="unk")
        with self.assertRaises(Exception):
            worker_type_by_worker(worker)

    # worker role tests

    def test_worker_role_by_worker_without_role(self):
        self.assertEqual(WORKER_STAFF, Worker().role)

    def test_worker_role_by_worker_with_secretary_role(self):
        worker = create_worker()
        worker.roles.update(name=ROLE_CHOICES_SECRETARY)
        self.assertEqual(ROLE_CHOICES_SECRETARY, worker.role)

    def test_worker_role_by_worker_with_health_partner_role(self):
        worker = create_worker()
        worker.roles.update(name=ROLE_CHOICES_HEALTH_PARTNER)
        self.assertEqual(ROLE_CHOICES_HEALTH_PARTNER, worker.role)

    def test_worker_role_by_worker_with_voucher_partner_role(self):
        worker = create_worker()
        worker.roles.update(name=ROLE_CHOICES_VOUCHER_PARTNER)
        self.assertEqual(ROLE_CHOICES_VOUCHER_PARTNER, worker.role)

    def test_worker_role_by_worker_with_invalid_role(self):
        worker = create_worker()
        worker.roles.update(name="unk")
        with self.assertRaises(Exception):
            _ = worker.role
