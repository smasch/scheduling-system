import logging

from django.test import TestCase

from web.tests.functions import create_voucher

logger = logging.getLogger(__name__)


class VoucherTests(TestCase):
    def test_to_string(self):
        voucher = create_voucher()

        self.assertTrue(voucher.number in str(voucher))
        self.assertTrue(voucher.number in str(voucher))
