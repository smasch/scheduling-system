import logging

from django.test import TestCase

from web.tests.functions import create_appointment_type

logger = logging.getLogger(__name__)


class AppointmentTypeTests(TestCase):
    def test_str(self):
        appointment_type = create_appointment_type()
        self.assertIsNotNone(str(appointment_type))
