import logging

from django.test import TestCase

from web.tests.functions import create_study

logger = logging.getLogger(__name__)


class StudyTests(TestCase):

    def test_image_img(self):
        study = create_study()
        self.assertTrue(study.name in str(study))

    def test_check_nd_number(self):
        study = create_study()
        # check the default regex
        self.assertTrue(study.check_nd_number('ND0001'))
