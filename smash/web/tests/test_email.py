# coding=utf-8

from django.core import mail
from django.test import TestCase

from web.smash_email import EmailSender


class TestEmailSender(TestCase):
    def test_send_email(self):
        self.assertEqual(0, len(mail.outbox))

        EmailSender.send_email("test", "test body<br/>and content", "piotr.gawron@uni.lu")
        self.assertEqual(1, len(mail.outbox))

        # console test that actually send email
        # > manage.py shell
        # from web.email import EmailSender; EmailSender().send_email("test","<h1>body</h1>","piotr.gawron@uni.lu");
