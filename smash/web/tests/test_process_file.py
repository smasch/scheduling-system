import locale
import logging
import os
import platform
import tempfile

from django.test import TestCase
from django.utils import timezone

from web.docx_helper import process_file, merge_files
from web.tests.functions import get_resource_path

logger = logging.getLogger(__name__)


class TestDocxProcessor(TestCase):
    def test_process_file(self):
        template_path = get_resource_path('template.docx')
        locale_name = "fr_FR"
        if platform.system() == 'Windows':
            locale_name = "French"
        locale.setlocale(locale.LC_TIME, locale_name)
        output_path = tempfile.mktemp()
        changes = {
            "##SIR##": "Mr",
            "##NAME##": "Jan",
            "##SURNAME##": "Gawron",
            "##ADDRESS1##": "Rock'n'roll 23",
            "##ADDRESS2##": "61-234, Poznan",
            "##COUNTRY##": "POLAND",
            "##CONTENT##": "1",
            "##DATE##": timezone.now().date().strftime("%A %d %B %Y"),
        }
        process_file(template_path, output_path, changes)
        self.assertTrue(os.path.isfile(output_path))
        os.remove(output_path)

    def test_merge_files(self):
        template_path = get_resource_path('template.docx')
        template_path_2 = get_resource_path('voucher_test.docx')

        output_path = tempfile.mktemp()

        merge_files([template_path, template_path_2], output_path)
        self.assertTrue(os.path.isfile(output_path))
        os.remove(output_path)
