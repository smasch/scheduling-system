# coding=utf-8
import django, sys, os
sys.path.append(sys.path.append(os.path.join(os.path.dirname(__file__), '..'))) #run script as it was on parent folder
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smash.settings")
django.setup()
import pandas as pd
import logging
from web.models import StudySubject

logging.basicConfig(handlers=[logging.FileHandler('log.txt', 'w', 'utf-8')], level=logging.DEBUG)

if len(sys.argv) < 2:
    logging.warn('Please, execute the program as: python {} file_path.xlsx'.format(sys.argv[0]))
    sys.exit(1)
file = sys.argv[1]
if not os.path.isfile(file):
    logging.warn('Please, execute the program with a valid file path.')
    sys.exit(1)

df = pd.read_csv(file, names=['Last Name', 'First Name', 'Screening'], encoding='utf-8')
for index, row in df.iterrows():
	s = StudySubject.objects.filter(subject__first_name=row['First Name'], subject__last_name=row['Last Name'])
	if len(s) == 0:
		logging.warn('NO RESULTS for {} {}'.format(row['First Name'], row['Last Name']))
	elif len(s) > 1:
		logging.warn('TOO MANY RESULTS for {} {}'.format(row['First Name'], row['Last Name']))
	else:
		logging.info('UPDATING {} {} with screening_number {} with new value: {}'.format(row['First Name'], row['Last Name'], s[0].screening_number, row['Screening']))
		s[0].screening_number = row['Screening']
		s[0].save()
		s = StudySubject.objects.filter(subject__first_name=row['First Name'], subject__last_name=row['Last Name'])
		logging.info('UPDATED {} {}. Current value: {}'.format(row['First Name'], row['Last Name'], s[0].screening_number))
		