#!/bin/env

python manage.py makemigrations web  \
&& python manage.py migrate \
&& python manage.py migrate sessions \
&& python manage.py collectstatic --noinput \
&& echo 'The server will start now...' \
&& gunicorn -b 0.0.0.0:8888 smash.wsgi:application --access-logfile access.log --error-logfile error.log
