import logging

from django.contrib import messages
from django.shortcuts import redirect

logger = logging.getLogger(__name__)


class Force2FAMiddleware:
    """
    Middleware restricting access to users with 2 factors authentication enabled
    Redirects to 2fa section if not enabled
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        If user is authenticated and user is not verified (2fa not enabled) and we are in one of the 2fa setting pages
        we redirect to the 2fa profile page
        """
        if request.user.is_authenticated and not request.user.is_verified() \
                and 'two_factor' not in request.path \
                and '/logout' not in request.path:
            messages.add_message(request, messages.WARNING,
                                 'Two-factor authentication must be enabled to use this system')
            return redirect('two_factor:profile')
        return self.get_response(request)
