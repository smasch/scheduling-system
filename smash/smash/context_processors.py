from web.models import ConfigurationItem
from web.models.constants import LOGIN_PAGE_BACKGROUND_IMAGE, DEFAULT_FROM_EMAIL


# noinspection PyUnusedLocal
def login_background(request):
    """
    Context processor - these values will be available to templates once registered in settings.py
    """
    return {
        'login_page_background': ConfigurationItem.objects.get(type=LOGIN_PAGE_BACKGROUND_IMAGE).value,
        'email': ConfigurationItem.objects.get(type=DEFAULT_FROM_EMAIL).value
    }
