"""smash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""
import re

from django.conf import settings
from django.conf.urls import include
from django.contrib import admin
from django.urls import re_path
from django.views.static import serve
from two_factor.urls import urlpatterns as tf_urls

from web import api_urls
from web import urls

handler404 = "web.views.e404_page_not_found"
handler500 = "web.views.e500_error"
handler403 = "web.views.e403_permission_denied"
handler400 = "web.views.e400_bad_request"

if settings.SMASCH_PATH_PREFIX != "":
    url_prefix = settings.SMASCH_PATH_PREFIX.lstrip("/").rstrip("/") + "/"
else:
    url_prefix = ""

urlpatterns = [
    re_path(rf"^{url_prefix}admin/", admin.site.urls),
    re_path(rf"{url_prefix}", include(urls)),
    re_path(rf"^{url_prefix}api/", include(api_urls)),
    re_path(rf"{url_prefix}", include(tf_urls)),
]

if settings.SERVE_STATIC or settings.DEBUG:
    urlpatterns.append(
        re_path(
            r"^%s(?P<path>.*)$" % re.escape(settings.MEDIA_URL.lstrip("/")),
            serve,
            {"document_root": settings.MEDIA_ROOT},
        )
    )
