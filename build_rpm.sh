#!/bin/bash

CURRENT_VERSION=`cat CHANGELOG |grep smasch |head -1 | cut -f2 -d'(' | cut -f1 -d')' | cut -f1 -d'-' `

find . -name "*.py[co]" -exec rm -f {} \;
find . -name "__pycache__" -exec rm -rf {} \;

DIR=$(pwd)/rpm

rm -rf $DIR
mkdir -p $DIR/SPECS/

cp rpm-files/smasch.spec $DIR/SPECS/

sed -i "s/__CURRENT_VERSION__/$CURRENT_VERSION/g" $DIR/SPECS/smasch.spec

rpmbuild -v -ba --build-in-place --define "_topdir $DIR" $DIR/SPECS/smasch.spec
