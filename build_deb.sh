#!/bin/bash
ROOT=`pwd`
rm -rf debian
rm smasch_*.debian.tar.xz smasch_*.dsc smasch_*.orig.tar.xz smasch_*_all.deb smasch_*_amd64.build smasch_*_amd64.changes

find . -name "*.py[co]" -exec rm -f {} \;
find . -name "__pycache__" -exec rm -rf {} \;

mkdir debian
mkdir debian/smash
cp -r smash/web debian/smash/web
cp -r smash/package.json debian/smash/
cp -r smash/package-lock.json debian/smash/
cp -r smash/smash debian/smash/smash
cp -r smash/db_scripts debian/smash/db_scripts
cp smash/manage.py debian/smash/manage.py
cp -r debian-template debian/debian-template
cp CHANGELOG debian/debian-template/changelog
rm debian/smash/smash/local_settings.py
cp -r debian-files debian/debian-files

CURRENT_VERSION=`cat CHANGELOG |grep smasch |head -1 | cut -f2 -d'(' | cut -f1 -d')' | cut -f1 -d'-' `

#collect npm dependencies to include it in debian package
cd debian/smash
echo "import os" > smash/local_settings.py
echo "STATIC_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)),'../tmp-static')" >> smash/local_settings.py
echo "UPLOAD_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)),'../tmp-static')" >> smash/local_settings.py
echo "SECRET_KEY ='tmp'" >> smash/local_settings.py
echo "STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'" >> smash/local_settings.py
npm ci
export PYTHONDONTWRITEBYTECODE=1
./manage.py collectstatic
rm -rf node_modules
mv tmp-static/npm node_modules

rm smash/local_settings.py
rm -rf tmp-static
cd ..


dh_make -p smasch_$CURRENT_VERSION -s --createorig -e piotr.gawron@uni.lu -y -t $ROOT/debian/debian-template
rm debian/*.ex
rm debian/*.EX
debuild -us -uc
