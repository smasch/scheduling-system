# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "Paste long random string here"  # Insert long random string

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SERVE_STATIC = True

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "smash",  # Insert your database's name
        "USER": "runner",  # Insert your database's user
        "PASSWORD": "password",  # Insert your user's password
        "HOST": "postgres",
        "PORT": "",
        "TEST": {
            "NAME": "dbtest",
        },  # '' === default one # Empty string is OK
        # If to use sqlite
        # 'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

STATIC_ROOT = (
    "/tmp/static"  # Warning! `/tmp` directory can be flushed in any moment; use a persistent one; e.g. ~/tmp/static
)
MEDIA_ROOT = (
    "/tmp/media"  # Warning! `/tmp` directory can be flushed in any moment; use a persistent one, e.g. ~/tmp/media
)
UPLOAD_ROOT = "/tmp/upload"
ETL_ROOT = "/tmp/etl"

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"

ALLOWED_HOSTS = ["127.0.0.1", "localhost"]
