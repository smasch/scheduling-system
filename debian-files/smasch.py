# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'Paste long random string here'  # Insert long random string

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Should the static/media files be served by Django
SERVE_STATIC = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/usr/lib/smasch/db.sqlite3',
    }
}

STATIC_ROOT = '/usr/lib/smasch/data/static'
MEDIA_ROOT = '/usr/lib/smasch/data/media'
UPLOAD_ROOT = '/usr/lib/smasch/data/upload'
ETL_ROOT = '/usr/lib/smasch/data/etl'

ALLOWED_HOSTS = ["127.0.0.1", "localhost"]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': '/var/log/smasch/smasch.log',
            'formatter': 'verbose'
        },
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple'
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'INFO',
        },
        'web': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
    },
}

TWO_FACTOR_SMS_GATEWAY = "web.nexmo_gateway.Nexmo"
NPM_ROOT_PATH = '/usr/lib/smasch/'
