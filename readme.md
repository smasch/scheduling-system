# SMart SCHeduling

[![coverage report](https://gitlab.lcsb.uni.lu/NCER-PD/scheduling-system/badges/master/coverage.svg)](https://gitlab.lcsb.uni.lu/NCER-PD/scheduling-system/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Docker version
If you'd like to try out the application without preparing the environment, you can use Docker (you must have _Docker_ and _docker-compose_ installed):

```
# Navigate to the project's directory
docker-compose build && docker-compose up
# To add a new user, type in the new terminal:
docker-compose exec web sh
python manage.py superworker
```

## Required software (on ubuntu's OS family):
  - install required dependencies on ubuntu
```bash
sudo apt-get install python3, libcurl4-gnutls-dev, libpng-dev, libfreetype6-dev, libpq-dev, gcc, g++, python3-dev, libgnutls28-dev, libjpeg-dev, libfreetype6-dev, git
```
  - install nodejs
```bash
curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get install nodejs
```


## Developer project installation
  - clone smasch:

```bash
git clone ssh://git@git-r3lab-server.uni.lu:8022/NCER-PD/scheduling-system.git
```

  - setup a venv with clean python3 working environment and start using it. Important, currently only python3.6-3.8 are supported

```bash
cd scheduling-system
python3 -m venv env
. env/bin/activate
```

  - install smasch dependencies

```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt
npm ci
```

  - Create `local_settings.py` file in `smash/smash` directory by copying the template in `smash/smash/local_settings.template` and edit your local_setttings.py file to set `FORCE_2FA = False` and change your database connection data.


### Database configuration
There are two databases supported: `sqlite3` or `postgresql`. You can configure database connection in `smash/smash/local_settings.py`.

- sqlite3 database configuration looks like:

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
    }
}
```

- postgresql database configuration looks like:

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'smaschdb',
        'USER': 'smaschuser',
        'PASSWORD': 'smaschpassword',
        'HOST': 'localhost',
        'PORT': '',
        'TEST': {
            'NAME': 'dbtest',
        },
    }
}
```

After database connection is configured setup a database by applying migration scripts and create admin user

```bash
./manage.py migrate
./manage.py superworker -u admin -e test@test.lu -f John -l Doe
```


## Development
Remember, that before working you have to activate _venv_ (in project directory), by:

```bash
. env/bin/activate
```
To add dummy data, run:
```bash
python smash/db_scripts/create_dummy_data.py
```
In order to run development server, run:
```bash
cd smash
./manage.py runserver
```

and go to `127.0.0.1:8000` in browser

### Mac Developers

In case of problems with the openssl version installed on the system:

```
export PYCURL_SSL_LIBRARY=openssl
pip install pycurl --global-option=build_ext --global-option="-I/usr/local/Cellar/openssl/1.0.2k/include" --global-option="-L/usr/local/Cellar/openssl/1.0.2k/lib" --upgrade
pip install psycopg2 --global-option=build_ext --global-option="-I/usr/local/Cellar/openssl/1.0.2k/include" --global-option="-L/usr/local/Cellar/openssl/1.0.2k/lib" --upgrade
```

## Release new version

1. Create git tag (for example `v1.0.2`). This will trigger pipeline that will create deb package.
2. Login to LCSB repository `repo-r3lab.uni.lu` using ssh:
```
ssh 10.240.6.115 -p 8022
```
3. Download deb package artifact from `build_debian` job and rpm package from `build_rpm` job. The url of deb package can be found by: Select tag (`v1.0.2`)&rarr;Click hash (`0c664f23`)&rarr;Pipelines&rarr;Stages;build:passed&rarr;build_debian&rarr;Job artifacts&rarr;Browse&rarr;smasch_1.0.2-1_all.deb&rarr;Download. Not sure how to do it easier. For example:
```
wget https://gitlab.lcsb.uni.lu/smasch/scheduling-system/-/jobs/282797/artifacts/raw/smasch_1.0.2-1_all.deb
```
4. Put deb file in repository. The password is shared in lastpass note.
```
sudo reprepro -b /var/www/html/debian includedeb stable smasch_1.0.2-1_all.deb
```
5. Put rpm file in repository.
```
sudo cp smasch-1.2.0~alpha.1-1.noarch.rpm /var/www/html/centOS/8/os/x86_64/
sudo rpm --addsign /var/www/html/centOS/8/os/x86_64/smasch-1.2.0~alpha.1-1.noarch.rpm
sudo createrepo --update /var/www/html/centOS/8/os/x86_64/
```


## Production deployment

### Debian/Ubuntu (deb package)
Smasch can be deployed using debian package provided in lcsb repository:

```bash
echo "deb http://repo-r3lab.uni.lu/debian/ stable main" | tee /etc/apt/sources.list.d/repo-r3lab.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xcb185f4e31872412
apt-get update
apt-get install -y smasch
```

### CentOS (rpm package)
```bash
echo -e "[lcsbrepo]\nname=LCSB Repository\nbaseurl=https://repo-r3lab.uni.lu/centOS/8/os/x86_64/\nenabled=1\n" > /etc/yum.repos.d/lcsb.repo
rpmkeys --import https://repo-r3lab.uni.lu/LCSB-REPO-GPG-KEY
yum install smasch
```
Keep in mind that if you use selinux you need to configure it by yourself.

After smasch is installed you can start/stop it using systemd:

```bash
service smasch start
service smasch stop
```

Smasch service will be listening on port 8888 - go to http://localhost:8888/. By default smasch will use sqlite3 database.

Configuration of smasch (`local_settings.py`) is in `/etc/smasch/smasch.py`.


## Operations

### Disable two steps authentication for a specific user


```bash
./manage.py two_factor_disable ${USERNAME}
```

### Public holidays

to import public holidays run:

```bash
./manage.py holidays ${YEARS}
```

where ${YEARS} should be a space separated list of years for which the holidays will be imported.

example:

```bash
./manage.py holidays 2017 2018 2019
```
