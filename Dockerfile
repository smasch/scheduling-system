FROM node:21 as builder
RUN mkdir -p /code/smash
ADD ./smash/package* /code/smash/

WORKDIR /code/smash
RUN node --version \
    && npm --version \
    && npm ci

FROM python:3.11-bookworm

RUN apt-get update \
    && apt-get install -y --allow-unauthenticated libsasl2-dev python-dev-is-python3 libldap2-dev libssl-dev locales locales-all default-libmysqlclient-dev

RUN mkdir /code
ADD . /code/
COPY --from=builder /code/smash/node_modules /code/smash/node_modules

WORKDIR /code
RUN pip install --upgrade pip
RUN pip install -r requirements.txt --default-timeout=180 \
    && pip install -r requirements-dev.txt --default-timeout=180

RUN cp local_settings_ci.py smash/smash/local_settings.py
WORKDIR /code/smash
RUN chmod +x start.sh

ENTRYPOINT [ "./start.sh" ]
