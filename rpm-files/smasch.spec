Summary: SMart SCHeduling - research study scheduling program
Name: smasch
Version: __CURRENT_VERSION__
Release: 1%{?dist}
License: AGPLv3
URL: https://smasch.pages.uni.lu/
Group: Science
Packager: Piotr Gawron
Requires: libcurl-devel
Requires: libpng-devel
Requires: freetype-devel
Requires: postgresql-libs
Requires: postgresql-devel
Requires: mariadb-devel
Requires: gcc
Requires: gcc-toolset-11
Requires: python39-devel
Requires: gnutls-devel
Requires: libjpeg-devel
Requires: pkg-config
Requires: cronie
Requires: cyrus-sasl-devel
Requires: openldap-devel
Requires: openssl-devel
BuildRoot: rpm/
BuildArch: noarch

%install
ROOT_DIR=$(pwd)
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_exec_prefix}/lib/%{name}
cp -r smash/web %{buildroot}%{_exec_prefix}/lib/%{name}/web
cp -r smash/package.json %{buildroot}%{_exec_prefix}/lib/%{name}
cp -r smash/package-lock.json %{buildroot}%{_exec_prefix}/lib/%{name}
cp -r smash/smash %{buildroot}%{_exec_prefix}/lib/%{name}/smash
cp -r smash/db_scripts %{buildroot}%{_exec_prefix}/lib/%{name}/db_scripts
cp smash/manage.py %{buildroot}%{_exec_prefix}/lib/%{name}/manage.py
cp requirements.txt %{buildroot}%{_exec_prefix}/lib/%{name}
cp requirements-dev.txt %{buildroot}%{_exec_prefix}/lib/%{name}


cd %{buildroot}%{_exec_prefix}/lib/%{name}
echo "import os" > smash/local_settings.py
echo "STATIC_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)),'../tmp-static')" >> smash/local_settings.py
echo "UPLOAD_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)),'../tmp-static')" >> smash/local_settings.py
echo "SECRET_KEY ='tmp'" >> smash/local_settings.py
echo "STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'" >> smash/local_settings.py
npm ci
export PYTHONDONTWRITEBYTECODE=1
./manage.py collectstatic
rm -rf node_modules
mv tmp-static/npm node_modules

rm smash/local_settings.py
rm -rf tmp-static
cd $ROOT_DIR
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/
cp rpm-files/smasch.py %{buildroot}%{_sysconfdir}/%{name}/

mkdir -p %{buildroot}%{_exec_prefix}/lib/systemd/system/
cp rpm-files/smasch.service %{buildroot}%{_exec_prefix}/lib/systemd/system/

mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d/
cp rpm-files/smasch-logrotate %{buildroot}%{_sysconfdir}/logrotate.d/smasch

%files
#default config file
%config(noreplace) %{_sysconfdir}/%{name}/smasch.py
#logrotate config
%config %{_sysconfdir}/logrotate.d/smasch
#our app
%{_exec_prefix}/lib/%{name}/*
#systemd+gunicorn
%{_exec_prefix}/lib/systemd/system/smasch.service

%post
set -e
LOG_FILE=%{_localstatedir}/log/%{name}/install.log

if [ $1 -eq 1 ]; then
    echo "First install complete"
    id -u smasch &>/dev/null || useradd smasch
    if ! getent group smasch >/dev/null; then
        addgroup --quiet --system smasch
    fi

    ln -sf %{_sysconfdir}/%{name}/smasch.py %{_exec_prefix}/lib/%{name}/smash/local_settings.py

    python3.9 -m venv %{_exec_prefix}/lib/%{name}/env

    source %{_exec_prefix}/lib/%{name}/env/bin/activate && pip install --upgrade pip -q --log $LOG_FILE

    chown smasch:smasch %{_sysconfdir}/%{name}/smasch.py
    chmod 0600 %{_sysconfdir}/%{name}/smasch.py

    mkdir -p %{_localstatedir}/log/%{name}
fi

echo "Installing python dependencies"
export PYCURL_SSL_LIBRARY=openssl #required by pycurl
{
  source %{_exec_prefix}/lib/%{name}/env/bin/activate && pip install -r %{_exec_prefix}/lib/%{name}/requirements.txt -q --log $LOG_FILE
  source %{_exec_prefix}/lib/%{name}/env/bin/activate && pip install -r %{_exec_prefix}/lib/%{name}/requirements-dev.txt  -q --log $LOG_FILE
} || {
  echo "There was an issue when installing python dependencies." >> $LOG_FILE
  echo "There was an issue when installing python dependencies. Reinstall of smasch could help (in case you are upgrading smasch the data should not be lost in the process): "
  echo "yum remove smasch"
  echo "yum install smasch"
  exit 1
}

echo "Collecting static files"
source %{_exec_prefix}/lib/%{name}/env/bin/activate && %{_exec_prefix}/lib/%{name}/manage.py collectstatic --no-input >> $LOG_FILE 2>&1

echo "Applying database changes"
source %{_exec_prefix}/lib/%{name}/env/bin/activate && %{_exec_prefix}/lib/%{name}/manage.py migrate --no-input >> $LOG_FILE 2>&1

chown -R smasch:smasch %{_exec_prefix}/lib/%{name}

chown -R smasch:smasch %{_localstatedir}/log/%{name}

if [ -d /run/systemd/system ]; then
  systemctl --system daemon-reload >/dev/null || true
fi

crontab -u smasch -l 2>/dev/null | grep -v "runcrons" | { cat; echo "*/5 * * * * . /usr/lib/smasch/env/bin/activate && python /usr/lib/smasch/manage.py runcrons >> /var/log/smasch/cronjob.log 2>&1"; } | crontab -u smasch -

%postun
if [ -d /run/systemd/system ]; then
  systemctl --system daemon-reload >/dev/null || true
fi

if [ $1 -eq 0 ]; then
  rm -rf %{_exec_prefix}/lib/%{name}/env/
  rm -rf %{_exec_prefix}/lib/%{name}/web/
  rm -rf %{_exec_prefix}/lib/%{name}/smash/
fi

%preun
if [ -d /run/systemd/system ]; then
  systemctl --system daemon-reload >/dev/null || true
  systemctl stop smasch
fi

crontab -u smasch -l 2>/dev/null | grep -v "runcrons" | crontab -u smasch -


%description
SMart SCHeduling - research study scheduling program
 The program controls and simplifies the scheduling, and is able to manage a
 big data base of patients. Smasch is also used to organize the daily
 planning's (delegation of tasks) for the different medical professionals
 such as doctors, nurses and neuropsychologists.