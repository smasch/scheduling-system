# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "Paste long random string here"  # Insert long random string

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "smash",
        "USER": "runner",
        "PASSWORD": "password",
        "HOST": "mariadb",
        "PORT": "",
        "TEST": {
            "NAME": "dbtest",
        },
    }
}

STATIC_ROOT = (
    "/tmp/static"  # Warning! `/tmp` directory can be flushed in any moment; use a persistent one; e.g. ~/tmp/static
)
MEDIA_ROOT = (
    "/tmp/media"  # Warning! `/tmp` directory can be flushed in any moment; use a persistent one, e.g. ~/tmp/media
)
UPLOAD_ROOT = "/tmp/upload"
ETL_ROOT = "/tmp/etl"

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"

ALLOWED_HOSTS = ["127.0.0.1", "localhost"]
